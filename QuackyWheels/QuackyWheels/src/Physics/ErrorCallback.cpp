#include "ErrorCallback.h"
#include <iostream>

ErrorCallback::ErrorCallback() {

}

ErrorCallback::~ErrorCallback() {

}

void ErrorCallback::reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line)
{
	//May need something more complicated later
	std::cout << message;
}
