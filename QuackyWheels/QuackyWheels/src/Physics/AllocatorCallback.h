#pragma once

#include "foundation/PxAllocatorCallback.h"


class AllocatorCallback : public physx::PxAllocatorCallback {
public:
	AllocatorCallback();
	virtual ~AllocatorCallback();

	void* allocate(size_t size, const char* typeName, const char* filename, int line);
	void deallocate(void* ptr);
};

