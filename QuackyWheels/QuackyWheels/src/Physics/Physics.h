#pragma once

#include "PxPhysicsAPI.h"

class CollisionHandler;
class Physics {
public:

	Physics();
	virtual ~Physics();

	physx::PxPhysics* getPhysics();
	physx::PxDefaultAllocator* getAllocator();
	physx::PxCooking* getCooking();
	physx::PxScene* getScene();

	bool getCollision();
	void setCollision(bool col);

	CollisionHandler* collisionHandler = NULL;
private:
	///<summary> The physics scene </summary>
	physx::PxScene* scene;

	physx::PxDefaultErrorCallback gDefaultErrorCallback;
	physx::PxDefaultAllocator gDefaultAllocatorCallback;

	physx::PxFoundation *mFoundation = NULL;
	physx::PxProfileZoneManager *mProfileZoneManager = NULL;
	physx::PxPhysics *mPhysics = NULL;
	physx::PxCooking *mCooking = NULL;

	bool recordMemoryAllocations = true;

	physx::PxSerializationRegistry* serializationRegistry = NULL;

};