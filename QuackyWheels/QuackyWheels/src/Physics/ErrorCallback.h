#pragma once

#include "foundation/PxErrorCallback.h"

class ErrorCallback : public physx::PxErrorCallback {
public:
	ErrorCallback();
	virtual ~ErrorCallback();

	void reportError(physx::PxErrorCode::Enum code, const char* message, const char* file, int line);
};

