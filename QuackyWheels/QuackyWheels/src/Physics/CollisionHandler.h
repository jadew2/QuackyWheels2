#pragma once

#include "PxPhysicsAPI.h"
#include <vector>

class GameObject;
class PhysicsVehicle;

class CollisionHandler : public physx::PxSimulationEventCallback
{
public:
	CollisionHandler();
	virtual ~CollisionHandler();

	//Fixme: SuperHax so we can get a list of triggers for milestone 2
	std::vector<physx::PxTriggerPair*> pairsLastFrame = std::vector<physx::PxTriggerPair*>();

	void onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count) override;
	void onWake(physx::PxActor** actors, physx::PxU32 count) override;
	void onSleep(physx::PxActor** actors, physx::PxU32 count) override;
	void onTrigger(physx::PxTriggerPair* pairs, physx::PxU32 count) override;
	void onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) override;
	bool getCollision();
	void setCollision(bool col);

	bool collision;
};