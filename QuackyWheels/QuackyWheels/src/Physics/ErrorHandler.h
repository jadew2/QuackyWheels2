#pragma once

#include "PxPhysicsAPI.h"
#include <iostream>
using namespace std;
using namespace physx;

class ErrorHandler : public PxErrorCallback
{
public:
	ErrorHandler();
	virtual ~ErrorHandler();

	virtual void reportError(PxErrorCode::Enum code, const char* message, const char* file, int line) override
	{
		cout << "PhysX Error: " << message << endl;
	}
};

