#include "CollisionHandler.h"
#include "../Components/ObjectType.h"
#include "../Components/PhysicsActor.h"
#include "../Components/PhysicsVehicle.h"
#include "../ComponentEntity/GameObject.h"
#include "../Components/PowerUpManager.h"
#include "../World/World.h"
#include "../Components/HUD.h"
#include "../Components/Mine.h"

CollisionHandler::CollisionHandler() {

	collision = false;
}


CollisionHandler::~CollisionHandler() {

}

void CollisionHandler::onConstraintBreak(physx::PxConstraintInfo* constraints, physx::PxU32 count) {
	PX_UNUSED(constraints);
	PX_UNUSED(count);
}

void CollisionHandler::onWake(physx::PxActor** actors, physx::PxU32 count) {
	PX_UNUSED(actors);
	PX_UNUSED(count);
}

void CollisionHandler::onSleep(physx::PxActor** actors, physx::PxU32 count) {
	PX_UNUSED(actors);
	PX_UNUSED(count);
}

void CollisionHandler::onTrigger(physx::PxTriggerPair* pair, physx::PxU32 count) {
	
	if (pair->triggerActor->userData == nullptr || pair->otherActor->userData == nullptr)
		return;
	
	for (physx::PxU32 i = 0; i < count; i++) {
		if (((GameObject*)pair->otherActor->userData)->GetComponent<PhysicsVehicle>() != nullptr) {
			if (((GameObject*)pair->triggerActor->userData)->GetComponent<PhysicsActor>()->getObjectType() == ObjectType::Trail) {
				if (pair->status & physx::PxPairFlag::eNOTIFY_TOUCH_FOUND) {
					((GameObject*)pair->otherActor->userData)->GetComponent<PhysicsVehicle>()->setInTrail(true);
				}
				else if (pair->status & physx::PxPairFlag::eNOTIFY_TOUCH_LOST) {
					((GameObject*)pair->otherActor->userData)->GetComponent<PhysicsVehicle>()->setInTrail(false);
				}
			}
		}
	}
}

void CollisionHandler::onContact(const physx::PxContactPairHeader& pairHeader, const physx::PxContactPair* pairs, physx::PxU32 nbPairs) {
	for (physx::PxU32 i = 0; i < nbPairs; i++)
	{
		const physx::PxContactPair& cp = pairs[i];

		if (cp.events & physx::PxPairFlag::eNOTIFY_TOUCH_FOUND)
		{
			//printf("Collision!\n");
			//Setup user data to point to the component. Eventually use user data type to check what type the colliding things are
			if ((((GameObject*)pairHeader.actors[0]->userData)->GetComponent<PhysicsVehicle>() != nullptr) && (((GameObject*)pairHeader.actors[1]->userData)->GetComponent<PhysicsVehicle>() != nullptr))
			{
				collision = true;
			}
			else if ((((GameObject*)pairHeader.actors[0]->userData)->GetComponent<PhysicsVehicle>() != nullptr) && (((GameObject*)pairHeader.actors[1]->userData)->GetComponent<PhysicsActor>() != nullptr) && (((GameObject*)pairHeader.actors[1]->userData)->GetComponent<PhysicsActor>()->getObjectType() == ObjectType::Obstacle))
			{
				//printf("obstacle\n");
				collision = true;
			}
			else if ((((GameObject*)pairHeader.actors[0]->userData)->GetComponent<PhysicsActor>() != nullptr) && (((GameObject*)pairHeader.actors[0]->userData)->GetComponent<PhysicsActor>()->getObjectType() == ObjectType::Obstacle) && (((GameObject*)pairHeader.actors[1]->userData)->GetComponent<PhysicsVehicle>() != nullptr))
			{
				//printf("obstacle\n");
				collision = true;
			}
			else
			{
				//Call both on contact for each actor
				collision = false;
			}
		}
	}
}

bool CollisionHandler::getCollision()
{
	return collision;
}

void CollisionHandler::setCollision(bool col)
{
	collision = col;
}
