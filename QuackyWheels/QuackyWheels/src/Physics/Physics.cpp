#include "Physics.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Exceptions/EngineException.h"
#include "CollisionHandler.h"
#include "ErrorHandler.h"

Physics::Physics()
{

	mFoundation = PxCreateFoundation(PX_PHYSICS_VERSION, gDefaultAllocatorCallback, gDefaultErrorCallback);
	if (!mFoundation) {
		throw new EngineException(std::string("Physics: Create PxCreateFoundation failed!"));
	}
	mProfileZoneManager = &physx::PxProfileZoneManager::createProfileZoneManager(mFoundation);
	if (!mProfileZoneManager) {
		throw new EngineException(std::string("Physics: Create PxProfileZoneManager failed!"));
	}
	physx::PxTolerancesScale scale = physx::PxTolerancesScale();
	scale.speed = 150.0f;
	mPhysics = PxCreatePhysics(PX_PHYSICS_VERSION, *mFoundation, scale, recordMemoryAllocations, mProfileZoneManager);
	if (!mPhysics) {
		throw new EngineException(std::string("Physics: Create PxPhysics failed!"));
	}
	mCooking = PxCreateCooking(PX_PHYSICS_VERSION, *mFoundation, physx::PxCookingParams(scale));
	if (!mCooking) {
		throw new EngineException(std::string("Physics: Create PxCreateCooking failed!"));
	}
	if (!PxInitExtensions(*mPhysics)) {
		throw new EngineException(std::string("Physics: PxInitExtensions failed!"));
	}

	physx::PxInitVehicleSDK(*mPhysics, serializationRegistry);
	physx::PxVehicleSetBasisVectors(physx::PxVec3(0, 1, 0), physx::PxVec3(0, 0, 1));
	physx::PxVehicleSetUpdateMode(physx::PxVehicleUpdateMode::eVELOCITY_CHANGE);

	collisionHandler = new CollisionHandler();

	//---Create the physics scene
	//Note: We need a central physics scene if we want to have additive world loading.
	//Unfortunately, this means we'll need to recreate a scene if you want to change the filter.
	//It would be best if there is a global filter shader until we can find a way around this.
	physx::PxSceneDesc sceneDesc(mPhysics->getTolerancesScale());
	sceneDesc.cpuDispatcher = physx::PxDefaultCpuDispatcherCreate(1);
	sceneDesc.gravity = physx::PxVec3(0.0f, -49.05f, 0.0f);
	sceneDesc.filterShader = VehicleFilterShader;
	sceneDesc.simulationEventCallback = collisionHandler;
	scene = mPhysics->createScene(sceneDesc);
	if (!scene)
		throw new EngineException(std::string("Physics: Could not initialize Physics Scene!"));
	
}

Physics::~Physics() {
	delete collisionHandler;
	scene->release();
	PxCloseVehicleSDK(serializationRegistry);
	mCooking->release();
	mPhysics->release();
	mProfileZoneManager->release();
	mFoundation->release();
}

physx::PxPhysics* Physics::getPhysics() {
	return mPhysics;
}

physx::PxDefaultAllocator* Physics::getAllocator() {
	return &gDefaultAllocatorCallback;
}

physx::PxCooking* Physics::getCooking() {
	return mCooking;
}

physx::PxScene* Physics::getScene()
{
	return scene;
}

bool Physics::getCollision()
{
	return collisionHandler->getCollision();
}

void Physics::setCollision(bool col)
{
	collisionHandler->setCollision(col);
}