//
// Created by Jade White on 2016-03-20.
//

#pragma once

#include "Types/Bounds.h"
#include "Types/Frustum.h"

class Camera;

///<summary> Helps with occulsion culling. </summary>
class CullingHelper
{
public:
	bool isCulled(Camera* camera, Bounds worldSpaceBounds);

private:
	bool boxInFrustum(Frustum fru, Bounds bounds);
};

#include "../Components/Camera.h"

