//
// Created by Jade White on 2016-02-29.
//

#pragma once
#include <glew.h>
#include <vector>

class GLMeshBuffers;
class Material;
///<summary> Handles the statey open GL things for meshRenderer. </summary>
class MeshRenderGL
{
private:
	GLint gpuTextureUnits = 0;

public:
	MeshRenderGL();
	virtual ~MeshRenderGL();

	///<summary> Sets the texture units on the GPU to use the textures. </summary>
	void setUpTextures(Material* material);

	///<summary> Handles drawing the mesh, enabling and disabling the Vertex Attributes. </summary>
	void drawMesh(GLMeshBuffers* mesh, bool vertexLines = false);
};


