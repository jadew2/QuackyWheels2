#include "RenderCollection.h"

#include "../Components/Camera.h"
#include "../Components/MeshRenderer.h"
#include "../Components/LightSource.h"
#include "../ComponentEntity/GameObject.h"


RenderCollection::RenderCollection()
{
}


void RenderCollection::addCamera(Camera* camera)
{
	cameras.push_back(camera);
}

void RenderCollection::removeCamera(Camera* camera)
{
	for (size_t i = 0; i < cameras.size(); ++i)
	{
		if (cameras[i] == camera)
			cameras.erase(cameras.begin() + i);
	}
}

void RenderCollection::addMeshRender(MeshRenderer* meshRenderer)
{
	meshRenderers.push_back(meshRenderer);
}

void RenderCollection::removeMeshRender(MeshRenderer* meshRenderer)
{
	for (size_t i = 0; i < meshRenderers.size(); ++i)
	{
		if (meshRenderers[i] == meshRenderer)
			meshRenderers.erase(meshRenderers.begin() + i);
	}
}

void RenderCollection::addUIRender(MeshRenderer* uiRenderer)
{
	uiRenderers.push_back(uiRenderer);
}

void RenderCollection::removeUIRender(MeshRenderer* uiRenderer)
{
	for (size_t i = 0; i < uiRenderers.size(); ++i)
	{
		if (uiRenderers[i] == uiRenderer)
			uiRenderers.erase(uiRenderers.begin() + i);
	}
}


void RenderCollection::addTempRenderer(GameObject* gameObject, MeshRenderer* renderer)
{
	tempRendererGameObjects.push_back(gameObject);
	tempRenderers.push_back(renderer);
}


void RenderCollection::clearTempRenderers()
{
	for (auto gameObject : tempRendererGameObjects)
		gameObject->SafeDelete();

	tempRendererGameObjects.clear();
	tempRenderers.clear();
}


void RenderCollection::addImageEffect(ImageEffectRenderer* imageEffect)
{
	imageEffects.push_back(imageEffect);
}

void RenderCollection::removeImageEffect(ImageEffectRenderer* imageEffect)
{
	for (size_t i = 0; i < imageEffects.size(); ++i)
	{
		if (imageEffects[i] == imageEffect)
			imageEffects.erase(imageEffects.begin() + i);
	}
}


std::vector<Camera*> RenderCollection::getCameras()
{
	return cameras;
}

std::vector<MeshRenderer*> RenderCollection::getSolidMeshRenderers()
{
	return meshRenderers;
}

std::vector<MeshRenderer*> RenderCollection::getUIRenderers()
{
	return uiRenderers;
}


std::vector<MeshRenderer*> RenderCollection::getTempRenderers()
{
	return tempRenderers;
}


std::vector<ImageEffectRenderer*> RenderCollection::getImageEffectRenderers()
{
	return imageEffects;
}


void RenderCollection::AddLight(LightSource* light)
{
	lights.push_back(light);
}

void RenderCollection::RemoveLight(LightSource* light)
{
	for (size_t i = 0; i < lights.size(); ++i)
	{
		if (lights[i] == light)
			lights.erase(lights.begin() + i);
	}
}

std::vector<LightSource*> RenderCollection::getLights()
{
	return lights;
}
