//
// Created by Jade White on 2016-03-21.
//

#include "RenderLayerMask.h"

RenderLayerMask::RenderLayerMask()
{
	layerMask = 0;
	add(RenderLayer::Default); //Add the default layer
}

void RenderLayerMask::add(RenderLayer layer)
{
	layerMask |= (unsigned long)layer;
}

void RenderLayerMask::remove(RenderLayer layer)
{
	layerMask &= ~((unsigned long)layer);
}

void RenderLayerMask::add(RenderLayerMask otherMask)
{
	layerMask |= otherMask.layerMask;
}

void RenderLayerMask::remove(RenderLayerMask otherMask)
{
	layerMask &= ~(otherMask.layerMask);
}

bool RenderLayerMask::collides(RenderLayerMask otherMask)
{
	return (layerMask & otherMask.layerMask) > 0;
}


