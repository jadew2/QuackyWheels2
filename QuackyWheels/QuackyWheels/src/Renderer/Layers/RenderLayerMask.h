//
// Created by Jade White on 2016-03-21.
//

#pragma once

#include "RenderLayer.h"

///<summary> Helps people define layers they want to include </summary>
class RenderLayerMask
{
private:
	unsigned long layerMask = 0;

public:
	///<summary> Creates a new Mask. The default later is added automatically. </summary>
	RenderLayerMask();

	///<summary> Adds the specified layer to the Mask. Remember the Default layer is added automatically. </summary>
	void add(RenderLayer layer);
	///<summary> Removes the specified layer to the Mask. </summary>
	void remove(RenderLayer layer);

	///<summary> Adds the layer(s) in the other mask to this mask. </summary>
	void add(RenderLayerMask otherMask);
	///<summary> Removes the layer(s) from the other mask on this mask. </summary>
	void remove(RenderLayerMask otherMask);

	///<summary> Returns true if the the passed mask has a matching layer to this one. Super fast. </summary>
	bool collides(RenderLayerMask otherMask);
};

