//
// Created by Jade White on 2016-03-21.
//

#pragma once


enum RenderLayer
{
	Default = 0x1,//..000001
	Player0 = 0x2,//..000010
	Player1 = 0x4,
	Player2 = 0x8,
	Player3 = 0x10,
	Layer5 = 0x20,
	Layer6 = 0x40,
	Layer7 = 0x80,
	Layer8 = 0x100,
	Layer9 = 0x200,
	Layer10 = 0x400,
	Layer11 = 0x800,
	//...We support up to 32 layers.
	//Just follow the pattern to add more.

	All = 0xFFFFFFFF,
};
