//
// Created by Jade White on 2016-02-29.
//

#include "MeshRenderGL.h"

#include <glew.h>
#include "Types/Mesh.h"
#include "Types/GLMesh.h"
#include "GLMeshBuffers.h"
#include "Types/Texture.h"
#include "Types/Material.h"

#include <vector>
using namespace std;

MeshRenderGL::MeshRenderGL()
{
	glGetIntegerv(GL_MAX_TEXTURE_IMAGE_UNITS, &gpuTextureUnits);
}

MeshRenderGL::~MeshRenderGL()
{

}

void MeshRenderGL::setUpTextures(Material* material)
{
	vector<Texture*> textures = material->getTextures();
	if (textures.size() > (unsigned int)gpuTextureUnits)
		std::cout << "MeshRenderGL: Warning: The number of GPU texture units are too small for this shader. Textures might be cut off" << std::endl;

	for (size_t i = 0; i < textures.size() && i < (unsigned int)gpuTextureUnits; ++i)
	{
		//When ApplyShaderProperties() is called, the textures are set in the shader to be the correct index.
		textures[i]->setTextureUnit((unsigned int)i);
		//material->setTexture(textures[i], i);

		//Activate this texture slot on the GPU...
		glActiveTexture(GL_TEXTURE0 + (unsigned int)i);
		glBindTexture(GL_TEXTURE_2D, textures[i]->getGLTexture()); //Set it to be this texture.
	}

	if (textures.size() > 1)
	{
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, textures[1]->getGLTexture()); //Set it to be this texture.
	}
}

void MeshRenderGL::drawMesh(GLMeshBuffers* glMesh, bool vertexLines)
{
	GLuint vertexArrayObject = glMesh->getVAO();
	glBindVertexArray(vertexArrayObject);

	//First enable all our vertex arrays
	for (GLuint i = 0; i < glMesh->attributeCount; ++i)
		glEnableVertexAttribArray(i);

	//Draw the mesh
	if (vertexLines)
		glDrawArrays(GL_LINE_STRIP, 0, glMesh->verticies.size());
	else
		glDrawElements(GL_TRIANGLES, glMesh->triangles.size(), GL_UNSIGNED_INT, (void*)0);

	//Disable all our vertex arrays
	for (GLuint i = 0; i < glMesh->attributeCount; ++i)
		glDisableVertexAttribArray(i);

	glBindVertexArray(0);
}
