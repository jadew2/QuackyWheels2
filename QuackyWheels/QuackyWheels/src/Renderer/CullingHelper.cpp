//
// Created by Jade White on 2016-03-20.
//

#include "CullingHelper.h"


bool CullingHelper::isCulled(Camera* camera, Bounds worldSpaceBounds)
{
	return ! boxInFrustum(*camera->getFrustum(), worldSpaceBounds);
}

///<summary> False if fully outside, true if inside or intersects </summary>
///<remarks> Thanks to http://www.iquilezles.org/www/articles/frustumcorrect/frustumcorrect.htm </remarks>
bool CullingHelper::boxInFrustum(Frustum fru, Bounds bounds)
{
	//Basically, for each point in the bounding box - if a vertex isn't inside the frustum, don't draw it.
	vector<Plane> frustumPlanes = fru.planes;

	//Get all the points of the bounding box.
	Vector3 max = bounds.GetMax();
	Vector3 min = bounds.GetMin();
	vector<Vector3> points = vector<Vector3>(8);
	points[0] = min;
	points[1] = Vector3(max.x,min.y,min.z);
	points[2] = Vector3(min.x,max.y,min.z);
	points[3] = Vector3(min.x,min.y,max.z);
	points[4] = Vector3(min.x,max.y,max.z);
	points[5] = max;
	points[6] = Vector3(max.x,max.y,min.z);
	points[7] = Vector3(max.x,min.y,min.z);

	//Now for each plane...
	for (int j = 0; j < 6; ++j)
	{
		Plane currPlane = frustumPlanes[j];

		for (unsigned int i = 0; i < points.size(); ++i)
		{
			if (currPlane.IsOnOrAbove(points[i]))
			{
				//A point is inside our Frustum. We can't cull this!
				return true;
			}
		}
	}
	//If we are here, no point on our box is inside the frustum. Looks like we can cull this.
	return false;

	// check bounds outside/inside of frustum
	//6 planes
	/*for(int i=0; i<6; i++ )
	{
		//planeNormals used to be mPlane
		int out = 0;
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMinX, bounds.mMinY, bounds.mMinZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMaxX, bounds.mMinY, bounds.mMinZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMinX, bounds.mMaxY, bounds.mMinZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMaxX, bounds.mMaxY, bounds.mMinZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMinX, bounds.mMinY, bounds.mMaxZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMaxX, bounds.mMinY, bounds.mMaxZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMinX, bounds.mMaxY, bounds.mMaxZ, 1.0f) ) < 0.0 )?1:0);
		out += ((dot( fru.planeNormals[i], vec4(bounds.mMaxX, bounds.mMaxY, bounds.mMaxZ, 1.0f) ) < 0.0 )?1:0);
		if(out == 8)
			return false;
	}

	// check frustum outside/inside bounds
	//8 points
	int out;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].x > bounds.mMaxX)?1:0); if( out==8 ) return false;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].x < bounds.mMinX)?1:0); if( out==8 ) return false;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].y > bounds.mMaxY)?1:0); if( out==8 ) return false;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].y < bounds.mMinY)?1:0); if( out==8 ) return false;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].z > bounds.mMaxZ)?1:0); if( out==8 ) return false;
	out = 0; for( int i=0; i<8; i++ ) out += ((fru.points[i].z < bounds.mMinZ)?1:0); if( out==8 ) return false;

	return true;
	 */
}