//
// Created by Jade White on 2016-02-10.
//

#pragma once

#include <string>
#include <glew.h>
#include "../../BaseTypes/Matrix4x4.h"
#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Vector2.h"
#include "Color.h"

///<summary> Class that represents a full shader (fragment and vertex shader joined) </summary>
class Texture;
class Shader
{
public:
	std::string Name;

///<summary>Shader File Representation for OpenGL </summary>
GLuint glShaderProgram = 0;

public:
	std::string source = "None";
	Shader(GLuint shaderProgram);
	Shader(GLuint shaderProgram, std::string shaderName, std::string source);
	virtual ~Shader();

	///<summary> Returns the unique ID of the uniform in the shader. Useful if you want to set things directly. </summary>
	GLint getPropertyIndex(std::string propertyName);

	bool hasProperty(std::string propertyName);

	///<summary> Get the shader program for OpenGL.</summary>
	GLuint getGLShader();

	int getInt(GLint propertyIndex);
	///<summary> Sets the integer directly on this shader. </summary>
	void setInt(GLint propertyIndex, int value);

	float getFloat(GLint propertyIndex);
	///<summary> Sets the float directly on this shader. </summary>
	void setFloat(GLint propertyIndex, float value);

	Vector3 getVector3(GLint propertyIndex);
	///<summary> Sets the Vector3 directly on this shader.  </summary>
	void setVector3(GLint propertyIndex, Vector3 value);

	Vector2 getVector2(GLint propertyIndex);
	///<summary> Sets the Vector3 directly on this shader.  </summary>
	void setVector2(GLint propertyIndex, Vector2 value);

	Color getColor(GLint propertyIndex);
	///<summary> Sets the Color directly on this shader. </summary>
	void setColor(GLint propertyIndex, Color value);

	///<summary> Sets the Matrix directly on this shader.  </summary>
	void setMatrix4x4(GLint propertyIndex, Matrix4x4 value);

	///<summary> Sets the Texture directly on this shader. </summary>
	void setTexture(GLint propertyIndex, Texture* texture);


//region Generic Methods

	//Same name version of the above methods. Useful for C++ generics.
	void set(GLint propertyIndex, int value);
	void set(GLint propertyIndex, float value);
	void set(GLint propertyIndex, Vector3 value);
	void set(GLint propertyIndex, Vector2 value);
	void set(GLint propertyIndex, Color value);
	void set(GLint propertyIndex, Matrix4x4 value);
	void set(GLint propertyIndex, Texture* value);

	int get(GLint propertyIndex, int dummyValue);
	float get(GLint propertyIndex, float dummyValue);
	Vector3 get(GLint propertyIndex, Vector3 dummyValue);
	Vector2 get(GLint propertyIndex, Vector2 dummyValue);
	Color get(GLint propertyIndex, Color dummyValue);
	//Texture and Mat4x4 are not possible to be fetched: Consider using Material to get these types.

//endregion

};

