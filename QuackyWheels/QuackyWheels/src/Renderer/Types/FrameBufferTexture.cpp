//
// Created by Jade White on 2016-04-05.
//

#include "FrameBufferTexture.h"
#include "../../Exceptions/EngineException.h"
#include "FrameBuffer.h"
#include <string>
using namespace std;

FrameBufferTexture::FrameBufferTexture(FrameBuffer* frameBuffer, GLuint width, GLuint height, int unit) : Texture(height,width)
{
	relatedFrameBuffer = frameBuffer;
	glFrameBuffer = relatedFrameBuffer->getGLFrameBuffer();
	relatedFrameBuffer->attachColorTexture(this, unit);
}


FrameBufferTexture::~FrameBufferTexture()
{
	relatedFrameBuffer->detachColorTexture(this);
}


Color FrameBufferTexture::getPixel(unsigned int x, unsigned int y)
{
	GLubyte bytes[4];

	glBindFramebuffer(GL_FRAMEBUFFER, glFrameBuffer);
	glReadPixels(x,y,1,1,GL_RGBA,GL_UNSIGNED_BYTE,bytes);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	Color newColor(bytes[0],bytes[1],bytes[2],bytes[3]);
	newColor.normalize(); //Clamp between 0 and 1.
	return newColor;
}

void FrameBufferTexture::setPixel(unsigned int x, unsigned int y, Color color)
{
	throw new EngineException(string("FrameBufferTexture: SetPixel: OpenGL provides no efficient way of setting a single pixel. Use SetPixels instead."));
}


void FrameBufferTexture::setPixels(std::vector<GLubyte> data)
{
	if (data.size() != pixelWidth*pixelHeight)
		throw new EngineException(string("FrameBufferTexture: setPixels: Data array is not the correct size to set all pixels of the framebuffer."));

	setPixels(pixelWidth,pixelHeight,data);
}

void FrameBufferTexture::setPixels(unsigned int width, unsigned int height, std::vector<GLubyte> data)
{
	if (data.size() != width*height)
		throw new EngineException(string("FrameBufferTexture: setPixels: Data array is not the correct size."));

	glBindFramebuffer(GL_FRAMEBUFFER, glFrameBuffer);
	glDrawPixels(width,height,GL_RGBA,GL_UNSIGNED_BYTE,data.data());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

std::vector<GLubyte> FrameBufferTexture::getData()
{
	glData.resize(pixelWidth*pixelWidth);
	glBindFramebuffer(GL_FRAMEBUFFER, glFrameBuffer);
	glReadPixels(0,0,pixelWidth,pixelHeight,GL_RGBA,GL_UNSIGNED_BYTE,glData.data());
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	return glData;
}



GLuint FrameBufferTexture::getGLFrameBuffer()
{
	return glFrameBuffer;
}


