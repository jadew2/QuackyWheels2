//
// Created by Jade White on 2016-02-28.
//

#pragma once

#include <vector>
#include "../../BaseTypes/Vector2.h"

///<summary> Class responsible for converting a AssImp mesh into our mesh. Separated it out into a class because it was huge. </summary>
class Mesh;
struct aiMesh;
class AIMeshConverter
{
public:
	///<summary> Converts the aiMesh to our Mesh, setting it's properties. </summary>
	void Convert(aiMesh* mesh, Mesh* to);

private:
	///<summary> Returns the UV coords for a UV component index. </summary>
	std::vector<Vector2> convertTextureCoords(aiMesh* from, int texCoordIndex);
};


