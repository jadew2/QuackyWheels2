//
// Created by Jade White on 2016-02-10.
//

#include "Material.h"

#include <iostream>
#include <glew.h>

#include "Shader.h"
#include "ShaderProperites/ShaderProperties.h"

Material::Material(Shader* shaderRef)
{
	shader = shaderRef;
	propertyDictionary = map<int, ShaderProperty*>();
	textures = std::vector<Texture*>();

	std::vector<ShaderProperty*> properties = generateShaderProperties(shaderRef);

	//Insert the properties into the list
	for (ShaderProperty* property : properties)
		propertyDictionary.insert(std::pair<int,ShaderProperty*>((int)property->getIndex(),property));

	drawType = DrawType::Default;
}

Material::~Material()
{
	//Delete all the shader properties.
	for(auto const &entry : propertyDictionary)
		delete entry.second;
}


Shader* Material::getShader()
{
	return shader;
}


void Material::setTexture(Texture* texture, unsigned int mainTexIndex)
{
	setTexture(string("mainTex") + std::to_string(mainTexIndex), texture);
}


std::vector<Texture*> Material::getTextures()
{
	return textures;
}

void Material::applyAllPropertiesOnShader()
{
	for(auto const &entry : propertyDictionary)
	{
		ShaderProperty* property = entry.second;
		property->apply();
	}
}


bool Material::hasProperty(std::string propertyname)
{
	return shader->hasProperty(propertyname);
}


int Material::getInt(string propertyName)
{
	IntProperty* intProperty = (IntProperty*)getProperty(propertyName);
	if (intProperty == nullptr)
		return 0;
	return intProperty->get();
}

void Material::setInt(string propertyName, int value)
{
	IntProperty* intProperty = (IntProperty*)getProperty(propertyName);
	if (intProperty == nullptr)
		return;
	intProperty->set(value);
}

float Material::getFloat(string propertyName)
{
	FloatProperty* floatProperty = (FloatProperty*)getProperty(propertyName);
	if (floatProperty == nullptr)
		return 0;
	return floatProperty->get();
}

void Material::setFloat(string propertyName, float value)
{
	FloatProperty* floatProperty = (FloatProperty*)getProperty(propertyName);
	if (floatProperty == nullptr)
		return;
	floatProperty->set(value);
}

Vector3 Material::getVector3(string propertyName)
{
	Vector3Property* vector3Property = (Vector3Property*)getProperty(propertyName);
	if (vector3Property == nullptr)
		return Vector3::Zero();
	return vector3Property->get();
}

void Material::setVector3(string propertyName, Vector3 value)
{
	Vector3Property* vector3Property = (Vector3Property*)getProperty(propertyName);
	if (vector3Property == nullptr)
		return;
	vector3Property->set(value);
}

Vector2 Material::getVector2(string propertyName)
{
	Vector2Property* vector2Property = (Vector2Property*)getProperty(propertyName);
	if (vector2Property == nullptr)
		return Vector2::Zero();
	return vector2Property->get();
}

void Material::setVector2(string propertyName, Vector2 value)
{
	Vector2Property* vector2Property = (Vector2Property*)getProperty(propertyName);
	if (vector2Property == nullptr)
		return;
	vector2Property->set(value);
}

Color Material::getColor(string propertyName)
{
	ColorProperty* colorProperty = (ColorProperty*)getProperty(propertyName);
	if (colorProperty == nullptr)
		return Color::Black();
	return colorProperty->get();
}

void Material::setColor(string propertyName, Color value)
{
	ColorProperty* colorProperty = (ColorProperty*)getProperty(propertyName);
	if (colorProperty == nullptr)
		return;
	colorProperty->set(value);
}

Matrix4x4 Material::getMatrix4x4(string propertyName)
{
	Matrix4x4Property* matrix4x4Property = (Matrix4x4Property*)getProperty(propertyName);
	if (matrix4x4Property == nullptr)
		return Matrix4x4::Identity();
	return matrix4x4Property->get();
}

void Material::setMatrix4x4(string propertyName, Matrix4x4 value)
{
	Matrix4x4Property* matrix4x4Property = (Matrix4x4Property*)getProperty(propertyName);
	if (matrix4x4Property == nullptr)
		return;
	matrix4x4Property->set(value);
}

Texture* Material::getTexture(string propertyName)
{
	TextureProperty* textureProperty = (TextureProperty*)getProperty(propertyName);
	if (textureProperty == nullptr)
		return 0;
	return textureProperty->get();
}

void Material::setTexture(string propertyName, Texture* texture)
{
	TextureProperty* textureProperty = (TextureProperty*)getProperty(propertyName);
	if (textureProperty == nullptr)
		return;

	//Optimizations for textures.
	removeTexture(textureProperty->get()); //Does nothing if texture is null
	addTexture(texture); //Does nothing if texture is null

	textureProperty->set(texture);
}


void Material::set(int propertyIndex, int value)
{
	IntProperty* aProperty = (IntProperty*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, float value)
{
	FloatProperty* aProperty = (FloatProperty*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, Vector3 value)
{
	Vector3Property* aProperty = (Vector3Property*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, Vector2 value)
{
	Vector2Property* aProperty = (Vector2Property*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, Color value)
{
	ColorProperty* aProperty = (ColorProperty*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, Matrix4x4 value)
{
	Matrix4x4Property* aProperty = (Matrix4x4Property*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

void Material::set(int propertyIndex, Texture* value)
{
	TextureProperty* aProperty = (TextureProperty*)propertyDictionary.at(propertyIndex);
	aProperty->set(value);
}

int Material::get(int propertyIndex, int dummyValue)
{
	IntProperty* aProperty = (IntProperty*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

float Material::get(int propertyIndex, float dummyValue)
{
	FloatProperty* aProperty = (FloatProperty*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

Vector3 Material::get(int propertyIndex, Vector3 dummyValue)
{
	Vector3Property* aProperty = (Vector3Property*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

Vector2 Material::get(int propertyIndex, Vector2 dummyValue)
{
	Vector2Property* aProperty = (Vector2Property*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

Color Material::get(int propertyIndex, Color dummyValue)
{
	ColorProperty* aProperty = (ColorProperty*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

Matrix4x4 Material::get(int propertyIndex, Matrix4x4 dummyValue)
{
	Matrix4x4Property* aProperty = (Matrix4x4Property*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

Texture* Material::get(int propertyIndex, Texture* dummyValue)
{
	TextureProperty* aProperty = (TextureProperty*)propertyDictionary.at(propertyIndex);
	return aProperty->get();
}

int Material::getPropertyIndex(std::string propertyName)
{
	int index = shader->getPropertyIndex(propertyName);
	if (index < 0)
		return -1;
	return index;
}


///<summary> String variation of getShaderProperty </summary>
ShaderProperty* Material::getProperty(string propertyName)
{
	GLint uniformIndex = shader->getPropertyIndex(propertyName);
	if (uniformIndex < 0)
	{
		std::cerr << "Material: getProperty(" << propertyName << "): failed. Could not find index." << std::endl;
		return nullptr;
	}
	return getProperty(uniformIndex);
}

///<summary> Returns the shaderproperty for a given index. </summary>
ShaderProperty* Material::getProperty(GLint propertyIndex)
{
	if (propertyDictionary.count(propertyIndex) == 1)
		return propertyDictionary.at(propertyIndex);
	else
	{
		std::cerr << "Material: getProperty: Warning: Property index " << std::to_string(propertyIndex) << " out of range. It is likely a unhandled type." << std::endl;
		return nullptr;
	}
}


void Material::addTexture(Texture* texture)
{
	if (texture == nullptr)
		return;
	textures.push_back(texture);
}

void Material::removeTexture(Texture* texture)
{
	if (texture == nullptr)
		return;

	for (unsigned int i = 0; i < textures.size(); ++i)
	{
		if (textures[i] == texture)
		{
			textures.erase(textures.begin() + i);
			return;
		}
	}
}

std::vector<ShaderProperty*> Material::generateShaderProperties(Shader* shader)
{
	GLuint glShader = shader->getGLShader();

	GLint uniformCount;
	glGetProgramiv(glShader,GL_ACTIVE_UNIFORMS,&uniformCount);

	std::vector<ShaderProperty*> properties = std::vector<ShaderProperty*>();
	properties.reserve((unsigned long)uniformCount);

	//Optimization
	const unsigned int charBufferLength = 64;
	std::vector<GLchar> charName; //Not string for a reason
	charName.resize(charBufferLength);

	for (int i = 0; i < uniformCount; ++i)
	{
		//First get the name and type
		GLsizei nameLength;
		GLint size;
		GLenum type;
		string name;

		glGetActiveUniform(glShader, (GLuint)i, charBufferLength, &nameLength, &size, &type, &charName[0]);
		name = string(charName.begin(), charName.begin() + nameLength);

		//Depending on what unhandled types your machine produces, the uniform location can vary!
		GLint uniformLocation = glGetUniformLocation(glShader, name.c_str());

		//Now create the shader property
		ShaderProperty* property = nullptr;
		switch (type)
		{
			case GL_INT:
				property = new IntProperty(shader, name, uniformLocation);
				break;
			case GL_FLOAT:
				property = new FloatProperty(shader, name, uniformLocation);
				break;
			case GL_FLOAT_MAT4: //Matrix4x4
				property = new Matrix4x4Property(shader, name, uniformLocation);
				break;
			case GL_SAMPLER_2D: //Texture 2D
				property = new TextureProperty(shader, name, uniformLocation);
				break;
			case GL_FLOAT_VEC2:
				property = new Vector2Property(shader, name, uniformLocation);
				if (name == "mainTex0Scale" || name == "mainTex1Scale")
					((Vector2Property*)property)->set(Vector2(1,1));
				break;
			case GL_FLOAT_VEC3:
				property = new Vector3Property(shader, name, uniformLocation);
				break;
			case GL_FLOAT_VEC4: //HACK: Right now, we assume all vec4's are Colors
				property = new ColorProperty(shader, name, uniformLocation);
				break;
			default:
				//If you get this message, create a property class for this type.
				std::cout << "Material: findShaderProperties: Warning: Uniform " << name << " has a unhandled type " << std::to_string(type) << ". This may have unpredicatable uniform values." << std::endl;
		}

		if (property != nullptr)
			properties.push_back(property);
	}

	return properties;
}

