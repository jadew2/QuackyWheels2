//
// Created by Jade White on 2016-02-10.
//

#include "Texture.h"
#include "../../Exceptions/EngineException.h"

Texture::Texture(GLuint height, GLuint width)
{
	glGenTextures(1, &glTexture);
	pixelHeight = height;
	pixelWidth = width;
	glTextureUnit = 0;

	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pixelWidth, pixelHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}


Texture::Texture(GLuint height, GLuint width, std::vector<GLubyte> rgbaDataRef) : Texture(height,width)
{
	glData = rgbaDataRef;

	//Update GL stored texture
	updateGLTexture();

	//Default is MipMapping, so the texture ends up black if this isn't specified.
	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	setInterpolation(Interpolation::MipMapping);
	//setWrapping(Wrapping::Repeat); //Dunno what's wrong with this
}

///<summary> Sends texture information to openGL. </summary>
void Texture::updateGLTexture()
{
	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, pixelWidth, pixelHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, glData.data());
	glBindTexture(GL_TEXTURE_2D, 0);
}


Texture::~Texture()
{
	glDeleteTextures(1, &glTexture);
}

Color Texture::getPixel(unsigned int x, unsigned int y)
{
	size_t rowStart = y * pixelWidth * 4; //4 for rgba
	size_t index = rowStart + x * 4;

	float r,g,b,a = 0;
	r = (float) glData[index];
	g = (float) glData[index+1];
	b = (float) glData[index+2];
	a = (float) glData[index+3];

	Color newColor(r,g,b,a);
	newColor.normalize(); //Clamp between 0 and 1.
	return newColor;
}

void Texture::setPixel(unsigned int x, unsigned int y, Color color)
{
	size_t rowStart = y * pixelWidth * 4; //4 for rgba
	size_t index = rowStart + x * 4;

	color.normalize();
	glData[index] = (unsigned char)(color.r * 255.0f);
	glData[index+1] = (unsigned char)(color.g * 255.0f);
	glData[index+2] = (unsigned char)(color.b * 255.0f);
	glData[index+3] = (unsigned char)(color.a * 255.0f);

	//resend this Info to OpenGL.
	updateGLTexture();
}

GLuint Texture::getHeight()
{
	return pixelHeight;
}

GLuint Texture::getWidth()
{
	return pixelWidth;
}


Texture::Wrapping Texture::getWrapping()
{
	return wrapping;
}

void Texture::setWrapping(Wrapping wrappingVal)
{
	GLint glWrapping = GL_REPEAT;

	switch (wrappingVal)
	{
		case Wrapping::Repeat :
			glWrapping = GL_REPEAT;
			break;
		case Wrapping::RepeatMirrored:
			glWrapping = GL_MIRRORED_REPEAT;
			break;
		case Wrapping::Clamp:
			glWrapping = GL_CLAMP_TO_EDGE;
			break;
		case Wrapping::ClampBorder:
			glWrapping = GL_CLAMP_TO_BORDER;
			break;
		default:
			throw new EngineException("Texture: SetWrapping: Unhandled wrapping type.");
	}

	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, glWrapping);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, glWrapping);
	glBindTexture(GL_TEXTURE_2D,0);

	wrapping = wrappingVal;
}

Texture::Interpolation Texture::getInterpolation()
{
	return interpolation;
}

void Texture::setInterpolation(Texture::Interpolation interpolationVal)
{
	GLint glInterpolationMin = GL_NEAREST;
	GLint glInterpolationMag = GL_NEAREST;

	switch (interpolationVal)
	{
		case Interpolation::Point :
			glInterpolationMin = GL_NEAREST;
			glInterpolationMag = GL_NEAREST;
			break;
		case Interpolation::Linear :
			glInterpolationMin = GL_LINEAR;
			glInterpolationMag = GL_LINEAR;
			break;
		case Interpolation::MipMapping:
			glInterpolationMin = GL_LINEAR;
			glInterpolationMag = GL_LINEAR_MIPMAP_LINEAR;
			break;
		default:
			throw new EngineException("Texture: SetInterpolation: Unhandled interpolation type.");
	}

	glBindTexture(GL_TEXTURE_2D, glTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, glInterpolationMin);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, glInterpolationMag);

	if (interpolationVal == Interpolation::MipMapping)
		glGenerateMipmap(GL_TEXTURE_2D);

	glBindTexture(GL_TEXTURE_2D,0);

	interpolation = interpolationVal;
}


void Texture::setTextureUnit(int unitIndex)
{
	if (unitIndex < 0)
		unitIndex = 0;
	glTextureUnit = unitIndex;
}

GLint Texture::getTextureUnit()
{
	return glTextureUnit;
}


GLuint Texture::getGLTexture()
{
	return glTexture;
}

std::vector<GLubyte> Texture::getData()
{
	return glData;
}


