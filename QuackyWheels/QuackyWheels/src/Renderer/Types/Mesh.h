//
// Created by Jade White on 2016-02-10.
//

#pragma once

#include <vector>
#include <string>
#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Vector2.h"
#include "../../BaseTypes/Matrix4x4.h"
#include "Color.h"
#include "Bounds.h"

///<summary> Class that represents a mesh and all it's properties </summary>
class GLMeshBuffers;
class NavMesh;
class Bounds;
class Mesh
{
public:
	std::string Name;

	Mesh();
	virtual ~Mesh();

	///	<summary> A centered default quad mesh. </summary>
	static Mesh* Quad();

	///<summary> Creates a quad with 4 points. </summary>
	static Mesh* Quad(Vector2 bl, Vector2 br, Vector2 tr, Vector2 tl);

	std::vector<Vector3> verticies;
	std::vector<unsigned long> triangles;

	std::vector<Vector3> tangents;

	std::vector<Vector3> normals;
	std::vector<Vector2> uv0;
	std::vector<Vector2> uv1;
	std::vector<Color> colors;

	///<summary> Scales the vertices in vertices. Note: This modifies the mesh data. </summary>
	void Scale(Vector3 scale);
	///<summary> Transforms, rotates, and scales the mesh data. Note: This modifies the mesh data. </summary>
	void Deform(Matrix4x4 trsMatrix);

	///<summary> Clones all info on this mesh onto the other mesh. </summary>
	void CloneTo(Mesh *otherMesh);
	///<summary> Duplicates this mesh </summary>
	Mesh* Clone();

//region Mesh Specializations

	//<summary> Set this true to have low level systems update their data structures for this mesh. </summary>
	void setDirty();

private:
	Bounds* bounds = nullptr;
	GLMeshBuffers* glMesh = nullptr;
	NavMesh* navMesh = nullptr;

	bool boundsDirty = false;
	bool glMeshDirty = false;
	bool navMeshDirty = false;

public:
	///<summary> Returns the mesh bounds. It's sometimes an intensive operation, so you can optionally not recalculate. </summary>
	Bounds* getBounds(bool recalculate = true);
	///<summary> Returns the mesh version to be used with rendering. Reflects the latest changes via lazy evaluation. </summary>
	GLMeshBuffers* getGLMesh();
	///<summary> Returns the mesh version to be used with rendering. Reflects the latest changes via lazy evaluation.  </summary>
	NavMesh* getNavMesh();
//endregion
};

std::ostream& operator<<(std::ostream& os, const Mesh& mesh);

