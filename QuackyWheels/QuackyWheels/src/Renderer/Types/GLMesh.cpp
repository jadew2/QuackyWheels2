//
// Created by Jade White on 2016-03-01.
//

#include "GLMesh.h"
#include "Mesh.h"

GLMesh::GLMesh()
{
	verticies = std::vector<glm::vec3>();
	triangles = std::vector<GLuint>();

	tangents = std::vector<glm::vec3>();
	normals = std::vector<glm::vec3>();

	uv0 = std::vector<glm::vec2>();
	uv1 = std::vector<glm::vec2>();

	colors = std::vector<glm::vec4>();
}


GLMesh::~GLMesh()
{

}

void GLMesh::Clear()
{
	verticies.clear();
	triangles.clear();

	tangents.clear();
	normals.clear();

	uv0.clear();
	uv1.clear();

	colors.clear();
}


void GLMesh::Set(Mesh* mesh)
{
	//Overwrites if possible
	//Corrects the lengths of all the vertex attribute arrays so they match vertexCount.

	{//Vertices
		size_t vertexCount = mesh->verticies.size();
		verticies.resize(vertexCount);
		for (size_t i = 0; i < vertexCount; ++i)
			verticies[i] = (mesh->verticies[i]);
	}

	{//Triangles
		size_t triangleCount = mesh->triangles.size();
		triangles.resize(triangleCount);
		for (size_t i = 0; i < triangleCount; ++i)
			triangles[i] = ((GLuint)mesh->triangles[i]);
	}

	{//Tangents
		size_t tangentCount = mesh->tangents.size();
		tangents.resize(tangentCount);
		for (size_t i = 0; i < tangentCount; ++i)
			tangents[i] = (mesh->tangents[i]);
	}

	{//Normals
		size_t normalCount = mesh->normals.size();
		normals.resize(normalCount);
		for (size_t i = 0; i < normalCount; ++i)
			normals[i] = (mesh->normals[i]);
	}

	{//uv0
		size_t uv0Count = mesh->uv0.size();
		uv0.resize(uv0Count);
		for (size_t i = 0; i < uv0Count; ++i)
			uv0[i] = (mesh->uv0[i]);
	}

	{//uv1
		size_t uv1Count = mesh->uv1.size();
		uv1.resize(uv1Count);
		for (size_t i = 0; i < uv1Count; ++i)
			uv1[i] = (mesh->uv1[i]);
	}

	{//Colors
		size_t colorCount = mesh->colors.size();
		colors.resize(colorCount);
		for (size_t i = 0; i < colorCount; ++i)
			colors[i] = mesh->colors[i].vec4();
	}

	//Now,resize all to be the length of vertexCount
	//We do this at the end to make sure there's no garbage to the right in the array.
	size_t length = mesh->verticies.size();

	verticies.resize(length,glm::vec3());
	//We don't resize triangles, they're unrelated.

	tangents.resize(length,glm::vec3(1,0,0));
	normals.resize(length,glm::vec3(0,1,0));

	uv0.resize(length,glm::vec2());
	uv1.resize(length,glm::vec2());

	colors.resize(length,glm::vec4());
}

GLulong GLMesh::getVerticesByteSize()
{
	return sizeof(glm::vec3)*verticies.size();
}

GLulong GLMesh::getTrianglesByteSize()
{
	return sizeof(GLuint)*triangles.size();
}

GLulong GLMesh::getUVByteSize()
{
	return sizeof(glm::vec2)*uv0.size();
}

GLulong GLMesh::getColorByteSize()
{
	return sizeof(glm::vec4)*colors.size();
}

