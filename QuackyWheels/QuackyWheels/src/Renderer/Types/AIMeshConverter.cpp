//
// Created by Jade White on 2016-02-28.
//

#include "AIMeshConverter.h"
#include <iostream>
#include <mesh.h>
#include "Mesh.h"
#include <string>
using namespace std;

void AIMeshConverter::Convert(aiMesh* mesh, Mesh* to)
{
	to->Name = string(mesh->mName.C_Str());

	unsigned long vertexCount = (unsigned long) mesh->mNumVertices;

	//Vertices are always present.
	to->verticies.resize(vertexCount);
	for (unsigned long i = 0; i < vertexCount; ++i)
		to->verticies[i] = Vector3(mesh->mVertices[i]);

	//Tangents may not be present.
	if (mesh->mTangents != nullptr)
	{
		to->tangents.resize(vertexCount);
		aiVector3D* aiTangents = mesh->mTangents;
		for (unsigned long i = 0; i < vertexCount; ++i)
			to->tangents[i] = Vector3(aiTangents[i]);
	}

	//Normals may not be present.
	if (mesh->mNormals != nullptr)
	{
		to->normals.resize(vertexCount);
		aiVector3D* aiNormals = mesh->mNormals;
		for (unsigned long i = 0; i < vertexCount; ++i)
			to->normals[i] = Vector3(aiNormals[i]);
	}
	else
		std::cout << std::string("AIMeshConverter: Warning: Normals missing.") << std::endl;

	//Faces might be Ngons.
	bool ngonsInMesh = false;
	if (mesh->mFaces != nullptr)
	{
		unsigned long numTriangles = (unsigned long) mesh->mNumFaces;
		to->triangles.resize(numTriangles*3);
		aiFace* aiFaces = mesh->mFaces;
		for (unsigned long i = 0; i < numTriangles; ++i)
		{
			aiFace face = aiFaces[i];

			//AssImp allows faces to have more than 3 vertices (Support for n-gons)
			//OpenGL doesn't like this though - so we'll ignore anything that's not tris.
			if (face.mNumIndices != 3)
			{
				ngonsInMesh = true;
				continue;
			}

			//If we're here, add the triangles.
			unsigned long triIndex = i*3;
			to->triangles[triIndex] = face.mIndices[0];
			to->triangles[triIndex+1] = face.mIndices[1];
			to->triangles[triIndex+2] = face.mIndices[2];
		}
	}
	if (ngonsInMesh)
		std::cout << std::string("AIMeshConverter: Warning: There were n-gons in the imported mesh. There may be holes. Be sure the model is tris.") << std::endl;

	//Convert uv0
	to->uv0 = convertTextureCoords(mesh,0);
	//Convert uv1
	to->uv1 = convertTextureCoords(mesh,1);
}


std::vector<Vector2> AIMeshConverter::convertTextureCoords(aiMesh* mesh, int texCoordIndex)
{
	std::vector<Vector2> result = std::vector<Vector2>();
	//Assimp allows for 1D-3D UV coords. Ignore them if they aren't 2D
	int uvComponents = mesh->mNumUVComponents[texCoordIndex];
	if (uvComponents <= 0)
	{
		std::cout << std::string("AIMeshConverter: Note: Mesh uv") + std::to_string(texCoordIndex) + std::string(" has no texture coords. Texture mapping will be missing.") << std::endl;
		return result;
	}
	else if (uvComponents >= 3)
		std::cout << std::string("AIMeshConverter: Warning: Mesh uv") + std::to_string(texCoordIndex) + std::string(" has 3D+ coords, scrapping extra data. Texture mapping may be incorrect.") << std::endl;

	unsigned long vertexCount = mesh->mNumVertices;
	result.resize(vertexCount);
	for (unsigned long i = 0; i < vertexCount; ++i)
	{
		Vector3 texCoord = Vector3(mesh->mTextureCoords[texCoordIndex][i]); //TexCoordIndex comes before i. Poor AssImp documentation.
		result[i].x = texCoord.x;
		result[i].y = texCoord.y;
	}

	return result;
}



