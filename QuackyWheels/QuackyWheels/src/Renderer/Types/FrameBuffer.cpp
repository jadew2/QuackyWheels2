//
// Created by Jade White on 2016-04-07.
//

#include "FrameBuffer.h"
#include "Texture.h"


FrameBuffer::FrameBuffer(int width, int height)
{
	glGenFramebuffers(1, &glFrameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, glFrameBuffer);

	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, width, height);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);

	colorAttachments.reserve(GL_MAX_COLOR_ATTACHMENTS);
}


FrameBuffer::~FrameBuffer()
{
	glDeleteFramebuffers(1, &glFrameBuffer);
}


void FrameBuffer::attachColorTexture(Texture* texture, int unit)
{
	unit = 0; //FIXME

	//Bind to the texture object
	glBindFramebuffer(GL_FRAMEBUFFER, glFrameBuffer);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0 + unit, texture->getGLTexture(),0);
	GLenum drawbuffers[1] = { GL_COLOR_ATTACHMENT0 + unit};
	glDrawBuffers(1, drawbuffers);
}

void FrameBuffer::detachColorTexture(Texture* texture)
{
	/*
	//Do something...
	for (size_t i = 0; i < colorAttachments.size(); ++i)
	{
		if (colorAttachments[i] == texture)
			colorAttachments.erase(colorAttachments.begin() + i);
	}
	 */
}


GLuint FrameBuffer::getGLFrameBuffer()
{
	return glFrameBuffer;
}