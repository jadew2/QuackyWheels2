//
//  Bounds.h
//  HW2
//
//  Created by Jade White on 2014-10-24.
//  Copyright (c) 2014 Jade White. All rights reserved.
//

#pragma once

#include <vector>
#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Matrix4x4.h"

class Mesh;
/**
 * A local bounding box. Useful for culling or intersection detection.
 * Note that it's in model space, (is not translated, rotated or scaled by Transform. You have to do that.)
 */
class Bounds
{
public:
    Vector3 Centre; 	//Origin of the mesh by geometry
	Vector3 Extents; 	//(FrontLeftCorner - Center)

	Vector3 GetMax();	//FrontLeftCorner point (Center + Extents) //Top,Right,Front point?
	Vector3 GetMin();	//BackRightCorner point (Center - Extents)
	Vector3 GetSize();	//x,y,z size of the bounding box. It's 2*Extents.

	Bounds();

	///<summary> Set properties based off the mesh. </summary>
	void Set(Mesh* mesh);

	///<summary> Returns a bounds which is translated, rotated, and scaled. </summary>
	Bounds deform(Matrix4x4 trs);
};