//
// Created by Jade White on 2016-03-20.
//

#include "Frustum.h"

Frustum::Frustum()
{

}

Frustum::Frustum(Camera* camera)
{
	planes = vector<Plane>();
	planes.resize(6); //A frustum is made of 6 planes.
	Set(camera);
}


Frustum::Frustum(Vector3 origin, Quaternion rotation, float aspectRatio, float fovRadians, float nearDistance, float farDistance)
{
	Set(origin, rotation, aspectRatio, fovRadians, nearDistance, farDistance);
}


void Frustum::Set(Camera* camera)
{
	Set(
			camera->GetTransform()->GetPosition(),
			camera->GetTransform()->GetRotation(),
			camera->getAspect(),
			camera->getFOV(),
			camera->getNearClipping(),
			camera->getFarClipping()
	);
}


void Frustum::Set(Vector3 origin, Quaternion rotation, float aspectRatio, float fovRadians, float nearDistance, float farDistance)
{
	//See http://gamedev.stackexchange.com/questions/71058/how-extract-frustum-planes-from-clip-coordinates

	/*
	+-----------+
	| \       / |
	|   +---+   |
	|   |   |   |
	|   +---+   |
	| /       \ |
	+-----------+
	*/

	Vector3 forwardVector = rotation * Vector3::Forward();
	Vector3 upVector = rotation * Vector3::Up();
	Vector3 rightVector = rotation * Vector3::Right();

	nearCenter = origin + (forwardVector * nearDistance);
	farCenter = origin + (forwardVector * farDistance);

	nearHeight = 2.0f * nearDistance * tan(fovRadians * 0.5f); //FIXME: Leave out x2?
	nearWidth = nearHeight * aspectRatio;

	farHeight = 2.0f * farDistance * tan(fovRadians * 0.5f);
	farWidth = nearHeight * aspectRatio;

	nearLeftTop     = nearCenter - rightVector * nearWidth + upVector * nearHeight;
	nearLeftBottom  = nearCenter - rightVector * nearWidth - upVector * nearHeight;
	nearRightTop    = nearCenter + rightVector * nearWidth + upVector * nearHeight;
	nearRightBottom = nearCenter + rightVector * nearWidth - upVector * nearHeight;

	farLeftTop      = farCenter - rightVector * farWidth + upVector * farHeight;
	farLeftBottom   = farCenter - rightVector * farWidth - upVector * farHeight;
	farRightTop     = farCenter + rightVector * farWidth + upVector * farHeight;
	farRightBottom  = farCenter + rightVector * farWidth - upVector * farHeight;

	planes.resize(6);

	planes[0] = Plane(nearCenter, forwardVector);
	planes[1] = Plane(farCenter, forwardVector * -1.0f);

	//From CalcPointAndNormal
	planes[2] = Plane(farLeftTop, (nearCenter + upVector * farWidth), farRightTop);
	planes[3] = Plane(farRightBottom, (nearCenter - upVector * nearWidth), farLeftBottom);

	planes[4] = Plane(farLeftBottom, (nearCenter - rightVector * nearWidth), farLeftTop);
	planes[5] = Plane(farRightTop, (nearCenter + rightVector * nearWidth), farRightBottom);
}


