//
//  Bounds.cpp
//  Created by Jade White on 2014-10-24.
//

#include "Bounds.h"
#include "Mesh.h"
#include <glew.h>

Bounds::Bounds()
{
	Centre = Vector3::Zero();
	Extents = Vector3::Zero();
}

Vector3 Bounds::GetMax()
{
    return Centre + Extents;
}

Vector3 Bounds::GetMin()
{
    return Centre - Extents;
}

Vector3 Bounds::GetSize()
{
    return Extents * 2.0f;
}


//Sets properties based off the mesh.
void Bounds::Set(Mesh* mesh)
{
	std::vector<Vector3> vertices = mesh->verticies;

	//Calculate "max" and "min".
	Vector3 max = Vector3::Zero();
	Vector3 min = Vector3::Zero();
	for (GLuint i = 0; i < vertices.size(); i++)
	{
		Vector3 curr = vertices[i];
		if (max.x < curr.x)
			max.x = curr.x;
		if (max.y < curr.y)
			max.y = curr.y;
		if (max.z < curr.z)
			max.z = curr.z;

		if (min.x > curr.x)
			min.x = curr.x;
		if (min.y > curr.y)
			min.y = curr.y;
		if (min.z > curr.z)
			min.z = curr.z;
	}

	Centre = (max + min) * 0.5f;

	//We now have the extents of our bounding box.
	Extents = max - Centre;
}



Bounds Bounds::deform(Matrix4x4 trs)
{
	Bounds newBounds;
	newBounds.Centre = trs.MultiplyPoint(Centre);
	newBounds.Extents = trs.MultiplyPoint(Extents); //Yes, not MultiplyVector(). Think of Extents as a local point.
	return newBounds;
}

