//
// Created by Jade White on 2016-02-10.
//

#include "Mesh.h"
#include "AIMeshConverter.h"
#include "../GLMeshBuffers.h"
#include "../../AI/NavMesh.h"
#include "Bounds.h"

Mesh::Mesh()
{
	verticies = std::vector<Vector3>();
	tangents = std::vector<Vector3>();
	triangles = std::vector<unsigned long>();

	normals = std::vector<Vector3>();
	uv0 = std::vector<Vector2>();
	uv1 = std::vector<Vector2>();

	colors = std::vector<Color>();

	//Initialize the specializations.
	//Note: Why do we hold specializations inside Mesh?
	//It's done this way because we can have multiple MeshRenderers that want to use the same OpenGL buffers for our mesh.
	//Alternative approaches are to have a reference count with a dirty flags.
	bounds = new Bounds();
	glMesh = new GLMeshBuffers();
	navMesh = new NavMesh();

	setDirty();
}

Mesh::~Mesh()
{
	//Destroy the specializations.
	delete bounds;
	delete glMesh;
	delete navMesh;
}

void Mesh::CloneTo(Mesh *otherMesh)
{
	otherMesh->verticies.clear();
	otherMesh->verticies = std::vector<Vector3>(verticies);

	otherMesh->tangents.clear();
	otherMesh->tangents = std::vector<Vector3>(tangents);

	otherMesh->triangles.clear();
	otherMesh->triangles = std::vector<unsigned long>(triangles);

	otherMesh->normals.clear();
	otherMesh->normals = std::vector<Vector3>(normals);

	otherMesh->uv0.clear();
	otherMesh->uv0 = std::vector<Vector2>(uv0);

	otherMesh->uv1.clear();
	otherMesh->uv1 = std::vector<Vector2>(uv1);

	otherMesh->colors.clear();
	otherMesh->colors = std::vector<Color>(colors);

	otherMesh->setDirty();
}

Mesh* Mesh::Clone()
{
	Mesh* newMesh = new Mesh();
	CloneTo(newMesh);
	return newMesh;
}

Mesh* Mesh::Quad()
{
	return Quad(Vector2(-0.5f, -0.5f), Vector2(0.5f, -0.5f), Vector2(0.5f, 0.5f), Vector2(-0.5f, 0.5f));
}

Mesh* Mesh::Quad(Vector2 bl, Vector2 br, Vector2 tr, Vector2 tl)
{
	Mesh* newMesh = new Mesh();
	newMesh->verticies = { Vector3(bl.x, bl.y, 0.0f),
		Vector3(br.x, br.y, 0.0f),
		Vector3(tr.x, tr.y, 0.0f),
		Vector3(tl.x, tl.y, 0.0f) };

	newMesh->triangles = { 2, 1, 0, 0, 3, 2 };

	/*newMesh.tangents = { Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f),
		Vector3(1.0f, 0.0f, 0.0f) };*/

	newMesh->normals = { Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f),
		Vector3(0.0f, 0.0f, 1.0f) };

	newMesh->uv0 = { Vector2(0,1),
		Vector2(1, 1),
		Vector2(1, 0),
		Vector2(0, 0) };
	newMesh->uv1 = newMesh->uv0;

	return newMesh;
}

void Mesh::Scale(Vector3 scale)
{
	setDirty();

	for (size_t i = 0; i < verticies.size(); ++i)
		verticies[i] = Vector3::Scale(verticies[i],scale);
}


std::ostream &operator<<(std::ostream &os, const Mesh &mesh)
{
	os << std::string("Mesh:")
	<< "\n   Vertices: " << std::to_string(mesh.verticies.size())
	<< "\n   Triangles: " << std::to_string(mesh.triangles.size())

	<< "\n   Tangents: " << std::to_string(mesh.tangents.size())
	<< "\n   Normals: " << std::to_string(mesh.normals.size())

	<< "\n   uv0: " << std::to_string(mesh.uv0.size())
	<< "\n   uv1: " << std::to_string(mesh.uv1.size())

	<< "\n   colors: " << std::to_string(mesh.colors.size()) << std::endl;
	return os;
}

void Mesh::Deform(Matrix4x4 trs)
{
	setDirty();
	//Deform all vertices
	for (size_t i = 0; i < verticies.size(); ++i)
		verticies[i] = trs.MultiplyPoint(verticies[i]);

	//Deform all normal and tangent directions
	for (size_t i = 0; i < tangents.size(); ++i)
		tangents[i] = trs.MultiplyVector(tangents[i]);
	for (size_t i = 0; i < normals.size(); ++i)
		normals[i] = trs.MultiplyVector(normals[i]);
}


Bounds* Mesh::getBounds(bool recalculate)
{
	if (recalculate && boundsDirty)
	{
		bounds->Set(this);
		boundsDirty = false;
	}

	return bounds;
}


GLMeshBuffers * Mesh::getGLMesh()
{
	//Updates the GL mesh if it's dirty.
	if (glMeshDirty)
	{
		glMesh->Set(this);
		glMeshDirty = false;
	}

	return glMesh;
}

NavMesh* Mesh::getNavMesh()
{
	//Updates the nav mesh if it's dirty.
	if (navMeshDirty)
	{
		navMesh->Set(this);
		navMeshDirty = false;
	}

	return navMesh;
}


void Mesh::setDirty()
{
	boundsDirty = true;
	glMeshDirty = true;
	navMeshDirty = true;
}