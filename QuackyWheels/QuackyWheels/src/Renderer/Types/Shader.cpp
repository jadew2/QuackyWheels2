//
// Created by Jade White on 2016-02-10.
//

#include "Shader.h"
#include "texture.h"
#include <gtc/type_ptr.hpp>
#include<fstream>
#include <iostream>

Shader::Shader(GLuint shaderProgram)
{
	Name = std::string("NoName");
	glShaderProgram = shaderProgram;
}

Shader::Shader(GLuint shaderProgram, std::string shaderName, std::string source) : Shader(shaderProgram)
{
	Name = shaderName;
	this->source = source;
}


Shader::~Shader()
{
	glDeleteProgram(glShaderProgram);
}

GLuint Shader::getGLShader()
{
	return glShaderProgram;
}

GLint Shader::getPropertyIndex(std::string propertyName)
{
	GLint result = glGetUniformLocation(glShaderProgram, propertyName.c_str());
	if (result < 0)
		std::cout << "Shader: getPropertyIndex: Warning: Shader property \"" << propertyName << "\" is not defined in shader :(" << std::endl;
	return result;
}


bool Shader::hasProperty(std::string propertyName)
{
	return glGetUniformLocation(glShaderProgram, propertyName.c_str()) >= 0;
}


int Shader::getInt(GLint propertyIndex)
{
	if (propertyIndex < 0)
		return 0;

	int value = 0;
	glProgramUniform1iv(glShaderProgram,propertyIndex,1,&value);
	return value;
}

void Shader::setInt(GLint propertyIndex, int value)
{
	if (propertyIndex < 0)
		return;
	glProgramUniform1i(glShaderProgram, propertyIndex, value);
}

float Shader::getFloat(GLint propertyIndex)
{
	if (propertyIndex < 0)
		return 0.0f;

	float value = 0;
	glProgramUniform1fv(glShaderProgram, propertyIndex, 1, &value);
	return value;
}

void Shader::setFloat(GLint propertyIndex, float value)
{
	if (propertyIndex < 0)
		return;
	glProgramUniform1f(glShaderProgram, propertyIndex, value);
}

Vector3 Shader::getVector3(GLint propertyIndex)
{
	if (propertyIndex < 0)
		return Vector3::Zero();

	float value[3];
	glProgramUniform3fv(glShaderProgram, propertyIndex, 1, value);
	return Vector3(value[0],value[1],value[2]);
}

void Shader::setVector3(GLint propertyIndex, Vector3 value)
{
	if (propertyIndex < 0)
		return;
	glProgramUniform3f(glShaderProgram, propertyIndex, value.x, value.y, value.z);
}

Vector2 Shader::getVector2(GLint propertyIndex)
{
	if (propertyIndex < 0)
		return Vector2::Zero();

	float value[2];
	glProgramUniform2fv(glShaderProgram, propertyIndex, 1, value);
	return Vector2(value[0], value[1]);
}

void Shader::setVector2(GLint propertyIndex, Vector2 value)
{
	if (propertyIndex < 0)
		return;
	glProgramUniform2f(glShaderProgram, propertyIndex, value.x, value.y);
}

Color Shader::getColor(GLint propertyIndex)
{
	if (propertyIndex < 0)
		return Color::Black();

	float value[4];
	glProgramUniform4fv(glShaderProgram, propertyIndex, 1, value);
	return Color(value[0],value[1],value[2],value[3]);
}

void Shader::setColor(GLint propertyIndex, Color value)
{
	if (propertyIndex < 0)
		return;
	glProgramUniform4f(glShaderProgram, propertyIndex, value.r, value.g, value.b, value.a);
}

void Shader::setMatrix4x4(GLint propertyIndex, Matrix4x4 value)
{
	if (propertyIndex < 0)
		return;

	glm::mat4 glMat4 = value.mat4x4();
	glProgramUniformMatrix4fv(glShaderProgram, propertyIndex, 1, false, value_ptr(glMat4));
}

///<summary> Sets the texture unit index from the passed texture. </summary>
void Shader::setTexture(GLint propertyIndex, Texture* texture)
{
	if (propertyIndex < 0)
		return;
	GLint glTextureUnit = 0;
	if (texture != nullptr)
		glTextureUnit = texture->getTextureUnit();

	glProgramUniform1i(glShaderProgram, propertyIndex, glTextureUnit);
}


void Shader::set(GLint propertyIndex, int value)
{
	setInt(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, float value)
{
	setFloat(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, Vector3 value)
{
	setVector3(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, Vector2 value)
{
	setVector2(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, Color value)
{
	setColor(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, Matrix4x4 value)
{
	setMatrix4x4(propertyIndex,value);
}

void Shader::set(GLint propertyIndex, Texture* value)
{
	setTexture(propertyIndex,value);
}


int Shader::get(GLint propertyIndex, int dummyValue)
{
	return getInt(propertyIndex);
}

float Shader::get(GLint propertyIndex, float dummyValue)
{
	return getFloat(propertyIndex);
}

Vector3 Shader::get(GLint propertyIndex, Vector3 dummyValue)
{
	return getVector3(propertyIndex);
}

Vector2 Shader::get(GLint propertyIndex, Vector2 dummyValue)
{
	return getVector2(propertyIndex);
}

Color Shader::get(GLint propertyIndex, Color dummyValue)
{
	return getColor(propertyIndex);
}

