//
// Created by Jade White on 2016-03-21.
//

#pragma once

#include "../../BaseTypes/Vector3.h"

class Plane
{
public:
	Vector3 point;
	Vector3 normal;

	Plane();
	Plane(Vector3 point, Vector3 normal);
	Plane(Vector3 p0,Vector3 p1,Vector3 p2);

	bool IsOnOrAbove(Vector3 point);

};

