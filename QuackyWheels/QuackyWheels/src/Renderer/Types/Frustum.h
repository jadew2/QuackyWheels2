//
// Created by Jade White on 2016-03-20.
//

#pragma once


#include <vector>
using namespace std;
#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Quaternion.h"
#include "../../Components/Transform.h"
#include "Plane.h"

class Camera;


/** <summary> Class that represents a camera frustum </summary>
 *
	+-----------+
	| \       / |
	|   +---+   |
	|   |   |   |
	|   +---+   |
	| /       \ |
	+-----------+
 */
class Frustum
{
public:
	Frustum();
	Frustum(Camera* camera);
	Frustum(Vector3 origin, Quaternion rotation, float aspectRatio, float fovRadians, float nearDistance, float farDistance);

	vector<Plane> planes;

	//All these points are world space, just so you know.
	Vector3 nearCenter;
	Vector3 farCenter;

	float nearHeight;
	float nearWidth;
	float farHeight;
	float farWidth;

	//Points
	Vector3 nearLeftTop;
	Vector3 nearRightTop;
	Vector3 nearLeftBottom;
	Vector3 nearRightBottom;

	Vector3 farLeftTop;
	Vector3 farRightTop;
	Vector3 farLeftBottom;
	Vector3 farRightBottom;

	void Set(Camera* camera);
	void Set(Vector3 origin, Quaternion rotation, float aspectRatio, float fovRadians, float nearDistance, float farDistance);

private:
};

#include "../../Components/Camera.h"
