//
// Created by Jade White on 2016-04-07.
//

#pragma once

#include <glew.h>
#include <vector>

class Texture;
class FrameBuffer
{
	GLuint glFrameBuffer;

	std::vector<Texture*> colorAttachments = std::vector<Texture*>();
public:
	FrameBuffer(int width, int height);
	virtual ~FrameBuffer();

	void attachColorTexture(Texture* texture, int unit = -1);
	void detachColorTexture(Texture* texture);

	GLuint getGLFrameBuffer();
};

