//
// Created by Jade White on 2016-03-01.
//

#pragma once

#include <vector>

#include <glew.h>
#include <glm.hpp>
#include <vector>

class Mesh;
///<summary> A version of mesh for use with OpenGL drawing. </summary>
///<remarks> Take a while to generate. So make use of dirty flags. </remarks>

class GLMesh
{
public:
	GLMesh();
	virtual ~GLMesh();

	std::vector<glm::vec3> verticies;
	std::vector<GLuint> triangles;


	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> normals;

	std::vector<glm::vec2> uv0;
	std::vector<glm::vec2> uv1;

	std::vector<glm::vec4> colors;


	GLulong getVerticesByteSize();
	GLulong getTrianglesByteSize();
	GLulong getUVByteSize();
	GLulong getColorByteSize();

	virtual void Set(Mesh* mesh);
	virtual void Clear();

};


