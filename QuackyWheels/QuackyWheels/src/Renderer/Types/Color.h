//
// Created by Jade White on 2016-02-29.
//

#pragma once

#include <texture.h>
#include "../../BaseTypes/Vector3.h"

class Color
{
public:
	float r,g,b,a = 0.0f; //This ranges from 0-1f

	Color();
	Color(Vector3 vector3);
	Color(float r, float g, float b);
	Color(float r, float g, float b, float a);
	Color(aiTexel aiColor);
	virtual ~Color();

	static Color Red();
	static Color Yellow();
	static Color Green();
	static Color Blue();
	static Color White();
	static Color Black();
	static Color Grey();

	static Color BlackInvisible();

	Vector3 vector3();

	static Color Lerp(Color a, Color b, float t);

	///<summary> Vec4 conversion for glm </summary>
	glm::vec4 vec4();

	///<summary> Brings rgba back into the range between 0-->1f </summary>
	void normalize();
};


