//
// Created by Jade White on 2016-03-16.
//

#include "FloatProperty.h"

void FloatProperty::apply()
{
	shader->setFloat(index,value);
}