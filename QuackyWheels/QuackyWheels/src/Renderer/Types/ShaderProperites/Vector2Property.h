//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "ValuedShaderProperty.h"

class Vector2Property : public ValuedShaderProperty<Vector2>
{
public:
	Vector2Property(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = Vector2::Zero();
	}
	void apply() override;
};
