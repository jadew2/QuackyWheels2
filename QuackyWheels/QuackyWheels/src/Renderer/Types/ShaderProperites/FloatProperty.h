//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "ValuedShaderProperty.h"

class FloatProperty : public ValuedShaderProperty<float>
{
public:
	FloatProperty(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = 0;
	}
	void apply() override;
};
