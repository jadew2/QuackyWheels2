//
// Created by Jade White on 2016-03-17.
//

#pragma once

///<summary> Header for all classes related to shader properties </summary>

#include "ShaderProperty.h"
#include "ValuedShaderProperty.h"

#include "IntProperty.h"
#include "FloatProperty.h"
#include "Matrix4x4Property.h"
#include "TextureProperty.h"
#include "Vector3Property.h"
#include "Vector2Property.h"
#include "ColorProperty.h"