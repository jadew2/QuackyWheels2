//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include <gtc/type_ptr.hpp>
#include <string>
#include <glew.h>
#include "../Shader.h"

using namespace std;

///<summary> Class for holding a standard shader property. </summary>
class ShaderProperty
{
protected:
	Shader* shader;
	string name;
	GLint index;
public:
	ShaderProperty(Shader* shader, string name, GLint index);

	virtual void apply();

	string getName();
	GLint getIndex();
};
