//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include <gtc/type_ptr.hpp>
#include <string>
#include <glew.h>
using namespace std;

#include "../../../BaseTypes/Vector3.h"
#include "../../../BaseTypes/Vector2.h"
#include "../../../BaseTypes/Matrix4x4.h"
#include "../Color.h"
#include "../Texture.h"

#include "ShaderProperty.h"

///<summary> Holds a value for the property. </summary>
template <typename T>
class ValuedShaderProperty : public ShaderProperty
{
protected:
	T value;
private:
	//bool valueDirty = true;
public:
	ValuedShaderProperty(Shader* shader, string name, GLint index) : ShaderProperty(shader,name,index){};

	T get()
	{
		return value;
	}

	void set(T valueRef)
	{
		value = valueRef;
		apply();
	}

};










