//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "../../../BaseTypes/Matrix4x4.h"
#include "ValuedShaderProperty.h"

class Matrix4x4Property : public ValuedShaderProperty<Matrix4x4>
{
public:
	Matrix4x4Property(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = Matrix4x4::Identity();
	}
	void apply() override;
};
