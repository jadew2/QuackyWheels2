//
// Created by Jade White on 2016-03-16.
//

#include "ColorProperty.h"

void ColorProperty::apply()
{
	shader->setColor(index,value);
}
