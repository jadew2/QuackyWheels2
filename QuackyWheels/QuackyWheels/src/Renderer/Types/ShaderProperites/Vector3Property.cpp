//
// Created by Jade White on 2016-03-16.
//

#include "Vector3Property.h"

void Vector3Property::apply()
{
	shader->setVector3(index,value);
}