//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "../Texture.h"
#include "ValuedShaderProperty.h"

class TextureProperty : public ValuedShaderProperty<Texture*>
{
public:
	TextureProperty(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = 0;
	}
	void apply() override;
};
