//
// Created by Jade White on 2016-03-16.
//

#include "IntProperty.h"

void IntProperty::apply()
{
	shader->setInt(index,value);
}