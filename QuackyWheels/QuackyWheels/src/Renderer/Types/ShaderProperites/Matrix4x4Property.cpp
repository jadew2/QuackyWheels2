//
// Created by Jade White on 2016-03-16.
//

#include "Matrix4x4Property.h"

void Matrix4x4Property::apply()
{
	shader->setMatrix4x4(index,value);
}