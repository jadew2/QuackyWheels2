//
// Created by Jade White on 2016-03-16.
//

#include "ShaderProperty.h"

ShaderProperty::ShaderProperty(Shader* shaderRef, string name, GLint index)
{
	this->shader = shaderRef;
	this->name = name;
	this->index = index;
}

void ShaderProperty::apply()
{
	//To be overridden
}

string ShaderProperty::getName()
{
	return name;
}

GLint ShaderProperty::getIndex()
{
	return index;
}