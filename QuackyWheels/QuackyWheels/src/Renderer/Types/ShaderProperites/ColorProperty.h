//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "../Color.h"
#include "ValuedShaderProperty.h"

class ColorProperty : public ValuedShaderProperty<Color>
{
public:
	ColorProperty(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = Color::Black();
	}
	void apply() override;
};

