//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "ValuedShaderProperty.h"

class Vector3Property : public ValuedShaderProperty<Vector3>
{
public:
	Vector3Property(Shader* shader, string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader,propertyName, propertyIndex)
	{
		value = Vector3::Zero();
	}
	void apply() override;
};
