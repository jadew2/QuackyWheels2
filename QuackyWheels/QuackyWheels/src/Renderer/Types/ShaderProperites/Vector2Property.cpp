//
// Created by Jade White on 2016-03-16.
//

#include "Vector2Property.h"

void Vector2Property::apply()
{
	shader->setVector2(index,value);
}