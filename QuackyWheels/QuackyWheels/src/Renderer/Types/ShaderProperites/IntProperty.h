//
// Created by Jade White on 2016-03-16.
//

#pragma once

#include "ValuedShaderProperty.h"

class IntProperty : public ValuedShaderProperty<int>
{
public:
	IntProperty(Shader* shader,string propertyName, GLint propertyIndex) : ValuedShaderProperty(shader, propertyName, propertyIndex)
	{
		value = 0;
	}
	void apply() override;
};

