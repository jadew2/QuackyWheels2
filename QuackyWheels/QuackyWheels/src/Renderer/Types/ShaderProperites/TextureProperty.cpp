//
// Created by Jade White on 2016-03-16.
//

#include "TextureProperty.h"

void TextureProperty::apply()
{
	shader->setTexture(index,value);
}