//
// Created by Jade White on 2016-02-10.
//

#pragma once

#include <map>
#include <vector>
#include <string>
#include <glew.h>

#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Vector2.h"
#include "../../BaseTypes/Matrix4x4.h"
#include "Color.h"
#include "../../Exceptions/EngineException.h"

///<summary> Class Representing a material. Has related texture and shader information. </summary>
class Shader;
class Texture;
class ShaderProperty;
class Material
{
	Shader* shader;

	///<summary> The dictionary of Shader Properties. Used to set the uniforms of the shader before drawing. </summary>
	std::map<int/*propertyIndex*/, ShaderProperty*> propertyDictionary;
	std::vector<Texture*> textures; //Optimization. We use the textures frequently.
public:
	Material(Shader* shader);
	virtual ~Material();

	///<summary> The draw type of the Material (Transparent or not) </summary>
	enum DrawType
	{
		Default,
		Transparent,
		Mulplicative,
		Additive,
	};
	DrawType drawType;

	Shader* getShader();

//region Property Getting/Setting
	//Note: If you're setting/getting every frame, use getShader() and use Shader's indexed method. It's much faster.

	int getInt(std::string propertyName);
	void setInt(std::string propertyName, int value);

	float getFloat(std::string propertyName);
	void setFloat(std::string propertyName, float value);

	Vector3 getVector3(std::string propertyName);
	void setVector3(std::string propertyName, Vector3 value);

	Vector2 getVector2(std::string propertyName);
	void setVector2(std::string propertyName, Vector2 value);

	Color getColor(std::string propertyName);
	void setColor(std::string propertyName, Color value);

	Matrix4x4 getMatrix4x4(std::string propertyName);
	void setMatrix4x4(std::string propertyName, Matrix4x4 value);

	Texture* getTexture(std::string propertyName);
	void setTexture(std::string propertyName, Texture* texture);

	///<summary> Shorthand for setTexture("mainTex[mainTexIndex]", texture). Easier to read. </summary>
	void setTexture(Texture* texture, unsigned int mainTexIndex = 0);

	///<summary> Returns textures set in this shader. </summary>
	std::vector<Texture*> getTextures();

	///<summary>
	// Applies all the properties stored in this Material on the shader itself.
	// When you call the Set() method, the value is set on the shader instance and saved in the material. So you don't usually have to use this.
	// You want to call this method if you're sharing a shader instance between Materials, and need to have things set before they're rendered.
	// Just a note of caution: This is slow.
	// </summary>
	void applyAllPropertiesOnShader();

	///<summary> Returns whether the shader has the specified uniform property. </summary>
	bool hasProperty(std::string propertyName);



//region Generic Methods
	//Same name version of the above methods. Useful for C++ generics.
	void set(int propertyIndex, int value);
	void set(int propertyIndex, float value);
	void set(int propertyIndex, Vector3 value);
	void set(int propertyIndex, Vector2 value);
	void set(int propertyIndex, Color value);
	void set(int propertyIndex, Matrix4x4 value);
	void set(int propertyIndex, Texture* value);

	int get(int propertyIndex, int dummyValue);
	float get(int propertyIndex, float dummyValue);
	Vector3 get(int propertyIndex, Vector3 dummyValue);
	Vector2 get(int propertyIndex, Vector2 dummyValue);
	Color get(int propertyIndex, Color dummyValue);
	Matrix4x4 get(int propertyIndex, Matrix4x4 dummyValue);
	Texture* get(int propertyIndex, Texture* dummyValue);
//endregion

	int getPropertyIndex(std::string propertyName);


//endregion
private:
	ShaderProperty* getProperty(std::string propertyName);
	///<summary> Returns the shader property for the uniform index.</summary>
	ShaderProperty* getProperty(GLint propertyIndex);

	///<summary> Generates a list of uniform shader properties from the shader. </summary>
	std::vector<ShaderProperty*> generateShaderProperties(Shader* shader);

	void addTexture(Texture* texture);
	void removeTexture(Texture* texture);

	/*
template <class T>
T get(std::string propertyName)
{
	if (typeid(T) == typeid(int))
	{
		int value = getInt(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(float))
	{
		float value = getFloat(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(Vector3))
	{
		Vector3 value = getVector3(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(Vector2))
	{
		Vector2 value = getVector2(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(Matrix4x4))
	{
		Matrix4x4 value = getMatrix4x4(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(Color))
	{
		Color value = getColor(propertyName);
		return *(T*)&value;
	}
	if (typeid(T) == typeid(Texture*))
	{
		return (T*)getTexture(propertyName);
	}
	else
		throw new EngineException(std::string("Material: get: Unknown type to get :("));
}

template <class T>
void set(std::string propertyName, T value)
{
	T* valuePtr = &value;

	//Most typical sets at the top.
	if (typeid(T) == typeid(Color))
		setColor(propertyName,*(Color*)valuePtr);
	else if (typeid(T) == typeid(Vector3))
		setVector3(propertyName,*(Vector3*)valuePtr);
	else if (typeid(T) == typeid(int))
		setInt(propertyName,*(int*)valuePtr);
	else if (typeid(T) == typeid(float))
		setFloat(propertyName,*(float*)valuePtr);
	else if (typeid(T) == typeid(Vector2))
		setVector2(propertyName,*(Vector2*)valuePtr);
	else if (typeid(T) == typeid(Matrix4x4))
		setMatrix4x4(propertyName,*(Matrix4x4*)valuePtr);
	else if (typeid(T) == typeid(Texture*))
		setTexture(propertyName,(Texture*)valuePtr);
	else
		throw new EngineException(std::string("Material: get: Unknown type to set :("));
}
 */
};