//
// Created by Jade White on 2016-02-10.
//

#pragma once


#include <vector>
#include <glew.h>
#include "Color.h"

///<summary> Class that represents a 2D texture. In RGBA format. </summary>
class Texture
{
protected:
	GLuint glTexture = 0;
	GLint glTextureUnit = 0;
	GLuint pixelHeight = 0;
	GLuint pixelWidth = 0;
	std::vector<GLubyte> glData;  //In RGBA format.
public:

	Texture(GLuint height, GLuint width);
public:
	Texture(GLuint height, GLuint width, std::vector<GLubyte> rgbaDataRef);
	virtual ~Texture();

	enum Wrapping
	{
		Repeat,
		RepeatMirrored,
		Clamp,
		ClampBorder
	};
	enum Interpolation
	{
		Point,
		Linear,
		MipMapping,
	};

private:
	Wrapping wrapping;
	Interpolation interpolation;

public:
	Wrapping getWrapping();
	void setWrapping(Wrapping wrapping);
	Interpolation getInterpolation();
	void setInterpolation(Interpolation interpolation);

	GLuint getHeight();
	GLuint getWidth();

	virtual Color getPixel(unsigned int x, unsigned int y);
	virtual void setPixel(unsigned int x, unsigned int y, Color color);
private:
	///<summary> Updates the GLTexture </summary>
	void updateGLTexture();


public:
	///<summary> Sets the texture unit returned by getTextureUnit(). It's default and clamped at 0. </summary>
	void setTextureUnit(int unitIndex);
	GLint getTextureUnit();
	GLuint getGLTexture();
	virtual std::vector<GLubyte> getData();


};


