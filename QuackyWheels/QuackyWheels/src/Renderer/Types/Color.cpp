//
// Created by Jade White on 2016-02-29.
//

#include "Color.h"

Color::Color()
{

}

Color::Color(float r, float g, float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = 1;
	normalize();
}

Color::Color(float r, float g, float b, float a)
{
	this->r = r;
	this->g = g;
	this->b = b;
	this->a = a;
	normalize();
}

Color::Color(Vector3 vector3) : Color(vector3.x, vector3.y, vector3.z) {}

Color::Color(aiTexel aiColor)
{
	r = aiColor.r;
	g = aiColor.g;
	b = aiColor.b;
	a = aiColor.a;
	normalize();
}

Color::~Color()
{

}


Color Color::Red()
{
	return Color(1.0f,0.0f,0.0f);
}

Color Color::Yellow()
{
	return Color(1.0f,1.0f,0.0f);
}

Color Color::Green()
{
	return Color(0.0f,1.0f,0.0f);
}

Color Color::Blue()
{
	return Color(0.0f,0.0f,1.0f);
}

Color Color::White()
{
	return Color(1.0f,1.0f,1.0f);
}

Color Color::Black()
{
	return Color(0.0f, 0.0f, 0.0f);
}

Color Color::Grey()
{
	return Color(0.5f, 0.5f, 0.5f);
}

Color Color::BlackInvisible()
{
	return Color(0,0,0,0);
}


Vector3 Color::vector3()
{
	return Vector3(r,g,b);
}

void Color::normalize()
{
	float highestValue = r;
	if (g > highestValue)
		highestValue = g;
	if (b > highestValue)
		highestValue = b;
	if (a > highestValue)
		highestValue = a;

	if (highestValue <= 1.0f)
		return;
	float multiplier = 1.0f / highestValue;
	r *= multiplier;
	g *= multiplier;
	b *= multiplier;
	a *= multiplier;
}


Color Color::Lerp(Color from, Color to, float t)
{
	//First, clamp t from 0 --> 1
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	//lerp is (to-from)*t + from
	float newR = (to.r - from.r) * t + from.r;
	float newG = (to.g - from.g) * t + from.g;
	float newB = (to.b - from.b) * t + from.b;
	float newA = (to.a - from.a) * t + from.a;

	return Color(newR,newG,newB,newA);
}


glm::vec4 Color::vec4()
{
	return glm::vec4(r,g,b,a);
}


