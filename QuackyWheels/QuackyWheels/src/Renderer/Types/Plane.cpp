//
// Created by Jade White on 2016-03-21.
//

#include "Plane.h"


Plane::Plane()
{
	this->point = Vector3::Zero();
	this->normal = Vector3::Up();
}


Plane::Plane(Vector3 point, Vector3 normal)
{
	this->point = point;
	this->normal = normal.Normalized();
}

Plane::Plane(Vector3 p0, Vector3 p1, Vector3 p2)
{
	Vector3 ab = (p0 - p1).Normalized();
	Vector3 ac = (p0 - p2).Normalized();

	this->point = (p0 + p1 + p2) * (1.0f/3.0f); //Get the centre point
	this->normal = Vector3::Cross(ab,ac).Normalized();
}


bool Plane::IsOnOrAbove(Vector3 point)
{
	Vector3 toPoint = point - this->point;
	Vector3 direction = toPoint.Normalized();

	float dot = Vector3::Dot(normal,direction);
	return (dot >= 0);
}


