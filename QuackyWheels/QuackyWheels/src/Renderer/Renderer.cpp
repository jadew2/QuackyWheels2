#include "Renderer.h"

#include <glew.h> 
#include <SDL_opengl.h> 
#include <stdio.h> 
#include <string>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <map>
#include <vector>

#include <gtx/transform.hpp>
#include <gtc/type_ptr.hpp>
#include <gtc/matrix_transform.hpp>
#include "../Shaders/ShaderTools.h"
#include "../Renderer/Types/Material.h"
#include "../Exceptions/EngineException.h"
#include "RenderCollection.h"
#include "MeshRendererSorter.h"
#include "../Components/ImageEffectRenderer.h"

using std::cout;
using std::endl;
using std::cerr;

Renderer::Renderer() 
{
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
		throw new EngineException(std::string("SDL: Could not initialize. Error: ") + SDL_GetError());

	//Use OpenGL 4.1 core
	SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	//Create window
	gWindow = SDL_CreateWindow("Quacky Wheels", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, (int)SCREEN_WIDTH, (int)SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
	if (gWindow == nullptr)
		throw new EngineException(std::string("SDL: Window could not be created. Error: ") + SDL_GetError());

	gScreenSurface = SDL_GetWindowSurface(gWindow);

	//Create context
	gContext = SDL_GL_CreateContext(gWindow);
	if (gContext == NULL)
		throw new EngineException(std::string("SDL: OpenGL context could not be created. Error: ") + SDL_GetError());

	//Initialize GLEW
	glewExperimental = GL_TRUE;
	GLenum glewError = glewInit();
	if (glewError != GLEW_OK)
		throw new EngineException(std::string("GLEW: Could not initialize. Error: ") + std::string((char*)glewGetErrorString(glewError)));

	//Initialize OpenGL
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glLineWidth(5.0f);


	//---Set up various effects

	//Set texture filtering to linear
	if (!SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "1"))
		throw new EngineException(std::string("Renderer: Linear texture filtering not enabled!"));

	//Set up vsync
	if (SDL_GL_SetSwapInterval(1) < 0)
		std::cout << std::string("SDL: Warning: Unable to set VSync. ") + SDL_GetError();

	//light = glm::vec3(50.0f, 100.0f, 100.0f);

	renderCollection = new RenderCollection();
	rendererSorter = new MeshRendererSorter();
}

Renderer::~Renderer()
{
	delete renderCollection;

	//Destroy window	
	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	//Quit SDL subsystems
	SDL_Quit();
}

///<summary> Renders the entire scene. </summary>
void Renderer::render()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(0.5f, 0.5f, 1.0f, 0.0f);

	std::vector<Camera*> cameras = renderCollection->getCameras();
	std::vector<MeshRenderer*> meshRenderers = renderCollection->getSolidMeshRenderers();
	std::vector<MeshRenderer*> uiRenderers = renderCollection->getUIRenderers();
	std::vector<MeshRenderer*> tempRenderers = renderCollection->getTempRenderers();
	std::vector<ImageEffectRenderer*> imageEffects = renderCollection->getImageEffectRenderers();


	for (auto camera : cameras)
	{
		///<summary> Set camera properties for rendering. </summary>
		camera->bindCamera();

		//Draw all 3D MeshRenderers
		for (size_t i = 0; i < meshRenderers.size(); ++i)
			meshRenderers[i]->render(camera);

		//Clean up
		camera->unbindCamera();

		//Post Process Image Effects
		for (size_t i = 0; i < imageEffects.size(); ++i)
			imageEffects[i]->render(camera);

		//Sort and draw UI renderers
		vector<MeshRenderer*> sortedUIRenderers = rendererSorter->sortUI(camera, uiRenderers);
		for (size_t i = 0; i < sortedUIRenderers.size(); ++i)
			sortedUIRenderers[i]->render(camera);

		//Draw Temporary Renderers (Widgets and Debug lines)
		for (size_t i = 0; i < tempRenderers.size(); ++i)
			tempRenderers[i]->render(camera);
	}

	renderCollection->clearTempRenderers(); //Delete all temporary renderers.

	//Display the rendered image
	SDL_GL_SwapWindow(gWindow);
}
