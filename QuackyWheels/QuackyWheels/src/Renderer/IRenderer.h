//
// Created by Jade White on 2016-04-07.
//

#pragma once

class Camera;
class IRenderer
{
public:
	virtual void render(Camera* camera, bool doCulling = true){}
};