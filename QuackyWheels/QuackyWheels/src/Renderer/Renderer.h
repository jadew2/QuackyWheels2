#pragma once
#include <SDL.h> 
#include <glm.hpp>
#include <mat4x4.hpp>
#include <vec3.hpp>
#include "../BaseTypes/Matrix4x4.h"

class RenderCollection;
class MeshRendererSorter;
class Camera;
class Renderer
{
public:
	//Screen dimension constants
	float SCREEN_WIDTH = 1920.0f;
	float SCREEN_HEIGHT = 1080.0f;
	float aspectRatio = SCREEN_WIDTH / SCREEN_HEIGHT;

	Renderer();
	virtual ~Renderer();

public:
	MeshRendererSorter* rendererSorter;
	RenderCollection* renderCollection;

	///<summary> Renders the entire scene. </summary>
	void render();

	//The window we'll be rendering to
	SDL_Window* gWindow = nullptr;

	//The surface contained by the window
	SDL_Surface* gScreenSurface = NULL;

	//OpenGL context
	SDL_GLContext gContext;
};
