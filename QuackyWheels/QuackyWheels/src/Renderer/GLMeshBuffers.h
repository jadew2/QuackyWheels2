//
// Created by Jade White on 2016-03-11.
//

#pragma once

#include "Types/GLMesh.h"

///<summary> A version of GLMesh that generates buffers alongside the GLMeshData. </summary>
class GLMeshBuffers : public GLMesh
{
	GLuint vertexArrayObject;

	GLuint vertexBuffer;
	GLuint triangleBuffer;

	GLuint tangentBuffer;
	GLuint normalBuffer;

	GLuint uv0Buffer;
	GLuint uv1Buffer;

	GLuint colorBuffer;

public:
	///<summary> The number of vertex attributes we support. </summary>
	const GLuint attributeCount = 6;

	GLMeshBuffers();
	virtual ~GLMeshBuffers();

	///<summary> Updates the open GL buffers. </summary>
	virtual void Set(Mesh* mesh) override;

	///<summary> Get the vertex array object. </summary>
	GLuint getVAO();
};
