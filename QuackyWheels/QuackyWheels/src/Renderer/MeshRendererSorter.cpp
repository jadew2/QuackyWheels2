//
// Created by Jade White on 2016-03-22.
//

#include "MeshRendererSorter.h"

#include  "../Components/Transform.h"
#include "../Renderer/Types/Material.h"



vector<MeshRenderer*> MeshRendererSorter::sort(Camera* camera, vector<MeshRenderer*> renderers)
{
	return sortInternal(camera, renderers, false);
}

vector<MeshRenderer*> MeshRendererSorter::sortUI(Camera* camera, vector<MeshRenderer*> renderers)
{
	return sortInternal(camera, renderers, true);
}


///<summary> Camera position used for sorting. Sorts it so the farthest object origins are first. </summary>
Camera* cam = nullptr;
Vector3 sortingCameraPos = Vector3::Zero();
bool uiSortMode = false;
bool compare(MeshRenderer* a, MeshRenderer* b)
{
	if (uiSortMode)
	{
		//We make sure closer objects are rendered last.
		float aZ = a->GetTransform()->GetPosition().z;
		float bZ = b->GetTransform()->GetPosition().z;

		return aZ > bZ; //This should make it so closer objects are last in the list
	}
	else
	{
		Transform* aTrans = a->GetTransform();
		Transform* bTrans = b->GetTransform();

		Vector3 aPos = aTrans->GetLocalPosition();
		Vector3 bPos = bTrans->GetLocalPosition();

		Matrix4x4 viewMat = cam->getViewMatrix();// es->renderer->viewMatrix;
		Matrix4x4 aModelViewMat = viewMat;
		//Matrix4x4 bModelViewMat = viewMat * bTrans->GetTRSMatrix();

		Vector3 aEyePos = aModelViewMat.MultiplyPoint(aPos);
		Vector3 bEyePos = aModelViewMat.MultiplyPoint(bPos);

		return aEyePos.SqrMagnitude() > bEyePos.SqrMagnitude();

		//Return true if you want A before


		/*
		Vector3 cameraPos = sortingCameraPos;

		//If they have the same transparency, sort them farthest to nearest.
		Vector3 toA = a->GetTransform()->GetPosition() - cameraPos;
		Vector3 toB = b->GetTransform()->GetPosition() - cameraPos;
		bool bCloser = toA.SqrMagnitude() > toB.SqrMagnitude();

		return bCloser; //A > B. This should sort it so closer objects are LAST in the list.
		*/
	}
}

vector<MeshRenderer*> MeshRendererSorter::sortInternal(Camera* camera, vector<MeshRenderer*> renderers, bool uiMode)
{
	//First, filter out any renderers that are culled this frame.

	/* Culling is done in render() now...
	vector<MeshRenderer*> rendered = vector<MeshRenderer*>();
	rendered.reserve(renderers.size());
	for (unsigned int i = 0; i < renderers.size(); ++i)
	{
		if (!renderers[i]->isCulled(camera))
			rendered.push_back(renderers[i]);
	}
	 */


	//Now sort the list
	cam = camera;
	uiSortMode = uiMode;
	std::sort(renderers.begin(),renderers.end(),compare);
	return renderers;
}







