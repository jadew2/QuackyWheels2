//
// Created by Jade White on 2016-03-22.
//

#pragma once

#include <vector>
using namespace std;

#include "../Components/MeshRenderer.h"
#include "../Components/Camera.h"

class MeshRendererSorter
{
public:
	///<summary> Prunes non visible renderers, and orders them back to closest to the camera. </summary>
	vector<MeshRenderer*> sort(Camera* camera, vector<MeshRenderer*> renderers);
	///<summary> Prunes non visible renderers, and orders renderers from lowest Z value to highest. </summary>
	vector<MeshRenderer*> sortUI(Camera* camera, vector<MeshRenderer*> renderers);

private:
	///<summary> Prunes non visible renderers, and sorts them back to front to the camera. </summary>
	vector<MeshRenderer*> sortInternal(Camera* camera, vector<MeshRenderer*> renderers, bool uiMode);

	//Camera* sortingCamera = nullptr;
	///<summary> Returns the closer meshRenderer to the camera. </summary>
	//bool compare(MeshRenderer* a, MeshRenderer* b);
};


