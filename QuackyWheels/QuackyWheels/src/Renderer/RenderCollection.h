#pragma once


#include <vector>
#include "../Components/MeshRenderer.h"

class MeshRenderer;
class GameObject;
class Camera;
class LightSource;
class ImageEffectRenderer;
class RenderCollection
{
protected:
	std::vector<MeshRenderer*> uiRenderers = std::vector<MeshRenderer*>();

	std::vector<Camera*> cameras = std::vector<Camera*>();
	std::vector<MeshRenderer*> meshRenderers = std::vector<MeshRenderer*>();

	std::vector<GameObject*> tempRendererGameObjects = std::vector<GameObject*>();
	std::vector<MeshRenderer*> tempRenderers = std::vector<MeshRenderer*>();

	std::vector<ImageEffectRenderer*> imageEffects = std::vector<ImageEffectRenderer*>();

	std::vector<LightSource*> lights = std::vector<LightSource*>();
public:
	RenderCollection();

	void addCamera(Camera* camera);
	void removeCamera(Camera* camera);

	void addMeshRender(MeshRenderer* meshRenderer);
	void removeMeshRender(MeshRenderer* meshRenderer);

	void addUIRender(MeshRenderer* uiRenderer);
	void removeUIRender(MeshRenderer* uiRenderer);

	void addTempRenderer(GameObject* rendererGameObject, MeshRenderer* renderer);
	void clearTempRenderers();

	void addImageEffect(ImageEffectRenderer* imageEffect);
	void removeImageEffect(ImageEffectRenderer* imageEffect);

	void AddLight(LightSource* light);
	void RemoveLight(LightSource* light);

	std::vector<Camera*> getCameras();
	std::vector<MeshRenderer*> getSolidMeshRenderers();
	std::vector<MeshRenderer*> getUIRenderers();
	std::vector<MeshRenderer*> getTempRenderers();
	std::vector<ImageEffectRenderer*> getImageEffectRenderers();


	std::vector<LightSource*> getLights();
};


