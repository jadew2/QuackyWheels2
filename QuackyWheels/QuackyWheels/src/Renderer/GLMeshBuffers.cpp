//
// Created by Jade White on 2016-03-11.
//

#include "GLMeshBuffers.h"

GLMeshBuffers::GLMeshBuffers()
{
	//First, create buffers for all the vertex data we're going to pass to shaders.
	//First, get UID's for each buffer.
	glGenVertexArrays(1, &vertexArrayObject);

	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &triangleBuffer);

	glGenBuffers(1, &normalBuffer);
	glGenBuffers(1, &tangentBuffer);

	glGenBuffers(1, &uv0Buffer);
	glGenBuffers(1, &uv1Buffer);

	glGenBuffers(1, &colorBuffer);

	//Now set the type parameters for each of these buffers (What are they going to hold?)
	glBindVertexArray(vertexArrayObject); //The following will set buffer parameters for vertexArrayObject.
	{
		//Triangles
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleBuffer); //Note this is declared as a Element array buffer.

		GLuint layoutLocation;

		//Vertices
		layoutLocation = 0; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
		glVertexAttribPointer(layoutLocation, 3 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);

		//Normals
		layoutLocation = 1; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
		glVertexAttribPointer(layoutLocation, 3 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);

		//Tangents
		layoutLocation = 2; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, tangentBuffer);
		glVertexAttribPointer(layoutLocation, 3 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);

		//uv0
		layoutLocation = 3; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, uv0Buffer);
		glVertexAttribPointer(layoutLocation, 2 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);

		//uv1
		layoutLocation = 4; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, uv1Buffer);
		glVertexAttribPointer(layoutLocation, 2 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);

		//color
		layoutLocation = 5; //Layout location in shader file.
		glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
		glVertexAttribPointer(layoutLocation, 4 /*# of components (XYZ)*/, GL_FLOAT /*Type*/, GL_FALSE /*AutoNormalized*/, 0 /*Stride*/, (void*) 0 /*Offset*/);
	}
	glBindVertexArray(0); //Stop binding for vertexArrayObject
}

GLMeshBuffers::~GLMeshBuffers()
{
	//Return the UID's to OpenGL
	glDeleteVertexArrays(1, &vertexArrayObject);
	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &triangleBuffer);

	glDeleteBuffers(1, &normalBuffer);
	glDeleteBuffers(1, &tangentBuffer);

	glDeleteBuffers(1, &uv0Buffer);
	glDeleteBuffers(1, &uv1Buffer);

	glDeleteBuffers(1, &colorBuffer);
}

void GLMeshBuffers::Set(Mesh* mesh)
{
	GLMesh::Set(mesh);

	//Vertices
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, getVerticesByteSize(), verticies.data(), GL_STATIC_DRAW);

	//Triangles
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triangleBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, getTrianglesByteSize(), triangles.data(), GL_STATIC_DRAW );

	//Normals
	glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
	glBufferData(GL_ARRAY_BUFFER, getVerticesByteSize(), normals.data(), GL_STATIC_DRAW);

	//Tangents
	glBindBuffer(GL_ARRAY_BUFFER, tangentBuffer);
	glBufferData(GL_ARRAY_BUFFER, getVerticesByteSize(), tangents.data(), GL_STATIC_DRAW);

	//uv0
	glBindBuffer(GL_ARRAY_BUFFER, uv0Buffer);
	glBufferData(GL_ARRAY_BUFFER, getUVByteSize(), uv0.data(), GL_STATIC_DRAW);

	//uv1
	glBindBuffer(GL_ARRAY_BUFFER, uv1Buffer);
	glBufferData(GL_ARRAY_BUFFER, getUVByteSize(), uv1.data(), GL_STATIC_DRAW);

	//colors
	glBindBuffer(GL_ARRAY_BUFFER, colorBuffer);
	glBufferData(GL_ARRAY_BUFFER, getColorByteSize(), colors.data(), GL_STATIC_DRAW);
}

GLuint GLMeshBuffers::getVAO()
{
	return vertexArrayObject;
}
