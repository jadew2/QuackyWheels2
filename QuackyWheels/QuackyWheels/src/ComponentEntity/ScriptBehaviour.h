//
// Created by Jade White on 2016-02-06.
//

#pragma once

#include "Behaviour.h"

class ScriptBehaviour : public Behaviour
{
	friend class ScriptBehaviourEvaluator; //The Evaluator class that invokes the protected methods.

public:
	ScriptBehaviour(GameObject* gameObject);
	virtual ~ScriptBehaviour();

protected:

	///<summary> Init is called if this is the Script’s first frame existing. Use this to get references to other components. Or register with lower components. </summary>
	virtual void Init();

	///<summary> Start is called if this is the first frame the script’s “bool enabled” was set to true. Not run after that </summary>
	virtual void Start();


	///<summary> Run every frame before Update. </summary>
	virtual void EarlyUpdate();
	///<summary> Run every frame. </summary>
	virtual void Update();
	///<summary> Run every frame after Update. </summary>
	virtual void AfterUpdate();


	///<summary> Like Update, but run at a fixed frame rate. Run right before physics is simulated. </summary>
	virtual void PhysicsUpdate();

	///<summary> Run after AfterUpdate, right before rendering. </summary>
	virtual void RenderUpdate();

	///<summary> Called before the destructor is called. Use it to clean up things in the scene, as they'll still exist. </summary>
	virtual void Destroy();

public:

	/// <summary> Deletes this component, ensuring script execution order is maintained. Use this over delete whenever possible. </summary>
	/// <remarks>
	/// Behind the scenes, this deletes this component after the Update Loop.
	/// This ensures all Destroy() calls execute before the script is deleted. Allowing you to get clean references to things.
	/// </remarks>
	void SafeDelete();
};

