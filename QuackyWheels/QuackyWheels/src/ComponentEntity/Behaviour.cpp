#include "Behaviour.h"

Behaviour::Behaviour(GameObject* gameObject) : Component(gameObject)
{
	eventCalls = true;
	wantedEnabled = false;
	enabled = false;
}

Behaviour::~Behaviour()
{
}

bool Behaviour::GetEnabled()
{
	return enabled;
}

void Behaviour::SetEnabled(bool value)
{
	if (eventCalls)
	{
		if (value != enabled)
		{
			if (value)
				OnEnable();
			else
				OnDisable();
		}
		enabled = value;
	}

	wantedEnabled = value;
}

void Behaviour::OnEnable()
{
	//Do Nothing (To be overridden)
}

void Behaviour::OnDisable()
{
	//Do Nothing (To be overridden)
}

void Behaviour::EnableBehaviourEventCalls(bool value)
{
	eventCalls = value;
}

bool Behaviour::GetWantedEnabled()
{
	return wantedEnabled;
}
