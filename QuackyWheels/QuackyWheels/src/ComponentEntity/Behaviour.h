#pragma once
#include "Component.h"

#define Behaviour_Defined
class Behaviour : public Component
{
private:
	///<summary> Flag on whether SetEnabled calls should invoke the OnEnable() on OnDisable() methods yet. </summary>
	bool eventCalls; //Default true. ScriptBehaviours set this to false on construction.
	bool wantedEnabled; //Default false, except for ScriptBehaviours.
	bool enabled; //Default false

public:
	Behaviour(GameObject* gameObject);
	virtual ~Behaviour();

	///<summary> Gets the Enabled value </summary>
	bool GetEnabled();
	///<summary> Changes the Enabled value. </summary>
	void SetEnabled(bool value);

	///<summary>
	/// If behaviour event calls are disabled, "enabled" is not changed on calls to SetEnabled(). However, "wantedEnabled" is.
	/// This is a useful value after re-enabling behaviour event calls.
	///</summary>
	///<remarks> Used behind the scenes to make sure OnEnable() comes after Awake. Be careful modifying this </remarks>
	bool GetWantedEnabled();
	///<summary> If set to false, calls to SetEnabled() do nothing but set "wantedEnabled". </summary>
	///<remarks> Used behind the scenes to make sure OnEnable() comes after Awake. Be careful modifying this </remarks>
	void EnableBehaviourEventCalls(bool value);
protected:
	///<summary> This event is called when Behaviour.enabled is set to true </summary>
	virtual void OnEnable();
	///<summary> This event is called when Behaviour.enabled is set to false </summary>
	virtual void OnDisable();
};