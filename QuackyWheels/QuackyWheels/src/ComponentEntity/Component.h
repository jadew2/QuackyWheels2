#pragma once

class GameObject;
class Transform;
class EngineSystems;
class Component
{
private:
	GameObject* gameObject = nullptr;

public:
	//Component(){}; NOTE: You should never be able to create a Component without a gameObject to attach it to!
	///<summary> The constructor for a component. You should never be able to create a Component without a gameObject to attach it to. </summary>
	Component(GameObject* gameObject);
	virtual ~Component(); //Keep the virtual

	///<summary> Returns the related GameObject </summary>
	GameObject* GetGameObject();
	///<summary> Returns the GameObject's Transform or PhysicsTransform. </summary>
	Transform* GetTransform();
	///<summary> Returns a reference to the The lower level engine systems. </summary>
	EngineSystems* getEngineSystems();
};