//
// Created by Jade White on 2016-02-06.
//

#include "ScriptBehaviour.h"
#include "../GameLoop/EngineSystems.h"
#include "../Script/Scripting.h"
#include "../Script/ScriptEvaluator.h"
#include "../Script/ScriptCollection.h"

ScriptBehaviour::ScriptBehaviour(GameObject *gameObject) : Behaviour(gameObject)
{
	Behaviour::EnableBehaviourEventCalls(false); //Wait until Init() is called before calls to OnEnable(), OnDisable() can happen.
	Behaviour::SetEnabled(true); //The default enabled value for ScriptBehaviours is true;

	//Unlike other components. ScriptBehaviours add themselves to the script collection on creation.
	//Use Init() and Destroy() for things that extend ScriptBehaviours.
	getEngineSystems()->scripting->evaluator->scriptCollection->AddScript(this);
}

ScriptBehaviour::~ScriptBehaviour()
{
	//Unlike other components. ScriptBehaviours remove themselves from the script collection on destruction.
	//Use Init() and Destroy() for things that extend ScriptBehaviours.
	getEngineSystems()->scripting->evaluator->scriptCollection->RemoveScript(this);
}

void ScriptBehaviour::Init()
{
	//Behaviour::EnableBehaviourEventCalls(true); Done in the ScriptBehaviourEvaluator.
	//Do nothing (To be overridden)
}

void ScriptBehaviour::Start()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::EarlyUpdate()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::Update()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::AfterUpdate()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::PhysicsUpdate()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::RenderUpdate()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::Destroy()
{
	//Do nothing (To be overridden)
}

void ScriptBehaviour::SafeDelete()
{
	getEngineSystems()->scripting->evaluator->scriptCollection->SetForDestroy(this);
}
