//
// Created by Jade White on 2016-01-29.
//

#include "GameObject.h"

#include <string>

#include "../World/Universe.h"
#include "../BaseTypes/Quaternion.h"
#include "../BaseTypes/Vector3.h"
#include "../Components/Transform.h"
#include "../GameLoop/EngineSystems.h"
#include "../Script/Scripting.h"
#include "../Script/Deleter.h"
#include "ScriptBehaviour.h"

///<summary> Creates a default GameObject. All other constructors should extend this. </summary>
GameObject::GameObject(World* worldRef) : GameObject(worldRef, std::string("New GameObject"))
{
	
}

//TODO By C++ standards this is ugly and should not work
//Cannot pass the address of a temporary
//Visual C++ allows it as an extension but should not be relied upon
//possibly fix by using new?
//Jade: What? We aren't passing the address of a temporary, we're copying a object. Passing pointers imo is more ugly.
GameObject::GameObject(World* worldRef, std::string Name) : GameObject(worldRef, Name, new Vector3(), new Quaternion())
{
	
}

GameObject::GameObject(World* worldRef, std::string Name, Vector3* position, Quaternion* rotation) {
	world = worldRef;

	this->Name = Name;
	Tags = std::vector<std::string>();
	components = std::vector<Component*>();

	this->transform = new Transform(this, position, rotation);

	//Add ourselves to the current scene
	world->addGameObject(this);
}

GameObject::GameObject(World* worldRef, std::string Name, physx::PxTransform* transform) {
	world = worldRef;

	this->Name = Name;
	Tags = std::vector<std::string>();
	components = std::vector<Component*>();

	this->transform = new Transform(this, transform);

	//Add ourselves to the current scene
	world->addGameObject(this);
}

GameObject::~GameObject()
{
	//Clear our parent, so we are no longer owned.
	transform->SetParent(nullptr);

	//Now delete our children
	std::vector<GameObject*> directChildren = GetChildren(false);
	for (size_t i = 0; i < directChildren.size(); i++)
	{
		GameObject* child = directChildren[i];
		delete child; //If this fails, it's likely one of your destructors wasn't marked virtual. Mark all destructors virtual.
	}

	//Now delete ourselves
	while (components.size() > 0)
		delete components[0];

	//Remove ourselves from the current scene
	world->removeGameObject(this);
	//If we were queued for safeDelete, remove ourselves by setting our entry to null.
	getEngineSystems()->scripting->gameObjectDeleter->NullUnQueue(this);
}

//Just return the private ref. We just don't want to let others set the transform component.
Transform* GameObject::GetTransform()
{
	return transform;
}

//The parent is fetched from the Transform Component.
GameObject* GameObject::GetParent()
{
	return transform->GetParent()->GetGameObject();
}

//The children are fetched from the Transform Component.
std::vector<GameObject*> GameObject::GetChildren(bool recursive)
{
	std::vector<Transform*> transformChildren = transform->GetChildren(recursive);
	std::vector<GameObject*> gameObjectChildren;

	for(auto &childTransform : transformChildren)
		gameObjectChildren.push_back(childTransform->GetGameObject());

	return gameObjectChildren;
}

World* GameObject::getWorld()
{
	return world;
}

EngineSystems* GameObject::getEngineSystems()
{
	return world->getUniverse()->getEngineSystems();
}


void GameObject::SafeDelete()
{
	//First, tell our children to die.
	std::vector<GameObject*> directChildren = GetChildren(false);
	for (auto child : directChildren)
		child->SafeDelete();

	//Now kill ourselves.
	std::vector<ScriptBehaviour*> scriptBehaviours = GetComponents<ScriptBehaviour>();
	for (auto scriptBehaviour : scriptBehaviours)
		scriptBehaviour->SafeDelete();

	//Queue this gameObject up to be deleted.
	getEngineSystems()->scripting->gameObjectDeleter->Queue(this);
}


