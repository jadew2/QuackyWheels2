//
// Created by Jade White on 2016-01-29.
//

#pragma once

#include <vector>
#include <algorithm>
#include <string>
#include "PxPhysicsAPI.h"

class Vector3;
class Quaternion;
class ScriptBehaviour;
class Component;
class Transform;
class World;
class EngineSystems;

/**
 * <summary>
 * A object used to represent items in the world.
 * Every GameObject has a:
 * 		1. Name, for visual sake
 * 		2. Collection of Tags, for grouping.
 * 		3. "Transform" component (or not), which holds the GameObject's position, rotation, and scale in the world.
 *
 * Each GameObject can have a infinite number of attached "Components" which allow it to have properties.
 * E.g. A AudioSource component makes the GameObject play music, a MeshRenderer component makes it have a model, etc.
 *
 * GameObject has a number of methods to help dynamically Add, Remove and Get these Components.
 * </summary>
 */
class GameObject
{
public:

	GameObject(World* world);
	GameObject(World* world, std::string Name);
	GameObject(World* world, std::string Name, Vector3* position, Quaternion* rotation);
	GameObject(World* world, std::string Name, physx::PxTransform* transform);

	virtual ~GameObject();

	///<summary> The name of this GameObject </summary>
	std::string Name;

	///<summary> A customizable list of labels on this GameObject </summary>
	std::vector<std::string> Tags;

private:
	World* world;
public:
	World* getWorld();
	EngineSystems* getEngineSystems();

//region The Component System
private:
	///<summary> The components attached to this GameObject </summary>
	std::vector<Component*> components;

	///<summary> The transform component of this GameObject </summary>
	Transform* transform; //Change to getActiveTransform();
	///<summary> Creates a component of type T and adds it to this Gameobject </summary>
	///<remarks> The type must extend Component. </remarks>

	friend class Component;
	void AddComponent(Component* t)
	{ //Because of C++ Templates, implementation must be in the header file.
		components.push_back(t);
	}

	///<summary> Removes a component from the gameObject </summary>
	///<remarks> The type must extend Component. </remarks>
	void RemoveComponent(Component* t)
	{ //Because of C++ Templates, implementation must be in the header file.
		components.erase(std::remove(components.begin(),components.end(),t),components.end());
	}

public:

	///<summary> Finds a component of the specified type. </summary>
	///<remarks> The type must extend Component </remarks>
	///<returns> Returns the found Component. Otherwise returns null </returns>
	template <typename T>
	T* GetComponent()
	{	//Because of C++ Templates, implementation must be in the header file.
		for (Component* componentPointer : components) //"foreach (var component in Components)..."
		{
			T* wantedComponentPointer = dynamic_cast<T*>(componentPointer);

			if (wantedComponentPointer != nullptr)
				return wantedComponentPointer;
		}
		return nullptr;
	}

	///<summary> Finds components of the specified type. </summary>
	///<remarks> The type must extend Component. </remarks>
	///<returns> Returns the found Components. Otherwise returns a empty vector. </returns>
	template <typename T>
	std::vector<T*> GetComponents()
	{	//Because of C++ Templates, implementation must be in the header file.
		std::vector<T*> foundComponents = std::vector<T*>();
		for (Component* componentPointer : components) //"foreach (var component in Components)..."
		{
			T* wantedComponentPointer = dynamic_cast<T*>(componentPointer);

			if (wantedComponentPointer != nullptr)
				foundComponents.push_back(wantedComponentPointer);
		}
		return foundComponents;
	}
//endregion

//region Transform Methods
public:
	///<summary> The transform of the GO> </summary>
	Transform* GetTransform();
	///<summary> Returns parent of this GO. </summary>
	GameObject* GetParent();
	///<summary> Returns the children of this GO. Is the same as transform. Note that this list is a copy. </summary>
	std::vector<GameObject*> GetChildren(bool recursive = false);
//endregion

public:
	/// <summary> Deletes this GameObject, ensuring script execution order is maintained. Use this over delete whenever possible. </summary>
	/// <remarks>
	/// Behind the scenes, this calls SafeDelete() on all script behaviours attached to this gameObject. After all Destroy() calls are done, deletes the GameObject.
	/// </remarks>
	void SafeDelete();
};