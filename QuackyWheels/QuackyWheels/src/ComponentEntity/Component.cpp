#include "Component.h"

#include "GameObject.h"

Component::Component(GameObject *gameObject)
{
	this->gameObject = gameObject;
	gameObject->AddComponent(this);
}

Component::~Component()
{
	gameObject->RemoveComponent(this);
	gameObject = nullptr; //At this point, our component is totally dead.
}

GameObject *Component::GetGameObject()
{
	return gameObject;
}

Transform *Component::GetTransform()
{
	return gameObject->GetTransform();
}


EngineSystems *Component::getEngineSystems()
{
	return GetGameObject()->getEngineSystems();
}




