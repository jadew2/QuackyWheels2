//
// Created by Jade White on 2016-03-26.
//

#include "MainMenuScreen.h"

#include "../../Components/MeshRenderer.h"
#include "../../Renderer/Types/Material.h"
#include "../../Renderer/Types/Mesh.h"
#include "../../AssetImport/Assets.h"
#include "../../Components/Standard/BasicTeeter.h"
#include "../../Components/Standard/BasicRotate.h"
#include "../../Prefabs/DuckPrefab.h"

MainMenuScreen::MainMenuScreen(World* worldRef, MenuManager* manager) : GameObject(worldRef)
{
	//The rocking logo
	GameObject* logo = new GameObject(worldRef, "QuackyWheelsLogo");
	{
		MeshRenderer* logoRenderer = new MeshRenderer(logo);
		logoRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		logoRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/QuackyWheelsLogo.png"));
		logoRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
		logoRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
		logoRenderer->setUI(true);

		//position/scale it
		logoRenderer->GetTransform()->SetPosition(Vector3(0.5f, 0.15f, 0.0f));
		logoRenderer->GetTransform()->SetLocalScale(Vector3(0.3f, 0.6f, 0.0f));

		//Make it teeter
		BasicTeeter* teeter = new BasicTeeter(logo);
		teeter->angle = 10.0f;
		teeter->speed = 3.0f;
	}
	logo->GetTransform()->SetParent(this->GetTransform());


	//The play button
	GameObject* playButtonGo = new GameObject(worldRef, "Play Button");
	PlayButton* playButton = new PlayButton(playButtonGo, manager);
	playButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.4f, 1.0f));
	playButtonGo->GetTransform()->SetParent(this->GetTransform());

	GameObject* controlsButtonGo = new GameObject(worldRef, "Control Button");
	ControlsButton* controls = new ControlsButton(controlsButtonGo, manager);
	controlsButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.6f, 0.0f));
	controlsButtonGo->GetTransform()->SetParent(this->GetTransform());

	GameObject* quitButtonGo = new GameObject(worldRef, "Quit Button");
	QuitButton* quit = new QuitButton(quitButtonGo, manager);
	quitButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.8f, 0.0f));
	quitButtonGo->GetTransform()->SetParent(this->GetTransform());

	//Quacky, which rotates around the camera. Intimidating.
	GameObject* quackyFaceParent = new GameObject(worldRef);
	quackyFaceParent->GetTransform()->SetParent(this->GetTransform());
	DuckPrefab* quackyFace = new DuckPrefab(worldRef);
	quackyFace->GetTransform()->SetParent(quackyFaceParent->GetTransform());
	quackyFace->GetTransform()->SetLocalPosition(Vector3(7, -2.0f, 0));
	quackyFace->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(0.0f, 220.0f, 20.0f)));
	quackyFace->GetTransform()->SetLocalScale(Vector3(2.0f, 2.0f, 2.0f));

	BasicRotate* rotater = new BasicRotate(quackyFaceParent);
	rotater->rotation = Vector3(0.0f, 1.0f, 0.0f);
	rotater->speed = 7.0f;

	BasicTeeter* quackyTeeter = new BasicTeeter(quackyFace);
	rotater->rotation = Vector3(0.0f, 1.0f, 0.0f);
	rotater->speed = -20.0f;
}


MainMenuScreen::~MainMenuScreen()
{

}

