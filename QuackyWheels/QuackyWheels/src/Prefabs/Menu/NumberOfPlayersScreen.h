//
// Created by Jade White on 2016-03-26.
//

#pragma once

#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Menu/NumPlayerButton.h"
#include "../../Components/Menu/BackButton.h"

//This is for number of players
class MenuManager;
class NumberOfPlayersScreen : public GameObject
{
public:
	NumberOfPlayersScreen(World* worldRef, MenuManager* manager);
	virtual ~NumberOfPlayersScreen();
};

