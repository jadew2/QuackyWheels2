#pragma once
#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Menu/BackButton.h"
#include "../../Components/Menu/TrackButton.h"
#include "../../Various/LevelSettings/PlayerSettings.h"
#include <vector>

//This is for choosing the track
class MenuManager;
class TrackMenuScreen : public GameObject
{
public:
	TrackMenuScreen(World* worldRef, MenuManager* manager, std::vector<PlayerSettings> players);
	virtual ~TrackMenuScreen();
};
