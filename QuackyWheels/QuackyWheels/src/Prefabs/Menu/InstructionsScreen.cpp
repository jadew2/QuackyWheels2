#include "InstructionsScreen.h"


InstructionsScreen::InstructionsScreen(World* worldRef, MenuManager* manager) : GameObject(worldRef)
{
	GameObject* instructions = new GameObject(worldRef, "Instructions");
	instructions->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* instructRenderer = new MeshRenderer(instructions);
	instructRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	

	instructRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	instructRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
	instructRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Instructions2.png"));

	instructRenderer->setUI(true);

	instructions->GetTransform()->SetPosition(Vector3(0.5f, 0.4f, 0.0f));
	instructions->GetTransform()->SetLocalScale(Vector3(0.6f, 0.8f, 0.0f));

	GameObject* backButtonGo = new GameObject(worldRef, "Back");
	backButtonGo->GetTransform()->SetParent(this->GetTransform());

	BackButton* backButton = new BackButton(backButtonGo, manager, 'm');

	backButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.9f, 0.0f));

}


InstructionsScreen::~InstructionsScreen()
{
}
