//
// Created by Jade White on 2016-03-26.
//

#pragma once

#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Menu/PlayButton.h"
#include "../../Components/Menu/QuitButton.h"
#include "../../Components/Menu/ControlsButton.h"

class MenuManager;
class MainMenuScreen : public GameObject
{
public:
	MainMenuScreen(World* worldRef, MenuManager* manager);
	virtual ~MainMenuScreen();
};


