//
// Created by Marc DeSorcy on 2016-03-27.
//

#pragma once

#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Menu/BackButton.h"
#include "../../Components/Menu/CharSelec.h"
#include "../../Various/LevelSettings/LevelSettings.h"

//This is for choosing duck
class MenuManager;
class DuckMenuScreen : public GameObject
{
private:
	std::vector<PlayerSettings> players;
	int thisPl;
public:
	DuckMenuScreen(World* worldRef, MenuManager* manager, std::vector<PlayerSettings>& playerVec, int numPlayers, int thisPlayer);
	virtual ~DuckMenuScreen();
};

