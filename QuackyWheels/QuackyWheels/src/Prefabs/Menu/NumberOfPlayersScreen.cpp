//
// Created by Jade White on 2016-03-26.
//

#include "NumberOfPlayersScreen.h"

#include "../../Components/MeshRenderer.h"
#include "../../Renderer/Types/Material.h"
#include "../../Renderer/Types/Mesh.h"
#include "../../AssetImport/Assets.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../Input/Controller.h"


NumberOfPlayersScreen::NumberOfPlayersScreen(World* worldRef, MenuManager* manager) : GameObject(worldRef)
{
	GameObject* display = new GameObject(worldRef, "Player Num");
	display->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* displayRenderer = new MeshRenderer(display);
	displayRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	displayRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	displayRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0

	displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/NumofPlayers.png"));
	
	displayRenderer->setUI(true);

	display->GetTransform()->SetPosition(Vector3(0.5f, 0.1f, 0.0f));
	display->GetTransform()->SetLocalScale(Vector3(0.6f, 0.5f, 0.0f));

	int players = this->getEngineSystems()->controller->getPlayers();

	GameObject* singlePlayerButtonGo = new GameObject(worldRef, "1");
	NumPlayerButton* onePlayer = new NumPlayerButton(singlePlayerButtonGo, manager, 1);
	singlePlayerButtonGo->GetTransform()->SetLocalPosition(Vector3(0.25f, 0.3f, 0.0f));
	singlePlayerButtonGo->GetTransform()->SetParent(this->GetTransform());

	if (players == 1)
	{
		GameObject* noSecondButton = new GameObject(worldRef, "no2");
		MeshRenderer* noSecondRenderer = new MeshRenderer(noSecondButton);
		noSecondRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
		noSecondRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		noSecondRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		noSecondRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/controller.png"));
		noSecondRenderer->setUI(true);
		noSecondButton->GetTransform()->SetLocalPosition(Vector3(0.75f, 0.3f, 0.0f));
		noSecondButton->GetTransform()->SetParent(this->GetTransform());
		noSecondButton->GetTransform()->SetLocalScale(Vector3(0.2f, 0.2f, 0.0f));
	}

	if (players < 3)
	{
		GameObject* noThirdButton = new GameObject(worldRef, "no3");
		MeshRenderer* noThirdRenderer = new MeshRenderer(noThirdButton);
		noThirdRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
		noThirdRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		noThirdRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		noThirdRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/controller.png"));
		noThirdRenderer->setUI(true);
		noThirdButton->GetTransform()->SetLocalPosition(Vector3(0.25f, 0.6f, 0.0f));
		noThirdButton->GetTransform()->SetParent(this->GetTransform());
		noThirdButton->GetTransform()->SetLocalScale(Vector3(0.2f, 0.2f, 0.0f));
	}

	if (players < 4)
	{
		GameObject* noFourthButton = new GameObject(worldRef, "no4");
		MeshRenderer* noFourthRenderer = new MeshRenderer(noFourthButton);
		noFourthRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
		noFourthRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		noFourthRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		noFourthRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/controller.png"));
		noFourthRenderer->setUI(true);
		noFourthButton->GetTransform()->SetLocalPosition(Vector3(0.75f, 0.6f, 0.0f));
		noFourthButton->GetTransform()->SetParent(this->GetTransform());
		noFourthButton->GetTransform()->SetLocalScale(Vector3(0.2f, 0.2f, 0.0f));
	}

	if (players > 2)
	{

		GameObject* triplePlayerButtonGo = new GameObject(worldRef, "3");
		NumPlayerButton* threePlayers = new NumPlayerButton(triplePlayerButtonGo, manager, 3);
		triplePlayerButtonGo->GetTransform()->SetLocalPosition(Vector3(0.25f, 0.6f, 0.0f));
		triplePlayerButtonGo->GetTransform()->SetParent(this->GetTransform());
	}

	if (players > 1)
	{
		GameObject* doublePlayerButtonGo = new GameObject(worldRef, "2");
		NumPlayerButton* twoPlayers = new NumPlayerButton(doublePlayerButtonGo, manager, 2);
		doublePlayerButtonGo->GetTransform()->SetLocalPosition(Vector3(0.75f, 0.3f, 0.0f));
		doublePlayerButtonGo->GetTransform()->SetParent(this->GetTransform());
	}
	if (players > 3)
	{

		GameObject* squarePlayerButtonGo = new GameObject(worldRef, "4");
		NumPlayerButton* fourPlayers = new NumPlayerButton(squarePlayerButtonGo, manager, 4);
		squarePlayerButtonGo->GetTransform()->SetLocalPosition(Vector3(0.75f, 0.6f, 0.0f));
		squarePlayerButtonGo->GetTransform()->SetParent(this->GetTransform());
	}

	GameObject* backButtonGo = new GameObject(worldRef, "Back");
	BackButton* backButton = new BackButton(backButtonGo, manager, 'm');
	backButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.8f, 0.0f));
	backButtonGo->GetTransform()->SetParent(this->GetTransform());
}


NumberOfPlayersScreen::~NumberOfPlayersScreen()
{

}
