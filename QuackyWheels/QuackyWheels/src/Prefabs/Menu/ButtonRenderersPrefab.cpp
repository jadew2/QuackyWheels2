//
// Created by Jade White on 2016-03-28.
//

#include "ButtonRenderersPrefab.h"
#include "../../BaseTypes/Vector3.h"
#include "../../Components/MeshRenderer.h"
#include "../../Renderer/Types/Color.h"
#include "../../Renderer/Types/Mesh.h"
#include "../../Components/Transform.h"
#include "../../Renderer/Types/Material.h"

#include "../../AssetImport/Assets.h"

ButtonRenderersPrefab::ButtonRenderersPrefab(World* world) : GameObject(world)
{
	//Create the background
	GameObject* labelBackground = new GameObject(world, "Label Background");
	labelBackground->GetTransform()->SetParent(GetTransform(), false);

	backgroundRenderer = new MeshRenderer(labelBackground);
	backgroundRenderer->getMaterial()->setColor("ambient",Color::BlackInvisible());
	backgroundRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	backgroundRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Menu/Button0.png"));
	backgroundRenderer->mesh = Mesh::Quad();
	backgroundRenderer->setUI(true);

	//Create the text
	text = new GameObject(world, "Label Text");
	text->GetTransform()->SetParent(GetTransform(), false);
	text->GetTransform()->SetLocalScale(Vector3(0.9f,2.3f,0.9f)); //Make it a tad smaller

	textRenderer = new MeshRenderer(text);
	textRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	textRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
	textRenderer->mesh = Mesh::Quad();
	textRenderer->setUI(true);
}


ButtonRenderersPrefab::ButtonRenderersPrefab(World* world, Texture* textTexture) : ButtonRenderersPrefab(world)
{
	setTextTexture(textTexture);
}


void ButtonRenderersPrefab::setBackgroundTexture(Texture* backgroundTexture, bool transparent)
{
	//Fix the mesh renderer properties a smidge for rendering a texture.
	if (transparent)
	{
		backgroundRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
		backgroundRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	}
	else
	{
		backgroundRenderer->getMaterial()->setColor("ambient", Color::Black());
	}
	backgroundRenderer->getMaterial()->setTexture(backgroundTexture);
}


void ButtonRenderersPrefab::setTextTexture(Texture* textTexture)
{
	textRenderer->getMaterial()->setTexture(textTexture);
}

void ButtonRenderersPrefab::compensateTextScaleFor(Vector3 scale)
{
	//Scale the text renderer up until it's scale is equal to 1
	Vector3 newScale = textRenderer->GetTransform()->GetLocalScale() * (1.0f / scale.Magnitude());
	textRenderer->GetTransform()->SetLocalScale(newScale);
}


