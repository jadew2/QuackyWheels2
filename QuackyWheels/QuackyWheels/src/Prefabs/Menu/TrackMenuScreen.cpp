#include "TrackMenuScreen.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Input/Controller.h"

TrackMenuScreen::TrackMenuScreen(World* worldRef, MenuManager* manager, vector<PlayerSettings> players) : GameObject(worldRef)
{
	GameObject* display = new GameObject(worldRef, "Track Select");
	display->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* displayRenderer = new MeshRenderer(display);
	displayRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	displayRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	displayRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0

	displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/selecttrack.png"));

	displayRenderer->setUI(true);

	display->GetTransform()->SetPosition(Vector3(0.5f, 0.2f, 0.0f));
	display->GetTransform()->SetLocalScale(Vector3(0.6f, 0.5f, 0.0f));

	GameObject* secondTrackGo = new GameObject(worldRef, "CurvyMountain");
	TrackButton* advancedTrack = new TrackButton(secondTrackGo, manager, 1, players);
	secondTrackGo->GetTransform()->SetLocalPosition(Vector3(0.3f, 0.5f, 0.0f));
	secondTrackGo->GetTransform()->SetParent(this->GetTransform());

	GameObject* starterTrackGo = new GameObject(worldRef, "CurvyHill");
	TrackButton* starterTrack = new TrackButton(starterTrackGo, manager, 0, players);
	starterTrackGo->GetTransform()->SetLocalPosition(Vector3(0.7f, 0.5f, 0.0f));
	starterTrackGo->GetTransform()->SetParent(this->GetTransform());

	GameObject* starterTrackDescription = new GameObject(worldRef, "stdescript");
	starterTrackDescription->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* stRenderer = new MeshRenderer(starterTrackDescription);
	stRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	stRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	stRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
	stRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/short.png"));
	stRenderer->setUI(true);
	starterTrackDescription->GetTransform()->SetPosition(Vector3(0.7f, 0.6f, 0.0f));
	starterTrackDescription->GetTransform()->SetLocalScale(Vector3(0.3f, 0.7f, 0.0f));

	GameObject* advancedTrackDescription = new GameObject(worldRef, "atdescript");
	advancedTrackDescription->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* atRenderer = new MeshRenderer(advancedTrackDescription);
	atRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	atRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	atRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
	atRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/long.png"));
	atRenderer->setUI(true);
	advancedTrackDescription->GetTransform()->SetPosition(Vector3(0.3f, 0.6f, 0.0f));
	advancedTrackDescription->GetTransform()->SetLocalScale(Vector3(0.3f, 0.5f, 0.0f));

	GameObject* backButtonGo = new GameObject(worldRef, "Back");
	BackButton* backButton = new BackButton(backButtonGo, manager, 'd', players);
	backButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.9f, 0.0f));
	backButtonGo->GetTransform()->SetParent(this->GetTransform());
}


TrackMenuScreen::~TrackMenuScreen()
{
}
