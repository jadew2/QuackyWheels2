#pragma once
#include "../../ComponentEntity/GameObject.h"
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../Various/LevelSettings/LevelSettings.h"
class MenuManager;
class LoadingScreen : public ScriptBehaviour
{
public:
	LoadingScreen(World* worldRef, LevelSettings level, GameObject* gameObject);
	virtual ~LoadingScreen();
	virtual void Update() override;

	LevelSettings lv;
	int load;
};

