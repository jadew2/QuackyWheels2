#include "DuckMenuScreen.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Input/Controller.h"
DuckMenuScreen::DuckMenuScreen(World* worldRef, MenuManager* manager, std::vector<PlayerSettings>& playerVec, int numPlayers, int thisPlayer) : GameObject(worldRef)
{
	GameObject* display = new GameObject(worldRef, "Player Item");
	display->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* displayRenderer = new MeshRenderer(display);
	displayRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	displayRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	displayRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
	if (thisPlayer == 1)
	{
		displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Player1.png"));
	}
	else if (thisPlayer == 2)
	{
		displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Player2.png"));
	}
	else if (thisPlayer == 3)
	{
		displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Player3.png"));
	}
	else if (thisPlayer == 4)
	{
		displayRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Player4.png"));
	}
	displayRenderer->setUI(true);

	display->GetTransform()->SetPosition(Vector3(0.5f, 0.2f, 0.0f));
	display->GetTransform()->SetLocalScale(Vector3(0.6f, 0.5f, 0.0f));

	players = playerVec;
	thisPl = thisPlayer;
	GameObject* charSelec1Go = new GameObject(worldRef, "Duck 1");
	charSelec1Go->GetTransform()->SetParent(this->GetTransform());
	CharSelec* charSelec1 = new CharSelec(charSelec1Go, manager, numPlayers, thisPlayer, 0, players);
	charSelec1Go->GetTransform()->SetLocalPosition(Vector3(0.3f, 0.4f, 0.0f));
	
	GameObject* charSelec2Go = new GameObject(worldRef, "Duck 2");
	charSelec2Go->GetTransform()->SetParent(this->GetTransform());
	CharSelec* charSelec2 = new CharSelec(charSelec2Go, manager, numPlayers, thisPlayer, 1, players);
	charSelec2Go->GetTransform()->SetLocalPosition(Vector3(0.8f, 0.4f, 0.0f));

	GameObject* charSelec3Go = new GameObject(worldRef, "Duck 3");
	charSelec3Go->GetTransform()->SetParent(this->GetTransform());
	CharSelec* charSelec3 = new CharSelec(charSelec3Go, manager, numPlayers, thisPlayer, 2, players);
	charSelec3Go->GetTransform()->SetLocalPosition(Vector3(0.3f, 0.7f, 0.0f));

	GameObject* charSelec4Go = new GameObject(worldRef, "Duck 4");
	charSelec4Go->GetTransform()->SetParent(this->GetTransform());
	CharSelec* charSelec4 = new CharSelec(charSelec4Go, manager, numPlayers, thisPlayer, 3, players);
	charSelec4Go->GetTransform()->SetLocalPosition(Vector3(0.8f, 0.7f, 0.0f));

	GameObject* backButtonGo = new GameObject(worldRef, "Back");
	BackButton* backButton = new BackButton(backButtonGo, manager, 'p');
	backButton->controllablePlayerIndex = thisPlayer - 1;
	backButtonGo->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.9f, 0.0f));
	backButtonGo->GetTransform()->SetParent(this->GetTransform());
}


DuckMenuScreen::~DuckMenuScreen()
{
}
