//
// Created by Jade White on 2016-03-28.
//

#pragma once


#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../ComponentEntity/GameObject.h"

class MeshRenderer;
class Texture;
///<summary> Renders a background mesh and a text mesh, commonly used in buttons </summary>
class ButtonRenderersPrefab : public GameObject
{
public:
	MeshRenderer* backgroundRenderer; //Please don't set these.
	MeshRenderer* textRenderer; //Please don't set these.
	GameObject* text;

	ButtonRenderersPrefab(World* world);
	ButtonRenderersPrefab(World* world, Texture* textTexture);

	void setBackgroundTexture(Texture* backgroundTexture, bool transparent);
	void setTextTexture(Texture* textTexture);

	void compensateTextScaleFor(Vector3 scale);
};
