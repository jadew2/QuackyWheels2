#include "LoadingScreen.h"
#include "../../Components/MeshRenderer.h"
#include "../../Renderer/Types/Material.h"
#include "../../AssetImport/Assets.h"
#include "../../Components/Transform.h"

#include "../../World/World0.h"
#include "../../Components/Menu/System/MenuManager.h"

LoadingScreen::LoadingScreen(World* worldRef, LevelSettings level, GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	lv = level;
	load = 0;
	GameObject* loading = new GameObject(worldRef, "Loading");
	loading->GetTransform()->SetParent(this->GetTransform());
	MeshRenderer* loadingRenderer = new MeshRenderer(loading);
	loadingRenderer->mesh = Mesh::Quad(); //A centered full screen quad.	
	loadingRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
	loadingRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
	loadingRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/loading.png"));
	loadingRenderer->setUI(true);

	loading->GetTransform()->SetPosition(Vector3(0.78f, 0.98f, 0.0f));
	loading->GetTransform()->SetLocalScale(Vector3(0.6f, 0.5f, 0.0f));
	//SafeDelete();
	//getWorld()->safeDelete();

	//World* world0 = new World0(getWorld()->getUniverse(), *level);
}

void LoadingScreen::Update()
{
	
	if (load > 10)
	{
		SafeDelete();
		this->GetGameObject()->getWorld()->safeDelete();

		//Unload all assets to do with this world.
		//Assets::UnLoad(this->GetGameObject()->getWorld());
		World* world0 = new World0(this->GetGameObject()->getWorld()->getUniverse(), lv);
	}
	else
		load++;
	
}
LoadingScreen::~LoadingScreen()
{
}
