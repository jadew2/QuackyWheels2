#pragma once

#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Menu/BackButton.h"
#include "../../Components/MeshRenderer.h"

#include "../../Renderer/Types/Material.h"
#include "../../AssetImport/Assets.h"
#include "../../Components/Transform.h"

//This is for instructions
class MenuManager;
class InstructionsScreen :	public GameObject
{
public:
	InstructionsScreen(World* worldRef, MenuManager* manager);
	virtual ~InstructionsScreen();
};

