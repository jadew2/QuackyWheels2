//
// Created by Jade White on 2016-04-08.
//

#pragma once

#include "../ComponentEntity/GameObject.h"

class Model;
class World;
class Mesh;
class Texture;
class Track2Full : public GameObject
{
public:
	Model* trackModel;

	Mesh* goalPoints;
	Mesh* navMesh;
public:
	Track2Full(World* world);

	void createMeshRenderer(Mesh* mesh, Texture* texture);

	void createPhysicsTriangleMesh(Mesh* mesh);

};


