//
// Created by Jade White on 2016-04-12.
//

#include "SnakePrefab.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Mesh.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../GameLoop/EngineSystems.h"
#include "../Physics/Physics.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Components/PhysicsTriangleMesh.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Transform.h"
#include "../Components/Standard/PointerDeleter.h"

#include "../Components/Standard/TextureSlider.h"

using namespace std;

SnakePrefab::SnakePrefab(World* world) : GameObject(world)
{
	Vector3 snakePosition = Vector3(210.9f, -207.5f, 79.0f);
	this->GetTransform()->SetLocalPosition(snakePosition);

	Model* snakeModel = Assets::Load<Model>("EngineAssets/Models/Snake.obj");
	vector<Mesh*> meshes = snakeModel->getMeshes();


	Texture* snakeTex = Assets::Load<Texture>("EngineAssets/Images/SnakeScales.png");
	Texture* snakeFaceTex = Assets::Load<Texture>("EngineAssets/Images/SnakeFace.png");

	//Snake Coil Renderer
	MeshRenderer* coilRenderer = new MeshRenderer(this);
	coilRenderer->mesh = meshes[2];
	coilRenderer->getMaterial()->setTexture(snakeTex);
	TextureSlider* coilSlider = new TextureSlider(this,coilRenderer->getMaterial());
	coilSlider->offsetSpeed = Vector2(0.05f, 0.0f);

	//The neck points. 0 is the joint with the body and the neck, 1 is the joint with the neck and the head at rest.
	Mesh* neckPointMesh = meshes[1];
	neckPoints = vector<Vector3>();
	neckPoints.push_back(neckPointMesh->verticies[0] + snakePosition);
	neckPoints.push_back(neckPointMesh->verticies[1] + snakePosition);

	Quaternion headRotation = Quaternion::LookRotation(neckPoints[1] - neckPoints[0], Vector3::Up());

	//Snake Head
	head = new GameObject(world,"SnakeHead");
	head->GetTransform()->SetLocalPosition(neckPoints[1]);
	head->GetTransform()->SetLocalRotation(headRotation);
	{
		MeshRenderer* headRenderer = new MeshRenderer(head);
		headRenderer->mesh = meshes[3];
		headRenderer->getMaterial()->setTexture(snakeFaceTex);
	}

	//Snake Neck
	neck = new GameObject(world,"SnakeNeck");
	neck->GetTransform()->SetLocalPosition(neckPoints[0]);
	neck->GetTransform()->SetLocalRotation(headRotation);
	{
		MeshRenderer* neckRenderer = new MeshRenderer(neck);
		neckRenderer->mesh = meshes[4];
		neckRenderer->getMaterial()->setTexture(snakeTex);
		TextureSlider* neckSlider = new TextureSlider(this,neckRenderer->getMaterial());
		neckSlider->offsetSpeed = Vector2(0.05f, 0.0f);
	}

	
	//Physics Snake
	Mesh* physicsSnake = meshes[0]->Clone();
	physicsSnake->Deform(Matrix4x4::TRS(snakePosition, Quaternion::Identity(), Vector3::One()));
	createPhysicsTriangleMesh(physicsSnake);
	PointerDeleter<Mesh>* physicsSnakeMeshDeleter = new PointerDeleter<Mesh>(this); //Deletes the cloned mesh to avoid memory leaks.
	physicsSnakeMeshDeleter->pointers.push_back(physicsSnake);
}



void SnakePrefab::createPhysicsTriangleMesh(Mesh* mesh)
{
	physx::PxMaterial* tempMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.9f, 0.9f, 0.1f);
	physx::PxMaterial* const* material = &tempMaterial;

	physx::PxFilterData qryFilterData;
	setupDrivableSurface(qryFilterData);

	physx::PxFilterData simFilterData;
	simFilterData.word0 = COLLISION_FLAG_GROUND;
	simFilterData.word1 = COLLISION_FLAG_GROUND_AGAINST;

	physx::PxShapeFlags shapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	std::vector<physx::PxVec3> trackPhysicsVerticies;
	trackPhysicsVerticies.resize(mesh->verticies.size());

	for (size_t i = 0; i < mesh->verticies.size(); i++)
	{
		//trackPhysicsVerticies[i] = trackMesh->verticies[i].pxVec3(); //<-- Try this.

		trackPhysicsVerticies[i].x = (physx::PxReal)mesh->verticies[i].x;
		trackPhysicsVerticies[i].y = (physx::PxReal)mesh->verticies[i].y;
		trackPhysicsVerticies[i].z = (physx::PxReal)mesh->verticies[i].z;
	}

	std::vector<physx::PxU32> testMeshPhysicsIndicies;
	testMeshPhysicsIndicies.resize(mesh->triangles.size());
	for (size_t i = 0; i < mesh->triangles.size(); i++)
	{
		testMeshPhysicsIndicies[i] = (physx::PxU32)mesh->triangles[i]; //See if PxU16 changes anything
	}

	physx::PxU16* materialIndex = nullptr;

	GameObject* physicsTrack = new GameObject(getWorld(), "PhysicsSnakeCoil");
	PhysicsRigidStatic* trackActor = new PhysicsRigidStatic(physicsTrack, physicsTrack->GetTransform(), ObjectType::Obstacle, false);
	PhysicsTriangleMesh* trackShape = new PhysicsTriangleMesh(physicsTrack, trackActor, mesh->verticies.size(), trackPhysicsVerticies.data(), mesh->triangles.size(), testMeshPhysicsIndicies.data(), 1, material, materialIndex, physicsTrack->GetTransform(), &qryFilterData, &simFilterData, &shapeFlags);
	this->getEngineSystems()->physics->getScene()->addActor(*trackActor->getRigidStatic());
}

