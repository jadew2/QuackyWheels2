#include "SpeedPowerUpPrefab.h"
#include "../Components/MeshRenderer.h"
#include "../Renderer/Types/Mesh.h"
#include "../Components/Transform.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Components/Animators/ScaleAnimator.h"
#include "../Components/Standard/BasicMove.h"
#include "../Various/Random.h"
#include "../AssetImport/Assets.h"
#include "../Components/Standard/TimedDestroy.h"
#include "../Components/AudioSource.h"

#include <string>

SpeedPowerUpPrefab::SpeedPowerUpPrefab(World* world) : GameObject(world) {
	const size_t count = 5;

	float prefabLifeTime = 4;
	TimedDestroy* timedDestroy = new TimedDestroy(this);
	timedDestroy->timeForAction = prefabLifeTime;

	//Create the Red Explosion
	for (size_t i = 0; i < count; ++i)
	{
		Random::setSeed(getEngineSystems()->time->GetRealTime() * 1000.0f);

		float particleDuration = 0.5f;

		GameObject* explosionParticle = new GameObject(world, "ExplosionParticle");
		explosionParticle->GetTransform()->SetParent(this->GetTransform(), false);

		MeshRenderer* explosionRenderer = new MeshRenderer(explosionParticle);
		explosionRenderer->mesh = Mesh::Quad();
		explosionRenderer->backFaceCulling = false;
		explosionRenderer->setMaterial( new Material(Assets::Load<Shader>("EngineAssets/Shaders/AdditiveParticle.glsl")));
		explosionRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
		explosionRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
		explosionRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Explosion0.png"));

		MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(explosionParticle, explosionRenderer->getMaterial(), "tint");
		tintAnimator->values = vector<Color>{Color(0, 0, 0, 0), Color(1, 1, 1, 0.5), Color(1, 1, 1, 0.5), Color(1, 1, 1, 0)}; //Black Invisible --> White for 1/2 of the time --> Fade out
		tintAnimator->duration = particleDuration;

		MaterialPropertyAnimator<Vector3>* scaleAnimator = new MaterialPropertyAnimator<Vector3>(explosionParticle, explosionRenderer->getMaterial(), "ParticleScale");
		scaleAnimator->values = vector<Vector3>{Vector3::Zero(), Vector3::One() * 10};
		scaleAnimator->duration = particleDuration;

		float speed = 20.0f;
		BasicMove* mover = new BasicMove(explosionParticle);
		mover->velocity = Random::randomOnUnitSphere() * speed;

		TimedDestroy* particleTimedDestroy = new TimedDestroy(explosionParticle);
		particleTimedDestroy->timeForAction = particleDuration;
	}
}


SpeedPowerUpPrefab::~SpeedPowerUpPrefab() {

}
