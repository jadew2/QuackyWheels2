//
// Created by Jade White on 2016-04-02.
//

#include "MinePrefab.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"

#include "../ComponentEntity/GameObject.h"
#include "../Components/Transform.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"

MinePrefab::MinePrefab(World* world) : GameObject(world)
{
	MeshRenderer* meshRenderer = new MeshRenderer(this);
	meshRenderer->mesh = Assets::Load<Mesh>("EngineAssets/Models/Mine.obj");
	meshRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/MineTex.png"));

	float particleDuration = 0.5f;

	GameObject* redLight = new GameObject(world, "RedLight");
	redLight->GetTransform()->SetParent(this->GetTransform(), false);

	MeshRenderer* explosionRenderer = new MeshRenderer(redLight);
	explosionRenderer->mesh = Mesh::Quad();
	explosionRenderer->backFaceCulling = false;
	explosionRenderer->setMaterial(new Material(Assets::Load<Shader>("EngineAssets/Shaders/AdditiveParticle.glsl")));
	explosionRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	explosionRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
	explosionRenderer->getMaterial()->setVector3("ParticleScale", Vector3::One() * 3);
	explosionRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Explosion0.png"));

	MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(redLight, explosionRenderer->getMaterial(), "tint");
	tintAnimator->values = vector<Color>{Color(1, 1, 1, 0), Color::Red(), Color(1, 1, 1, 0)}; //Black Invisible --> White for 1/2 of the time --> Fade out
	tintAnimator->duration = 1;
	tintAnimator->loopType = ValueAnimator<Color>::LoopType::Loop;

}

