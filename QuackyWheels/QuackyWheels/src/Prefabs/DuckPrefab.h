//
// Created by Jade White on 2016-04-06.
//

#pragma once

#include "../ComponentEntity/GameObject.h"

class Texture;
class MeshRenderer;
class VehicleManager;
///<summary> A prefab for the standard player duck. </summary>
class DuckPrefab : public GameObject
{
public:
	MeshRenderer* bodyRenderer;
	MeshRenderer* stillFootRenderer;
	MeshRenderer* spinnerFootRenderer;

	DuckPrefab(World* world);
	DuckPrefab(World* world, VehicleManager* vehicleManager, int index);

	void setTexture(Texture* texture);
};
