//
// Created by Jade White on 2016-04-08.
//

#include "Track2Prefab.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Components/PhysicsTriangleMesh.h"
#include "../GameLoop/EngineSystems.h"
#include "../Physics/Physics.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Components/Standard/TextureSlider.h"

Track2Prefab::Track2Prefab(World* worldRef) : GameObject(worldRef)
{
	trackModel = Assets::Load<Model>("EngineAssets/Models/Track2Full.obj");
	vector<Mesh*> meshes = trackModel->getMeshes();

	Texture* pathTex = Assets::Load<Texture>("EngineAssets/Images/Path00.bmp");
	Texture* rockTex = Assets::Load<Texture>("EngineAssets/Images/Rock01.png");
	Texture* whiteRockTex = Assets::Load<Texture>("EngineAssets/Images/Snow.png");

	Texture* cobbleStoneTex = Assets::Load<Texture>("EngineAssets/Images/Cobblestone0.png");
	Texture* woodTex0 = Assets::Load<Texture>("EngineAssets/Images/Wood0.png");
	Texture* woodTex2 = Assets::Load<Texture>("EngineAssets/Images/Wood2.png");
	Texture* chest = Assets::Load<Texture>("EngineAssets/Images/ChestFinal.png");
	Texture* flagTex = Assets::Load<Texture>("EngineAssets/Images/Flag.png");
	Texture* water2Tex = Assets::Load<Texture>("EngineAssets/Images/Water.png");
	Texture* lightHouseTex = Assets::Load<Texture>("EngineAssets/Images/LightHouse.png");
	Texture* planeTex = Assets::Load<Texture>("EngineAssets/Images/Plane.png");


	//Visual Parts of the track
	{
		//TrackPart1 (Track under starting line)
		createMeshRenderer(meshes[5],pathTex);

		//TrackPart2 (Wood platform)
		createMeshRenderer(meshes[10],woodTex2);

		//TrackPart3 (Under Waterfall)
		createMeshRenderer(meshes[9],rockTex);

		//TrackPart4 (Inside cobblestone tunnerl)
		createMeshRenderer(meshes[8],cobbleStoneTex);

		//TrackPart5 (Inside cave)
		createMeshRenderer(meshes[7],rockTex);

		//TrackPart6 (Bottom turn path)
		createMeshRenderer(meshes[11],pathTex);

		//TrackPart7 (Near boulders at end of track)
		createMeshRenderer(meshes[6], whiteRockTex);

		//Tunnel entrances
		createMeshRenderer(meshes[13],cobbleStoneTex);

		//Tunnel
		createMeshRenderer(meshes[14],cobbleStoneTex);

		//Terrain
		createMeshRenderer(meshes[19],rockTex);

		//Supports
		createMeshRenderer(meshes[15],woodTex0);

		//Chests
		createMeshRenderer(meshes[16],chest);

		//Fences
		createMeshRenderer(meshes[17],woodTex0);

		//Flag
		createMeshRenderer(meshes[18],flagTex);

		//LightHouse
		createMeshRenderer(meshes[20],lightHouseTex);

		//Triggerpoints
		Mesh* triggerPointsMesh = meshes[0];
		triggerPoints = triggerPointsMesh->verticies;

		Mesh* boulderSpawnMesh = meshes[21];
		boulderSpawn = boulderSpawnMesh->verticies[0];

		//Waterfall 0
		createWaterfallMeshRenderer(meshes[4]);

		//Waterfall 1
		createWaterfallMeshRenderer(meshes[3]);

		//Waterfall 2
		createWaterfallMeshRenderer(meshes[12],water2Tex);

		//Waterfall 3
		createWaterfallMeshRenderer(meshes[2]);

		//Waterfall 4
		createWaterfallMeshRenderer(meshes[1]);

		//Fancy Plane
		createMeshRenderer(meshes[33],planeTex);
	}

	//Physics Meshes
	{
		//Tunnel Entrances
		createPhysicsTriangleMesh(meshes[24]);

		//Supports
		createPhysicsTriangleMesh(meshes[25]);

		//Terrain
		createPhysicsTriangleMesh(meshes[23]);

		//Track
		createPhysicsTriangleMesh(meshes[26]);

		//Tunnel
		createPhysicsTriangleMesh(meshes[27]);

		//Fence
		createPhysicsTriangleMesh(meshes[28]);

		//LightHouse
		createPhysicsTriangleMesh(meshes[22]);
	}
	//Visual Curve 28
	detailedGoalPoints = meshes[30];
	 //29
	goalPoints = meshes[31];
	navMesh = meshes[32];
}


void Track2Prefab::createMeshRenderer(Mesh* mesh, Texture* texture)
{
	MeshRenderer* newRenderer = new MeshRenderer(this);
	newRenderer->mesh = mesh;
	newRenderer->getMaterial()->setTexture(texture);
}


void Track2Prefab::createWaterfallMeshRenderer(Mesh* mesh, Texture* waterTexture)
{
	if (waterTexture == nullptr)
		waterTexture = Assets::Load<Texture>("EngineAssets/Images/Water02.png");
	MeshRenderer* newRenderer = new MeshRenderer(this);
	newRenderer->mesh = mesh;
	newRenderer->getMaterial()->setTexture(waterTexture);
	newRenderer->getMaterial()->setColor("tint",Color(1,1,1,0.5f));
	newRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible());
	newRenderer->getMaterial()->drawType = Material::DrawType::Transparent;

	TextureSlider* textureSlider = new TextureSlider(this,newRenderer->getMaterial());
	textureSlider->offsetSpeed = Vector2(0.1f,0.7f);
}


void Track2Prefab::createPhysicsTriangleMesh(Mesh* mesh)
{
	physx::PxMaterial* tempMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.9f, 0.9f, 0.1f);
	physx::PxMaterial* const* material = &tempMaterial;

	physx::PxFilterData qryFilterData;
	setupDrivableSurface(qryFilterData);

	physx::PxFilterData simFilterData;
	simFilterData.word0 = COLLISION_FLAG_GROUND;
	simFilterData.word1 = COLLISION_FLAG_GROUND_AGAINST;

	physx::PxShapeFlags shapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	std::vector<physx::PxVec3> trackPhysicsVerticies;
	trackPhysicsVerticies.resize(mesh->verticies.size());

	for (size_t i = 0; i < mesh->verticies.size(); i++)
	{
		//trackPhysicsVerticies[i] = trackMesh->verticies[i].pxVec3(); //<-- Try this.

		trackPhysicsVerticies[i].x = (physx::PxReal)mesh->verticies[i].x;
		trackPhysicsVerticies[i].y = (physx::PxReal)mesh->verticies[i].y;
		trackPhysicsVerticies[i].z = (physx::PxReal)mesh->verticies[i].z;
	}

	std::vector<physx::PxU32> testMeshPhysicsIndicies;
	testMeshPhysicsIndicies.resize(mesh->triangles.size());
	for (size_t i = 0; i < mesh->triangles.size(); i++)
	{
		testMeshPhysicsIndicies[i] = (physx::PxU32)mesh->triangles[i]; //See if PxU16 changes anything
	}

	physx::PxU16* materialIndex = nullptr;

	GameObject* physicsTrack = new GameObject(getWorld(), "physicsTrack");
	PhysicsRigidStatic* trackActor = new PhysicsRigidStatic(physicsTrack, physicsTrack->GetTransform(), ObjectType::Obstacle, false);
	PhysicsTriangleMesh* trackShape = new PhysicsTriangleMesh(physicsTrack, trackActor, mesh->verticies.size(), trackPhysicsVerticies.data(), mesh->triangles.size(), testMeshPhysicsIndicies.data(), 1, material, materialIndex, physicsTrack->GetTransform(), &qryFilterData, &simFilterData, &shapeFlags);
	this->getEngineSystems()->physics->getScene()->addActor(*trackActor->getRigidStatic());
}




