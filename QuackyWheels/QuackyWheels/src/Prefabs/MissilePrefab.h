//
// Created by Jade White on 2016-03-24.
//

#pragma once

#include "../World/World.h"
#include "../ComponentEntity/GameObject.h"
#include <string>

///<summary> A animated missile gameObject. </summary>
class MissilePrefab : public GameObject
{
public:
	MissilePrefab(World* world);
	MissilePrefab(World* world, std::string name);
	virtual ~MissilePrefab();
};


