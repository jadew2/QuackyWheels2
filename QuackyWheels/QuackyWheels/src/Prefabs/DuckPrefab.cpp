//
// Created by Jade White on 2016-04-06.
//



#include "DuckPrefab.h"

#include "../Components/Transform.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Animators/DuckFootAnimator.h"

DuckPrefab::DuckPrefab(World* world) : DuckPrefab(world, nullptr, -1){}


DuckPrefab::DuckPrefab(World* world, VehicleManager* vehicleManager, int index) : GameObject(world)
{
	Model* duckModel = Assets::Load<Model>("EngineAssets/Models/Quacky.obj");
	vector<Mesh*> duckMeshes = duckModel->getMeshes();
	Mesh* bodyMesh = duckMeshes[0];
	Mesh* feetStillMesh = duckMeshes[1];
	Mesh* feetSpinnerMesh = duckMeshes[2];

	Texture* defaultDuckTexture = Assets::Load<Texture>("EngineAssets/Images/Quacky.png");

	//The body
	GameObject* body = new GameObject(world, "DuckBody");
	body->GetTransform()->SetParent(this->GetTransform(), false);
	{
		bodyRenderer = new MeshRenderer(body);
		bodyRenderer->mesh = bodyMesh;
		bodyRenderer->getMaterial()->setTexture(defaultDuckTexture);
	}

	//The Still Feet
	GameObject* stillFeet = new GameObject(world, "DuckStillFeet");
	stillFeet->GetTransform()->SetParent(this->GetTransform(), false);
	{
		stillFootRenderer = new MeshRenderer(stillFeet);
		stillFootRenderer->mesh = feetStillMesh;
		stillFootRenderer->getMaterial()->setTexture(defaultDuckTexture);
	}

	//The Spinning Feet
	GameObject* spinnerFeet = new GameObject(world, "DuckSpinFeet");
	spinnerFeet->GetTransform()->SetParent(this->GetTransform(), false);
	{
		spinnerFootRenderer = new MeshRenderer(spinnerFeet);
		spinnerFootRenderer->mesh = feetSpinnerMesh;
		spinnerFootRenderer->getMaterial()->setTexture(defaultDuckTexture);
	}

	//The animator for spinning feet
	if (vehicleManager != nullptr)
	{
		DuckFootAnimator* footAnimator = new DuckFootAnimator(this, vehicleManager, index);
		footAnimator->setDuckFootProperties(spinnerFeet->GetTransform(), spinnerFootRenderer, stillFootRenderer);
		footAnimator->velocityForRotation = 2.0f;
		footAnimator->rotationAngle = -0.5f;
		footAnimator->rotationAxis = Vector3::Right();
	}

}


void DuckPrefab::setTexture(Texture* texture)
{
	bodyRenderer->getMaterial()->setTexture(texture);
	stillFootRenderer->getMaterial()->setTexture(texture);
	spinnerFootRenderer->getMaterial()->setTexture(texture);
}

