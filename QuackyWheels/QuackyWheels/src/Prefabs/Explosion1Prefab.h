//
// Created by Jade White on 2016-03-25.
//

#pragma once


#include "../ComponentEntity/GameObject.h"
#include <string>


class Explosion1Prefab : public GameObject
{
public:
	Explosion1Prefab(World* world);
	Explosion1Prefab(World* world, std::string name);
};


