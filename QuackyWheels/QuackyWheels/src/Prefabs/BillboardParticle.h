//
// Created by Jade White on 2016-04-02.
//

#pragma once

#include "../ComponentEntity/GameObject.h"

class MeshRenderer;
class Texture;
class TimedDestroy;
class BillboardParticle : public GameObject
{
public:
	MeshRenderer* particleRenderer;
	BillboardParticle(World* world);

private:
	TimedDestroy* timedDestroy;

public:
	///<summary> Sets the texture of the particle renderer. </summary>
	void setTexture(Texture* texture);

	///<summary> Sets the lifetime of this particle gameObject. Be careful not to use the GameObject past this time. </summary>
	void setLifeTime(float seconds);


};


