//
// Created by Jade White on 2016-03-24.
//

#include "MissilePrefab.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Standard/BasicRotate.h"
#include "../Components/Transform.h"
#include "../Components/Standard/BasicJiggle.h"
#include "../Components/Standard/TextureSlider.h"

using namespace std;

MissilePrefab::MissilePrefab(World* worldRef) : GameObject(worldRef)
{
	Name = "MissilePrefab";
	//Load the mesh
	Model* missileModel = Assets::Load<Model>("EngineAssets/Models/Missile.obj");
	vector<Mesh*> meshes = missileModel->getMeshes();
	Mesh* missileMesh = meshes[1];
	Mesh* fireMesh = meshes[0];

	//Create the missile GO
	GameObject* missileMeshGO = new GameObject(worldRef,string("MissileMesh"));
	missileMeshGO->GetTransform()->SetParent(this->GetTransform(),false);
	{
		MeshRenderer* missileRenderer = new MeshRenderer(missileMeshGO);
		missileRenderer->mesh = missileMesh;
		missileRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Missile.png"));

		//BasicRotate* missileRotater = new BasicRotate(missileMeshGO);
		//missileRotater->rotation = Vector3(0,0,270);
		//missileRotater->speed = 1;

		//BasicJiggle* missileJiggle = new BasicJiggle(missileMeshGO);
		//missileJiggle->addedScale = Vector3(0,0,0.05f);
		//missileJiggle->speed = 10.0f;
	}

	//Create the fire out back
	GameObject* fireMeshGO = new GameObject(worldRef,string("MissileFireMesh"));
	fireMeshGO->GetTransform()->SetParent(this->GetTransform(),false);
	{
		MeshRenderer* fireRenderer = new MeshRenderer(fireMeshGO);
		fireRenderer->mesh = fireMesh;
		fireRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Fire.png"));
		fireRenderer->getMaterial()->drawType = Material::DrawType::Transparent;

		//Slide the fire texture
		TextureSlider* textureSlider = new TextureSlider(fireMeshGO);
		textureSlider->material = fireRenderer->getMaterial();
		textureSlider->offsetSpeed = Vector2(0,1.0f);

		//BasicRotate* missileRotater = new BasicRotate(fireMeshGO);
		//missileRotater->rotation = Vector3(0,0,180);
		//missileRotater->speed = 1;

		//BasicJiggle* missileJiggle = new BasicJiggle(fireMeshGO);
		//missileJiggle->addedScale = Vector3(0,0,0.07f);
		//missileJiggle->speed = 9.0f;
	}

}


MissilePrefab::MissilePrefab(World* worldRef, std::string name) : MissilePrefab(worldRef)
{
	Name = name;
}


MissilePrefab::~MissilePrefab()
{
}
