//
// Created by Jade White on 2016-04-02.
//

#include "BillboardParticle.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Shader.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Components/Standard/TimedDestroy.h"


BillboardParticle::BillboardParticle(World* world) : GameObject(world)
{
	timedDestroy = nullptr;

	particleRenderer = new MeshRenderer(this);
	particleRenderer->mesh = Mesh::Quad();
	particleRenderer->backFaceCulling = false;
	particleRenderer->setMaterial( new Material(Assets::Load<Shader>("EngineAssets/Shaders/AdditiveParticle.glsl")));
	particleRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	particleRenderer->getMaterial()->setColor("ambient",Color::BlackInvisible());
	particleRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Explosion0.png"));


	/*
	MaterialPropertyAnimator<Color>* animator = new MaterialPropertyAnimator<Color>(explosionParticle, explosionRenderer->getMaterial(), "tint");
	animator->loopType = MaterialPropertyAnimator<Color>::LoopType::Once;
	animator->values = vector<Color>{Color(0,0,0,0),Color::White(),Color::White(),Color(1,1,1,0)}; //Black Invisible --> White for 1/2 of the time --> Fade out
	animator->duration = particleDuration;
	 */

}


void BillboardParticle::setLifeTime(float seconds)
{
	if (seconds <= 0)
	{
		if (timedDestroy != nullptr)
			timedDestroy->SafeDelete();
		return;
	}

	if (timedDestroy == nullptr)
		timedDestroy = new TimedDestroy(this);
	timedDestroy->timeForAction = seconds;
}


void BillboardParticle::setTexture(Texture* texture)
{
	particleRenderer->getMaterial()->setTexture(texture);
}



