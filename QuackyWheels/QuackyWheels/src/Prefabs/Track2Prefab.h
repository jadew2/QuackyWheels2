//
// Created by Jade White on 2016-04-08.
//

#pragma once

#include "../ComponentEntity/GameObject.h"
#include "../BaseTypes/Vector3.h"
#include <vector>

class Model;
class World;
class Mesh;
class Texture;
class Track2Prefab : public GameObject
{
public:
	Model* trackModel;

	Mesh* detailedGoalPoints;
	Mesh* goalPoints;
	Mesh* navMesh;
	std::vector<Vector3> triggerPoints;
	Vector3 boulderSpawn;
public:
	Track2Prefab(World* world);

private:
	void createMeshRenderer(Mesh* mesh, Texture* texture);
	void createWaterfallMeshRenderer(Mesh* mesh, Texture* waterTexture = nullptr);


	void createPhysicsTriangleMesh(Mesh* mesh);

};


