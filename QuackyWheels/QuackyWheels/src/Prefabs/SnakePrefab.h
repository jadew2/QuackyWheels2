//
// Created by Jade White on 2016-04-12.
//

#pragma once


#include "../ComponentEntity/GameObject.h"
#include "../BaseTypes/Vector3.h"

#include <vector>

class Mesh;
class World;
class SnakePrefab : public GameObject
{
public:
	GameObject* head;
	GameObject* neck;
	std::vector<Vector3> neckPoints;

	SnakePrefab(World* world);

	void createPhysicsTriangleMesh(Mesh* mesh);

};

