//
// Created by Jade White on 2016-03-25.
//

#pragma once


#include "../ComponentEntity/GameObject.h"
#include <string>


class Explosion0Prefab : public GameObject
{
public:
	Explosion0Prefab(World* world);
	Explosion0Prefab(World* world, std::string name);

};


