//
// Created by Jade White on 2016-04-08.
//

#include "Track2Full.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Components/PhysicsTriangleMesh.h"
#include "../GameLoop/EngineSystems.h"
#include "../Physics/Physics.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Vehicle/VehicleFilterShader.h"

Track2Full::Track2Full(World* worldRef) : GameObject(worldRef)
{
	trackModel = Assets::Load<Model>("EngineAssets/Models/Track2Full.obj");
	vector<Mesh*> meshes = trackModel->getMeshes();

	Texture* pathTex = Assets::Load<Texture>("EngineAssets/Images/Path00.bmp");
	Texture* rockTex = Assets::Load<Texture>("EngineAssets/Images/Rock01.png");
	Texture* cobbleStoneTex = Assets::Load<Texture>("EngineAssets/Images/Cobblestone0.png");
	Texture* woodTex0 = Assets::Load<Texture>("EngineAssets/Images/Wood0.png");
	Texture* woodTex2 = Assets::Load<Texture>("EngineAssets/Images/Wood2.png");
	Texture* chest = Assets::Load<Texture>("EngineAssets/Images/ChestFinal.png");
	Texture* flagTex = Assets::Load<Texture>("EngineAssets/Images/Flag.png");

	//Visual Parts of the track
	{
		//TrackPart1 (Track under starting line)
		createMeshRenderer(meshes[0],pathTex);

		//TrackPart2 (Wood platform)
		createMeshRenderer(meshes[5],woodTex2);

		//TrackPart3 (Under Waterfall)
		createMeshRenderer(meshes[4],rockTex);

		//TrackPart4 (Inside cobblestone tunnerl)
		createMeshRenderer(meshes[3],cobbleStoneTex);

		//TrackPart5 (Inside cave)
		createMeshRenderer(meshes[2],rockTex);

		//TrackPart6 (Bottom turn path)
		createMeshRenderer(meshes[6],pathTex);

		//TrackPart5 (Near boulders at end of track)
		createMeshRenderer(meshes[1],rockTex);

		//Tunnel entrances
		createMeshRenderer(meshes[7],cobbleStoneTex);

		//Tunnel
		createMeshRenderer(meshes[8],cobbleStoneTex);

		//Terrain
		createMeshRenderer(meshes[9],rockTex);

		//Supports
		createMeshRenderer(meshes[10],woodTex0);

		//Chests
		createMeshRenderer(meshes[11],chest);

		//Fences
		createMeshRenderer(meshes[12],woodTex0);

		//Flag
		createMeshRenderer(meshes[13],flagTex);
	}

	//Physics Meshes
	{
		//Tunnel Entrances
		createPhysicsTriangleMesh(meshes[14]);

		//Supports
		createPhysicsTriangleMesh(meshes[15]);

		//Terrain
		createPhysicsTriangleMesh(meshes[16]);

		//Track
		createPhysicsTriangleMesh(meshes[17]);

		//Tunnel
		createPhysicsTriangleMesh(meshes[18]);

		//Fence
		createPhysicsTriangleMesh(meshes[19]);
	}

	goalPoints = meshes[21];
	navMesh = meshes[22];

}


void Track2Full::createMeshRenderer(Mesh* mesh, Texture* texture)
{
	MeshRenderer* newRenderer = new MeshRenderer(this);
	newRenderer->mesh = mesh;
	newRenderer->getMaterial()->setTexture(texture);
}

void Track2Full::createPhysicsTriangleMesh(Mesh* mesh)
{
	physx::PxMaterial* tempMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.9f, 0.9f, 0.1f);
	physx::PxMaterial* const* material = &tempMaterial;

	physx::PxFilterData qryFilterData;
	setupDrivableSurface(qryFilterData);

	physx::PxFilterData simFilterData;
	simFilterData.word0 = COLLISION_FLAG_GROUND;
	simFilterData.word1 = COLLISION_FLAG_GROUND_AGAINST;

	physx::PxShapeFlags shapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	std::vector<physx::PxVec3> trackPhysicsVerticies;
	trackPhysicsVerticies.resize(mesh->verticies.size());

	for (size_t i = 0; i < mesh->verticies.size(); i++)
	{
		//trackPhysicsVerticies[i] = trackMesh->verticies[i].pxVec3(); //<-- Try this.

		trackPhysicsVerticies[i].x = (physx::PxReal)mesh->verticies[i].x;
		trackPhysicsVerticies[i].y = (physx::PxReal)mesh->verticies[i].y;
		trackPhysicsVerticies[i].z = (physx::PxReal)mesh->verticies[i].z;
	}

	std::vector<physx::PxU32> testMeshPhysicsIndicies;
	testMeshPhysicsIndicies.resize(mesh->triangles.size());
	for (size_t i = 0; i < mesh->triangles.size(); i++)
	{
		testMeshPhysicsIndicies[i] = (physx::PxU32)mesh->triangles[i]; //See if PxU16 changes anything
	}

	physx::PxU16* materialIndex = nullptr;

	GameObject* physicsTrack = new GameObject(getWorld(), "physicsTrack");
	PhysicsRigidStatic* trackActor = new PhysicsRigidStatic(physicsTrack, physicsTrack->GetTransform(), ObjectType::Obstacle, false);
	PhysicsTriangleMesh* trackShape = new PhysicsTriangleMesh(physicsTrack, trackActor, mesh->verticies.size(), trackPhysicsVerticies.data(), mesh->triangles.size(), testMeshPhysicsIndicies.data(), 1, material, materialIndex, physicsTrack->GetTransform(), &qryFilterData, &simFilterData, &shapeFlags);
	this->getEngineSystems()->physics->getScene()->addActor(*trackActor->getRigidStatic());
}




