//
// Created by Jade White on 2016-02-17.
//

#include "EngineSystems.h"
#include "../Time/Time.h"
#include "../Input/Controller.h"
#include "../Physics/Physics.h"
#include "../Script/Scripting.h"
#include "../Sound/Sound.h"
#include "../Renderer/Renderer.h"

EngineSystems::EngineSystems()
{
	time = new Time();
	scripting = new Scripting();
	controller = new Controller();
	physics = new Physics();
	renderer = new Renderer();
	sound = new Sound();
}

EngineSystems::~EngineSystems()
{
	delete time;
	delete scripting;
	delete controller;
	delete physics;
	delete renderer;
	delete sound;
}
