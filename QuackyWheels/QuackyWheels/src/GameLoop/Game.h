#pragma once
#include <memory>

class Universe;
class EngineSystems;


class Game
{
public:
	Game();
	virtual ~Game();
	void Run();

	//The engine systems
	std::unique_ptr <EngineSystems> es;
	std::unique_ptr <Universe> universe;
};
