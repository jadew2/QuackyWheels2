#include "Game.h"

#include <time.h>

#include "../Time/TimeUpdater.h"
#include "../World/Universe.h"
#include "../World/World0.h"
#include "../Input/Controller.h"
#include "../Physics/Physics.h"
#include "../Renderer/Renderer.h"
#include "../Script/Scripting.h"
#include "../Time/Time.h"
#include "../Script/Deleter.h"
#include "../Script/ScriptEvaluator.h"
#include "EngineSystems.h"
#include "../World/IntroWorld.h"
#include "../World/MenuWorld.h"
#include "../World/TestWorld.h"

Game::Game() {
	
}


Game::~Game() {

}


void Game::Run() 
{
	const bool testWorld = false; //Debug Value
	const bool introFirst = true; //Debug Value
	const bool menuFirst = true; //Debug value.
	std::unique_ptr<EngineSystems> es(new EngineSystems());
	std::unique_ptr<Universe> universe(new Universe(es.get()));

	World* engineWorld = new World(universe.get());
	Debug::engineWorld = engineWorld;


	//---Load the First world
	if (testWorld)
		World* testWorld = new TestWorld(universe.get());
	else if (introFirst)
		World* introWorld = new IntroWorld(universe.get());
	else if (menuFirst)
		World* menuWorld = new MenuWorld(universe.get());

	else
		World* world0 = new World0(universe.get(), LevelSettings(std::vector<PlayerSettings>{PlayerSettings(0), PlayerSettings(0), PlayerSettings(0), PlayerSettings(0)}, 1, 1));

	//---Load the first level
	//World* world0 = new World0(universe);

	//---Various variables for loop
	bool shouldQuit = false;
	float missingPhysicsTime = 0.0f;

	//---Start Time class updater
	TimeUpdater timeClassUpdater(es->time);
	timeClassUpdater.Start();

	time_t start = time(0);

	while(!shouldQuit)
	{
		//Do level loading...
		//We're might need a world list or a World::Init() method to load world without freezing the loop here.

		//--- Get Input
		shouldQuit = es->controller->ProcessInput(); //Handle input with commands from the Keyboard/Controller.
		shouldQuit |= universe->getNumberOfWorlds() == 0; //If we have no worlds loaded, it's time to quit.
		if (shouldQuit)
			universe->safeDelete(); //If we should quit, use this frame to clean everything up.

		//--- Early Script Method Calls
		es->scripting->evaluator->PreUpdates();

		//—-- Physics Method Calls and Simulation
		float deltaTime = es->time->GetUnscaledDeltaTime();
		float fixedDeltaTime = es->time->GetPhysicsTimestep();
		float maxPhysicsTime = es->time->MaxPhysicsTime;

		float simulatedPhysicsTime = 0.0f;
		if (deltaTime < maxPhysicsTime) //Don't add deltaTime if it's MASSIVE.
			missingPhysicsTime += deltaTime; // “missingPhysicsTime += timeItTookToRenderLastFrame”

		timeClassUpdater.ResetPhysicsCounter();
		while (missingPhysicsTime >= fixedDeltaTime && simulatedPhysicsTime < maxPhysicsTime)
		{
			es->scripting->evaluator->PhysicsUpdates();

			es->physics->getScene()->simulate((physx::PxReal)fixedDeltaTime);
			es->physics->getScene()->fetchResults(true); //Calls collision events
			timeClassUpdater.IncrementPhysicsCounter();
			missingPhysicsTime -= fixedDeltaTime;
			simulatedPhysicsTime += fixedDeltaTime;
		}

 		//--- Other Script method calls
		es->scripting->evaluator->Updates(); //Handles ScriptBehaviour safe deletion too.

		es->scripting->gameObjectDeleter->DoAction(); //Do GameObject safe deletion. Must be after Updates().
		es->scripting->worldDeleter->DoAction(); //Do World safe deletion. (Level unloading) Must be after Updates().

		es->scripting->evaluator->PostUpdates();

		//--- Render the scene
		es->renderer->render();

		//Update deltaTime and realTime
		timeClassUpdater.Update();
	}
}