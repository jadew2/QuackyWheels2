//
// Created by Jade White on 2016-02-17.
//

#pragma once

class Time;
class Scripting;
class Controller;
class Sound;
class Renderer;
class Physics;

///<summary> Holds a pointer to all of the low level game engine systems. </summary>
///<remarks>
/// Be careful only to use these pointers for stuff that couldn't be done in Components. Otherwise, the lower engine systems will bloat fast.
///
/// Follow these general guidelines:
/// 1. Can what you're doing use existing components? (Create components that talk to the lower engine for you. Talk to them instead of talking to the lower engine.)
/// 2. Can what you're doing be a separate component? (Don't make a game logic script that talks directly to the lower engine)
///</remarks>
#define EngineSystems_Defined
class EngineSystems
{
public:
	EngineSystems();
	virtual ~EngineSystems();

	Time* time;
	Scripting* scripting;
	Controller* controller;
	Sound* sound;
	Renderer* renderer;
	Physics* physics;



	//Universe

	//Scene* scene; //Stays in SceneEngineSystems
};

