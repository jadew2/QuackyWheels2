#include "ShaderTools.h"
#include <vector>
#include<iostream>
#include<fstream>

/* Shader Program Helper
	This function will attempt to create a shader program that has
	the Vertex and Fragement shader code that you pass in. If successfully 
	compiled and linked to the program, the unique program ID given by
	OpenGL will be returned. This ID will be >1 if successful, or 0 (an
	invalid ID) if any of the above fails.

	Most of the Code below is for checking if the shaders compiled and 
	linked correctly.
*/
ShaderTools::ShaderTools() {

}

ShaderTools::~ShaderTools() {

}

GLuint ShaderTools::CreateShaderProgram(	const std::string &vsSource,
				const std::string &fsSource )
{
	GLuint programID = glCreateProgram();
	GLuint vsID = glCreateShader( GL_VERTEX_SHADER );
	GLuint fsID = glCreateShader( GL_FRAGMENT_SHADER );

	if( programID == 0 || vsID == 0 || fsID == 0 )
	{
		// Clean up others that were created
		glDeleteProgram( programID );
		glDeleteShader( vsID );
		glDeleteShader( fsID );

		std::cerr << "ShaderTools: Cannot create Shaders or Program" << std::endl;
		return 0; // invalid ID
	}

	// glShaderSource() expects char**, so these are helper variables
	const char * vsSourceArray = vsSource.c_str();
	const char * fsSourceArray = fsSource.c_str();

	//https://www.opengl.org/sdk/docs/man4/xhtml/glShaderSource.xml
	glShaderSource(	vsID,
			1,
			&vsSourceArray,
			NULL );

	glShaderSource( fsID,
			1,
			&fsSourceArray,
			NULL );

	// Compile the Shader Sources, check for errors
	glCompileShader( vsID );
	glCompileShader( fsID );

	if( 	!checkCompileStatus( vsID ) ||
		!checkCompileStatus( fsID ) )
	{
		// Clean up others that were created
		glDeleteProgram( programID );
		glDeleteShader( vsID );
		glDeleteShader( fsID );

		std::cerr << "ShaderTools: Could not compile shaders: \nShader: " << vsSource << "\nShader: " << fsSource << std::endl;
		return 0; // invalid ID
	}

	glAttachShader( programID, vsID );
	glAttachShader( programID, fsID );

	glLinkProgram( programID );

	if( !checkLinkStatus( programID ) )
	{
		// Clean up others that were created
		glDeleteProgram( programID );
		glDeleteShader( vsID );
		glDeleteShader( fsID );

		std::cerr << "Cannot create Shaders or Program" << std::endl;
		return 0; // invalid ID
	}

	return programID;
}

bool ShaderTools::checkLinkStatus( GLint programID )
{
	GLint result;
	int infoLogLength;
	glGetProgramiv( programID, GL_LINK_STATUS, &result );
	if( !result )
	{
		glGetProgramiv( programID, GL_INFO_LOG_LENGTH, &infoLogLength );
		std::vector<char> errorMsg( infoLogLength );
		glGetProgramInfoLog(	programID,
					infoLogLength,
					NULL,
					errorMsg.data() );
		return false;
	}
	return true;
}

bool ShaderTools::checkCompileStatus( GLint shaderID )
{
	GLint result;
	int infoLogLength;
	glGetShaderiv( shaderID, GL_COMPILE_STATUS, &result );
	if( !result )
	{
		glGetShaderiv( shaderID, GL_INFO_LOG_LENGTH, &infoLogLength );
		std::vector<char> errorMsg( infoLogLength );
		glGetShaderInfoLog(	shaderID,
					infoLogLength,
					NULL,
					errorMsg.data() );
		std::cerr << errorMsg.data() << std::endl;
		return false;
	}
	return true; // otherwise
}

// Returns Empty String if can't load from file
std::string ShaderTools::loadShaderStringfromFile( const std::string & filePath )
{
	std::string shaderCode;
	std::ifstream fileStream( filePath.c_str(), std::ios::in );
	if( fileStream.is_open() )
	{
		std::string line;
		while( std::getline( fileStream, line ) )
		{
			shaderCode += "\n" + line;
		}
		fileStream.close();
	
	}
	else
	{
		std::cerr << "Could Not Open File " << filePath << std::endl;
	}
	return shaderCode;	
}
