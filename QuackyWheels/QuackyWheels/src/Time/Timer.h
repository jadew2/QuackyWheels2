#pragma once

#include <SDL.h>
#include <stdint.h>

class Timer {
public:
	Timer();
	~Timer();

	void start();
	void reset();
	void pause();
	void resume();


	uint32_t getTicks();
	float GetSeconds();

	bool isStarted();
	bool isPaused();

private:
	//The clock time when the timer started
	uint32_t mStartTicks;

	//The ticks stored when the timer was paused
	uint32_t mPausedTicks;

	//The timer status
	bool mPaused; 
	bool mStarted;
};

