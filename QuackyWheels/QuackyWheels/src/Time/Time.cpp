//
// Created by Jade White on 2016-02-17.
//

#include "Time.h"

Time::Time()
{
	realtime = 0.0f;
	deltaTime = 1.0f/60.0f;
	physicsTimestep = 1.0f/60.0f;
	timeScale = 1.0f;
	physicsUpdates = 0;

	realTimer = new Timer();
	realTimer->start();
}

Time::~Time()
{
	delete realTimer;
}


float Time::GetDeltaTime()
{
	return deltaTime * timeScale;
}

float Time::GetUnscaledDeltaTime()
{
	return deltaTime;
}

void Time::SetDeltaTime(float value)
{
	deltaTime = value;
}

float Time::GetPhysicsTimestep()
{
	return physicsTimestep;
}

void Time::SetPhysicsTimestep(float value)
{
	physicsTimestep = value;
}

unsigned int Time::GetPhysicsUpdateCount()
{
	return physicsUpdates;
}

void Time::SetPhysicsUpdateCount(unsigned int value)
{
	physicsUpdates = value;
}


float Time::GetTimeScale()
{
	return timeScale;
}

void Time::SetTimeScale(float value)
{
	timeScale = value;
}

float Time::GetRealTime()
{
	return realTimer->GetSeconds();
}


float Time::GetTime()
{
	return time;
}

void Time::SetTime(float value)
{
	time = value;
}


