//
// Created by Jade White on 2016-02-17.
//

#pragma once


#include "Timer.h"

class Time
{
private:
	Timer* realTimer;


	///<summary> The time from when the app started in seconds. </summary>
	float realtime;
	///<summary> The time it took to render the last frame in seconds. </summary>
	float deltaTime;
	///<summary> The time from when the app started, scaled like deltaTime. </summary>
	float time;
	///<summary> The time between each call to ComponentScript.PhysicsUpdate() in seconds.</summary>
	float physicsTimestep;
	///<summary> the number of physics updates this frame </summary>	
	unsigned int physicsUpdates = 0;
	///<summary> The flow of time. This value is multiplied with deltaTime before being sent out. </summary>
	float timeScale;

public:
	float const MaxPhysicsTime = 0.33f;

	Time();
	~Time();

	///<summary> The time from when the app started in seconds. </summary>
	float GetRealTime();

	///<summary> The time it took to render the last frame in seconds. </summary>
	float GetDeltaTime();
	///<summary> The time it took to render the last frame. </summary>
	float GetUnscaledDeltaTime();
	void SetDeltaTime(float value);

	///<summary> The time from when the app started, scaled like deltaTime, so not always realtime. </summary>
	float GetTime();
	void SetTime(float value);

	///<summary> The time between each call to ComponentScript.PhysicsUpdate() in seconds. </summary>
	float GetPhysicsTimestep();
	void SetPhysicsTimestep(float value);

	///<summary> The time between each call to ComponentScript.PhysicsUpdate() in seconds. </summary>
	unsigned int GetPhysicsUpdateCount();
	void SetPhysicsUpdateCount(unsigned int value);

	///<summary> The speed multiplier of the time. This value is multiplied with deltaTime. </summary>
	float GetTimeScale();
	void SetTimeScale(float value);
};

