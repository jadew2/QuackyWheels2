//
// Created by Jade White on 2016-02-17.
//

#include "TimeUpdater.h"
#include "Time.h"
#include "Timer.h"

TimeUpdater::TimeUpdater(Time *timeClassRef)
{
	timeRef = timeClassRef;
	deltaTimer = new Timer();
}

TimeUpdater::~TimeUpdater()
{
	Reset();
	delete deltaTimer;
}

void TimeUpdater::Start()
{
	deltaTimer->start();
}

void TimeUpdater::Pause()
{
	deltaTimer->pause();
}

void TimeUpdater::Resume()
{
	deltaTimer->resume();
}


void TimeUpdater::ResetPhysicsCounter()
{
	timeRef->SetPhysicsUpdateCount(0);
}

void TimeUpdater::IncrementPhysicsCounter()
{
	timeRef->SetPhysicsUpdateCount(timeRef->GetPhysicsUpdateCount() + 1);
}


void TimeUpdater::Reset()
{
	deltaTimer->reset();
}

void TimeUpdater::Update()
{
	//Update deltaTime
	float deltaTime = deltaTimer->GetSeconds();
	if (deltaTime > 3.0f)
		deltaTime = 1.0f / 60.0f;

	timeRef->SetDeltaTime(deltaTime);
	deltaTimer->reset();
	deltaTimer->start();

	//Update time
	timeRef->SetTime(timeRef->GetTime() + deltaTime);
}


