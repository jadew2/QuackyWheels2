//
// Created by Jade White on 2016-02-17.
//

#pragma once

///<summary> Class that updates the Time class. Changes happen in Update(). </summary>
class Timer;
class Time;
class TimeUpdater
{
	Timer* deltaTimer;

	Time* timeRef;

public:
	TimeUpdater(Time* timeClassRef);
	~TimeUpdater();

	///<summary> Call this to start the timers. </summary>
	void Start();

	void ResetPhysicsCounter();
	void IncrementPhysicsCounter();

	///<summary> Pauses the delta timer (expiremental) </summary>
	void Pause();

	///<summary> Resumes the delta timer (expiremental) </summary>
	void Resume();

	///<summary> Call this to stop the timers. </summary>
	void Reset();

	///<summary> Changes the values of the time class for this frame. Uses internal timers. </summary>
	void Update();
};

