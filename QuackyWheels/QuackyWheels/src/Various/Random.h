//
// Created by Jade White on 2016-03-26.
//

#pragma once

#include <stdlib.h>     /* srand, rand */
#include "../BaseTypes/Vector3.h"

class Random
{
public:
	static void setSeed(unsigned int value);
	static void setSeed(float value);

	///<summary> Generates a random normalized vector direction. </summary>
	static Vector3 randomOnUnitSphere();

	///<summary> Generates a random number from min to max inclusive. </summary>
	static float range(float min, float max);
};


