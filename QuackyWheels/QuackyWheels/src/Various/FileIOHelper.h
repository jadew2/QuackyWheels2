//
// Created by Jade White on 2016-02-24.
//

#pragma once

#include <string>

///<summary> Various methods that help you with file IO </summary>
class FileIOHelper
{
public:
	///<summary> Returns the file extension of a file in lower case. </summary>
	static std::string GetExtension(std::string filePath);

	///<summary> Returns the string in lower case. </summary>
	static std::string toLowerCase(std::string input);
};
