//
// Created by Jade White on 2016-02-11.
//

#include "Debug.h"
#include "../ComponentEntity/GameObject.h"
#include "../Renderer/RenderCollection.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Material.h"
#include "../GameLoop/EngineSystems.h"

#include "../World/World.h"
#include "../Renderer/Renderer.h"
#include "../Components/Standard/PointerDeleter.h"

using namespace std;

World* Debug::engineWorld;


void Debug::Log(std::string message)
{
	onLogMessage(Default, std::string("Log: ") + message);
}

void Debug::LogWarning(std::string message)
{
	onLogMessage(Default, std::string("Log Warning: ") + message);
}

void Debug::LogError(std::string message)
{
	onLogMessage(Default, std::string("Log Error: ") + message);
}

void Debug::onLogMessage(Debug::LogType logType, std::string message)
{
	//You can do anything extra you want with debug logs here
	//i.e. send them to display over the game, or to a file.
	printf("%s", message.c_str());
}


void Debug::DrawLine(Vector3 p0, Vector3 p1, Color color, bool ui)
{
	Mesh* lineMesh = new Mesh();
	lineMesh->verticies.push_back(p0);
	lineMesh->verticies.push_back(p1);

	GameObject* rendererGameObject = new GameObject(engineWorld, "DebugLine");
	MeshRenderer* renderer = new MeshRenderer(rendererGameObject);
	renderer->mesh = lineMesh;
	renderer->drawType = MeshRenderer::DrawType::VertexLine;
	renderer->getMaterial()->setColor("ambient",color);

	if (ui) 
		renderer->setUI(ui);

	renderer->EnableBehaviourEventCalls(true);
	renderer->SetEnabled(true);

	PointerDeleter<Mesh>* deleter = new PointerDeleter<Mesh>(rendererGameObject);
	deleter->pointers.push_back(lineMesh);

	engineWorld->getEngineSystems()->renderer->renderCollection->addTempRenderer(rendererGameObject, renderer);
}

void Debug::DrawRay(Vector3 origin, Vector3 direction, float distance, Color color)
{
	Vector3 p0 = origin;
	Vector3 p1 = origin + direction * distance;

	DrawLine(p0,p1,color);
}

