//
// Created by Jade White on 2016-03-28.
//

#include "LevelSettings.h"
#include "../../Exceptions/EngineException.h"
#include "../../AssetImport/Assets.h"

LevelSettings::LevelSettings()
{

}

LevelSettings::LevelSettings(std::vector<PlayerSettings> playerSettings, int trk, unsigned long numPlayers)
{
	this->playerSettings = playerSettings;
	this->trackNumber = trk;
	players = numPlayers;
}


unsigned long LevelSettings::getNumberOfPlayers()
{
	return players;
}

PlayerSettings LevelSettings::getPlayerSettings(int playerIndex)
{
	return playerSettings[playerIndex];
}

Texture* LevelSettings::getPlayerTexture(int playerIndex)
{
	if (playerIndex > (int)playerSettings.size() - 1 || playerIndex < 0)
		throw EngineException("LevelSettings: getPlayerTexture: player index out of range.");

	return playerSettings[playerIndex].getPlayerModelTexture();
}

int LevelSettings::getTrackNumber()
{
	return trackNumber;
}