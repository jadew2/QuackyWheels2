//
// Created by Jade White on 2016-03-28.
//

#pragma once

class Texture;
class PlayerSettings
{
public:
	enum PlayerModel
	{
		YellowDuck,
		GreenDuck,
		RedDuck,
		BlackDuck,
	};

	PlayerModel model = PlayerModel::YellowDuck;

	PlayerSettings(PlayerModel modelType);
	PlayerSettings(int modelNum);

	int getModel();
	//Also get Prefab could eventually go in here
	Texture* getPlayerModelTexture();
};

