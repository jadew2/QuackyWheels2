//
// Created by Jade White on 2016-03-28.
//

#include "PlayerSettings.h"
#include "../../AssetImport/Assets.h"


PlayerSettings::PlayerSettings(PlayerSettings::PlayerModel modelType)
{
	this->model = modelType;
}

PlayerSettings::PlayerSettings(int modelNum)
{
	if (modelNum == 0)
	{
		this->model = YellowDuck;
	}
	else if (modelNum == 1)
	{
		this->model = GreenDuck;
	}
	else if (modelNum == 2)
	{
		this->model = RedDuck;
	}
	else if (modelNum == 3)
	{
		this->model = BlackDuck;
	}
}

Texture* PlayerSettings::getPlayerModelTexture()
{
	PlayerSettings::PlayerModel type = model;
	switch (type)
	{
		case PlayerModel::YellowDuck:
			return Assets::Load<Texture>("EngineAssets/Images/Quacky.png");
		case PlayerModel::GreenDuck:
			return Assets::Load<Texture>("EngineAssets/Images/DuckyMacDuck.png");
		case PlayerModel::RedDuck:
			return Assets::Load<Texture>("EngineAssets/Images/Firequacker.png");
		case PlayerModel::BlackDuck:
			return Assets::Load<Texture>("EngineAssets/Images/FowlPlayDuck.png");
		default:
			throw EngineException("PlayerSettings: getPlayerModelTexture: Unhandled player texture type.");
	}
}

int PlayerSettings::getModel()
{
	return this->model;
}

