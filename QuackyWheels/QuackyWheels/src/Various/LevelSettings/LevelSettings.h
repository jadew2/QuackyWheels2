//
// Created by Jade White on 2016-03-28.
//

#pragma once

#include <vector>
#include "PlayerSettings.h"

class Texture;
class LevelSettings
{
	std::vector<PlayerSettings> playerSettings;
public:
	LevelSettings();
	LevelSettings(std::vector<PlayerSettings> playerSettings, int trk, unsigned long numPlayers);

	unsigned long trackNumber = 0;
	unsigned long players;
	unsigned long getNumberOfPlayers();
	PlayerSettings getPlayerSettings(int playerIndex);

	//To be replaced by get model prefab eventually
	Texture* getPlayerTexture(int playerIndex);

	int getTrackNumber();
};


