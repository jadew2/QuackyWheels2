#pragma once

#include "../AI/NavMesh.h"
#include "../AI/NavMeshNode.h"
#include "../AI/kdtree.h"

#include <queue>
#include <unordered_map>
#include <functional>
#include <iostream>
using std::priority_queue;
using std::unordered_map;

class AI {
public:
	AI();
	AI(NavMesh* playing);
	virtual ~AI();
	std::vector<NavMeshNode> getPath();

	void setNavMesh(NavMesh* playing);
	void setGoalList(std::vector<NavMeshNode> list);
	void update_pos(Vector3 newPos, bool close);
	void change_goal();
	//void change_goal(Vector3 toGoTo);
	void update(bool change);

	kdTree* nearestTree;

private:
	//I have just realized we will likely need an array of potential goal positions as well
	std::vector<NavMeshNode*> nodeRef;
	std::vector<NavMeshNode> pathRef;
	std::vector<NavMeshNode> goalList;
	//navigate;

	NavMeshNode* currPosition = nullptr;
	NavMeshNode* goalSpot;

	NavMeshNode* findClosest(Vector3 target);
	float heuristic(NavMeshNode a, NavMeshNode b);
	std::vector<NavMeshNode> a_star(NavMeshNode* start, NavMeshNode* goal);
	std::vector<NavMeshNode> createPath(NavMeshNode* start, NavMeshNode* goal,
		std::unordered_map <NavMeshNode*, NavMeshNode*> parents);
	void calculateFCost(NavMeshNode& node);
	float calculateGCost(NavMeshNode& node1, NavMeshNode& node2);

	void clearCosts(NavMeshNode* node);
};

template<typename T, typename Number = float>
struct PriorityQueue {
	typedef std::pair<Number, T> PQElement;
	std::priority_queue<PQElement, std::vector<PQElement>,
		std::greater<PQElement >> elements;

	friend bool operator<(const PQElement& p1, const PQElement& p2)
	{
		return p1.first < p2.first;
	}

	inline bool empty() const { return elements.empty(); }

	inline void put(T item, Number priority) {
		elements.emplace(priority, item);
		
	}

	inline T get() {
		T best_item = elements.top().second;
		elements.pop();
		return best_item;
	}
};

/*
Const wall_type = 1
Const player_type = 2
Const ground_type = 3
Const target_type = 4
Const walker_type = 5

Dim grid(15, 15); used to hold the world data

Global path[224]; used to store the found path
Global pathCount = 0; how many nodes are stored in path() array

Global query = 0; return value for checking if shorter path

Dim heap(224); this is the heap
Dim nodeheap(224); this corresponds to the node's position in the heap
Global heapCount = 0; this is the index into the heap

Global currentX, currentY; the current node's x/y coordinate
Global currentNode; the current node
Global targetX, targetY; the target's x/y coordinate
Global found = 0; flag for if target node is found

Type node
Field x, y; x / y coordinate of the node
Field parent.node; holds the node's parent
Field gCost; node's G Cost
Field hCost; node's H Cost
Field fCost; node's F Cost
Field state; 0 = undefined, 1 = open, 2 = closed
Field walkable; 0 = no, 1 = yes
Field number; holds the node number
End Type*/