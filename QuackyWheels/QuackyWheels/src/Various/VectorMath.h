//
// Created by Jade White on 2016-03-26.
//

#pragma once


#include "../BaseTypes/Vector3.h"

class VectorMath
{
public:
	///<summary> Returns the closest point on a line. x1 and x2 are points on the infinite line. </summary>
	///<remarks> Thanks to: Collision Detection PPS, Essential math for games. </remarks>
	static Vector3 ClosestPointOnLine(Vector3 to, Vector3 x1, Vector3 x2);

	///<summary> Returns the closest point clamped by start and end. If you need a infinite line, use closestPointOnLine(). </summary>
	///<remarks> Thanks to: Grumdog, see http://stackoverflow.com/questions/849211/shortest-distance-between-a-point-and-a-line-segment </remarks>
	static Vector3 ClosestPointOnLineSegment(Vector3 to, Vector3 lineStart, Vector3 lineEnd);

	///<summary> Same as above, just returning a value t 0f-->1f </summary>
	static float ClosestTOnLineSegment (Vector3 to, Vector3 lineStart, Vector3 lineEnd);
};


