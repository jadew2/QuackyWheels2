//
// Created by Jade White on 2016-03-24.
//

#pragma once

#include <vector>

///<summary> Helps lerp in a list of colors. </summary>
template <class T>
class ListLerper
{
public:
	///<summary> Linearly interpolates the list and returns the value at T. T must implement static Lerp(from,to,t) and the default constructor. </summary>
	T getValueForT(std::vector<T> values, float t)
	{
		//Handle some edge cases.
		if (values.size() <= 0)
			return T();
		else if (values.size() == 1)
			return values[0];

		//If we're here, we have enough colors to interpolate

		//Clamp t from 0-->1
		if (t < 0.0f)
			t = 0.0f;
		else if (t > 1.0f)
			t = 1.0f;

		//The index of the "from" of our lerp.
		int startIndex = (int)(t * (float)values.size());

		if (startIndex >= (int)(values.size() - 1))
			return values[values.size() - 1];

		T a = values[startIndex];
		T b = values[startIndex + 1];

		//Calculate the 0-->1 segment t value.
		float segmentT = (t * (float)values.size()) - (float)startIndex;
		return T::Lerp(a,b,segmentT);
	}

};


