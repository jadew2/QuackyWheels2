//
// Created by Jade White on 2016-02-24.
//

#include "FileIOHelper.h"

std::string FileIOHelper::GetExtension(std::string filePath)
{
	size_t i = filePath.rfind('.', filePath.length());
	if (i != std::string::npos)
	{
		std::string extension = toLowerCase(filePath.substr(i+1, filePath.length() - i)); //Find everything
		if (extension.rfind('/',extension.length()) != std::string::npos || extension.rfind('\\',extension.length()) != std::string::npos)
			return std::string(); //If we find a / or \ in the extension. Chances are we're including a path with ".". There is no extension to our file.
	}

	return std::string();
}

std::string FileIOHelper::toLowerCase(std::string input)
{
	for (unsigned int i=0; i < input.length(); i++)
		input[i] = (char)tolower(input[i]);
	return input;
}
