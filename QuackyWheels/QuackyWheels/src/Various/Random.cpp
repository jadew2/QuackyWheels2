//
// Created by Jade White on 2016-03-26.
//

#include "Random.h"

void Random::setSeed(unsigned int value)
{
	srand(value);
}


void Random::setSeed(float value)
{
	srand((unsigned int)(value * 1000.0f));
}


Vector3 Random::randomOnUnitSphere()
{
	float x = 0;
	float y = 0;
	float z = 0;

	while (x == 0 && y == 0 && z == 0) // Make sure we don't pass in the zero vector3.
	{
		x = range(-1.0f,1.0f);
		y = range(-1.0f,1.0f);
		z = range(-1.0f,1.0f);
	}

	return Vector3(x,y,z).Normalized();
}

float Random::range(float min, float max)
{
	return ((max-min)*((float)rand()/RAND_MAX))+min;
}


