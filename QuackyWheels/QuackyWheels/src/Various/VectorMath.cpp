//
// Created by Jade White on 2016-03-26.
//

#include "VectorMath.h"


Vector3 VectorMath::ClosestPointOnLine(Vector3 to, Vector3 x1, Vector3 x2)
{
	Vector3 direction = (x2 - x1).Normalized();
	return x1 + direction * Vector3::Dot(direction, to - x1);
}


Vector3 VectorMath::ClosestPointOnLineSegment(Vector3 to, Vector3 lineStart, Vector3 lineEnd)
{
	Vector3 line = (lineEnd - lineStart);
	float lengthSquared = line.SqrMagnitude();
	if (lengthSquared == 0.0f) //If the line points are equal, return one of them.
		return lineStart;

	float t = Vector3::Dot(to - lineStart, line)/lengthSquared;
	if (t < 0)
		return lineStart;//We are beyond the 'lineStart' end of the segment.
	else if (t > 1)
		return lineEnd;	 //We are beyond the 'lineEnd' end of the segment.
	else
		return lineStart + line * t ; //We are between the two points. Return the point on the line.
}


float VectorMath::ClosestTOnLineSegment(Vector3 to, Vector3 lineStart, Vector3 lineEnd)
{
	Vector3 line = (lineEnd - lineStart);
	float lengthSquared = line.SqrMagnitude();
	if (lengthSquared == 0.0f) //If the line points are equal, return one of them.
		return 0.0f;

	float t = Vector3::Dot(to - lineStart, line)/lengthSquared;
	if (t < 0)
		return 0;//We are beyond the 'lineStart' end of the segment.
	else if (t > 1)
		return 1;	 //We are beyond the 'lineEnd' end of the segment.
	else
		return t; //We are between the two points. Return the point on the line.
}

