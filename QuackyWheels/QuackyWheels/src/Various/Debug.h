//
// Created by Jade White on 2016-02-11.
//

#pragma once

#include <string>
#include "../Renderer/RenderCollection.h"
#include "../Renderer/Types/Color.h"
#include "../BaseTypes/Vector3.h"

class World;
///<summary>
/// Class for debug helping. e.g. Error message reporting.
/// You can redirect these error messages to a file or a on-screen log. Or use it to draw quick lines for debug purposes.
/// Pretty much a duplicate of Unity's
///</summary>
class Debug
{
private:
	static World* engineWorld;

	friend class Game;
public:
	enum LogType {Default,Warning,Error};

	///<summary> Simply logs a message. Use it for notifying when a important state is reached.  </summary>
	static void Log(std::string message);
	///<summary> Like Log, but for Warnings, situations that should be accounted for, but won't crash the game. </summary>
	static void LogWarning(std::string message);
	///<summary> Like LogWarning, but for errors. Situations that will end up halting code for a part of the game. </summary>
	static void LogError(std::string message);

	///<summary> Draws a line in 3D space for 1 frame. </summary>
	static void DrawLine(Vector3 p0, Vector3 p1, Color color = Color::White(), bool UI = false);
	///<summary> Draws a line in 3D space for 1 frame. </summary>
	static void DrawRay(Vector3 origin, Vector3 direction, float distance = 1, Color color = Color::White());
private:
	static void onLogMessage(LogType logType, std::string message);

};


