//
// Created by Jade White on 2016-01-31.
//

#pragma once

#include <stdexcept>
#include <string>
#include <iostream>


/**
 * An exception for methods that have not been implemented. Use it as a placeholder.
 */
class NotImplementedException : public std::logic_error
{
public:

	NotImplementedException(const std::string message) : logic_error(std::string("Not Implemented Exception: ") + message)
	{
		std::cerr << std::string("NotImplementedException: ") << message << std::endl;
	};
};