//
// Created by Jade White on 2016-01-31.
//

#pragma once

#include <stdexcept>
#include <string>
#include <iostream>


/**
 * <summary> A generic exception class for Engine Exceptions </summary>
 */
class EngineException : public std::runtime_error
{
public:
	EngineException(const std::string message) : runtime_error(std::string("Engine Exception: ") + message)
	{
		std::cerr << std::string("Engine Exception: ") << message << std::endl;
	}
};