//
// Created by Jade White on 2016-02-01.
//

#pragma once

#include "glm.hpp"
#include <gtc/quaternion.hpp>
#include <gtx/quaternion.hpp>
#include <gtx/matrix_interpolation.hpp>

#include <gtx/euler_angles.hpp>
#include <gtc/matrix_transform.hpp> //For Lookat
#include <foundation/PxQuat.h>
#include "../Exceptions/NotImplementedException.h"
#include "../Various/Debug.h"
#include "Vector3.h"


#include <string>

/**
 * A class that acts as a facade to GLM's quaternion, but is implicitly convertable to it.
 * (Why?) Chances are the physics system uses a different class than glm.
 * This isolates us from changes in API from GLM (to a extent).
 */
class Quaternion
{
public:
	float x,y,z,w = 0.0f;
	Quaternion();
	Quaternion(physx::PxQuat pxQuat);
	Quaternion(glm::fquat glmQuat);
	Quaternion(float x, float y, float z, float w);
	Vector3 GetEulerAngles();

	static Quaternion Identity();
	static Quaternion Euler(Vector3 eulerAngles);

	void Rotate(Vector3 eulerAngles);
	void Rotate(float angle, Vector3 axis);

	static Quaternion LookRotation(Vector3 forward, Vector3 upwards);
	static Quaternion AngleAxis(float angle, Vector3 axis);
	//static void GetAngleAxis(Quaternion quat, float& angle, Vector3& axis);

	static Quaternion Slerp(Quaternion a, Quaternion b, float t);
	static Quaternion Lerp(Quaternion a, Quaternion b, float t);

	void Set(Quaternion other);

	Quaternion Inverse();

	///<summary> Multiplies two quaternions (and adds them) </summary>
	Quaternion operator * (const Quaternion& b);

	Vector3 operator*(const Vector3& b);

	bool operator == (const Quaternion& b);
	bool operator != (const Quaternion& b);

	///<summary> Conversion for glm </summary>
	operator glm::fquat();
	///<summary> Conversion for glm </summary>
	glm::fquat quat() const;

	///<summary> Conversion for physX </summary>
	operator physx::PxQuat();
	///<summary> Conversion for physX </summary>
	physx::PxQuat pxQuat();
};

std::ostream& operator<<(std::ostream& os, const Quaternion&quaternion);

