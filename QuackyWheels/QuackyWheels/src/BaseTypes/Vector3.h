//
// Created by Jade White on 2016-01-31.
//

#pragma once

#include <math.h>
#include <iomanip>
#include <foundation/PxVec3.h>

#include "glm.hpp"
#include <gtc/constants.hpp>

#include <types.h>

#include "Vector2.h"

/**
 * A class that acts as a facade to GLM's vec3, but is implicitly convertable to it.
 * (Why?) Chances are the physics system uses a different vec3 than the renderer.
 *
 * This isolates us from changes in API from GLM (to a extent).
 */
class Vector3
{
public:
	float x, y, z = 0.0f; //Float is typically 32 bit, but it isn't garunteed.

	Vector3();
	Vector3(Vector2 vec2, float z = 0);
	Vector3(aiVector3D aiVector3D);
	Vector3(physx::PxVec3 pxVec3);
	Vector3(glm::vec3 glmVec3);
	Vector3(float x, float y, float z);

	//Some Presets
	static Vector3 One();
	static Vector3 Zero();
	static Vector3 Forward();
	static Vector3 Backward();
	static Vector3 Left();
	static Vector3 Right();
	static Vector3 Up();
	static Vector3 Down();
	
	/// <summary> The cross product between two vectors (It's also known as the determinant. How cool!) </summary>
	Vector3 Cross(Vector3 b);
	/// <summary> The cross product between two vectors (It's also known as the determinant. How cool!) </summary>
	static Vector3 Cross(Vector3 a, Vector3 b);
	
	/// <summary> Returns the dot product between two vectors. </summary>
	float Dot(Vector3 b);
	/// <summary> Returns the dot product between two vectors. </summary>
	static float Dot(Vector3 a, Vector3 b);

	static Vector3 Project(Vector3 vector, Vector3 onNormal);

	static Vector3 ProjectOnPlane(Vector3 vector, Vector3 planeNormal);

	/// <summary> Returns the length of this vector. </summary>
	float Magnitude();
	/// <summary> Returns a non squared version of magnitude. Good if you want to compare which vector is longer fast, but don't need the exact length. </summary>
	float SqrMagnitude();
	
	/// <summary> Returns a vector with length = 1, in the same direction as this vector. If the vector's length is too small, returns the zero vector.</summary>
	Vector3 Normalized();
	/// <summary> Returns a vector with length = 1, in the same direction as the passed vector. </summary>
	static Vector3 Normalize(Vector3 vec);
	
	/// <summary> Sine Lerp. Has some uses for rotation. Works very similar to Lerp. </summary>
	static Vector3 Slerp(Vector3 from, Vector3 to, float t);
	
	/// <summary>
	/// Linearly interpolates between two points using a normalized percentage.
	/// E.g. when
	/// t = 0, returns point "from"
	/// t = 1, returns point "to"
	/// t = 0.5, returns point halfway between "from" and "to"
	/// </summary>
	static Vector3 Lerp(Vector3 from, Vector3 to, float t);

	static float InverseLerp(Vector3 from, Vector3 to, Vector3 value);

	/// <summary> Returns the distance between two points.</summary>
	static float Distance(Vector3 a, Vector3 b);
	
	/// <summary> Multiplies the components of two vectors individually. </summary>
	static Vector3 Scale(Vector3 a, Vector3 b);

	static float Angle(Vector3 a, Vector3 b);

	Vector3 operator + (const Vector3& b);
	Vector3 operator += (const Vector3& b);

	Vector3 operator - (const Vector3& b);
	Vector3 operator -= (const Vector3& b);

	Vector3 operator * (float b) const;
	
	bool operator == (const Vector3& b) const;
	bool operator != (const Vector3& b) const;

	///<summary> Vec3 conversion for glm </summary>
	operator glm::vec3();
	///<summary> Vec3 conversion for glm </summary>
	glm::vec3 vec3();

	///<summary> Vector conversion for physX </summary>
	operator physx::PxVec3();
	///<summary> Vector conversion for physX </summary>
	physx::PxVec3 pxVec3();

	///<summary> Vector3D conversion for AssImp </summary>
	operator aiVector3D();
	///<summary> Vector3D conversion for AssImp </summary>
	aiVector3D aiVec3();

};

std::ostream& operator<<(std::ostream& os, const Vector3& vector3);
