//
// Created by Jade White on 2016-02-11.
//

#pragma once

#include <math.h>

#include <glm.hpp>
#include <gtc/matrix_transform.hpp>
#include <gtx/matrix_decompose.hpp>
#include <gtx/matrix_interpolation.hpp>

#include <foundation/PxMat44.h>
#include "Vector3.h"
#include "Quaternion.h"


class Matrix4x4
{
public:
	float mat[4][4];

	Matrix4x4();
	Matrix4x4(glm::fmat4x4 glmMat4x4);

	///<summary> The Zero Matrix </summary>
	static Matrix4x4 Zero();
	///<summary> The Identity Matrix </summary>
	static Matrix4x4 Identity();
	///<summary> Generates a Model Matrix for the passed pos, rot and scale </summary>
	static Matrix4x4 TRS(Vector3 position, Quaternion rotation, Vector3 scale);

	///<summary> Copies the values of the passed matrix. </summary>
	void Set(Matrix4x4 value);
	///<summary> Sets this matrix to a Model Matrix for the passed pos,rot and scale </summary>
	void SetTRS(Vector3 position, Quaternion rotation, Vector3 scale);


	///<summary> Returns a column of the matrix in a vector 3 </summary>
	Vector3 GetColumnVector3(int columnIndex);
	///<summary> Extracts the position from a TRS matrix. </summary>
	Vector3 GetPosition();
	///<summary> Extracts the rotation from a TRS matrix. </summary>
	Quaternion GetRotation();
	///<summary> Extracts the scale from a TRS matrix. </summary>
	Vector3 GetScale();

	///<summary> Returns the inverse of this matrix. </summary>
	Matrix4x4 Inverse();

	///<summary> Multiplies a Vector3 point by this matrix </summary>
	Vector3 MultiplyPoint(Vector3 point);

	///<summary> Multiplies a Vector3 direction by this matrix </summary>
	Vector3 MultiplyVector(Vector3 direction);

	bool operator == (const Matrix4x4& b);
	bool operator != (const Matrix4x4& b);

	///<summary> Multiplcation operator. Be careful! Order matters. </summary>
	Matrix4x4 operator * (const Matrix4x4& b);

	///<summary> Conversion for glm </summary>
	operator glm::fmat4x4();
	///<summary> Conversion for glm </summary>
	glm::fmat4x4 mat4x4();
	glm::fmat4x4 mat4x4() const; //Const variation

	///<summary> Conversion for physX </summary>
	operator physx::PxMat44();
	///<summary> Conversion for physX </summary>
	physx::PxMat44 pxMat44();

};

