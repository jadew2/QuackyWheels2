//
// Created by Jade White on 2016-02-01.
//

#include "Vector2.h"

Vector2::Vector2()
{
	x = 0.0f;
	y = 0.0f;
}

Vector2::Vector2(glm::vec2 vec2)
{
	this->x = vec2.x;
	this->y = vec2.y;
}


Vector2::Vector2(float x, float y)
{
	this->x = x;
	this->y = y;
}

Vector2 Vector2::Zero(){return Vector2(0.0f, 0.0f);}

Vector2 Vector2::Right(){return Vector2(1.0f, 0.0f); }

Vector2 Vector2::Left(){return Vector2(-1.0f, 0.0f);}

Vector2 Vector2::Up(){return Vector2(0.0f, 1.0f);}

Vector2 Vector2::Down(){return Vector2(0.0f, -1.0f);}

float Vector2::Cross(Vector2 b)
{
	return Vector2::Cross(*this , b);
}

float Vector2::Cross(Vector2 a, Vector2 b)
{
	return a.x * b.y - a.y * b.x;
}

float Vector2::Dot(Vector2 b)
{
	return Vector2::Dot(*this,b);
}

float Vector2::Dot(Vector2 a, Vector2 b)
{
	return a.x * b.x + a.y * b.y;
}

float Vector2::Magnitude()
{
	return sqrtf(x * x + y * y);
}

float Vector2::SqrMagnitude()
{
	return x * x + y * y;
}

Vector2 Vector2::Normalized()
{
	return Vector2::Normalize(*this);
}

Vector2 Vector2::Normalize(Vector2 vec)
{
	float magnitude = vec.Magnitude();
	if (magnitude <= 0)
	{
		//printf("Vector2: Warning: Trying to normalize a 0 vector.");
		return Vector2::Zero();
	}
	return vec * (1.0f / magnitude);
}


Vector2 Vector2::Slerp(Vector2 from, Vector2 to, float t)
{
	//First, clamp t from 0 --> 1
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	//Slerp is a weird version of Lerp
	return (to*t) + (from*(1.0f-t));
}

Vector2 Vector2::Lerp(Vector2 from, Vector2 to, float t)
{
	//First, clamp t from 0 --> 1
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	//Lerp is (to-from)*t + from
	return (to - from) * t + from;
}

float Vector2::Distance(Vector2 a, Vector2 b)
{
	return (b-a).Magnitude();
}

Vector2 Vector2::Scale(Vector2 a, Vector2 b)
{
	return Vector2(a.x * b.x, a.y * b.y);
}

Vector2 Vector2::operator + (const Vector2 &b)
{
	return Vector2(x + b.x, y + b.y);
}

Vector2 Vector2::operator - (const Vector2 &b)
{
	return Vector2(x - b.x, y - b.y);
}

Vector2 Vector2::operator * (float b)
{
	return Vector2(x * b, y * b);
}

bool Vector2::operator== (const Vector2& b)
{
	if (this == &b) //If they are both nullptr or the same instance...
		return true;
	return (x == b.x && y == b.y);
}

bool Vector2::operator != (const Vector2 &b)
{
	return !(*this == b);
}

Vector2::operator glm::vec2()
{
	return glm::vec2(x,y);
}

glm::vec2 Vector2::vec2()
{
	return glm::vec2(x,y);
}

Vector2::operator physx::PxVec2()
{
	return physx::PxVec2(x,y);
}

physx::PxVec2 Vector2::pxVec2()
{
	return physx::PxVec2(x,y);
}


std::ostream &operator<<(std::ostream &os, const Vector2 &vector2)
{
	os << std::string("Vector2: ( ")
	<< std::to_string(vector2.x) << ", "
	<< std::to_string(vector2.y) << " )";
	return os;
}


