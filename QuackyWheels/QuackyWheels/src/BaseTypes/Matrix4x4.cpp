//
// Created by Jade White on 2016-02-11.
//

#include "Matrix4x4.h"
#include "../Exceptions/EngineException.h"

Matrix4x4::Matrix4x4()
{
	for (size_t i = 0; i < 4; i++)
	{
		for (size_t j = 0; j < 4; j++)
		{
			mat[i][j] = 0.0f;
		}
	}
}

Matrix4x4::Matrix4x4(glm::fmat4x4 glmMat4x4)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			mat[i][j] = glmMat4x4[i][j];
		}
	}
}

Matrix4x4 Matrix4x4::Zero()
{
	return Matrix4x4(glm::mat4(0.0f));
}

Matrix4x4 Matrix4x4::Identity()
{
	return Matrix4x4(glm::mat4(1.0f));
}


void Matrix4x4::SetTRS(Vector3 position, Quaternion rotation, Vector3 scale)
{
	Matrix4x4 trsMatrix = Matrix4x4::TRS(position,rotation,scale);
	Set(trsMatrix);
}

Matrix4x4 Matrix4x4::TRS(Vector3 position, Quaternion rotation, Vector3 scaling)
{
	glm::quat rot = rotation.quat();
	//vec3 axis = glm::axis(rot);
	//float angle = glm::angle(rot);

	glm::fmat4x4 translate = glm::translate(glm::fmat4x4(1.0f), position.vec3()); //Position
	glm::fmat4x4 resultRot = mat4_cast(rotation.quat());//glm::rotate(fmat4x4(1.0f), angle, axis);
	glm::fmat4x4 scale = glm::scale(glm::fmat4x4(1.0f), scaling.vec3());

	//fmat4x4 rotate = 	glm::toMat4(rotation.quat()); //TODO: Check << This make -1,-1, 1 for the identity quaternion for some reason.


	return Matrix4x4(translate * resultRot * scale);// scale * rotate * translate; ///translate*rotate*scale;//scale * rotate * translate; //This needs to be reversed (mathy reasons)
}



void Matrix4x4::Set(Matrix4x4 value)
{
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			mat[i][j] = value.mat[i][j];
		}
	}
}

bool Matrix4x4::operator==(const Matrix4x4 &b)
{
	if (this == &b) //If they are both nullptr or the same instance...
		return true;

	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			if (mat[i][j] != b.mat[i][j])
				return false;
		}
	}
	return true;
}

bool Matrix4x4::operator!=(const Matrix4x4 &b)
{
	return !(*this == b);
}

Matrix4x4::operator glm::fmat4x4()
{
	return mat4x4();
}

glm::fmat4x4 Matrix4x4::mat4x4()
{
	glm::fmat4x4 glmMat = glm::fmat4x4();
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			glmMat[i][j] = mat[i][j];
		}
	}
	return glmMat;

}

glm::fmat4x4 Matrix4x4::mat4x4() const
{
	glm::fmat4x4 glmMat = glm::fmat4x4();
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			glmMat[i][j] = mat[i][j];
		}
	}
	return glmMat; 
}

Matrix4x4::operator physx::PxMat44()
{
	return pxMat44();
}

physx::PxMat44 Matrix4x4::pxMat44()
{
	physx::PxMat44 physxMat = physx::PxMat44();
	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			physxMat[i][j] = mat[i][j];
		}
	}
	return physxMat;
}


Matrix4x4 Matrix4x4::operator * (const Matrix4x4 &b)
{
	return Matrix4x4(this->mat4x4() * b.mat4x4());
}

Vector3 Matrix4x4::GetColumnVector3(int columnIndex)
{
	if (columnIndex > 3 || columnIndex < 0)
		throw new EngineException(std::string("Matrix4x4: GetColumnVector3: Column Index ") + std::to_string(columnIndex) + std::string(" is out of range!"));

	return Vector3(
			mat[columnIndex][0],
			mat[columnIndex][1],
			mat[columnIndex][2]
	);
}

Vector3 Matrix4x4::GetPosition()
{
	return GetColumnVector3(3); //TODO: Check (Edit: looks good so far)
}

Quaternion Matrix4x4::GetRotation()
{
	physx::PxTransform trans = physx::PxTransform(this->pxMat44());
	Quaternion result = trans.q;
	return result;

/*
	//Note: This does not work! PhysX does it correctly for some reason.
	vec3 scale;
	quat rot;
	vec3 trans;
	vec3 skew;
	vec4 pers;
	glm::decompose(this->mat4x4(), scale, rot, trans, skew, pers);
	return Quaternion(rot);
*/

}

//<summary> Returns 1 if value >= 0. -1 otherwise.
float sign(float value)
{
	return value >= 0.0f ? 1.0f : -1.0f;
}

Vector3 Matrix4x4::GetScale()
{
	return Vector3( //TODO: Check
			GetColumnVector3(0).Magnitude(),
			GetColumnVector3(1).Magnitude(),
			GetColumnVector3(2).Magnitude()
	);
}


//Use GLM's
Matrix4x4 Matrix4x4::Inverse()
{
	return Matrix4x4(glm::inverse(this->mat4x4()));
}

Vector3 Matrix4x4::MultiplyPoint(Vector3 point)
{
	glm::vec4 vec4Point = glm::vec4(point.vec3(),1.0f); //First make it a vec4 so we can multiply it by our matrix.
	glm::vec4 modifiedPoint = (*this).mat4x4() * vec4Point;
	return Vector3(modifiedPoint.x,modifiedPoint.y,modifiedPoint.z);
}

Vector3 Matrix4x4::MultiplyVector(Vector3 direction)
{
	//Take only the rotation of the matrix into consideration.
	Quaternion rotation = this->GetRotation();
	return rotation * direction;
}
