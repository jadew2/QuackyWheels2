//
// Created by Jade White on 2016-02-01.
//


#include "Quaternion.h"

#include <glm.hpp>

Quaternion::Quaternion()
{
	x = 0;
	y = 0;
	z = 0;
	w = 1;
}

Quaternion::Quaternion(physx::PxQuat pxQuat)
{
	x = pxQuat.x;
	y = pxQuat.y;
	z = pxQuat.z;
	w = pxQuat.w;
}

Quaternion::Quaternion(glm::fquat glmQuat)
{
	x = glmQuat.x;
	y = glmQuat.y;
	z = glmQuat.z;
	w = glmQuat.w;
}

Quaternion::Quaternion(float X, float Y, float Z, float W) 
{
	glm::fquat theQuat = glm::fquat(W, X, Y, Z);
	x = theQuat.x;
	y = theQuat.y;
	z = theQuat.z;
	w = theQuat.w;
}

Quaternion Quaternion::Identity()
{
	return Quaternion(0,0,0,1);
}

Quaternion Quaternion::Euler(Vector3 eulerAngles)
{
	Vector3 radians = eulerAngles * (glm::pi<float>() / 180.0f)/*Deg2Rad*/;
	return Quaternion(glm::quat(radians.vec3()));
}

Vector3 Quaternion::GetEulerAngles()
{
	return Vector3(glm::eulerAngles(this->quat()));
}


Quaternion Quaternion::operator*(const Quaternion &b)
{
	glm::fquat result = this->quat() * b.quat();
	//result = glm::normalize(result);

	//Hardcore matrix multiplication baby
	/* Note: This doesn't work for some reason.
	Quaternion myResult = Quaternion(
			x * b.x - y * b.y - z * b.z - w * b.w,
			x * b.y + y * b.x - z * b.w + w * b.z,
			x * b.z + y * b.w + z * b.x - w * b.y,
			x * b.w - y * b.z + z * b.y + w * b.x
	);
	*/

	return result;
}

bool Quaternion::operator==(const Quaternion &b)
{
	if (this == &b) //If they are both nullptr or the same instance...
		return true;
	return (x == b.x && y == b.y && z == b.z && w == b.w);
}

bool Quaternion::operator!=(const Quaternion &b)
{
	return !(*this == b);
}

void Quaternion::Rotate(Vector3 eulerAngles)
{
	Quaternion result = (*this) * Quaternion::Euler(eulerAngles);
	Set(result); //Apply this to ourselves
}

void Quaternion::Rotate(float angle, Vector3 axis)
{
	Quaternion result = (*this) * Quaternion::AngleAxis(angle,axis);
	Set(result); //Apply this to ourselves
}

Quaternion Quaternion::LookRotation(Vector3 forward, Vector3 upwards = Vector3::Up())
{	//Glm doesn't give this to us. It looks complex. See GLM::LookAt

	if (forward == Vector3::Zero())
	{
		Debug::LogWarning("Quaternion: LookRotation: Forward vector was zero.");
		forward = Vector3::Forward();
	}
	if (upwards == Vector3::Zero())
	{
		Debug::LogWarning("Quaternion: LookRotation: Up vector was zero.");
		upwards = Vector3::Up();
	}

	glm::mat4 rotMatrix = glm::lookAt(glm::vec3(0.0f, 0.0f, 0.0f), forward.vec3(), upwards.vec3());
	Quaternion result = (Quaternion(conjugate(quat_cast(rotMatrix))));
	return result;
}

//Use GLM's
Quaternion Quaternion::AngleAxis(float angle, Vector3 axis)
{
	float radianAngle = angle * (glm::pi<float>() / 180.0f);
	return Quaternion(glm::angleAxis(radianAngle, axis.vec3()));
}

/*
Quaternion Quaternion::GetAngleAxis(Quaternion quat, float& angle, Vector3& axis)
{
	//if (w > 1)
	//angle = 2 * glm::acos(w);
	//double s = glm::sqrt()

	return Quaternion::Identity();
}
*/

//Use GLM's
Quaternion Quaternion::Slerp(Quaternion a, Quaternion b, float t)
{
	return Quaternion(glm::slerp(a.quat(),b.quat(), t));
}

//Use GLM's
Quaternion Quaternion::Lerp(Quaternion a, Quaternion b, float t)
{
	return Quaternion(glm::lerp(a.quat(),b.quat(), t));
}

void Quaternion::Set(Quaternion other)
{
	x = other.x;
	y = other.y;
	z = other.z;
	w = other.w;
}

//Use GLM's
Quaternion Quaternion::Inverse()
{
	return Quaternion(glm::inverse(this->quat()));
}

Quaternion::operator glm::fquat()
{
	return glm::fquat(w,x,y,z);
}

glm::fquat Quaternion::quat() const
{
	return glm::fquat(w, x, y, z); //W, X, Y, Z HOLY FUUUU
}

Quaternion::operator physx::PxQuat()
{
	return physx::PxQuat(x,y,z,w);
}

physx::PxQuat Quaternion::pxQuat()
{
	return physx::PxQuat(x,y,z,w);
}

std::ostream &operator<<(std::ostream &os, const Quaternion &quaternion)
{
	os << std::string("Quat( ")
	<< std::to_string(quaternion.x) << ", "
	<< std::to_string(quaternion.y) << ", "
	<< std::to_string(quaternion.z) << ", "
	<< std::to_string(quaternion.w) <<" )";
	return os;
}

Vector3 Quaternion::operator*(const Vector3 &b)
{
	//Optimized implementation. Keep researching a replacement (I have no idea how this works)
	//Essentially, it creates a rotation matrix from the quaternion, and uses it to rotate a vector.
	float x2 = x * 2.0f;
	float y2 = y * 2.0f;
	float z2 = z * 2.0f;
	float xSqr2 = x * x2;
	float ySqr2 = y * y2;
	float zSqr2 = z * z2;
	float xy2 = x * y2;
	float xz2 = x * z2;
	float yz2 = y * z2;
	float wx2 = w * x2;
	float wy2 = w * y2;
	float wz2 = w * z2;
	Vector3 result;
	result.x = (1.0f - (ySqr2 + zSqr2)) * b.x + (xy2 - wz2) * b.y + (xz2 + wy2) * b.z;
	result.y = (xy2 + wz2) * b.x + (1.0f - (xSqr2 + zSqr2)) * b.y + (yz2 - wx2) * b.z;
	result.z = (xz2 - wy2) * b.x + (yz2 + wx2) * b.y + (1.0f - (xSqr2 + ySqr2)) * b.z;
	return result;

	/*
	//Thanks to http://gamedev.stackexchange.com/questions/28395/rotating-vector3-by-a-quaternion
	// Extract the vector part of the quaternion
	Vector3 u(x, y, z);

	// Extract the scalar part of the quaternion
	float s = w;

	// Do the math
	Vector3 val = 2.0f * Vector3::Dot(u, b) * u
			 + b * (s * s - Vector3::Dot(u, u))
			 + 2.0f * s * Vector3::Cross(u, b); //This doesn't work right!!!
	*/
}
