//
// Created by Jade White on 2016-02-01.
//

#pragma once

#include <math.h>
#include <iomanip>
#include <foundation/PxVec2.h>

#include "glm.hpp"

#include <string>

/**
 * A class that acts as a facade to GLM's vec2, but is implicitly convertable to it.
 * (Why?)
 * 1. Chances are the physics system uses a different vec3 than the renderer. And the renderer from occluder-etc.
 * 2. It makes things simpler to use for us.
 * 3. It also prevents us from including GLM in lower code like Components.
 *
 * (Downsides)
 * 1. More code to maintain
 *
 * This isolates us from changes in API from GLM (to a extent).
 */
class Vector2
{
public:
	float x, y = 0.0f;

	Vector2();
	Vector2(glm::vec2 vec2);
	Vector2(float x, float y);

	//Some Presets
	static Vector2 Zero();
	static Vector2 Right();
	static Vector2 Left();
	static Vector2 Up();
	static Vector2 Down();

	/// <summary> The cross product between two vectors (It's also known as the determinant. How cool!) </summary>
	float Cross(Vector2 b);
	/// <summary> The cross product between two vectors (It's also known as the determinant. How cool!) </summary>
	static float Cross(Vector2 a, Vector2 b);

	/// <summary> Returns the dot product between two vectors. </summary>
	float Dot(Vector2 b);
	/// <summary> Returns the dot product between two vectors. </summary>
	static float Dot(Vector2 a, Vector2 b);

	/// <summary> Returns the length of this vector. </summary>
	float Magnitude();
	/// <summary> Returns a non squared version of magnitude. Good if you want to compare which vector is longer fast, but don't need the exact length. </summary>
	float SqrMagnitude();

	/// <summary> Returns a vector with length = 1, in the same direction as this vector. </summary>
	Vector2 Normalized();
	/// <summary> Returns a vector with length = 1, in the same direction as the passed vector. If the length is 0, returns a 0 vector.</summary>
	static Vector2 Normalize(Vector2 vec);

	/// <summary> Sine Lerp. Has some uses for rotation. Works very similar to Lerp. </summary>
	static Vector2 Slerp(Vector2 from, Vector2 to, float t);

	/// <summary>
	/// Linearly interpolates between two points using a normalized percentage.
	/// E.g. when
	/// t = 0, returns point "from"
	/// t = 1, returns point "to"
	/// t = 0.5, returns point halfway between "from" and "to"
	/// </summary>
	static Vector2 Lerp(Vector2 from, Vector2 to, float t);

	/// <summary> Returns the distance between two points.</summary>
	static float Distance(Vector2 a, Vector2 b);

	/// <summary> Multiplies the components of two vectors individually. </summary>
	static Vector2 Scale(Vector2 a, Vector2 b);

	Vector2 operator + (const Vector2& b);

	Vector2 operator - (const Vector2& b);

	Vector2 operator * (float b);

	bool operator == (const Vector2& b);
	bool operator != (const Vector2& b);

	///<summary> Vector conversion for glm </summary>
	operator glm::vec2 ();
	///<summary> Vector conversion for glm </summary>
	glm::vec2 vec2();

	///<summary> Vector conversion for physX </summary>
	operator physx::PxVec2();
	///<summary> Vec3 conversion for physX </summary>
	physx::PxVec2 pxVec2();
};

std::ostream& operator<<(std::ostream& os, const Vector2& vector2);
