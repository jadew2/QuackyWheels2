//
// Created by Jade White on 2016-01-31.
//

#include "Vector3.h"

Vector3::Vector3()
{
	x = y = z = 0.0f;
}

Vector3::Vector3(aiVector3D assImpVector3)
{
	x = assImpVector3.x;
	y = assImpVector3.y;
	z = assImpVector3.z;
}


Vector3::Vector3(Vector2 vec2, float Z)
{
	x = vec2.x;
	y = vec2.y;
	z = Z;
}

Vector3::Vector3(physx::PxVec3 physxVector3)
{
	x = physxVector3.x;
	y = physxVector3.y;
	z = physxVector3.z;
}

Vector3::Vector3(glm::vec3 glmVector3)
{
	x = glmVector3.x;
	y = glmVector3.y;
	z = glmVector3.z;
}

Vector3::Vector3(float X, float Y, float Z)
{
	x = X;
	y = Y;
	z = Z;
}

Vector3 Vector3::Zero() {return Vector3(); }
Vector3 Vector3::One() { return Vector3(1.0f,1.0f,1.0f); }
Vector3 Vector3::Forward() {return Vector3(0,0,1.0f); }
Vector3 Vector3::Backward() {return Vector3(0,0,-1.0f); }
Vector3 Vector3::Left() {return Vector3(1.0f,0,0); }
Vector3 Vector3::Right() {return Vector3(-1.0f,0,0); }
Vector3 Vector3::Up() {return Vector3(0,1.0f,0); }
Vector3 Vector3::Down(){return Vector3(0,-1.0f,0);}

Vector3 Vector3::Cross(Vector3 b)
{
	return Vector3::Cross(*this,b);
}

Vector3 Vector3::Cross(Vector3 a, Vector3 b)
{
	return Vector3(
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x
	);
}

float Vector3::Dot(Vector3 b)
{
	return Vector3::Dot(*this,b);
}

float Vector3::Dot(Vector3 a, Vector3 b)
{
	return a.x * b.x + a.y * b.y + a.z * b.z;
}


Vector3 Vector3::Project(Vector3 vector, Vector3 onNormal)
{
	Vector3 normal = onNormal.Normalized(); //We don't trust userspace.
	return normal * Vector3::Dot(vector,normal);
}

Vector3 Vector3::ProjectOnPlane(Vector3 vector, Vector3 planeNormal)
{
	return vector - Project(vector,planeNormal);
}

float Vector3::Magnitude()
{
	return sqrtf(x * x + y * y + z * z);
}

float Vector3::SqrMagnitude()
{
	return (x * x) + (y * y) + (z * z);
}

Vector3 Vector3::Normalized()
{
	return Vector3::Normalize(*this);
}

Vector3 Vector3::Normalize(Vector3 vec)
{
	float magnitude = vec.Magnitude();
	if (magnitude <= 0)
	{
		printf("Vector3: Warning: Trying to normalize a 0 vector.");
		return Vector3::Zero();
	}
	return vec * (1.0f / magnitude);
}

Vector3 Vector3::Slerp(Vector3 from, Vector3 to, float t)
{
	//First, clamp t from 0 --> 1
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	//Slerp is a weird version of lerp
	return (to*t) + (from*(1.0f-t));
}

Vector3 Vector3::Lerp(Vector3 from, Vector3 to, float t)
{
	//First, clamp t from 0 --> 1
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	//lerp is (to-from)*t + from
	return (to - from) * t + from;
}

float Vector3::Distance(Vector3 a, Vector3 b)
{
	return (b-a).Magnitude();
}

Vector3 Vector3::Scale(Vector3 a, Vector3 b)
{
	return Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
}

Vector3 Vector3::operator+(const Vector3 &b)
{
	return Vector3(x + b.x, y + b.y, z + b.z);
}


Vector3 Vector3::operator+=(const Vector3 &b)
{
	x += b.x;
	y += b.y;
	z += b.z;
	return *this;
}


Vector3 Vector3::operator-(const Vector3 &b)
{
	return Vector3(x - b.x, y - b.y, z - b.z);
}

Vector3 Vector3::operator-=(const Vector3 &b)
{
	x -= b.x;
	y -= b.y;
	z -= b.z;
	return *this;
}

Vector3 Vector3::operator*(float b) const
{
	return Vector3(x * b, y * b, z * b);
}

bool Vector3::operator==(const Vector3 &b) const
{
	if (this == &b) //If they are both nullptr or the same instance...
		return true;
	return (x == b.x && y == b.y && z == b.z);
}

bool Vector3::operator!=(const Vector3 &b) const
{
	return !(*this == b);
}


Vector3::operator glm::vec3()
{
	return glm::vec3(x,y,z);
}

glm::vec3 Vector3::vec3()
{
	return glm::vec3(x,y,z);
}

Vector3::operator physx::PxVec3()
{
	return physx::PxVec3(x,y,z);
}

physx::PxVec3 Vector3::pxVec3()
{
	return physx::PxVec3(x,y,z);
}

Vector3::operator aiVector3D()
{
	return aiVector3D(x,y,z);
}

aiVector3D Vector3::aiVec3()
{
	return aiVector3D(x,y,z);
}

std::ostream &operator<<(std::ostream &os, const Vector3 &vector3)
{
	os << std::string("Vector3: ( ")
	<< std::to_string(vector3.x) << ", "
	<< std::to_string(vector3.y) << ", "
	<< std::to_string(vector3.z) << " )";
	return os;
}


float Vector3::Angle(Vector3 a, Vector3 b)
{
	float dot = Vector3::Dot(a.Normalized(),b.Normalized());

	//Clamp dot
	if (dot > 1.0f)
		dot = 1.0f;
	else if (dot < -1.0f)
		dot = -1.0f;

	return acosf(dot) * 180.0f/glm::pi<float>();
}

float Vector3::InverseLerp(Vector3 from, Vector3 to, Vector3 value)
{
	Vector3 valueDirection = value - from;
	Vector3 direction = to - from;
	float t = valueDirection.Magnitude()/direction.Magnitude();

	//Clamp the value
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;
	return t;


}


