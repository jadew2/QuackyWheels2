//
// Created by Jade White on 2016-02-28.
//

#pragma once

#include "DeferredActionQueue.h"

///<summary> Helps delete objects after scriptBehaviour Destroy() calls.</summary>
template <class T>
class Deleter : public DeferredActionQueue<T>
{
public:
	Deleter() : DeferredActionQueue<T>() {}
	virtual ~Deleter(){}

	virtual void DoAction() override
	{
		for (size_t i = 0; i < this->objects.size(); i++)
		{
			if (this->objects[i] == nullptr)
				continue;
			delete this->objects[i];
		}
		this->objects.clear();
	}
};

