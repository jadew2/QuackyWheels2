#pragma once
#include <vector>

class ScriptBehaviour;
class ScriptBehaviourEvaluator;
class ScriptCollection
{
public:
	std::vector<ScriptBehaviourEvaluator*> scriptBehaviourEvaluators = std::vector<ScriptBehaviourEvaluator*>();

public:
	virtual void AddScript(ScriptBehaviour *scriptBehaviour);
	virtual void RemoveScript(ScriptBehaviour *scriptBehaviour);

	///<summary> Makes Destroy() be called by the end of this frame, followed by the script's destruction. </summary>
	///<remarks> Don't touch until you understand how ScriptBehaviour/GameObject deletion works. </remarks>
	void SetForDestroy(ScriptBehaviour* scriptBehaviour);
};