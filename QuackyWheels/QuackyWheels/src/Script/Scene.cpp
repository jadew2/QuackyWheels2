//
// Created by Jade White on 2016-02-22.
//

#include "Scene.h"
#include "../ComponentEntity/GameObject.h"

Scene::Scene()
{

}

Scene::~Scene()
{
	//Hard delete everything in the scene.
	for (GameObject* gameObject : gameObjects)
		delete gameObject;
	gameObjects.clear();
}

void Scene::Add(GameObject *gameObjectRef)
{
	gameObjects.push_back(gameObjectRef);
}

void Scene::Remove(GameObject *gameObjectRef)
{
	for (size_t i = 0; i < gameObjects.size(); ++i)
	{
		if (gameObjects[i] == gameObjectRef)
			gameObjects.erase(gameObjects.begin() + i);
	}
}


template<class T>
T* Scene::GetComponentInScene()
{
	for (GameObject* gameObject : gameObjects)
	{
		Component* foundComponent = gameObject->GetComponent<T>();
		if (foundComponent != nullptr)
			return foundComponent;
	}
	return nullptr;
}

template<class T>
std::vector<T*> Scene::GetComponentsInScene()
{
	std::vector<T*> result = std::vector<T*>();
	for (GameObject* gameObject : gameObjects)
	{
		std::vector<T*> foundComponents = gameObject->GetComponents<T>();
		result.insert(std::end(result), std::begin(foundComponents), std::end(foundComponents)); //Append lists.
	}
	return result;
}

void Scene::SetForDeletion()
{
	for (GameObject* gameObject : gameObjects)
	{
		gameObject->SafeDelete();
	}
}

GameObject *Scene::Find(std::string name)
{
	for (GameObject* gameObject : gameObjects)
	{
		if (gameObject->Name == name)
			return gameObject;
	}
	return nullptr;
}

std::vector<GameObject *> Scene::FindWithTag(std::string tagName)
{
	std::vector<GameObject*> result = std::vector<GameObject*>();
	for (GameObject* gameObject : gameObjects)
	{
		for(std::string tag : gameObject->Tags)
		{
			if (tag == tagName)
			{
				result.push_back(gameObject);
				break;
			}
		}
	}

	return result;
}


