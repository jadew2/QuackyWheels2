//
// Created by Jade White on 2016-02-27.
//

#pragma once

#include "../ComponentEntity/GameObject.h"
#include "../World/World.h"
#include "Deleter.h"

class ScriptEvaluator;
///<summary>
/// Manages classes responsible for managing the invocation of ScriptBehaviours.
/// This includes GameObject deletion, because it is closely related.
/// </summary>
class Scripting
{
public:
	ScriptEvaluator* evaluator;
	Deleter<GameObject>* gameObjectDeleter;
	Deleter<World>* worldDeleter;

	Scripting();
	virtual ~Scripting();
};