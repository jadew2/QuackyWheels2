#include "ScriptEvaluator.h"
#include "../ComponentEntity/ScriptBehaviour.h"
#include "ScriptBehaviourEvaluator.h"
#include "ScriptCollection.h"


ScriptEvaluator::ScriptEvaluator()
{
	scriptCollection = new ScriptCollection();
}

ScriptEvaluator::~ScriptEvaluator()
{
	delete scriptCollection;
}

void ScriptEvaluator::PreUpdates()
{
	std::vector<ScriptBehaviourEvaluator*> evaluators = scriptCollection->scriptBehaviourEvaluators;
	for (size_t i = 0; i < evaluators.size(); i++)
		evaluators[i]->PreUpdates();
}

void ScriptEvaluator::PhysicsUpdates()
{
	std::vector<ScriptBehaviourEvaluator*> evaluators = scriptCollection->scriptBehaviourEvaluators;
	for (size_t i = 0; i < evaluators.size(); i++)
		evaluators[i]->PhysicsUpdates();
}

void ScriptEvaluator::Updates()
{
	//First call all the update() methods
	std::vector<ScriptBehaviourEvaluator*> evaluators = scriptCollection->scriptBehaviourEvaluators;
	for (size_t i = 0; i < evaluators.size(); i++)
		evaluators[i]->Updates();

	//Now call all the Destroy() methods.
	evaluators = scriptCollection->scriptBehaviourEvaluators;
	for (size_t i = 0; i < evaluators.size(); i++)
		evaluators[i]->DestroyUpdates();

	//Now add to our "to delete" list if the behaviour is ready this frame.
	std::vector<ScriptBehaviourEvaluator*> evaluatorsToDelete = std::vector<ScriptBehaviourEvaluator*>();
	for (size_t i = 0; i < evaluators.size(); ++i)
	{
		ScriptBehaviourEvaluator* behaviourEvaluator = evaluators[i];
		if (behaviourEvaluator->IsDestroyed())
			evaluatorsToDelete.push_back(behaviourEvaluator);
	}

	for (size_t j = 0; j < evaluatorsToDelete.size(); ++j)
	{
		//Delete the ScriptBehaviour.
		//ScriptBehaviour's destructor will remove the ScriptBehaviourEvaluator from evaluators.
		delete evaluatorsToDelete[j]->GetScriptBehaviour();
	}
}


void ScriptEvaluator::PostUpdates()
{
	std::vector<ScriptBehaviourEvaluator*> evaluators = scriptCollection->scriptBehaviourEvaluators;
	for (size_t i = 0; i < evaluators.size(); i++)
		evaluators[i]->PostUpdates();
}





