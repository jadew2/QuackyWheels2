//
// Created by Jade White on 2016-02-22.
//

#pragma once

#include <vector>

///<summary> Class that represents the World Scene. </summary>
class GameObject;
class Scene
{
private:
	///<summary> List of GameObjects in this scene. </summary>
	///<remarks> Keep it private preferably, use the search methods to get what you're looking for.</remarks>
	std::vector<GameObject*> gameObjects = std::vector<GameObject*>();
public:
	Scene();
	virtual ~Scene();

	///<summary> Adds a GameObject to this scene. </summary>
	void Add(GameObject* gameObjectRef);
	///<summary> Removes a GameObject from this scene. Does not call destroy or the destructor. </summary>
	void Remove(GameObject* gameObjectRef);

//region Scene Search Methods
	///<summary> Returns the first GameObject with the specified name in the scene. </summary>
	GameObject* Find(std::string name);
	///<summary> Returns all GameObjects which contain the specified tag. </summary>
	std::vector<GameObject*> FindWithTag(std::string tag);

	///<summary> Returns the first found component of the given type in the scene </summary>
	template <typename T>
	T* GetComponentInScene();

	///<summary> Returns all Components of the given type in the current loaded scene </summary>
	template <typename T>
	std::vector<T*> GetComponentsInScene();
//endregion

	///<summary> Sets all Gameobjects to be deleted this frame. </summary>
	void SetForDeletion();
};