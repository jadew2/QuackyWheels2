//
// Created by Jade White on 2016-02-22.
//

#pragma once


class ScriptBehaviour;
///<summary> Class responsible for evaluating ScriptBehaviours </summary>
class ScriptBehaviourEvaluator
{
private:
	enum State
	{
		New, //Just added to the collection. Awake() is the next call.
		Awakened, //Awake() was called. Start() was not. Not known if behaviour is enabled.
		Started, //Start() was called. Not known if behaviour is enabled.
		Dying, //Behavour is to be destroyed.
		Destroyed, //ScriptBehaviour is done.
	};
	State state;

	ScriptBehaviour* scriptBehaviour;

public:
	ScriptBehaviourEvaluator(ScriptBehaviour* scriptBehaviourRef);
	ScriptBehaviour* GetScriptBehaviour();

	bool IsDestroyed();

	///<summary> Calls Init(),Enable(), Start()</summary>
	void PreUpdates();

	///<summary> Calls PhysicsUpdate() </summary>
	void PhysicsUpdates();

	///<summary> Calls Update(),LateUpdate() </summary>
	void Updates();

	///<summary> Calls Disable(),And Destroy() calls() </summary>
	void DestroyUpdates();

	///<summary> Declares this script behaviour should be destroyed on the next call. </summary>
	void SetForDestroy();

	///<summary> If Destroy() was not called yet, calls it immediately. </summary>
	void DestroyImmediate();

	///<summary> Calls RenderUpdate(). </summary>
	void PostUpdates();
};