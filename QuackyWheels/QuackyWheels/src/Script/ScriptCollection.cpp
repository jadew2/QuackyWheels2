
#include "ScriptCollection.h"
#include "ScriptBehaviourEvaluator.h"

using namespace std;

void ScriptCollection::AddScript(ScriptBehaviour* scriptBehaviour)
{
	scriptBehaviourEvaluators.push_back(new ScriptBehaviourEvaluator(scriptBehaviour));
}

void ScriptCollection::RemoveScript(ScriptBehaviour* scriptBehaviour)
{
	for (size_t i = 0; i < scriptBehaviourEvaluators.size(); ++i)
	{
		if (scriptBehaviourEvaluators[i]->GetScriptBehaviour() == scriptBehaviour)
		{
			//Also help destroy the script behaviour
			//Recall the only time script behaviours remove themselves is during the destructor.
			//Call disable and destroy on them if not done already.
			scriptBehaviourEvaluators[i]->DestroyImmediate();

			//Delete and remove the evaluator.
			delete scriptBehaviourEvaluators[i];
			scriptBehaviourEvaluators.erase(scriptBehaviourEvaluators.begin() + i);
			--i; //Correct the loop.
		}
	}
}

void ScriptCollection::SetForDestroy(ScriptBehaviour* scriptBehaviour)
{
	for (size_t i = 0; i < scriptBehaviourEvaluators.size(); ++i)
	{
		if (scriptBehaviourEvaluators[i]->GetScriptBehaviour() == scriptBehaviour)
		{
			scriptBehaviourEvaluators[i]->SetForDestroy();
			return;
		}
	}
}

