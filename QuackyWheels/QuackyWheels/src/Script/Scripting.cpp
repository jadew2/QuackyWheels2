//
// Created by Jade White on 2016-02-27.
//

#include "Scripting.h"
#include "../Script/Deleter.h"
#include "../Script/ScriptEvaluator.h"

Scripting::Scripting()
{
	evaluator = new ScriptEvaluator();
	gameObjectDeleter = new Deleter<GameObject>();
	worldDeleter = new Deleter<World>();
}

Scripting::~Scripting()
{
	delete evaluator;
	delete gameObjectDeleter;
	delete worldDeleter;
}
