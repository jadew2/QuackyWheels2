//
// Created by Jade White on 2016-02-21.
//

#pragma once

#include <vector>

///<summary>
/// A base collection class for anything consisting of a collection. Good for prototyping.
///</summary>
template <class T> class Collection
{
protected:
	std::vector<T*> items = std::vector<T*>();

public:

	virtual void Add(T* scriptBehaviour)
	{
		items.push_back(scriptBehaviour);
	}

	virtual void Remove(T* scriptBehaviour)
	{
		for (size_t i = 0; i < items.size(); ++i)
		{
			if (items[i] == scriptBehaviour)
				items.erase(items.begin() + i);
		}
	}

};

