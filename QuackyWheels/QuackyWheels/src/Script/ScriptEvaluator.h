#pragma once

/// <summary>
/// The behaviour evaluator has methods for evaluatig all scripts attached to gameobjects in a scene.
/// </summary>

class ScriptCollection;
class ScriptEvaluator
{
public:
	ScriptCollection* scriptCollection;
	ScriptEvaluator();
	virtual ~ScriptEvaluator();

	///<summary> Calls Init(),Enable(), Start() </summary>
	void PreUpdates();

	///<summary> Calls PhysicsUpdate() </summary>
	void PhysicsUpdates();

	///<summary> Calls EarlyUpdate(),Update(),LateUpdate(),Disable(),Destroy() </summary>
	void Updates();

	///<summary> Calls RenderUpdate()</summary>
	void PostUpdates();

};