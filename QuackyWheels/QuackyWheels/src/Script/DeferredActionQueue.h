//
// Created by Jade White on 2016-02-28.
//

#pragma once

#include <vector>

///<summary> Helps invoke actions on a group of objects at a later time.</summary>
///<remarks> Super useful for deleting objects at the correct time, or creating a world at a later time. The possibilities are endless! </remarks>
template <class T>
class DeferredActionQueue
{
protected:
	std::vector<T*> objects = std::vector<T*>();

public:
	DeferredActionQueue(){}
	virtual ~DeferredActionQueue(){}

	///<summary> Adds the object to a list. Checks and prevents duplicates. </summary>
	void Queue(T* object)
	{
		if (object == nullptr)
			return;

		//Prevent adding duplicates.
		for (size_t i = 0; i < objects.size(); ++i)
		{
			if (objects[i] == object)
				return;
		}

		//Add the object.
		objects.push_back(object);
	}

	///<summary> Finds the matching element in the list and sets it to nullptr. </summary>
	///<remarks> This is useful if you want to "remove" a element from a list you are currently iterating over somewhere else. </remarks>
	void NullUnQueue(T* object)
	{
		if (object == nullptr)
			return;

		//Remove it from the list
		for (size_t i = 0; i < objects.size(); ++i)
		{
			if (objects[i] == object)
			{
				objects[i] = nullptr;
				return;
			}
		}
	}

	///<summary> Calls a method on all the queued objects. To be overridden. </summary>
	virtual void DoAction(){}
};


