//
// Created by Jade White on 2016-02-22.
//

#include "ScriptBehaviourEvaluator.h"
#include "../ComponentEntity/ScriptBehaviour.h"
#include "../ComponentEntity/GameObject.h"

ScriptBehaviourEvaluator::ScriptBehaviourEvaluator(ScriptBehaviour* scriptBehaviourRef)
{
	scriptBehaviour = scriptBehaviourRef;
	state = State::New;
}

///<summary> Calls Init(),Enable() </summary>
void ScriptBehaviourEvaluator::PreUpdates()
{
	if (state == State::New)
	{
		scriptBehaviour->EnableBehaviourEventCalls(false); //Prevent OnEnable()/OnDisable() calls.
		scriptBehaviour->Init();

		//Call OnEnable() if the behaviour is to be enabled.
		scriptBehaviour->EnableBehaviourEventCalls(true); //Calls to SetEnabled will now call OnEnable() and OnDisable()
		scriptBehaviour->SetEnabled(scriptBehaviour->GetWantedEnabled());

		state = State::Awakened;
	}

	if (scriptBehaviour->GetEnabled() && state == State::Awakened) //If we are enabled...
	{
		scriptBehaviour->Start();
		state = State::Started;
	}
	if (scriptBehaviour->GetEnabled())
		scriptBehaviour->EarlyUpdate();
}

void ScriptBehaviourEvaluator::PhysicsUpdates()
{
	if (scriptBehaviour->GetEnabled())
		scriptBehaviour->PhysicsUpdate();
}

///<summary> Calls Start(),EarlyUpdate(),Update(),LateUpdate(), and Destroy() </summary>
void ScriptBehaviourEvaluator::Updates()
{

	if (scriptBehaviour->GetEnabled())
		scriptBehaviour->Update();
	if (scriptBehaviour->GetEnabled())
		scriptBehaviour->AfterUpdate();	
}

///<summary> Calls Disable(),And Destroy() calls() </summary>
void ScriptBehaviourEvaluator::DestroyUpdates()
{
	if (scriptBehaviour->GetGameObject()->Name == "trailPiece")
		int i = 73829;

	if (state == State::Dying) //We were told to die this frame. Time to shut down.
	{
		scriptBehaviour->SetEnabled(false); //Disable ourselves if not already.
		scriptBehaviour->Destroy();
		scriptBehaviour->SetEnabled(false); //Force disabled.
		state = State::Destroyed;
	}
}

ScriptBehaviour* ScriptBehaviourEvaluator::GetScriptBehaviour()
{
	return scriptBehaviour;
}

void ScriptBehaviourEvaluator::SetForDestroy()
{
	if (state == State::Destroyed)
	{
		//We are already destroyed. Do nothing.
	}
	else if (state == State::New)
	{
		//Init() was never called. No need to call Destroy(). Just flag that we are destroyed.
		state = State::Destroyed;
	}
	else
		state = State::Dying; //Allow us to be destroyed in Update().
}

bool ScriptBehaviourEvaluator::IsDestroyed()
{
	return (state == State::Destroyed);
}

void ScriptBehaviourEvaluator::DestroyImmediate()
{
	if (!IsDestroyed())
	{
		scriptBehaviour->SetEnabled(false); //Disable ourselves if not already.
		scriptBehaviour->Destroy();
		scriptBehaviour->SetEnabled(false); //Force disabled.
		state = State::Destroyed;
	}
}

void ScriptBehaviourEvaluator::PostUpdates()
{
	if (scriptBehaviour->GetEnabled())
		scriptBehaviour->RenderUpdate();
}
