#include "Sound.h"
#include "../Exceptions/EngineException.h"

#include <SDL.h> 
#include <SDL_mixer.h>  
#include <string>

Sound::Sound()
{

	SDL_version compile_version;
	const SDL_version *link_version=Mix_Linked_Version();
	SDL_MIXER_VERSION(&compile_version);
	printf("compiled with SDL_mixer version: %d.%d.%d\n",
		   compile_version.major,
		   compile_version.minor,
		   compile_version.patch);
	printf("running with SDL_mixer version: %d.%d.%d\n",
		   link_version->major,
		   link_version->minor,
		   link_version->patch);


	//Initialize SDL
	if (SDL_Init(SDL_INIT_AUDIO) < 0)
	{
		throw new EngineException(std::string("SDL could not initialize! Error: ") + SDL_GetError());
	}

	//Initialize SDL_mixer
	if( Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0 )
	{
		throw new EngineException(std::string("SDL_mixer could not initialize! Error: ") + SDL_GetError());
	}

	Mix_AllocateChannels(defaultChannels);
	//Initialize the usedChannels vector.
	for (int i = 0; i < defaultChannels; ++i)
		usedChannels.push_back(false);

}

Sound::~Sound()
{
	Mix_Quit();
	SDL_Quit();
}

///<summary> Creates a channel and returns the unique integer ID for it. </summary>
int Sound::GetNewChannel()
{
	for (size_t i = 0; i < usedChannels.size(); ++i)
	{
		if (usedChannels[i] == false)
		{
			usedChannels[i] = true;
			return (int)i;
		}
	}

	//If we're here, all our channels are used! Time to add new ones.
	int newNumChannels = (int)usedChannels.size() + 1;
	usedChannels.push_back(true); //Add one new channel
	Mix_AllocateChannels((int)newNumChannels);
	return newNumChannels - 1; //Return the index of the last channel.
}

///<summary> Closes a channel and frees the integer ID so others can use it. </summary>
///<remarks> Also tries to decrease the number of channels </remarks>
void Sound::ReturnChannel(int id)
{
	usedChannels[id] = false; //Note that the channel with the id is no longer used.
	Mix_HaltChannel(id); //Stop it just in case.

	//---Now, let's free up unused channels off the right (if possible)
	int usedChannelsSize = (int)usedChannels.size();
	if (usedChannelsSize <= defaultChannels)
		return; //Don't need to free anything.

	//Search for the farthest left used channel, starting from the right.
	int i;
	for (i = usedChannelsSize-1; i >= 0; --i)
	{
		if (usedChannels[i] == true)
			break;
	}

	//If there's unused channels at the end, we can free them.
	int channelsToRemove = (usedChannelsSize-1) - i;
	if (channelsToRemove <= 0)
		return; //Nothing to do here...

	int newNumChannels = usedChannelsSize - channelsToRemove;
	if (newNumChannels < defaultChannels) //Place a lower limit on the number of channels
		newNumChannels = defaultChannels;

	//Now actually free the channels.
	usedChannels.erase(usedChannels.begin() + newNumChannels, usedChannels.end());
	Mix_AllocateChannels(newNumChannels);
}


