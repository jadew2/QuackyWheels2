//
// Created by Jade White on 2016-02-21.
//

#pragma once

//Class representing an audio file
struct Mix_Chunk;
class Audio
{
private:
	Mix_Chunk* audioData;

public:
	///<summary> The number of AudioSources uses this audio. (important for cleaning up) </summary>
	int users = 0; //
	///<summary> The SDL audio Chunk </summary>
	Mix_Chunk* getChunk();

	///<summary> Sets the volume of the Audio Chunk. This is later multiplied with the volume value of the AudioSource. </summary>
	void SetVolume(float value);

	Audio(Mix_Chunk* audioData);
	virtual ~Audio();
};


