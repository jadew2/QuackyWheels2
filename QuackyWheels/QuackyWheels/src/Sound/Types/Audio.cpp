//
// Created by Jade White on 2016-02-21.
//

#include "Audio.h"
#include <SDL_mixer.h>

Audio::Audio(Mix_Chunk* audioDataRef)
{
	audioData = audioDataRef;
}

Audio::~Audio()
{
	//Destroy the loaded audio data.
	Mix_FreeChunk(audioData);
}

Mix_Chunk *Audio::getChunk()
{
	return audioData;
}

void Audio::SetVolume(float value)
{
	//First constrain value from 0-->1
	if (value > 1.0f)
		value = 1.0f;
	else if (value < 0.0f)
		value = 0.0f;

	int SDLVolume = (int)(128.0f * value);

	//Volume is set from 0-128 (int).
	Mix_VolumeChunk(audioData,SDLVolume);
}
