#pragma once

//Using SDL, SDL_image, SDL_mixer, standard IO, and strings 
#include <vector>

class Sound
{
public:
	Sound();
	virtual ~Sound();


//region Channel Management
private:
	///<summary> The number of channels available on initialization. </summary>
	const int defaultChannels = 32;
	///<summary> A list indicating whether the channel index is used. </summary>
	///<example> usedChannels[2] == true means channel 2 is being used by a AudioSource. </example>
	std::vector<bool> usedChannels = std::vector<bool>();

public:
	///<summary> Creates a channel and returns the unique integer ID for it. </summary>
	int GetNewChannel();
	///<summary> Closes a channel and frees the integer ID so others can use it. </summary>
	void ReturnChannel(int id);
//endregion

};