/* Quacky Wheels Project by the Wretched Waffles
	By John, Jade, Marc, and David
	Main game file
*/
#include <SDL.h>
#include <stdexcept>
#include <iostream>
#include <memory>
#include "GameLoop/Game.h"

int main(int argc, char* args[]) {
	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) < 0) {
		printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
	} else {
		try {
			std::unique_ptr<Game> game(new Game());
			game->Run();
		} catch (const std::runtime_error & ex) {
			std::cerr << ex.what() << std::endl;
		}
	}

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}