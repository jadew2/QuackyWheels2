#pragma once

#include <SDL.h>

#include <glm.hpp>
#include <vec2.hpp>
#include <vector>

#include "DrivingInput.h"

class Controller {
public:
	Controller();
	~Controller();

	///<summary> Gets and stores input from the controller. </summary>
	///<returns> Returns true if the application should quit </returns>
	bool ProcessInput();

	void processJoyAxisMotion(SDL_Event e);
	void processJoyButtonDown(SDL_Event e);
	void processJoyButtonUp(SDL_Event e);
	void calculateDeadzone();
	void playerTwo(int player);
	int getIndex();
	int getPlayers();
	void reset();

	void setDrivingInput(int vehicleIndex, DrivingInput input);

	DrivingInput* getControllerInput();

	static const int MAX_NUM_CONTROLLERS = 4;

	const int LEFT_JOY_X_AXIS = 0;
	const int LEFT_JOY_Y_AXIS = 1;
	const int LEFT_TRIGGER = 2;
	const int RIGHT_TRIGGER = 5;
	const int A_BUTTON = 0;
	const int B_BUTTON = 1;
	const int X_BUTTON = 2;
	const int Y_BUTTON = 3;
	const int RIGHT_BUMPER = 5;

	//Analog joystick dead zone
	const float JOYSTICK_DEAD_ZONE = 0.25f;

	//Joystick scale factor
	const double JOYSTICK_SCALE_FACTOR = 1.0 / 32767.0;

	//Trigger scale factor
	const double TRIGGER_SCALE_FACTOR = 1.0 / 65535.0;

	std::vector <SDL_Joystick*> controllers;
	std::vector <glm::dvec2*> controllerleftJoyXY;

private:
	
	int numJoysticks = 0;
	int numHumans = 0;
	int players = 0;

	DrivingInput controllerInput[MAX_NUM_CONTROLLERS];

	bool buttonA = false;
	bool buttonB = false;
	bool buttonX = false;
	bool buttonY = false;
	bool buttonA1 = false;
	bool buttonB1 = false;
	bool buttonX1 = false;
	bool buttonY1 = false;
};
