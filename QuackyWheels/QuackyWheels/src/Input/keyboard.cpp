#include <SDL.h>

void keyboardController(SDL_Event e)
{
	//Main loop flag 
	bool quit = false;

	//While application is running 
	while (!quit)
	{
		//Handle events on queue 
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit 
			if (e.type == SDL_QUIT)
			{
				quit = true;
			}
		}

		const Uint8* currentKeyStates = SDL_GetKeyboardState(NULL);
		if (currentKeyStates[SDL_SCANCODE_W])
		{
			//accelerate code
		}
		else if (currentKeyStates[SDL_SCANCODE_S])
		{
			//break/reverse code
		}
		else if (currentKeyStates[SDL_SCANCODE_A])
		{
			//turn left code
		}
		else if (currentKeyStates[SDL_SCANCODE_D])
		{
			//turn right code
		}
		else if (currentKeyStates[SDL_SCANCODE_SPACE])
		{
			//use power-up code
		}
		else if (currentKeyStates[SDL_SCANCODE_LSHIFT])
		{
			//change camera code
		}
		else if (currentKeyStates[SDL_SCANCODE_ESCAPE])
		{
			//open menu
		}
		else if (currentKeyStates[SDL_SCANCODE_UP]) 
		{
			//up on menu
		}
		else if (currentKeyStates[SDL_SCANCODE_DOWN])
		{ 
			//down on menu
		}
		else if (currentKeyStates[SDL_SCANCODE_RETURN]) 
		{
			//confirm
		}	
	}
}