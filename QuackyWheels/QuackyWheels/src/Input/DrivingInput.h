#pragma once
struct DrivingInput
{
	double acceleration;
	double leftSteer;
	double rightSteer;
	double brake;
	double handBrake;
	bool quack;
	bool reset;
	bool sandstorm;
	bool powerup;
	bool pause;
};

