/* Comment added
*/
#include "Controller.h"
#include "../Exceptions/EngineException.h"
#include <algorithm>
#include <stdio.h>

Controller::Controller() 
{

	if (SDL_Init(SDL_INIT_JOYSTICK) < 0)
		throw new EngineException(std::string("Controller: Could not Init SDL joystick!"));

	numJoysticks = 1;//SDL_NumJoysticks();
	numHumans = numJoysticks;
	players = SDL_NumJoysticks();

	//Check for joysticks 
	if (numJoysticks > 0) {
		//Load joystick 
		for (int i = 0; i < numJoysticks; i++)
		{
			SDL_Joystick* cont = SDL_JoystickOpen(i);
			controllers.push_back(cont);
			if (controllers[i] == NULL) {
				printf("Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError());
				return;
		}
		} 
	} else {
		printf("Warning: No joysticks connected.\n");
		return;
	}
	
	for (int i = 0; i < MAX_NUM_CONTROLLERS; i++) {
		controllerInput[i].acceleration = 0.0f;
		controllerInput[i].brake = 0.0f;
		controllerInput[i].leftSteer = 0.0f;
		controllerInput[i].rightSteer = 0.0f;
		controllerInput[i].handBrake = 0.0f;
		controllerInput[i].quack = false;
		controllerInput[i].reset = false;
		controllerInput[i].sandstorm = false;
		controllerInput[i].powerup = false;
		controllerInput[i].pause = false;
	}
	for (int i = 0; i < MAX_NUM_CONTROLLERS; i++)
	{
		controllerleftJoyXY.push_back(new glm::dvec2(0.0, 0.0));
	}
}

Controller::~Controller() {
	//Close game controller
	if (controllers.size() > 0)
	{
		for (int i = 0; i < numHumans; i++)
		{
			SDL_JoystickClose(controllers[i]);
			controllers[i] = NULL;
		}
	}
}

void Controller::reset()
{
	if (controllers.size() > 1)
	{
		int conSize = controllers.size();
		for (int i = 1; i < conSize; i++)
		{
			SDL_JoystickClose(controllers[i]);
			controllers.pop_back();
		}
	}

	numJoysticks = 1;
	numHumans = numJoysticks;
	players = SDL_NumJoysticks();
}
void Controller::playerTwo(int player)
{
	//Check for joysticks 
	if (SDL_NumJoysticks() > 1) {
		//Load joystick 
		for (int i = 1; i < SDL_NumJoysticks(); i++)
		{
			SDL_Joystick* cont = SDL_JoystickOpen(i);
			controllers.push_back(cont);
			
			numHumans++;
			if (controllers[i] == NULL) {
				printf("Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError());
				return;
			}
		}
	}
	else {
		printf("Warning: No joysticks connected.\n");
		for (int i = 1; i < player; i++)
		{

			numJoysticks++;
		}
		return;
	}
	for (int i = 1; i < player; i++)
	{
		
		numJoysticks++;
	}
}

void Controller::setDrivingInput(int vehicleIndex, DrivingInput input)
{
	controllerInput[vehicleIndex] = input;
}

DrivingInput* Controller::getControllerInput() {
	/*cout << "AI Input: Accel = " << controllerInput[0].acceleration << "; Brk = " << controllerInput[0].brake << "; Hndbrk = " << controllerInput[0].handBrake
		<< "; Str = (" << controllerInput[0].leftSteer << ", " << controllerInput[0].rightSteer << ");\n";*/
	return controllerInput;
}

void Controller::processJoyAxisMotion(SDL_Event e) {


	for (int i = 0; i < numHumans; i++)
	{
		//printf("Joy axis %02x\n", e.jaxis.axis & 0xff);
		//Motion on controller 0
		if (e.jaxis.which == SDL_JoystickInstanceID(controllers[i])) {
			//left joystick X axis motion
			if (e.jaxis.axis == LEFT_JOY_X_AXIS) {
				controllerleftJoyXY[i]->x = (double)e.jaxis.value * JOYSTICK_SCALE_FACTOR;
				if (controllerleftJoyXY[i]->x > 0) {
					controllerInput[i].rightSteer = controllerleftJoyXY[i]->x;
					controllerInput[i].leftSteer = 0.0f;
					if (controllerInput[i].rightSteer > 1.0f) {
						controllerInput[i].rightSteer = 1.0f;
					}
				}
				else {
					controllerInput[i].leftSteer = -controllerleftJoyXY[i]->x;
					controllerInput[i].rightSteer = 0.0f;
					if (controllerInput[i].leftSteer > 1.0f) {
						controllerInput[i].leftSteer = 1.0f;
					}
				}
				//left joystick Y axis motion
			}
			else if (e.jaxis.axis == LEFT_JOY_Y_AXIS) {
				controllerleftJoyXY[i]->y = (double)e.jaxis.value * JOYSTICK_SCALE_FACTOR;
				//left trigger motion
			}
			else if (e.jaxis.axis == LEFT_TRIGGER) {
				controllerInput[i].brake = ((double)e.jaxis.value + 32768) * TRIGGER_SCALE_FACTOR;
				//right trigger motion
			}
			else if (e.jaxis.axis == RIGHT_TRIGGER) {
				controllerInput[i].acceleration = (((double)e.jaxis.value + 32768) * TRIGGER_SCALE_FACTOR);
			}

		}
			//printf("left X motion %f, left Y motion %f, left trigger %f, right trigger %f\n", controller0leftJoyXY->x, controller0leftJoyXY->y, controllerInput[0].brake, controllerInput[0].acceleration);
	}
}

void Controller::processJoyButtonDown(SDL_Event e) {
	for (int i = 0; i < numHumans; i++)
	{
		//printf("Button number %02x\n", e.jbutton.button & 0xff);
		//Button on controller 0
		if (e.jbutton.which == SDL_JoystickInstanceID(controllers[i])) {
			//A button pressed
			if (e.jbutton.button == A_BUTTON) {
				buttonA = true;
				controllerInput[i].handBrake = 1.0;
				//B button pressed
			}
			else if (e.jbutton.button == B_BUTTON) {
				buttonB = true;
				controllerInput[i].powerup = true;
				//Y button pressed
			}
			else if (e.jbutton.button == Y_BUTTON) {
				buttonY = true;
				controllerInput[i].sandstorm = true;
				//X button pressed
			}
			else if (e.jbutton.button == X_BUTTON) {
				buttonX = true;
				controllerInput[i].quack = true;
			}
			else if (e.jbutton.button == RIGHT_BUMPER) {
				controllerInput[i].reset = true;
			}
			//printf("A button %d, B button %d, X button %d, Y button %d\n", buttonA, buttonB, buttonX, buttonY);
			//printf("bumper %d\n", controllerInput[i].reset);
		}
		
	}
}

void Controller::processJoyButtonUp(SDL_Event e) {

	for (int i = 0; i < numHumans; i++)
	{
		//printf("Button number %02x\n", e.jbutton.button & 0xff);
		//Button on controller 0
		if (e.jbutton.which == SDL_JoystickInstanceID(controllers[i])) {
			//A button pressed
			if (e.jbutton.button == A_BUTTON) {
				buttonA = false;
				controllerInput[i].handBrake = 0.0;
				//B button pressed
			}
			else if (e.jbutton.button == B_BUTTON) {
				buttonB = false;
				controllerInput[i].powerup = false;
				//Y button pressed
			}
			else if (e.jbutton.button == Y_BUTTON) {
				buttonY = false;
				controllerInput[i].sandstorm = false;
				//X button pressed
			}
			else if (e.jbutton.button == X_BUTTON) {
				buttonX = false;
				controllerInput[i].quack = false;
			}
			else if (e.jbutton.button == RIGHT_BUMPER) {
				controllerInput[i].reset = false;
			}
			//printf("A button %d, B button %d, X button %d, Y button %d\n", buttonA, buttonB, buttonX, buttonY);
			//printf("bumper %d\n", controllerInput[i].reset);
		}
	}
}

/* Deadzone for the left stick is radial and a sharp cut-off (not gradually scaled).
	This is because we are only concerned with it being zero when the joystick is not being moved by the user
	otherwise we are only concerned with the direction not with the magnitude (joystick does not control acceleration). 
	Generally the joystick will be pushed to the outer bounds when the user is steering the car.
*/
void Controller::calculateDeadzone()
{
	if (controllerleftJoyXY.size() <= 0)
		return;

	for (int i = 0; i < numHumans; i++)
	{
		if (std::abs(controllerleftJoyXY[i]->x) < JOYSTICK_DEAD_ZONE) {
			controllerInput[i].leftSteer = 0.0;
			controllerInput[i].rightSteer = 0.0;
		} /*else {
			if (controllerInput[0].leftSteer == 0) {
				controllerInput[0].rightSteer = controllerInput[0].rightSteer * ((controllerInput[0].rightSteer - JOYSTICK_DEAD_ZONE) / (1 - JOYSTICK_DEAD_ZONE));
			} else {
				controllerInput[0].leftSteer = controllerInput[0].leftSteer * ((controllerInput[0].leftSteer - JOYSTICK_DEAD_ZONE) / (1 - JOYSTICK_DEAD_ZONE));
			}
		}*/
	
	}
}

bool Controller::ProcessInput()
{
	//Handle events on queue
	//Event handler
	SDL_Event e;

	while (SDL_PollEvent(&e) != 0) {
		//User requests quit
		if (e.type == SDL_QUIT)
			return true;
			//User moves the joystick
		else if (e.type == SDL_JOYAXISMOTION)
		{
			processJoyAxisMotion(e);
		}
		else if (e.type == SDL_JOYBUTTONDOWN)
		{
			processJoyButtonDown(e);
		}
		else if (e.type == SDL_JOYBUTTONUP)
		{
			processJoyButtonUp(e);
		}
	}

	calculateDeadzone();

	return false;
}

int Controller::getIndex()
{
	numJoysticks++;

	int index = numJoysticks - 1;
	//SDL_Joystick* cont = SDL_JoystickOpen(index);
	//controllers.push_back(cont);
	return index;
}

int Controller::getPlayers()
{
	return players;
}