//
// Created by Jade White on 2016-02-23.
//

#pragma once

#include <vector>

///<summary> A class that holds a Navigation mesh. Contains methods and properties to make search algorithms easier. Hey Marc :) </summary>
class Mesh;
class NavMeshNode;
class NavMesh
{
private:

	std::vector<NavMeshNode*> nodes = std::vector<NavMeshNode*>();

public:
	NavMesh();
	~NavMesh();

	///<summary> Fetches the navigation mesh nodes. </summary>
	std::vector<NavMeshNode*> getNodes();

public:
	///<summary> Generates the nav mesh nodes. Updates internal arrays to match the mesh. </summary>
	void Set(Mesh* mesh);
};