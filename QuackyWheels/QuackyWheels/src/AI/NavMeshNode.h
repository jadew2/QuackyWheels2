//
// Created by Jade White on 2016-02-28.
//

#pragma once

#include "../BaseTypes/Vector3.h"

#include <vector>

class NavMeshNode
{
public:
	NavMeshNode();
	NavMeshNode(Vector3 pos);

	Vector3 Position;
	std::vector<NavMeshNode*> ConnectedNodes = std::vector<NavMeshNode*>();
	void ConnectNewNode(NavMeshNode* node);
	void blockNode();
	void unblockNode();

	float gCost;
	float hCost;
	float fCost;
	int state;
	int moveable;

	void setParent(NavMeshNode* par);
	NavMeshNode* getParent(){ return parent; };

	bool operator==(const NavMeshNode &b) const;
	bool operator!=(const NavMeshNode &b) const;

	//These two references are used for kd-tree implementation
	NavMeshNode* left = NULL;
	NavMeshNode* right = NULL;

	int depth_mod;

#ifdef __APPLE__
	bool operator<(const NavMeshNode &b) const ;
#endif

private:
	NavMeshNode* parent;
	int id;
};

namespace std
{
	template <>
	struct std::hash<NavMeshNode>
	{
		size_t operator()(const NavMeshNode& n) const
		{
			return std::hash<float>()(n.Position.x) ^ (std::hash<float>()(n.Position.y));
		}
	};

/*bool operator<(const NavMeshNode& a , const NavMeshNode&b)
{
	std::hash<NavMeshNode> aHash;
	std::hash<NavMeshNode> bHash;

	return aHash(a) < bHash(b);
}*/

}

struct less_than_x
{
	inline bool operator() (const NavMeshNode* node1, const NavMeshNode* node2)
	{
		return (node1->Position.x < node2->Position.x);
	}
};
struct less_than_y
{
	inline bool operator() (const NavMeshNode* node1, const NavMeshNode* node2)
	{
		return (node1->Position.y < node2->Position.y);
	}
};
struct less_than_z
{
	inline bool operator() (const NavMeshNode* node1, const NavMeshNode* node2)
	{
		return (node1->Position.z < node2->Position.z);
	}
};

/* The following defines the portions of a NavMeshMode that are needed to implement the AI

Type node
int x, y;    //x / y coordinate of the node
NavMeshNode parent;   //holds the node's parent
int gCost;	//node's G Cost
int hCost;	//node's H Cost
int fCost;	//node's F Cost
int state;    //0 = undefined, 1 = open, 2 = closed. This is needed for A* pathfinding to increase in efficiency
int moveable; //0 = no, 1 = yes. Defines if the node can be moved through
int number;  //holds the node ID number
*/

