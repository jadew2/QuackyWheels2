//
// Created by Jade White on 2016-02-23.
//

#include "NavMesh.h"
#include "NavMeshNode.h"
#include "../Renderer/Types/Mesh.h"

NavMesh::NavMesh()
{

}


void NavMesh::Set(Mesh* mesh)
{
	//Clear the old nodes
	for (auto node : nodes)
		delete node;
	nodes.clear();

	//Generate the new nodes.
	for (size_t i = 0; i < mesh->verticies.size(); ++i)
	{
		NavMeshNode* newNode = new NavMeshNode();
		newNode->Position = mesh->verticies[i];

		nodes.push_back(newNode);
	}

	for (size_t i = 0; i < mesh->triangles.size(); i += 3)
	{
		unsigned long tri0 = mesh->triangles[i];
		unsigned long tri1 = mesh->triangles[i + 1];
		unsigned long tri2 = mesh->triangles[i + 2];

		nodes[tri0]->ConnectedNodes.push_back(nodes[tri1]); //Connect 0-1
		nodes[tri1]->ConnectedNodes.push_back(nodes[tri0]); //Connect 1-0

		nodes[tri0]->ConnectedNodes.push_back(nodes[tri2]); //Connect 0-2
		nodes[tri2]->ConnectedNodes.push_back(nodes[tri0]); //Connect 2-0

		nodes[tri1]->ConnectedNodes.push_back(nodes[tri2]); //Connect 1-2
		nodes[tri2]->ConnectedNodes.push_back(nodes[tri1]); //Connect 2-1
	}
}

std::vector<NavMeshNode*> NavMesh::getNodes()
{
	return nodes;
}

NavMesh::~NavMesh()
{
	for (auto node : nodes)
		delete node;
	nodes.clear();
}

