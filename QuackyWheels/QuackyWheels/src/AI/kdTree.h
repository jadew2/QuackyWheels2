#pragma once

#include "NavMeshNode.h"
#include <algorithm>

class NearestNode
{
private:

public:
	NearestNode();
	NearestNode(Vector3 pnt) { pnt_in = pnt; };
	~NearestNode();

	Vector3 pnt_in;
	NavMeshNode* pnt_nn;
	float min_sq = 1000000.0f;

	void update(NavMeshNode* node);

};

class kdTree
{
private:
	const int k = 3;
	NavMeshNode* root;

	NavMeshNode* findMedian(std::vector<NavMeshNode*> mesh);
	float planeDistance(NavMeshNode* node, Vector3 point);

public:
	kdTree(std::vector<NavMeshNode*> mesh);
	~kdTree();

	NavMeshNode* build_tree(std::vector<NavMeshNode*> mesh, int depth);

	NearestNode* getNearest(Vector3 node);
	NearestNode* getNearest(NearestNode* nn, bool reset_min_sq);
	void getNearest(NearestNode* nn, NavMeshNode* node);
};