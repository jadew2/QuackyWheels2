//
// Created by Jade White on 2016-02-28.
//

#include "NavMeshNode.h"

NavMeshNode::NavMeshNode()
{
	moveable = 1;
}

NavMeshNode::NavMeshNode(Vector3 pos)
{
	Position = pos;
}

void NavMeshNode::ConnectNewNode(NavMeshNode* node)
{
	ConnectedNodes.push_back(node);
}

void NavMeshNode::blockNode()
{
	moveable = 0;
}

void NavMeshNode::unblockNode()
{
	moveable = 1;
}

void NavMeshNode::setParent(NavMeshNode* par)
{
	parent = par;
}

bool NavMeshNode::operator==(const NavMeshNode &b) const
{
	if (this == &b) //If they are both nullptr or the same instance...
		return true;
	return (Position == b.Position);
}

bool NavMeshNode::operator!=(const NavMeshNode &b) const
{
	return !(*this == b);
}

#ifdef __APPLE__
bool NavMeshNode::operator<(const NavMeshNode &b) const
{
	return (Position.x < b.Position.x) && (Position.y< b.Position.y);
}
#endif