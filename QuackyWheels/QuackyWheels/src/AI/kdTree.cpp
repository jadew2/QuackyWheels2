#include "kdTree.h"


NearestNode::NearestNode()
{
}

void NearestNode::update(NavMeshNode* node)
{
	float dx = node->Position.x - pnt_in.x;
	float dy = node->Position.y - pnt_in.y;
	float dz = node->Position.z - pnt_in.z;

	float cur_sq = dx*dx + dy*dy + dz*dz;
	if (cur_sq < min_sq)
	{
		min_sq = cur_sq;
		pnt_nn = node;
	}
}

NearestNode::~NearestNode()
{
}

kdTree::kdTree(std::vector<NavMeshNode*> mesh)
{
	NavMeshNode* r = build_tree(mesh, 0);
	root = r;
}


kdTree::~kdTree()
{
}

//Constructs a kdTree from a vector of NavMeshNodes
NavMeshNode* kdTree::build_tree(std::vector<NavMeshNode*> mesh, int depth)
{
	int axis = depth % k;
	if (mesh.size() == 1)
	{
		NavMeshNode* node = mesh[0];
		node->left = NULL;
		node->right = NULL;
		node->depth_mod = axis;
		return node;
	}
	else if (mesh.size() == 0)
	{
		return NULL;
	}

	if (axis == 0)
	{
		std::sort(mesh.begin(), mesh.end(), less_than_x());
	}
	else if (axis == 1)
	{
		std::sort(mesh.begin(), mesh.end(), less_than_y());
	}
	else
	{
		std::sort(mesh.begin(), mesh.end(), less_than_z());
	}

	NavMeshNode* median = findMedian(mesh);
	size_t n = mesh.size() / 2;

	std::vector<NavMeshNode*> left_half(mesh.begin(), mesh.begin() + n);
	std::vector<NavMeshNode*> right_half(mesh.begin() + n + 1, mesh.end());

	median->left = build_tree(left_half, depth + 1);
	median->right = build_tree(right_half, depth + 1);

	median->depth_mod = axis;

	return median;
}

NavMeshNode* kdTree::findMedian(std::vector<NavMeshNode*> mesh)
{
	size_t n = mesh.size()/2;
	return mesh[n];
}

NearestNode* kdTree::getNearest(Vector3 node)
{
	NearestNode* nn = new NearestNode(node);
	getNearest(nn, root);
	return nn;
}

NearestNode* kdTree::getNearest(NearestNode* nn, bool reset_min_sq)
{
	if (reset_min_sq)
	{
		nn->min_sq = 1000000.0f;
	}
	getNearest(nn, root);
	return nn;
}

void kdTree::getNearest(NearestNode* nn, NavMeshNode* node)
{
	if (node == nullptr)
	{
		//Node is null - do nothing
	}
	else if ((node->left == nullptr) && (node->right == nullptr))
	{
		nn->update(node);
	}
	else
	{
		float dist_to_hyperplane = planeDistance(node, nn->pnt_in);

		//Check space that the point is in
		NavMeshNode* branchTraverse;
		NavMeshNode* opposite;
		if (dist_to_hyperplane < 0)
		{
			branchTraverse = node->left;
			opposite = node->right;
		}
		else
		{
			branchTraverse = node->right;
			opposite = node->left;
		}
		getNearest(nn, branchTraverse);

		//Update against current node
		nn->update(node);

		//Check other space when current distance is greater to the other space's plane
		if ((dist_to_hyperplane*dist_to_hyperplane) < nn->min_sq)
		{
			getNearest(nn, opposite);
		}
	}
}

float kdTree::planeDistance(NavMeshNode* node, Vector3 point)
{
	if (node->depth_mod == 0)
	{
		return (point.x - node->Position.x);
	}
	else if (node->depth_mod == 1)
	{
		return (point.y - node->Position.y);
	}
	else
	{
		return (point.z - node->Position.z);
	}
}