//
// Created by Jade White on 2016-04-11.
//

#include "WorldAssetTracker.h"

#include <string>
#include <vector>
#include <map>
using namespace std;

WorldAssetTracker::WorldAssetTracker()
{
	worldFilePathsDictionary = std::map<World*,ImportedFilePaths*>();
}

WorldAssetTracker::~WorldAssetTracker()
{
}

void WorldAssetTracker::addAsset(World* worldRef, std::string filePath)
{
	if(worldFilePathsDictionary.count(worldRef) == 1) //If it exists in the dictionary...
	{
		//Add it to a existing filepath list
		worldFilePathsDictionary.at(worldRef)->add(filePath);
	}
	else
	{
		//Add a new world filepath list into the dictionary
		ImportedFilePaths* importedFilePaths = new ImportedFilePaths();
		importedFilePaths->add(filePath);
		worldFilePathsDictionary.insert(std::map<World*,ImportedFilePaths*>::value_type(worldRef,importedFilePaths));
	}
}

vector<string> WorldAssetTracker::removeAssets(World* worldRef)
{
	if(worldFilePathsDictionary.count(worldRef) == 1) //If it exists in the dictionary...
	{
		ImportedFilePaths* filePathsClass = worldFilePathsDictionary.at(worldRef);
		vector<string> filePaths = filePathsClass->getFilePaths();
		worldFilePathsDictionary.erase(worldRef);
		delete filePathsClass;
		return filePaths;
	}
	else
		return vector<string>(); //Return a empty list.
}


void WorldAssetTracker::clear()
{
	worldFilePathsDictionary.clear();
}



