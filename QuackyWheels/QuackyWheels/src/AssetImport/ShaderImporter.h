//
// Created by Jade White on 2016-02-29.
//

#pragma once


#include <string>
#include "NonDuplicateImporter.h"
#include <glew.h>


///
/** <summary> Class that imports Shaders and generates shader programs for OpenGL </summary>
 * <description> Came initally from Shader Tools.
 * Shader Program Helper
	This function will attempt to create a shader program that has
	the Vertex and Fragment shader code that you pass in. If successfully
	compiled and linked to the program, the unique program ID given by
	OpenGL will be returned. This ID will be >1 if successful, or 0 (an
	invalid ID) if any of the above fails.

	Most of the Code below is for checking if the shaders compiled and
	linked correctly. </description>
*/

class Shader;
class ShaderImporter : public NonDuplicateImporter<Shader>
{
	string shaderMacros = string("Nothing");
public:
	ShaderImporter();

	///<summary> Imports and compiles a shader from filepath. </summary>
	Shader* import(std::string filepath);

private:
	std::string loadText(std::string filepath);

	///<summary> Returns whether the compile was successful. Prints error messages if not. </summary>
	bool compileOkay(GLuint shader);
	///<summary> Returns whether the shader joining was successful. Prints error messages if not. </summary>
	bool linkOkay(GLuint program);

};

