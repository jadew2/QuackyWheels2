//
// Created by Jade White on 2016-03-06.
//

#pragma once

#include <string>
#include <vector>
#include "NonDuplicateImporter.h"
#include <glew.h>

class Texture;
class TextureImporter : public NonDuplicateImporter<Texture>
{
public:
	///<summary> Imports the passed file into a Texture </summary>

	Texture* import(std::string filePath);
	
private:
	///<summary> Flips the image data over the upward axis. </summary>
	std::vector<GLubyte> flipImageY(unsigned int width, unsigned int height, int channels, std::vector<GLubyte> data);
	/// <summary>
	//  If the number of channels is < 4, resizes the data so channels are 4.
	/// Fills values with 0, sets alpha to 1 if data was resized.
	/// </summary>
	std::vector<GLubyte> fitToRGBA(unsigned int width, unsigned int height, int channels, std::vector<GLubyte> data);
};
