//
// Created by Jade White on 2016-02-28.
//

#include "ModelImporter.h"
#include "Importer.hpp"
#include "scene.h"
#include "../Exceptions/EngineException.h"
#include <postprocess.h>
#include "../Renderer/Types/AIMeshConverter.h"

#include <glew.h>

#include "Model.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Texture.h"
#include "../Renderer/Types/Material.h"


ModelImporter::ModelImporter()
{
	importer = new Assimp::Importer();
}

ModelImporter::~ModelImporter()
{
	delete importer;
}

Model* ModelImporter::import(std::string filePath)
{
	//Search for a previously imported model
	Model* previousModel = findPreviousImport(filePath);
	if (previousModel != nullptr)
		return previousModel;
	//If we are here, we have to load it from disk.
	cout << string("Assets: Loading ") << filePath << string(" ...") << endl;

	const aiScene* scene = importer->ReadFile(filePath.c_str(),
											  //aiProcess_CalcTangentSpace       |
											  aiProcess_Triangulate            |
											  aiProcess_JoinIdenticalVertices  |
											  aiProcess_GenSmoothNormals	
											  );
	// If the import failed, report it.
	if (!scene)
		throw new EngineException(std::string("ModelImporter: Could not import ") + filePath + std::string("\nReason: ") + importer->GetErrorString());

	//Convert the scene's component and generate a Model class.
	std::vector<Mesh*> meshes = generateMeshes(scene);
	std::vector<Material*> materials = generateMaterials(scene);
	std::vector<Texture*> textures = generateTextures(scene);

	Model* newModel = new Model(meshes,materials,textures);


	//Keep track of this for later
	addImported(filePath,newModel);

	return newModel;
}


std::vector<Material*> ModelImporter::generateMaterials(const aiScene* modelScene)
{
	std::vector<Material*> materials = std::vector<Material*>();
	for (unsigned int i = 0; i < modelScene->mNumMaterials; ++i)
	{
		aiMaterial* aiMaterial = modelScene->mMaterials[i];
		//TODO: Actually import materials. Lol.
		//materials.push_back(new Material(Assets::Load<Shader>));
	}
	return materials;
}

std::vector<Mesh*> ModelImporter::generateMeshes(const aiScene* modelScene)
{
	AIMeshConverter meshConverter;

	std::vector<Mesh*> meshes = std::vector<Mesh*>();
	for (unsigned int i = 0; i < modelScene->mNumMeshes; ++i)
	{
		aiMesh* aiMesh = modelScene->mMeshes[i];

		//Create a new Mesh instance
		Mesh* newMesh = new Mesh();
		meshConverter.Convert(aiMesh, newMesh); //Copy data over.

		meshes.push_back(newMesh);
	}
	return meshes;
}

std::vector<Texture*> ModelImporter::generateTextures(const aiScene* modelScene)
{
	//TextureImporter textureImporter;

	std::vector<Texture*> textures = std::vector<Texture*>();
	for (unsigned int i = 0; i < modelScene->mNumTextures; ++i)
	{
		aiTexture* aiTexture = modelScene->mTextures[i];

		//TODO: Import other texture formats.
		if (aiTexture->mHeight == 0)
		{
			//The texture is compressed (Jpeg,Png) We have to decompress it...
			//aiTexture->achFormatHint; //4 letter Format hint
			//aiTexture->mWidth; //Length of pcData
			//aiTexture->pcData; //Array of bytes to be decompressed.
			throw new EngineException(string("Texture: passed aiTexture was compressed in the ") + aiTexture->achFormatHint + "format. We can't handle this yet.");
		}
		else
		{
			//If the texture is in ARGB8888.
			unsigned int height = aiTexture->mHeight;
			unsigned int width  = aiTexture->mWidth;

			aiTexel* texels = aiTexture->pcData;

			std::vector<GLubyte> data;
			data.resize(height * width * 4 /*rgba*/);
			for (size_t j = 0; j < data.size(); ++j)
			{
				aiTexel texel = texels[j];
				//NOTE: Stores RGBA.
				data[j] = texel.r;
				data[j + 1] = texel.g;
				data[j + 2] = texel.b;
				data[j + 3] = texel.a;
			}

			//Let the texture importer take things from here.
			//Texture * newTexture = textureImporter.createTextureFromRGBA(width, height, data);
			//textures.push_back(newTexture);
		}
	}
	return textures;
}
