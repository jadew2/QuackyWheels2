//
// Created by Jade White on 2016-02-07.
//

#pragma once

#include <string>
#include <iostream>
#include <vector>
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Shader.h"
#include "../Renderer/Types/Texture.h"
#include "../Sound/Types/Audio.h"
#include "Model.h"
#include "ModelImporter.h"
#include "ShaderImporter.h"
#include "TextureImporter.h"
#include "AudioImporter.h"

#include "../Exceptions/EngineException.h"
#include "WorldAssetTracker.h"

class World;
///<summary> Class responsible for importing new things into the game </summary>
class Assets
{
private:
	static ModelImporter modelImporter;
	static ShaderImporter shaderImporter;
	static TextureImporter textureImporter;
	static AudioImporter audioImporter;

	static WorldAssetTracker worldAssetTracker;
public:
	///<summary> Loads a asset, and marks it as associated with a world. </summary>
	template <typename T>
	static T* Load(World* associatedWorld, std::string filePath)
	{
		worldAssetTracker.addAsset(associatedWorld,filePath);
		return Assets::Load<T>(filePath);
	}

	///<summary> Loads assets in from filenames. Use the type to specify what item you want. </summary>
	///<example> Mesh* newMesh = Assets::Load<Mesh>("EngineAssets/Foo.obj"); will load the first mesh in Foo.obj. </example>
	template <typename T>
	static T* Load(std::string filePath)
	{
		if (typeid(T) == typeid(Mesh))
		{
			Model* model = modelImporter.import(filePath);
			std::vector<Mesh*> meshes = model->getMeshes();

			if (meshes.size() > 0)
				return (T*)meshes[0];
			else
				throw new EngineException(std::string("Assets: Could not find a mesh in ") + filePath);
		}
		else if (typeid(T) == typeid(Texture))
		{
			return (T*)textureImporter.import(filePath);
		}
		else if (typeid(T) == typeid(Shader))
		{
			return (T*)shaderImporter.import(filePath);
		}
		else if (typeid(T) == typeid(Model))
		{
			return (T*) modelImporter.import(filePath);
		}
		else if (typeid(T) == typeid(Audio))
		{
			return (T*)audioImporter.import(filePath);
		}
		else
			throw new EngineException(std::string("Assets: Unknown Import Type. Did you type in the wrong class?"));
	}

	///<summary> Deletes assets associated with the specified world. Careful where you call this. </summary>
	static void UnLoad(World* world)
	{
		vector<string> filePaths = worldAssetTracker.removeAssets(world);
		for (size_t i = 0; i < filePaths.size(); ++i)
		{
			string filePath = filePaths[i];
			modelImporter.deleteImported(filePath);
			textureImporter.deleteImported(filePath);
			shaderImporter.deleteImported(filePath);
			audioImporter.deleteImported(filePath);
		}
	}

	///<summary> Deletes all assets. Careful where you call this. </summary>
	static void UnloadAll()
	{
		worldAssetTracker.clear();
		modelImporter.deleteAllImported();
		textureImporter.deleteAllImported();
		shaderImporter.deleteAllImported();
		audioImporter.deleteAllImported();
	}
};
