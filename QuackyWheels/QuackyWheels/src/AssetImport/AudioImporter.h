#pragma once

#include <string>
#include <vector>

#include "NonDuplicateImporter.h"


class Audio;
class AudioImporter : public NonDuplicateImporter<Audio>
{
protected:
	class ImportedAudio
	{
	public:
		std::string filePath = std::string("None");
		Audio* importedAudio = nullptr;
	};

	std::vector<ImportedAudio*> importedAudioList = std::vector<ImportedAudio*>();

public:
	AudioImporter();
	virtual ~AudioImporter();

	Audio* import(std::string filePath);
};

