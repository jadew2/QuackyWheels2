#include "AudioImporter.h"

#include "../Sound/Types/Audio.h"
#include <SDL_mixer.h>
#include <iostream>


AudioImporter::AudioImporter()
{
}


AudioImporter::~AudioImporter()
{

}

Audio* AudioImporter::import(std::string filePath)
{
	//First, return old audiosources if they already exist.
	Audio* previous = findPreviousImport(filePath);
	if (previous != nullptr)
		return previous;

	std::cout << std::string("Assets: Loading ") << filePath << std::string(" ...") << endl;

	//If we are here, we don't have any old audio files to use. Import it.
	Audio* newAudio = new Audio(Mix_LoadWAV(filePath.c_str()));

	//Add it to the list.
	addImported(filePath,newAudio);
	return newAudio;
}