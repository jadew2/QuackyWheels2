//
// Created by Jade White on 2016-04-11.
//

#pragma once

#include <string>
#include <vector>
#include <map>

class World;
///<summary> Helps track what filepaths are associated to a world. </summary>
class WorldAssetTracker
{
public:
	///<summary> Class that simply holds a list of filepaths. += Map likes this better. </summary>
	class ImportedFilePaths
	{
		std::vector<std::string> filePaths;
	public:
		ImportedFilePaths()
		{
			filePaths = std::vector<std::string>();
		}
		///<summary> Checks for doubles, and if none exist, adds the filepath. </summary>
		void add(std::string filePath)
		{
			for (size_t i = 0; i < filePaths.size(); ++i)
			{
				if (filePaths[i] == filePath)
					return;
				filePaths.push_back(filePath);
			}
		}
		///<summary> Returns a list of filepaths. </summary>
		std::vector<std::string> getFilePaths()
		{
			return filePaths;
		}
	};

	std::map<World*,ImportedFilePaths*> worldFilePathsDictionary;
public:

	WorldAssetTracker();
	virtual ~WorldAssetTracker();

	///<summary> Registers a filepath to the world. </summary>
	void addAsset(World* worldRef, std::string filePath);
	///<summary> Deletes internal lists and returns the related FilePaths to that world. </summary>
	std::vector<std::string> removeAssets(World* worldRef);

	///<summary> Wipes memory. </summary>
	void clear();
};

