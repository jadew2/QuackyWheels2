//
// Created by Jade White on 2016-02-29.
//


#include "ShaderImporter.h"
#include <iostream>
#include <vector>
#include "../Renderer/Types/Shader.h"
#include "../Exceptions/EngineException.h"
#include <fstream>
#include <stdio.h>


ShaderImporter::ShaderImporter()
{
	shaderMacros = loadText("EngineAssets/Shaders/ShaderMacros.glsl");
}


Shader* ShaderImporter::import(std::string filepath)
{
	std::string source;

	Shader* previousImport = findPreviousImport(filepath);
	if (previousImport != nullptr)
		return previousImport;
	else
	{
		//If we are here, we need to import a new shader.
		std::cout << std::string("Assets: Loading ") << filepath << std::string(" ...") << std::endl;
		source = loadText(filepath);
	}

	/* Elegant Shader loading
	 * Basically, we can combine the vertex and fragment shader into one file using macros
	 * http://www.gamedev.net/topic/651404-shaders-glsl-in-one-file-is-it-practical/
	 *
	 * Wait, hear me out. We can also define macros that are replaced by some commonly used code.
	 *
	 * For instance,
	 * - we can have code auto-inserted that handles directional, point, and spot lights.
	 * - Code auto inserted that handles shadows
	 * - Code auto inserted that does the world vertex shading (which basically every shader in our game will have)
	 *
	 * You just prepend these macro definitions to the source string, and pass it to the openGL shader compiler. It's that simple.
	 */
	//Alright, first compile the vertex shader

	//First, create the programs and shader ID.
	GLuint program = glCreateProgram();
	GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
	GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

	if(program == 0 || vertexShader == 0 || fragmentShader == 0 )
	{
		// Clean up others that were created
		glDeleteProgram(program);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		throw new EngineException(std::string("ShaderImporter: Could not create program or shader objects."));
	}

	const char* version = "#version 410 core\n";
	const char* requiredExtension = "" ; //"#extension GL_ARB_explicit_attrib_location : require\n #extension GL_ARB_explicit_uniform_location : require\n";

	//Alright, now set the source for each of the shaders. (Send in the source file string)
	//https://www.opengl.org/sdk/docs/man4/xhtml/glShaderSource.xml
	//glShaderSource() expects char**, so we need c_str().
	const char * sourceCStr = source.c_str();
	const char * macrosCStr = shaderMacros.c_str();

	const char* vertexSources[5] = {version,requiredExtension,"#define Vertex_Shader\n", sourceCStr, macrosCStr}; //Define the vertex shader Macro.
	glShaderSource(vertexShader, 5, vertexSources, NULL );

	const char* fragmentSources[5] = {version,requiredExtension,"#define Fragment_Shader\n", sourceCStr, macrosCStr}; //Define the fragment shader Macro.
	glShaderSource(fragmentShader, 5, fragmentSources, NULL );

	//Now compile the shaders
	glCompileShader(vertexShader);
	glCompileShader(fragmentShader);

	if(! (compileOkay(vertexShader) && compileOkay(fragmentShader)))
	{
		glDeleteProgram(program);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		throw new EngineException(std::string("ShaderImporter: Could not compile shader :("));
	}

	//Now link the shaders together to make a program
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

	if(! linkOkay(program))
	{
		glDeleteProgram(program);
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);

		throw new EngineException(std::string("ShaderImporter: Could not link shader :("));
	}

	//If we're here, the shader is all Compiled :)
	//Don't need these anymore.
	//glDeleteShader(vertexShader); //TODO: Check if this is okay.
	//glDeleteShader(fragmentShader);

	//Generate the Shader object
	Shader* newShader = new Shader(program,filepath,source);

	//Keep track of it for later.
	if (previousImport == nullptr)
		addImported(filepath,newShader);
	return newShader;

}

std::string ShaderImporter::loadText(std::string filePath)
{
	// Returns Empty String if can't load from file
	std::string shaderCode;
	std::ifstream fileStream( filePath.c_str(), std::ios::in );
	if( fileStream.is_open() )
	{
		std::string line;
		while( std::getline( fileStream, line ) )
			shaderCode += "\n" + line;
		fileStream.close();
	}
	else
		throw new EngineException(std::string("ShaderImporter: Could not open shader: ") + filePath);

	return shaderCode;
}

bool ShaderImporter::compileOkay(GLuint shader)
{
	GLint result;
	int infoLogLength;
	glGetShaderiv( shader, GL_COMPILE_STATUS, &result );
	if( !result )
	{
		glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &infoLogLength );
		std::vector<char> errorMsg( infoLogLength );
		glGetShaderInfoLog(	shader,
							   infoLogLength,
							   NULL,
							   errorMsg.data() );
		std::cerr << std::string("ShaderImporter: Compile Error: ") << errorMsg.data() << std::endl;
		return false;
	}
	return true;
}

bool ShaderImporter::linkOkay(GLuint program)
{
	GLint result;
	int infoLogLength;
	glGetProgramiv( program, GL_LINK_STATUS, &result );
	if( !result )
	{
		glGetProgramiv( program, GL_INFO_LOG_LENGTH, &infoLogLength );
		std::vector<char> errorMsg( infoLogLength );
		glGetProgramInfoLog(	program,
								infoLogLength,
								NULL,
								errorMsg.data() );
		std::cerr << std::string("ShaderImporter: Link Error: ") << errorMsg.data() << std::endl;
		return false;
	}
	return true;
}
