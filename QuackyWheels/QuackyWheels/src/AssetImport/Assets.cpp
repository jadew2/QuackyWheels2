//
// Created by Jade White on 2016-02-07.
//

#include "Assets.h"

//Static instances of importer classes.
ModelImporter Assets::modelImporter;
ShaderImporter Assets::shaderImporter;
TextureImporter Assets::textureImporter;
AudioImporter Assets::audioImporter;

WorldAssetTracker Assets::worldAssetTracker;

