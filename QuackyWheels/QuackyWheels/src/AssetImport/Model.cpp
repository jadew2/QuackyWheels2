//
// Created by Jade White on 2016-02-28.
//

#include "Model.h"
#include "scene.h"
#include "../Renderer/Types/Material.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Texture.h"

Model::Model()
{

}

Model::Model(std::vector<Mesh*> meshesRef, std::vector<Material*> materialsRef, std::vector<Texture*> texturesRef) : Model()
{
	materials = materialsRef;
	meshes = meshesRef;
	textures = texturesRef;
}

Model::~Model()
{

}

std::vector<Mesh*> Model::getMeshes() const
{
	return meshes;
}

std::vector<Material*> Model::getMaterials() const
{
	return materials;
}

std::vector<Texture*> Model::getTextures() const
{
	return textures;
}


void Model::Deform(Matrix4x4 trsMatrix)
{
	for (size_t i = 0; i < meshes.size(); ++i)
		meshes[i]->Deform(trsMatrix);
}

///<summary> Clones all info on this model onto the other model. </summary>
void Model::CloneTo(Model* otherModel)
{
	otherModel->meshes.clear();
	otherModel->materials.clear();
	otherModel->textures.clear();

	for (size_t i = 0; i < meshes.size(); i++)
		otherModel->meshes.push_back(meshes[i]->Clone());
}
///<summary> Duplicates this mesh </summary>
Model* Model::Clone()
{
	Model* newModel = new Model();
	CloneTo(newModel);
	return newModel;
}

std::ostream &operator<<(std::ostream &os, Model &model)
{
	std::vector<Mesh*> meshes = model.getMeshes();
	os << std::string("Model:")
	<< "\n   Meshes: " << std::to_string(meshes.size());
	for (size_t i = 0; i < meshes.size(); ++i)
	{
		os << std::string("Index: ") + std::to_string(i) << std::endl;
		os << meshes[i];
	}

	os << "\n   Materials: " << std::to_string(model.getMaterials().size())
	<< "\n   Textures: " << std::to_string(model.getTextures().size()) << std::endl;
	return os;
}
