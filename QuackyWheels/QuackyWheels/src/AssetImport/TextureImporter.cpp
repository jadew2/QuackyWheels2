//
// Created by Jade White on 2016-03-06.
//

#include "TextureImporter.h"
#include "../Renderer/Types/Texture.h"
#include "SOIL.h"

#include "../Exceptions/EngineException.h"
#include <iostream>

///<summary> Imports a image from a filepath.
/// With SOIL: Supports
/// BMP - non-1bpp, non-RLE (from stb_image documentation)
/// PNG - non-interlaced (from stb_image documentation)
/// JPG - JPEG baseline (from stb_image documentation)
/// TGA - greyscale or RGB or RGBA or indexed, uncompressed or RLE
/// DDS - DXT1/2/3/4/5, uncompressed, cubemaps (can't read 3D DDS files yet)
/// PSD - (from stb_image documentation)
/// HDR - converted to LDR, unless loaded with *HDR* functions (RGBE or RGBdivA or RGBdivA2)
///</summary>

Texture* TextureImporter::import(std::string filePath)
{
	//Return a previously imported texture
	Texture* previousTexture = findPreviousImport(filePath);
	if (previousTexture != nullptr)
		return previousTexture;
	//If we are here, there was no old texture to use :(, time to import a new one.
	std::cout << std::string("Assets: Loading ") << filePath << std::string(" ...") << std::endl;

	//Load the image data
	const char* imagepath = filePath.c_str();

	int width = 0;
	int height = 0;
	int channels = 0; //Should be 4 because we force RGBA. Note: Despite forcing to to be RGBA, it sometimes only imports RGB. We fix this later.
	unsigned char* data = SOIL_load_image(imagepath,&width,&height,&channels, SOIL_LOAD_RGBA);
	//Error out if it fails.
	if (data == nullptr)
		throw new EngineException(string("TextureImporter: Could not import texture: ") + SOIL_last_result());

	unsigned int uWidth = (unsigned int)width;
	unsigned int uHeight = (unsigned int)height;

	//Now copy the byte data into a vector
	size_t dataLength = uWidth * uHeight * channels;

	std::vector<unsigned char> vectorData;
	vectorData.resize(dataLength);
	for (size_t i = 0; i < dataLength; ++i)
		vectorData[i] = data[i];

	//Correct the imported data to be RGBA, and flipped (because SOIL has annoying bugs like these)
	vectorData = flipImageY(uWidth,uHeight,channels,vectorData);
	vectorData = fitToRGBA(uWidth,uHeight,channels,vectorData);

	//Create the new texture
	Texture* newTexture = new Texture(uWidth,uHeight,vectorData);

	//Let us use this texture later if we try to re-import it.
	addImported(filePath,newTexture);

	return newTexture;
}

std::vector<GLubyte> TextureImporter::flipImageY(unsigned int width, unsigned int height, int channels, std::vector<GLubyte> data)
{
	//Super unreadable. But basically, for each row, swap the color in the row on the other side of the texture.

	//For half the rows...
	size_t halfHeight = height / 2;
	for (size_t i = 0; i < halfHeight; i++)
	{
		unsigned long imageByteLength = width * channels;

		size_t topIndex = i * imageByteLength; //Index of the start of the top row
		size_t bottomIndex = (height - i - 1) * imageByteLength; //Index of the start of the bottom row.

		//For each element in the row...
		for (size_t j = 0; j < imageByteLength; j++)
		{
			//Swap the two values
			GLubyte temp = data[topIndex + j];
			data[topIndex + j] = data[bottomIndex + j];
			data[bottomIndex + j] = temp;
		}
	}
	return data;
}
std::vector<GLubyte> TextureImporter::fitToRGBA(unsigned int width, unsigned int height, int channels, std::vector<GLubyte> data)
{
	if (channels == 4)
		return data;
	if (channels > 4)
		throw new EngineException(std::string("TextureImporter: fitToRGBA: ") + std::to_string(channels) + " is too many channels! Cannot fit to RGBA safely.");

	size_t pixelLength = width * height;
	size_t dataLength = pixelLength * 4;

	std::vector<GLubyte> newData = std::vector<GLubyte>();
	newData.resize(dataLength,0);

	for (size_t i = 0; i < pixelLength; ++i)
	{
		size_t oldDataIndex = i * channels;
		size_t dataIndex = i * 4;
		//Copy over the existing colors
		for (int j = 0; j < channels; ++j)
			newData[dataIndex+j] = data[oldDataIndex+j];
	}

	//Set the last element, Alpha, to 255
	if (channels < 4)
	{
		for (size_t i = 3; i < dataLength; i += 4)
			newData[i] = 255;
	}

	return newData;
}