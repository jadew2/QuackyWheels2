//
// Created by Jade White on 2016-03-15.
//

#pragma once

#include <string>
#include <vector>
#include <map>
using namespace std;

///<summary> Base class for importer classes. Prevents loading the same object from disk twice by maintaining a list of previously imported objects. </summary>
template <class T>
class NonDuplicateImporter
{
protected:
	///<summary> Class to hold references to imported objects. </summary>
	class ImportedObject
	{
	public:
		ImportedObject()
		{
			object = nullptr;
		}
		virtual ~ImportedObject()
		{
			if (object != nullptr)
				delete object;
		}

		string filePath = string("None");
		T* object = nullptr;
	};

private:
	///<summary> Dictionary of imported objects. </summary>
	map<string,ImportedObject*> importedDictionary = map<string,ImportedObject*>();
public:
	NonDuplicateImporter(){}
	virtual ~NonDuplicateImporter()
	{
		deleteAllImported();
	}

protected:
	///<summary> Returns a previously imported instance of the object if it exists. Returns nullptr if it doesn't. </summary>
	T* findPreviousImport(string filePath)
	{
		if(importedDictionary.count(filePath) == 1) //If it exists in the dictionary...
			return importedDictionary.at(filePath)->object; //Return it.
		return nullptr;
	}

	///<summary> Registers this object so it can be returned again later. </summary>
	void addImported(string filePath, T* objectRef)
	{
		ImportedObject* importedObject = new ImportedObject();
		importedObject->filePath = filePath;
		importedObject->object = objectRef;

		importedDictionary.insert(std::pair<string,ImportedObject*>(filePath,importedObject));
	}

public:

	///<summary>
	// Deletes the object imported from the filepath.
	// Be careful! The loaded asset pointer will be garbage.
	// </summary>
	void deleteImported(string filePath)
	{
		if(importedDictionary.count(filePath) == 1) //If it exists in the dictionary...
		{
			delete importedDictionary.at(filePath);
			importedDictionary.erase(filePath);
		}
	}

	///<summary> Deletes all imported objects, including their contents. </summary>
	void deleteAllImported()
	{
		for(auto const &entry : importedDictionary)
			delete entry.second; //Delete ImportedObject class.

		importedDictionary.clear();
	}
};

