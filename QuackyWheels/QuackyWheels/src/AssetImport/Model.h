//
// Created by Jade White on 2016-02-28.
//


#pragma once

#include <vector>
#include "../BaseTypes/Matrix4x4.h"
///<summary>
/// Class for holding model information from assimp. (meshes, texture, material information etc. (+ Transform hierarchy?)
/// This class essentially generates our version of the needed things from Assimp, and stores it for your use.
/// </summary>

class Mesh;
class Material;
class Texture;
class Model
{
	std::vector<Mesh*> meshes;
	std::vector<Material*> materials;
	std::vector<Texture*> textures;

public:
	Model();
	Model(std::vector<Mesh*> meshes, std::vector<Material*> materials, std::vector<Texture*> textures);
	virtual ~Model();

	std::vector<Mesh*> getMeshes() const;
	std::vector<Material*> getMaterials() const;
	std::vector<Texture*> getTextures() const;

	///<summary> Deforms all the meshes in the model by the matrix permanently. </summary>
	void Deform(Matrix4x4 trsMatrix);

	///<summary> Clones all info on this model onto the other model. </summary>
	void CloneTo(Model* otherModel);
	///<summary> Duplicates this mesh </summary>
	Model* Clone();
};

std::ostream& operator<<(std::ostream& os, Model& mesh);
