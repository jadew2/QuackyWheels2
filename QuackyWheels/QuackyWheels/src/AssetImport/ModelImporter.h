//
// Created by Jade White on 2016-02-28.
//

#pragma once

#include <string>
#include <vector>
#include "NonDuplicateImporter.h"

//<summary> Class for importing converting the models from AssImp into the Model class. </summary>

//<summary> Class for importing models from AssImp </summary>
namespace Assimp {
	class Importer;
}
class Model;
class Mesh;
class Texture;
class Material;
struct aiScene;
class ModelImporter : public NonDuplicateImporter<Model>
{
private:
	Assimp::Importer* importer = nullptr;

public:
	ModelImporter();
	virtual ~ModelImporter();

	Model* import(std::string filePath);

private:
	//Extracts the models from the aiScene and converts them to Mesh classes.
	std::vector<Mesh*> generateMeshes(const aiScene* modelScene);
	//Extracts the materials from the aiScene and converts them to Material classes.
	std::vector<Material*> generateMaterials(const aiScene* modelScene);
	//Extracts the Textures from the aiScene and converts them to Texture classes.
	std::vector<Texture*> generateTextures(const aiScene* modelScene);
};

