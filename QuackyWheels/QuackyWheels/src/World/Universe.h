//
// Created by Jade White on 2016-02-27.
//

#pragma once

#include <vector>

#include "World.h"

class EngineSystems;
class GameObject;
///<summary> Maintains a list of worlds currently loaded </summary>
class Universe {
	std::vector<World*> worlds;

	EngineSystems* engineSystems;
public:
	Universe(EngineSystems* engineSystems);
	virtual ~Universe();

	///<summary> Adds a world to the list. Called by World. Do not call. </summary>
	void addWorld(World* world);
	///<summary> Adds a world to the list. Called by World. Do not call. </summary>
	void removeWorld(World* world);

	///<summary> Returns the world currently loaded at a index. </summary>
	World* getWorld(int index);
	///<summary> Returns the number of worlds currently loaded. </summary>
	int getNumberOfWorlds();

	EngineSystems* getEngineSystems();

	GameObject* find(std::string name);

	///<summary> Finds a component of the specified type. </summary>
	///<remarks> The type must extend Component </remarks>
	///<returns> Returns the found Component. Otherwise returns null </returns>
	template <typename T>
	T* GetComponentInUniverse()
	{	
		for (World* world : worlds) 
		{
			T* foundComponent = world->GetComponentInWorld<T>();
			if (foundComponent != nullptr)
				return foundComponent;
		}
		return nullptr;
	}

	///<summary> Finds components of the specified type. </summary>
	///<remarks> The type must extend Component. </remarks>
	///<returns> Returns the found Components. Otherwise returns a empty vector. </returns>
	template <typename T>
	std::vector<T*> GetComponentsInUniverse()
	{	
		std::vector<T*> foundComponents = std::vector<T*>();
		for (World* world : worlds)
		{
			std::vector<T*> worldComponents = world->GetComponentsInWorld<T>();
			foundComponents.insert(foundComponents.end(), worldComponents.begin(), worldComponents.end);
		}
		return foundComponents;
	}

	///<summary> Safe deletes all worlds. </summary>
	void safeDelete();
};
