
#pragma once

#include "World.h"
#include "../Various/LevelSettings/LevelSettings.h"

class Universe;
class TestWorld : public World {

public:
	TestWorld(Universe* universe);
	virtual ~TestWorld();
};

