#include "IntroWorld.h"
#include "../Prefabs/Menu/LoadingScreen.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Transform.h"
#include "../Components/Standard/TextureSlider.h"
#include "../Components/Camera.h"
#include "../Components/Standard/BasicRotate.h"
#include "../World/World0.h"
#include "../Components/Animators/EnabledAnimator.h"
#include "../Components/Intro/RendererPulsator.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Components/MainMenuLoader.h"

IntroWorld::IntroWorld(Universe* universeRef) : World(universeRef)
{
	GameObject* cameraGO = new GameObject(this, "Intro Camera");
	Camera* camera = new Camera(cameraGO);

	//The Animator component.
	GameObject* animatorGO = new GameObject(this, "EnabledAnimator");
	EnabledAnimator* animator = new EnabledAnimator(animatorGO);

	GameObject* skybox = new GameObject(this, "SkyBox");
	{
		MeshRenderer* skyboxRenderer = new MeshRenderer(skybox);
		skyboxRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/SkyBox.obj");
		skyboxRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Skybox.bmp"));
		skyboxRenderer->getMaterial()->setColor("tint", Color::Black());

		MaterialPropertyAnimator<Color>* materialPropertyAnimator = new MaterialPropertyAnimator<Color>(skybox,skyboxRenderer->getMaterial(),"tint");
		materialPropertyAnimator->SetEnabled(false);//Start disabled.
		materialPropertyAnimator->values = vector<Color>{Color::Black(),Color::White()};
		materialPropertyAnimator->duration = 1.0f;

		animator->AddKeyframe(4.0f,FLT_MAX,materialPropertyAnimator);
	}

	//The checker texture over the background
	GameObject* screenCheckers = new GameObject(this, "ScreenCheckers");
	{
		MeshRenderer* screenCheckerRenderer = new MeshRenderer(screenCheckers);
		screenCheckerRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		screenCheckerRenderer->getMaterial()->setTexture(Assets::Load<Texture>(this,"EngineAssets/Images/CheckerFlag.png"));
		screenCheckerRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
		screenCheckerRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		screenCheckerRenderer->getMaterial()->setColor("tint", Color::BlackInvisible()); //Multiplies with the texture, I use it to tone down the alpha from 1.
		screenCheckerRenderer->getMaterial()->setVector2("mainTex0Scale", Vector2(0.7f, 0.7f)); //Change this to change the rendered scale of the checker texture
		screenCheckerRenderer->setUI(true);
		screenCheckers->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 0.9f));

		MaterialPropertyAnimator<Color>* materialPropertyAnimator = new MaterialPropertyAnimator<Color>(screenCheckers, screenCheckerRenderer->getMaterial(), "tint");
		materialPropertyAnimator->SetEnabled(false);//Start disabled.
		materialPropertyAnimator->values = vector<Color>{Color::BlackInvisible(), Color(0.1f, 0.1f, 0.1f, 0.8f)};
		materialPropertyAnimator->duration = 3.0f;

		animator->AddKeyframe(2.0f, 5.5f, materialPropertyAnimator);
	}

	//The Team Logo
	GameObject* wretchedWaffles = new GameObject(this, "WretchedWaffles");
	{
		MeshRenderer* wretchedWafflesRenderer = new MeshRenderer(screenCheckers);
		wretchedWafflesRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		wretchedWafflesRenderer->getMaterial()->setTexture(Assets::Load<Texture>(this, "EngineAssets/Images/WretchedWafflesLogo.png"));
		wretchedWafflesRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
		wretchedWafflesRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		wretchedWafflesRenderer->getMaterial()->setColor("tint", Color(0.1f, 0.1f, 0.1f, 0.8f)); //Multiplies with the texture, I use it to tone down the alpha from 1.
		wretchedWafflesRenderer->setUI(true);
		wretchedWaffles->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.5f, 0.8f));

		MaterialPropertyAnimator<Color>* materialPropertyAnimator = new MaterialPropertyAnimator<Color>(wretchedWaffles, wretchedWafflesRenderer->getMaterial(), "tint");
		materialPropertyAnimator->SetEnabled(false);//Start disabled.
		materialPropertyAnimator->values = vector<Color>{Color::BlackInvisible(), Color::White(), Color::White(), Color::White(), Color::White(), Color::White(), Color::BlackInvisible()};
		materialPropertyAnimator->duration = 4.0f;

		animator->AddKeyframe(0.0f, 4.5f, materialPropertyAnimator);
	}

	//Loading message that appears.
	GameObject* loadingMessage = new GameObject(this, "LoadingMessage");
	loadingMessage->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 0.0f));
	{
		//Create a black invisible loading message renderer.
		MeshRenderer* loadingMessageRenderer = new MeshRenderer(loadingMessage);
		loadingMessageRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		loadingMessageRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		loadingMessageRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		loadingMessageRenderer->getMaterial()->setColor("tint",Color::BlackInvisible());
		loadingMessageRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/loading.png"));
		loadingMessageRenderer->setUI(true);

		MaterialPropertyAnimator<Color>* materialPropertyAnimator = new MaterialPropertyAnimator<Color>(loadingMessage,loadingMessageRenderer->getMaterial(),"tint");
		materialPropertyAnimator->SetEnabled(false);//Start disabled.
		materialPropertyAnimator->values = vector<Color>{Color::BlackInvisible(),Color::White()};
		materialPropertyAnimator->duration = 1.0f;

		animator->AddKeyframe(4.0f,FLT_MAX,materialPropertyAnimator);
	}

	GameObject* mainMenuLoaderGO = new GameObject(this, "MainMenuLoader");
	MainMenuLoader* mainMenuLoader = new MainMenuLoader(mainMenuLoaderGO);
	mainMenuLoader->SetEnabled(false); //Start disabled.
	animator->AddKeyframe(5.0f,FLT_MAX,mainMenuLoader); //Load the main menu after 6 seconds.
}

IntroWorld::~IntroWorld()
{
	
}
