#include "TestWorld.h"
#include "../Prefabs/Menu/LoadingScreen.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Transform.h"
#include "../Components/Standard/TextureSlider.h"
#include "../Components/Camera.h"
#include "../World/World0.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Prefabs/DuckPrefab.h"
#include "../ComponentEntity/GameObject.h"

TestWorld::TestWorld(Universe* universeRef) : World(universeRef)
{
	GameObject* cameraGO = new GameObject(this, "Intro Camera");
	Camera* camera = new Camera(cameraGO);

	Material* waterMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/WaterShader.glsl"));
	waterMaterial->setTexture(Assets::Load<Texture>("EngineAssets/Images/Water02.png"));
	Texture* texture = Assets::Load<Texture>("EngineAssets/Images/WaterMaskRadial.png");
	texture->setTextureUnit(1);
	waterMaterial->setTexture(texture, 1);
	waterMaterial->drawType = Material::DrawType::Transparent;
	Material* iceMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/DefaultShader.glsl"));
	iceMaterial->setColor("tint", Color::White());
	iceMaterial->setTexture(Assets::Load<Texture>("EngineAssets/Images/ice.png"));

	GameObject* trail0 = new GameObject(this, "Trail0");
	MeshRenderer* trailRenderer = new MeshRenderer(trail0);
	trailRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/WaterQuad.obj");
	trailRenderer->setMaterial(waterMaterial);
	trail0->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(-90,0,0)));
	trail0->GetTransform()->SetLocalPosition(Vector3(0.0f, -1.0f, 14.0f));
	trail0->GetTransform()->SetLocalScale(Vector3::One() * 3);

	GameObject* trail1 = new GameObject(this, "Trail1");
	MeshRenderer* trailRenderer1 = new MeshRenderer(trail1);
	trailRenderer1->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/WaterQuad.obj");
	trailRenderer1->setMaterial(waterMaterial);
	trail1->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(-90, 0, 0)));
	trail1->GetTransform()->SetLocalPosition(Vector3(0.0f, 1.0f, 14.0f));
	trail1->GetTransform()->SetLocalScale(Vector3::One() * 3);

	DuckPrefab* quackyFace = new DuckPrefab(this);
	quackyFace->GetTransform()->SetLocalPosition(Vector3(5, -2.0f, 7));
	quackyFace->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(0.0f, -90.0f, 0.0f)));
	quackyFace->GetTransform()->SetLocalScale(Vector3(2.0f, 2.0f, 2.0f));

}

TestWorld::~TestWorld()
{
	
}
