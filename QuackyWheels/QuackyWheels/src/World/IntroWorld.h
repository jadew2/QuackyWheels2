
#pragma once

#include "World.h"

class Universe;
class IntroWorld : public World {

public:
	IntroWorld(Universe* universe);
	virtual ~IntroWorld();
};

