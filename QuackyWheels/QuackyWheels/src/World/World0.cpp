#include "World0.h"
#include "../ComponentEntity/GameObject.h"
#include "../Components/WinLoseMessage.h"
#include "../GameLoop/EngineSystems.h"
#include "../Components/Transform.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Renderer/Types/Material.h"
#include "../Physics/Physics.h"
#include "../Components/Camera.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Components/PhysicsRigidDynamic.h"
#include "../Components/PhysicsRigidDynamicBox.h"
#include "../Components/PhysicsRigidStaticBox.h"
#include "../Components/PhysicsRigidDynamicSphere.h"
#include "../Components/PhysicsTriangleMesh.h"
#include "../Components/VehicleManager.h"
#include "../Components/PhysicsVehicle.h"
#include "../Components/SoundEffects.h"
#include "../Components/BackgroundMusic.h"
#include "../Components/MeshRenderer.h"
#include "../Components/VehicleAI.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "../AI/NavMesh.h"
#include "../Components/PlayerCamera.h"
#include "../Components/CopyPositionConstraint.h"
#include "../Components/GameFinishTracker.h"
#include "../Components/VehiclePositionTracker.h"
#include "../Components/PowerUpManager.h"
#include "../Components/HUD.h"
#include "../Components/Countdown.h"
#include "../Components/PowerUpBox.h"
#include "../Input/Controller.h"
#include "../Various/LevelSettings/LevelSettings.h"
#include <vector>
#include "../Components/AudioSource.h"
#include "../Renderer/Renderer.h"
#include "../Components/PrefabTester.h"
#include "../Components/BolderSpawner.h"
#include "../Components/SpeedPad.h"
#include "../Components/Animators/ValueAnimator.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Components/ImageEffectRenderer.h"
#include "../Prefabs/Track2Prefab.h"
#include "../Prefabs/DuckPrefab.h"
#include "../Components/Standard/BasicRotate.h"
#include "../Prefabs/SnakePrefab.h"
#include "../Components/Animators/SnakeAnimator.h"
#include "../Components/Snake.h"
#include "../Components/CameraResizer.h"
#include "../Renderer/Types/FrameBufferTexture.h"
#include "../Components/MotionBlurEffect.h"
#include "../Components/CameraResizer.h"


World0::World0(Universe* universeRef, LevelSettings levelSettings) : World(universeRef)
{
	//Preload assets
	Assets::Load<Model>("EngineAssets/Models/Mine.obj");
	Assets::Load<Model>("EngineAssets/Models/Missile.obj");
	Assets::Load<Model>("EngineAssets/DebugModels/WaterQuad.obj");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/Mine.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/Missile.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/None.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/Remove.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/Speed.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerUps/Wall.png");
	Assets::Load<Texture>("EngineAssets/Images/Explosion0.png");
	Assets::Load<Texture>("EngineAssets/Images/MineTex.png");
	Assets::Load<Texture>("EngineAssets/Images/Missile.png");
	Assets::Load<Texture>("EngineAssets/Images/PowerupParticle.png");

	EngineSystems* systems = getEngineSystems();

	unsigned long trackNumber = levelSettings.trackNumber;
	Texture* player0Texture = Assets::Load<Texture>("EngineAssets/Images/Quacky.png");
	Texture* player1Texture = Assets::Load<Texture>("EngineAssets/Images/DuckyMacDuck.png");
	Texture* player2Texture = Assets::Load<Texture>("EngineAssets/Images/Firequacker.png");
	Texture* player3Texture = Assets::Load<Texture>("EngineAssets/Images/FowlPlayDuck.png");
	unsigned long numberOfPlayers = levelSettings.getNumberOfPlayers();
	//if (numberOfPlayers >= 1)
		player0Texture = levelSettings.getPlayerTexture(0);
	//if (numberOfPlayers >= 2)
		player1Texture = levelSettings.getPlayerTexture(1);
	//if (numberOfPlayers >= 3)
		player2Texture = levelSettings.getPlayerTexture(2);
	//if (numberOfPlayers >= 4)
		player3Texture = levelSettings.getPlayerTexture(3);
	///...

	physx::PxTransform startTransform(physx::PxVec3(0.0f, 2.5f, 3.0f), physx::PxQuat(0.0f, 0.7071f, 0.0f, 0.7071f)*physx::PxQuat(physx::PxIdentity));
	physx::PxTransform startTransform2(physx::PxVec3(0.0f, 2.5f, -3.0f), physx::PxQuat(0.0f, 0.7071f, 0.0f, 0.7071f)*physx::PxQuat(physx::PxIdentity));
	physx::PxTransform startTransform3(physx::PxVec3(0.0f, 2.5f, -9.0f), physx::PxQuat(0.0f, 0.7071f, 0.0f, 0.7071f)*physx::PxQuat(physx::PxIdentity));
	physx::PxTransform startTransform4(physx::PxVec3(0.0f, 2.5f, 9.0f), physx::PxQuat(0.0f, 0.7071f, 0.0f, 0.7071f)*physx::PxQuat(physx::PxIdentity));

	physx::PxMaterial* groundMaterial = systems->physics->getPhysics()->createMaterial(0.9f, 0.9f, 0.1f); //static friction, dynamic friction, restitution
	physx::PxMaterial* const* material = &groundMaterial;
	if (!groundMaterial) {
		//fatalError("createMaterial failed!");
	}

	physx::PxMaterial* carMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.5f, 0.5f, 0.6f);
	physx::PxMaterial* const* material2 = &carMaterial;

	physx::PxFilterData qryFilterData;
	setupDrivableSurface(qryFilterData);

	physx::PxFilterData simFilterData;
	simFilterData.word0 = COLLISION_FLAG_GROUND;
	simFilterData.word1 = COLLISION_FLAG_GROUND_AGAINST;

	physx::PxShapeFlags shapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	GameObject* manager = new GameObject(this, "gameManager");
	VehicleManager* vehicleManager = new VehicleManager(manager, material);

	PhysicsVehicle* vehicleList[4];

	unsigned long players = std::max((int)numberOfPlayers, 1);
	getEngineSystems()->controller->reset();
	if (players > 1)
	{
		getEngineSystems()->controller->playerTwo(players);
	}
	

	// --Player 1
	GameObject* playerVehicle = new GameObject(this, "playerVehicle");
	PhysicsVehicle* physicsVehicle = new PhysicsVehicle(playerVehicle, &startTransform, ObjectType::Chassis, vehicleManager, levelSettings.trackNumber);
	AudioSource* whoosh = new AudioSource(playerVehicle);
	whoosh->SetAudio(Assets::Load<Audio>("EngineAssets/Sounds/Swooshing.wav"));
	whoosh->SetVolume(1.0f);
	vehicleList[0] = physicsVehicle;

	DuckPrefab* playerVehicle0Prefab = new DuckPrefab(this, vehicleManager, 0);
	playerVehicle0Prefab->GetTransform()->SetParent(playerVehicle->GetTransform());
	playerVehicle0Prefab->setTexture(player0Texture);

	// -- Player 2	
	GameObject* playerVehicle1 = new GameObject(this, "playerVehicle2");
	PhysicsVehicle* physicsVehicle1 = new PhysicsVehicle(playerVehicle1, &startTransform2, ObjectType::Chassis, vehicleManager, levelSettings.trackNumber);
	vehicleList[1] = physicsVehicle1;

	DuckPrefab* playerVehicle1Prefab = new DuckPrefab(this, vehicleManager, 1);
	playerVehicle1Prefab->GetTransform()->SetParent(playerVehicle1->GetTransform());
	playerVehicle1Prefab->setTexture(player1Texture);

	// -- Ai Player 1
	GameObject* aiVehicle = new GameObject(this, "aiVehicle");
	PhysicsVehicle* aiPhysicsVehicle = new PhysicsVehicle(aiVehicle, &startTransform3, ObjectType::Chassis, vehicleManager, levelSettings.trackNumber);
	vehicleList[2] = aiPhysicsVehicle;


	DuckPrefab* playerVehicle2Prefab = new DuckPrefab(this, vehicleManager, 2);
	playerVehicle2Prefab->GetTransform()->SetParent(aiVehicle->GetTransform());
	playerVehicle2Prefab->setTexture(player2Texture);

	// -- Ai Player 2
	GameObject* aiVehicle2 = new GameObject(this, "aiVehicle2");
	PhysicsVehicle* aiPhysicsVehicle2 = new PhysicsVehicle(aiVehicle2, &startTransform4, ObjectType::Chassis, vehicleManager, levelSettings.trackNumber);
	vehicleList[3] = aiPhysicsVehicle2;

	DuckPrefab* playerVehicle3Prefab = new DuckPrefab(this, vehicleManager, 3);
	playerVehicle3Prefab->GetTransform()->SetParent(aiVehicle2->GetTransform());
	playerVehicle3Prefab->setTexture(player3Texture);

	Model* missile = Assets::Load<Model>("EngineAssets/Models/Missile.obj");
	Texture* missileTexture = Assets::Load<Texture>("EngineAssets/Images/Missile.png");
	Model* mine = Assets::Load<Model>("EngineAssets/Models/Mine.obj");
	Texture* mineTexture = Assets::Load<Texture>("EngineAssets/Images/MineTex.png");

	GameObject* bgMusic = new GameObject(this, "bgMusic");
	GameObject* soundFX = new GameObject(this, "sfx");
	GameObject* soundFX2 = new GameObject(this, "sfx2");
	GameObject* soundFX3 = new GameObject(this, "sfx3");
	GameObject* soundFX4 = new GameObject(this, "sfx4");

	BackgroundMusic* bgm = new BackgroundMusic(bgMusic);

	// -- Sound
	SoundEffects* sound = new SoundEffects(soundFX, playerVehicle, 0);
	if (players > 1)
	{
		SoundEffects* sound2 = new SoundEffects(soundFX2, playerVehicle1, playerVehicle1->GetComponent<PhysicsVehicle>()->getVehicleIndex());
	}
	if (players > 2)
	{
		SoundEffects* sound3 = new SoundEffects(soundFX3, aiVehicle, aiVehicle->GetComponent<PhysicsVehicle>()->getVehicleIndex());
	}
	if (players > 3)
	{
		SoundEffects* sound4 = new SoundEffects(soundFX4, aiVehicle2, aiVehicle2->GetComponent<PhysicsVehicle>()->getVehicleIndex());
	}
	

	Mesh* trackAISpline = nullptr;// trackMeshes[1]; 	//Track AI waypoints
	Mesh* trackSpline = nullptr;// trackMeshes[1]; 	//Track waypoints
	Mesh* trackMesh = nullptr; //trackMeshes[2]; 		//The track (the part with a texture)
	Mesh* trackNavMesh = nullptr; //trackMeshes[3]; 	//Track NavMesh
	Mesh* trackClouds = nullptr; //trackMeshes[4];		//The clouds under the track
	Mesh* physXTrackMesh = nullptr;
	float powerupYOffset = 0.0f;
	int killplane;

	if (trackNumber == 0)
	{
		//Import the Track
		Model* importedTrackModel = Assets::Load<Model>("EngineAssets/DebugModels/Track1Cyclic2.obj");

		Model* trackModel = importedTrackModel->Clone();
		trackModel->Deform(Matrix4x4::TRS(Vector3::Zero(), Quaternion::Identity(), Vector3(5, 5, 5))); //Scale the track by 5,5,5
		std::vector<Mesh*> trackMeshes = trackModel->getMeshes();

		trackSpline = trackMeshes[1]; 	//Track waypoints
		trackAISpline = trackSpline;
		trackMesh = trackMeshes[2];		//The track (the part with a texture)
		trackNavMesh = trackMeshes[3]; 	//Track NavMesh
		trackClouds = trackMeshes[4];	//The clouds under the track
		Mesh* importedPhysXTrackMesh = Assets::Load<Mesh>("EngineAssets/DebugModels/Track1Cyclic1NoTexCoords.obj");
		physXTrackMesh = importedPhysXTrackMesh->Clone();
		physXTrackMesh->Deform(Matrix4x4::TRS(Vector3::Zero(), Quaternion::Identity(), Vector3(5, 5, 5))); //Scale the track by 5,5,5

		killplane = -15;
		powerupYOffset = -4.0f;

		std::vector<physx::PxVec3> trackPhysicsVerticies;
		trackPhysicsVerticies.resize(physXTrackMesh->verticies.size());

		for (size_t i = 0; i < physXTrackMesh->verticies.size(); i++)
		{
			//trackPhysicsVerticies[i] = trackMesh->verticies[i].pxVec3(); //<-- Try this.

			trackPhysicsVerticies[i].x = (physx::PxReal)physXTrackMesh->verticies[i].x;
			trackPhysicsVerticies[i].y = (physx::PxReal)physXTrackMesh->verticies[i].y;
			trackPhysicsVerticies[i].z = (physx::PxReal)physXTrackMesh->verticies[i].z;
		}

		std::vector<physx::PxU32> testMeshPhysicsIndicies;
		testMeshPhysicsIndicies.resize(physXTrackMesh->triangles.size());
		for (size_t i = 0; i < physXTrackMesh->triangles.size(); i++)
		{
			testMeshPhysicsIndicies[i] = (physx::PxU32)physXTrackMesh->triangles[i]; //See if PxU16 changes anything
		}

		physx::PxU16* materialIndex = nullptr;

		GameObject* physicsTrack = new GameObject(this, "physicsTrack");
		PhysicsRigidStatic* trackActor = new PhysicsRigidStatic(physicsTrack, physicsTrack->GetTransform(), ObjectType::Obstacle, false);
		PhysicsTriangleMesh* trackShape = new PhysicsTriangleMesh(physicsTrack, trackActor, physXTrackMesh->verticies.size(), trackPhysicsVerticies.data(), physXTrackMesh->triangles.size(), testMeshPhysicsIndicies.data(), 1, material, materialIndex, physicsTrack->GetTransform(), &qryFilterData, &simFilterData, &shapeFlags);
		this->getEngineSystems()->physics->getScene()->addActor(*trackActor->getRigidStatic());

	}
	else if (trackNumber == 1)
	{
		Vector3 boulderPositon(0, 3, 0);
		Vector3 bolderPosition2(0, 3, 0);
		//Import the Track
		Track2Prefab* track2 = new Track2Prefab(this);

		vector<Vector3> triggerPoints = track2->triggerPoints;
		boulderPositon = track2->boulderSpawn;
		trackAISpline = track2->goalPoints;
		trackSpline = track2->detailedGoalPoints;
		trackNavMesh = track2->navMesh;

		killplane = -190;
		powerupYOffset = 0.0f;

		GameObject* bolderSpawnerObject = new GameObject(this, "bolderSpawner");
		BolderSpawner* spawner = new BolderSpawner(bolderSpawnerObject, vehicleManager, killplane, boulderPositon);

		GameObject* speedPadSpawner = new GameObject(this, "speedPadSpawner");
		SpeedPad* speedSpawner = new SpeedPad(speedPadSpawner, vehicleManager, Vector3(149.0f, -86.0f, -62.0f), Quaternion(0.00095f, -0.9753f, 0.061189f, -0.21173f));

		GameObject* speedPadSpawner2 = new GameObject(this, "speedPadSpawner2");
		SpeedPad* speedSpawner2 = new SpeedPad(speedPadSpawner2, vehicleManager, Vector3(157.9f, -129.0f, 205.7f), Quaternion(0.096085f, 0.609659f, 0.082562f, -0.782475f));

	}

	
	/*GameObject* navMeshRendererGo = new GameObject(this, "NavMeshRenderer");
	MeshRenderer* navMeshRenderer = new MeshRenderer(navMeshRendererGo);
	navMeshRenderer->mesh = trackNavMesh;
	navMeshRenderer->drawType = MeshRenderer::DrawType::WireFrame;*/
	


	NavMesh* navMesh = trackNavMesh->getNavMesh();

	vector<Camera*> playerCameras = vector<Camera*>();

	GameObject* cameraGo = new GameObject(this, "Camera");
	Camera* camera = new Camera(cameraGo);
	cameraGo->GetTransform()->SetLocalPosition(Vector3(-100.0f, 100.0f, 0.0f));
	cameraGo->GetTransform()->SetLocalRotation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
	PlayerCamera* playerCamera = new PlayerCamera(cameraGo);
	playerCamera->SetPlayerGameObject(playerVehicle);
	camera->mask.add(RenderLayer::Player0);

	playerCameras.push_back(camera);

	if (players > 1)
	{
		GameObject* cameraGo2 = new GameObject(this, "Camera2");
		Camera* camera2 = new Camera(cameraGo2);
		cameraGo2->GetTransform()->SetLocalPosition(Vector3(-100.0f, 100.0f, 0.0f));
		cameraGo2->GetTransform()->SetLocalRotation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
		PlayerCamera* playerCamera2 = new PlayerCamera(cameraGo2);
		playerCamera2->SetPlayerGameObject(playerVehicle1);
		camera2->mask.add(RenderLayer::Player1);

		playerCameras.push_back(camera2);
	}
	if (players > 2)
	{
		GameObject* cameraGo3 = new GameObject(this, "Camera3");
		Camera* camera3 = new Camera(cameraGo3);
		cameraGo3->GetTransform()->SetLocalPosition(Vector3(-100.0f, 100.0f, 0.0f));
		cameraGo3->GetTransform()->SetLocalRotation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
		PlayerCamera* playerCamera3 = new PlayerCamera(cameraGo3);
		playerCamera3->SetPlayerGameObject(aiVehicle);
		camera3->mask.add(RenderLayer::Player2);

		playerCameras.push_back(camera3);
		
	}
	if (players > 3)
	{
		GameObject* cameraGo4 = new GameObject(this, "Camera4");
		Camera* camera4 = new Camera(cameraGo4);
		cameraGo4->GetTransform()->SetLocalPosition(Vector3(-100.0f, 100.0f, 0.0f));
		cameraGo4->GetTransform()->SetLocalRotation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
		PlayerCamera* playerCamera4 = new PlayerCamera(cameraGo4);
		playerCamera4->SetPlayerGameObject(aiVehicle2);
		camera4->mask.add(RenderLayer::Player3);

		playerCameras.push_back(camera4);
	}

	//Resize the player's viewports based on how many players there are.
	GameObject* cameraResizer = new GameObject(this, "Camera Resizer");
	CameraResizer* resizer = new CameraResizer(cameraResizer);
	resizer->resizeCameras(playerCameras);


	//Add motion blur effects
	if (playerCameras.size() == 1)
	{
		for (Camera* playerCamera : playerCameras)
		{
			GameObject* playerCameraGO = playerCamera->GetGameObject();
			GameObject* playerVehicleGO = playerCameraGO->GetComponent<PlayerCamera>()->getPlayerCar();

			MotionBlurEffect* motionBlurEffect = new MotionBlurEffect(playerCameraGO);
			motionBlurEffect->setParameters(playerVehicleGO->GetComponent<PhysicsVehicle>(), playerCamera, 0);
		}
	}

	//Skybox
	{
		GameObject* skybox = new GameObject(this, "Skybox");
		MeshRenderer* skyboxRenderer = new MeshRenderer(skybox);
		Model* sky = Assets::Load<Model>("EngineAssets/DebugModels/SkyBox.obj");
		skyboxRenderer->mesh = sky->getMeshes()[0];
		Texture* skyboxTexture = Assets::Load<Texture>("EngineAssets/Images/Skybox.bmp");
		skyboxRenderer->getMaterial()->setTexture(skyboxTexture);
	}

	Texture* texture = Assets::Load<Texture>("EngineAssets/Images/Path00.bmp");
	GameObject* track = new GameObject(this, "Track");
	MeshRenderer* trackRenderer = new MeshRenderer(track);
	trackRenderer->mesh = trackMesh;
	trackRenderer->getMaterial()->setTexture(texture);

	Model* goalPost = Assets::Load<Model>("EngineAssets/DebugModels/Banner.obj");
	GameObject* goal = new GameObject(this, "Goal");
	MeshRenderer* goalRenderer = new MeshRenderer(goal);
	goalRenderer->mesh = goalPost->getMeshes()[0];
	Texture* bannerStartTexture = Assets::Load<Texture>("EngineAssets/Images/BannerStart.bmp");
	Texture* bannerFinishTexture = Assets::Load<Texture>("EngineAssets/Images/BannerFinish.bmp");
	goalRenderer->getMaterial()->setTexture(bannerStartTexture);
	goal->GetTransform()->SetLocalScale(Vector3(4.5, 4.5, 4.5));


	/*
	GameObject* goalBB = new GameObject(this, "Goal");
	goalBB->GetTransform()->SetParent(goal->GetTransform(), false);
	goalBB->GetTransform()->SetLocalPosition(worldBounds.Centre);
	goalBB->GetTransform()->SetLocalScale(worldBounds.Extents);

	MeshRenderer* goalBoundingBox = new MeshRenderer(goalBB);
	goalBoundingBox->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/Cube.obj");
	goalBoundingBox->setMaterial( new Material(Assets::Load<Shader>("EngineAssets/Shaders/TransparentShader.glsl")));
	goalBoundingBox->getMaterial()->drawType = Material::DrawType::Transparent;
	goalBoundingBox->getMaterial()->setColor("ambient", Color(0,0,1,0.5f));
	*/

	//The clouds under the track
	MeshRenderer* trackCloudRenderer = new MeshRenderer(track);
	trackCloudRenderer->getMaterial()->setColor("ambient", Color(1, 1, 1, 0));
	trackCloudRenderer->getMaterial()->setColor("tint", Color(1, 1, 1, 0.9f));
	trackCloudRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	trackCloudRenderer->mesh = trackClouds;

	//AI
	std::vector<Vector3> goalPoints = trackAISpline->verticies;

	if (players == 1)
	{
		GameObject*playerAI = new GameObject(this, "playerAI"); //the aiVehicle's AI
		VehicleAI* aip = new VehicleAI(playerVehicle1, physicsVehicle1, navMesh);
		aip->SetNavMesh(navMesh);
		aip->SetGoalList(goalPoints);
	}

	if (players < 3)
	{
		GameObject* vehicleAI = new GameObject(this, "vehicleAI"); //the aiVehicle's AI
		VehicleAI* ai = new VehicleAI(aiVehicle, aiPhysicsVehicle, navMesh);
		ai->SetNavMesh(navMesh);
		ai->SetGoalList(goalPoints);
	}
	
	if (players < 4)
	{
		GameObject* vehicleAI2 = new GameObject(this, "vehicleAI2"); //the aiVehicle's AI
		VehicleAI* ai2 = new VehicleAI(aiVehicle2, aiPhysicsVehicle2, navMesh);
		ai2->SetNavMesh(navMesh);
		ai2->SetGoalList(goalPoints);
	}
	

	//The win/lose logic

	//Win Lose message
	GameObject* winLoseGameObject = new GameObject(this, "WinLoseMessagePosition");
	//Component that displays the win/lose message.
	WinLoseMessage* winLoseMessage = new WinLoseMessage(winLoseGameObject, cameraGo->GetTransform());

	//Component that reads from collisions, and triggers the win/ lose message.

	//GameFinishTracker* gameFinishTracker = new GameFinishTracker(winLoseGameObject, winLoseMessage, playerVehicleActor->getRigidDynamic(), playerVehicleActor2->getRigidDynamic(), triggers);

	std::vector<physx::PxVec3>* trackSplinePoints = new std::vector<physx::PxVec3>(trackSpline->verticies.size());
	for (unsigned int i = 0; i < trackSpline->verticies.size(); i++) {
		(*trackSplinePoints)[i].x = trackSpline->verticies[i].x;
		(*trackSplinePoints)[i].y = trackSpline->verticies[i].y;
		(*trackSplinePoints)[i].z = trackSpline->verticies[i].z;
	}
	VehiclePositionTracker* vehiclePositionTracker = new VehiclePositionTracker(manager, vehicleManager, *trackSplinePoints, killplane);

	if (trackNumber == 1) {
		SnakePrefab* snake = new SnakePrefab(this);
		SnakeAnimator* snakeAnimator = new SnakeAnimator(snake, snake->head, snake->neck, snake->neckPoints);
		Snake* snakeLogic = new Snake(snake, vehicleManager, snakeAnimator, vehiclePositionTracker);
	}
	PowerUpManager* playerPowers = new PowerUpManager(manager, vehicleList, vehiclePositionTracker);

	vector<GameObject*> powerUpBoxes;
	int numBoxes = ((trackSplinePoints->size() / 3) - 1);

	if (trackNumber == 0)
	{
		powerUpBoxes.resize(numBoxes);
	}
	else if (trackNumber == 1)
	{
		powerUpBoxes.resize(numBoxes * 3);
	}
	
	int j = 0;
	for (int i = 0; i < numBoxes; i++) {
		physx::PxVec3 p = (*trackSplinePoints)[3 * (i + 1)] * 0.5f + (*trackSplinePoints)[(3 * (i + 1)) + 1] * 0.5f;
		physx::PxVec3 alignVec = (*trackSplinePoints)[(3 * (i + 1)) + 1] - p;
		alignVec.normalize();

		physx::PxVec3 right = alignVec.cross(physx::PxVec3(0.0, 1.0, 0.0));
		right.normalize();

		powerUpBoxes[j] = new GameObject(this, "powerUpBox");
		powerUpBoxes[j]->GetTransform()->SetLocalPosition(Vector3(p) + Vector3(0,powerupYOffset,0));
		powerUpBoxes[j]->GetTransform()->SetLocalRotation(Quaternion::LookRotation(alignVec, Vector3::Up())); //Quaternion(powerUpTransform.q));
		powerUpBoxes[j]->GetTransform()->SetLocalScale(Vector3::One() * 2.0f);
		PowerUpBox* powerUpBoxComponent = new PowerUpBox(powerUpBoxes[j], vehicleManager, playerPowers);
		powerUpBoxComponent->triggerRadius = 8.0f;
		BasicRotate* powerUpBoxRotater = new BasicRotate(powerUpBoxes[j]);
		powerUpBoxRotater->rotation = Vector3(0, 1, 0);
		powerUpBoxRotater->speed = 180;

		if (trackNumber == 1)
		{

			powerUpBoxes[j + 1] = new GameObject(this, "powerUpBox");
			powerUpBoxes[j + 1]->GetTransform()->SetLocalPosition(Vector3(p) + (Vector3(right) * 10.0f) + Vector3(0, powerupYOffset, 0));
			powerUpBoxes[j + 1]->GetTransform()->SetLocalRotation(Quaternion::LookRotation(alignVec, Vector3::Up())); //Quaternion(powerUpTransform.q));
			powerUpBoxes[j + 1]->GetTransform()->SetLocalScale(Vector3::One() * 2.0f);
			PowerUpBox* powerUpBoxComponent2 = new PowerUpBox(powerUpBoxes[j + 1], vehicleManager, playerPowers);
			powerUpBoxComponent2->triggerRadius = 8.0f;
			BasicRotate* powerUpBoxRotater2 = new BasicRotate(powerUpBoxes[j + 1]);
			powerUpBoxRotater2->rotation = Vector3(0, 1, 0);
			powerUpBoxRotater2->speed = 180;

			powerUpBoxes[j + 2] = new GameObject(this, "powerUpBox");
			powerUpBoxes[j + 2]->GetTransform()->SetLocalPosition(Vector3(p) + (Vector3(right) * -10.0f) + Vector3(0, powerupYOffset, 0));
			powerUpBoxes[j + 2]->GetTransform()->SetLocalRotation(Quaternion::LookRotation(alignVec, Vector3::Up())); //Quaternion(powerUpTransform.q));
			powerUpBoxes[j + 2]->GetTransform()->SetLocalScale(Vector3::One() * 2.0f);
			PowerUpBox* powerUpBoxComponent3 = new PowerUpBox(powerUpBoxes[j + 2], vehicleManager, playerPowers);
			powerUpBoxComponent3->triggerRadius = 8.0f;
			BasicRotate* powerUpBoxRotater3 = new BasicRotate(powerUpBoxes[j + 2]);
			powerUpBoxRotater3->rotation = Vector3(0, 1, 0);
			powerUpBoxRotater3->speed = 180;

			j += 3;
		}
		else if (trackNumber == 0)
		{
			j++;
		}
	}

	GameObject* countdownObject = new GameObject(this, "CountdownTimer");
	Countdown* count = new Countdown(countdownObject);

	GameObject* hudGameObject = new GameObject(this, "HUDRendering");
	HUD* ui_display = new HUD(hudGameObject, vehiclePositionTracker, count, 0, playerPowers, vehicleManager);

	/*
	GameObject* tester = new GameObject(this, "ExplosionTester");
	PrefabTester* prefabTester = new PrefabTester(tester);
	*/

	/* ImageEffect Bad refactor version.
	GameObject* effectTester = new GameObject(this, "ImageEffectTester");
	ImageEffectRenderer* screenTint = new ImageEffectRenderer(effectTester);
	Material* screenMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/ScreenTintEffect.glsl"));
	screenTint->setMaterial(screenMaterial);

	MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(effectTester,screenMaterial, "tint");
	tintAnimator->loopType = ValueAnimator<Color>::LoopType::Loop;
	tintAnimator->values = vector<Color>{Color::Blue(), Color::Red(), Color::Green(), Color::Yellow()};
	tintAnimator->duration = 1.0f;
	 */

	/*  Hacky version
	GameObject* effectTester = new GameObject(this, "ImageEffectTester");
	MeshRenderer* screenTint = new MeshRenderer(effectTester);
	screenTint->mesh = Mesh::Quad();
	screenTint->setMaterial( new Material(Assets::Load<Shader>("EngineAssets/Shaders/ScreenTintEffect.glsl")));
	screenTint->depthTest = false;
	screenTint->getMaterial()->drawType = Material::DrawType::EndImageEffect;

	MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(effectTester, screenTint->getMaterial(), "tint");
	tintAnimator->loopType = ValueAnimator<Color>::LoopType::Loop;
	tintAnimator->values = vector<Color>{Color::Blue(), Color::Red(), Color::Green(), Color::Yellow()};
	tintAnimator->duration = 1.0f;
	 */
}

World0::~World0()
{
}