#include "World2.h"
#include "../Prefabs/Menu/LoadingScreen.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Transform.h"
#include "../Components/Standard/TextureSlider.h"
#include "../Components/Camera.h"
#include "../Components/Standard/BasicRotate.h"
#include "../World/World0.h"

World2::World2(Universe* universeRef, LevelSettings levelSettings) : World(universeRef)
{
	level = levelSettings;
	GameObject* skybox = new GameObject(this, "SkyBox");
	{
		MeshRenderer* skyboxRenderer = new MeshRenderer(skybox);
		skyboxRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/SkyBox.obj");
		skyboxRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Skybox.bmp"));

		//MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(skybox, skyboxRenderer->getMaterial(), "tint");
		//tintAnimator->loopType = ValueAnimator<Color>::LoopType::Loop;
		//tintAnimator->values = vector<Color>{Color::Blue(), Color::Red(), Color::Green(), Color::Yellow()};
		//tintAnimator->duration = 0.5f;
	}

	//The sliding checker texture over the background
	GameObject* screenCheckers = new GameObject(this, "ScreenCheckers");
	{
		MeshRenderer* screenCheckerRenderer = new MeshRenderer(screenCheckers);
		screenCheckerRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		screenCheckerRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/CheckerFlag.png"));
		screenCheckerRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		screenCheckerRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		screenCheckerRenderer->getMaterial()->setColor("tint", Color(1.0f, 1.0f, 1.0f, 0.1f)); //Multiplies with the texture, I use it to tone down the alpha from 1.
		screenCheckerRenderer->getMaterial()->setVector2("mainTex0Scale", Vector2(0.7f, 0.7f)); //Change this to change the rendered scale of the checker texture
		screenCheckerRenderer->setUI(true);

		//Center it
		screenCheckers->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 1.0f));

		//Slides the texture
		TextureSlider* textureSlider = new TextureSlider(screenCheckers);
		textureSlider->material = screenCheckerRenderer->getMaterial();
		textureSlider->propertyName = string("mainTex0Offset");
		textureSlider->offsetSpeed = Vector2(0.02f, 0.005f); //Change this to change the sliding direction
	}
	GameObject* camera = new GameObject(this, "Menu Camera");
	{
		Camera* cameraGO = new Camera(camera);
		cameraGO->GetTransform()->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
		cameraGO->GetTransform()->SetRotation(Quaternion::Euler(Vector3(-20.0f, 0.0f, 0.0f))); //Look upwards a bit

		//Spins the camera around
		BasicRotate* rotater = new BasicRotate(skybox);
		rotater->rotation = Vector3(0.0f, 1.0f, 0.0f);
		rotater->speed = 5.0f;
	}
	LoadingScreen* loading = new LoadingScreen(this, level, camera);
	
}

World2::~World2()
{
	
}
