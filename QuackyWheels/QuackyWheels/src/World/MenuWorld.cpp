#include "MenuWorld.h"


#include "../Components/Menu/System/MenuManager.h"
#include "../Components/Standard/TextureSlider.h"
#include "../Components/MeshRenderer.h"
#include "../ComponentEntity/GameObject.h"
#include "../Components/Transform.h"
#include "../Components/Camera.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Components/AudioSource.h"
#include "../Components/Animators/MaterialPropertyAnimator.h"
#include "../Components/Standard/BasicRotate.h"
#include "../Components/Standard/TimedScriptDestroy.h"

#include "../Prefabs/DuckPrefab.h"
#include "../Prefabs/Explosion0Prefab.h"
#include "../Input/Controller.h"
#include "../Renderer/Types/FrameBufferTexture.h"
#include "../Components/ImageEffectRenderer.h"


//This will create rendering objects for the main menu
MenuWorld::MenuWorld(Universe* universeRef) : World(universeRef)
{
	getEngineSystems()->controller->reset();
	getEngineSystems()->controller->playerTwo(getEngineSystems()->controller->getPlayers());

	//Preload textures for the menu
	vector<string> textureFilepaths = vector<string>
	{
		"EngineAssets/Images/QuackyWheelsLogo.png",
		"EngineAssets/Images/PlayButton.png",
		"EngineAssets/Images/ControlLabel.png",
		"EngineAssets/Images/QuitButton.png",
		"EngineAssets/Images/Instructions2.png",
		"EngineAssets/Images/BackLabel.png",
		"EngineAssets/Images/NumofPlayers.png",
		"EngineAssets/Images/OneLabel.png",
		"EngineAssets/Images/TwoLabel.png",
		"EngineAssets/Images/ThreeLabel.png",
		"EngineAssets/Images/FourLabel.png",
		"EngineAssets/Images/Player1.png",
		"EngineAssets/Images/Player2.png",
		"EngineAssets/Images/Player3.png",
		"EngineAssets/Images/Player4.png",
		"EngineAssets/Images/quackers.png",
		"EngineAssets/Images/Mac.png",
		"EngineAssets/Images/fireq.png",
		"EngineAssets/Images/Fowl.png",

		"EngineAssets/Images/selecttrack.png",
		"EngineAssets/Images/Track0Label.png",
		"EngineAssets/Images/dmm.png",
		"EngineAssets/Images/controller.png",
	};
	for (size_t i = 0; i < textureFilepaths.size(); ++i)
		Assets::Load<Texture>(textureFilepaths[i]);



	GameObject* skybox = new GameObject(this, "SkyBox");
	{
		MeshRenderer* skyboxRenderer = new MeshRenderer(skybox);
		skyboxRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/SkyBox.obj");
		skyboxRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Skybox.bmp"));

		//Spins the skybox
		BasicRotate* rotater = new BasicRotate(skybox);
		rotater->rotation = Vector3(0.0f, 1.0f, 0.0f);
		rotater->speed = 6.0f;

		MaterialPropertyAnimator<Color>* skyboxAnimator = new MaterialPropertyAnimator<Color>(skybox, skyboxRenderer->getMaterial(), "tint");
		skyboxAnimator->values = vector<Color>{Color::Black(), Color::Red(), Color::Black(), Color::Red(), Color::White()};
		skyboxAnimator->loopType = MaterialPropertyAnimator<Color>::LoopType::Once;
		skyboxAnimator->duration = 1.0f;
		//TimedScriptDestroy* skyboxAnimatorDestroyer = new TimedScriptDestroy(skybox, skyboxAnimator);
		//skyboxAnimatorDestroyer->timeForAction = 2.0f;

	}

	//The sliding checker texture over the background
	GameObject* screenCheckers = new GameObject(this, "ScreenCheckers");
	{
		MeshRenderer* screenCheckerRenderer = new MeshRenderer(screenCheckers);
		screenCheckerRenderer->mesh = Mesh::Quad(); //A centered full screen quad.
		screenCheckerRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/CheckerFlag.png"));
		screenCheckerRenderer->getMaterial()->drawType = Material::DrawType::Transparent; //Marks to use transparent draw settings
		screenCheckerRenderer->getMaterial()->setColor("ambient", Color::BlackInvisible()); //The default alpha of Color::Black() is 1, while this one is 0
		screenCheckerRenderer->getMaterial()->setColor("tint", Color(1.0f, 1.0f, 1.0f, 0.1f)); //Multiplies with the texture, I use it to tone down the alpha from 1.
		screenCheckerRenderer->getMaterial()->setVector2("mainTex0Scale", Vector2(0.7f, 0.7f)); //Change this to change the rendered scale of the checker texture
		screenCheckerRenderer->setUI(true);

		//Center it
		screenCheckers->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 1.0f));

		//Slides the texture
		TextureSlider* textureSlider = new TextureSlider(screenCheckers);
		textureSlider->material = screenCheckerRenderer->getMaterial();
		textureSlider->propertyName = string("mainTex0Offset");
		textureSlider->offsetSpeed = Vector2(0.04f, 0.01f); //Change this to change the sliding direction
	}

	//GameObject* playButton = new GameObject(this, "playButton");

	GameObject* mainMenuGo = new GameObject(this,"Main Menu");
	MenuManager* mainMenu = new MenuManager(mainMenuGo);

	//The "Play: Controls: Exit:" menu
	MainMenuScreen* startMenu = new MainMenuScreen(this, mainMenu);

	GameObject* cameraGO = new GameObject(this, "Menu Camera");
	Camera* camera = new Camera(cameraGO);
	camera->GetTransform()->SetPosition(Vector3(0.0f, 0.0f, 0.0f));
	camera->GetTransform()->SetRotation(Quaternion::Euler(Vector3(-20.0f, 0.0f, 0.0f))); //Look upwards a bit

	//Epic music
	GameObject* darude = new GameObject(this, "Darude");
	AudioSource* sandstorm = new AudioSource(darude);
	sandstorm->SetAudio("EngineAssets/Sounds/Darude_-_Sandstorm_full.ogg");
	if (!sandstorm->IsPlaying())
		sandstorm->Play(0);

	Explosion0Prefab* explosion = new Explosion0Prefab(this);
	explosion->GetTransform()->SetLocalPosition(Vector3(0, 0, 5));

	Explosion0Prefab* explosion2 = new Explosion0Prefab(this);
	explosion->GetTransform()->SetLocalPosition(Vector3(2, -2, 5));

	Explosion0Prefab* explosion3 = new Explosion0Prefab(this);
	explosion->GetTransform()->SetLocalPosition(Vector3(-2, -2, 5));
}

MenuWorld::~MenuWorld()
{
}
