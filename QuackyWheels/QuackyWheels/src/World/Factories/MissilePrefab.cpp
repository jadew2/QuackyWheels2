//
// Created by Jade White on 2016-03-24.
//

#include "MissilePrefab.h"
#include "../../Components/MeshRenderer.h"
#include "../../AssetImport/Model.h"
#include "../../AssetImport/Assets.h"
#include "../../Renderer/Types/Material.h"
#include "../../Components/Standard/BasicRotate.h"
#include "../../Components/Transform.h"
#include "../../Components/Standard/BasicJiggle.h"
#include "../../Components/Standard/TextureSlider.h"

#include <string>
using namespace std;

MissilePrefab::MissilePrefab(World* worldRef)
{
	world = worldRef;
}

GameObject* MissilePrefab::createMissile()
{
	//Load the mesh
	Model* missileModel = Assets::Load<Model>("EngineAssets/Models/Missile.obj");
	vector<Mesh*> meshes = missileModel->getMeshes();
	Mesh* missileMesh = meshes[0];
	Mesh* fireMesh = meshes[1];

	//Create the missile GO
	GameObject* missile = new GameObject(world,string("Missile"));

	GameObject* missileMeshGO = new GameObject(world,string("MissileMesh"));
	missileMeshGO->GetTransform()->SetParent(missile->GetTransform(),false);
	{
		MeshRenderer* missileRenderer = new MeshRenderer(missileMeshGO);
		missileRenderer->mesh = missileMesh;
		missileRenderer->Materials[0]->setTexture(Assets::Load<Texture>("EngineAssets/Images/Missile.png"));

		BasicRotate* missileRotater = new BasicRotate(missileMeshGO);
		missileRotater->rotation = Vector3(0,0,270);
		missileRotater->speed = 1;

		BasicJiggle* missileJiggle = new BasicJiggle(missileMeshGO);
		missileJiggle->addedScale = Vector3(0,0,0.05f);
		missileJiggle->speed = 10.0f;
	}

	GameObject* fireMeshGO = new GameObject(world,string("MissileFireMesh"));
	missileMeshGO->GetTransform()->SetParent(missile->GetTransform(),false);
	{
		MeshRenderer* fireRenderer = new MeshRenderer(fireMeshGO);
		fireRenderer->mesh = missileMesh;
		fireRenderer->Materials[0]->setTexture(Assets::Load<Texture>("EngineAssets/Images/Missile.png"));

		BasicRotate* missileRotater = new BasicRotate(fireMeshGO);
		missileRotater->rotation = Vector3(0,0,180);
		missileRotater->speed = 1;

		BasicJiggle* missileJiggle = new BasicJiggle(fireMeshGO);
		missileJiggle->addedScale = Vector3(0,0,0.05f);
		missileJiggle->speed = 10.0f;

		//Slide the fire texture
		TextureSlider* textureSlider = new TextureSlider(fireMeshGO);
		textureSlider->material = fireRenderer->Materials[0];
		textureSlider->offsetSpeed = Vector2(0,1.0f);
	}

}

