//
// Created by Jade White on 2016-03-24.
//

#pragma once

#include "../World.h"
#include "../../ComponentEntity/GameObject.h"

///<summary> Creates missile objects </summary>
class MissilePrefab
{
	World* world;
public:
	MissilePrefab(World* universe);

	GameObject* createMissile();

};


