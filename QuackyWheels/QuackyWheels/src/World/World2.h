
#pragma once

#include "World.h"
#include "../Various/LevelSettings/LevelSettings.h"

class Universe;
class LevelSettings;
class World2 : public World {

public:
	World2(Universe* universe, LevelSettings levelSettings);
	virtual ~World2();
	LevelSettings level;
};

