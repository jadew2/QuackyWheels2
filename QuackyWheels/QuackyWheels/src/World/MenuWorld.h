#pragma once
#include "World.h"
#include "../Prefabs/Menu/MainMenuScreen.h"

class Universe;
class MenuWorld : public World
{
public:
	MenuWorld(Universe* universeRef);
	virtual ~MenuWorld();
};

