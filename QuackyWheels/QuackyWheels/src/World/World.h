#pragma once

#include <vector>
#include <map>
#include <string>
#include "../ComponentEntity/GameObject.h"

class Universe;
class EngineSystems;
class World
{
protected:
	Universe* universe;

	//std::vector<GameObject*> gameObjects;
	std::vector<GameObject*> gameObjects;

public:
	World(Universe* universe);
	virtual ~World();

	void addGameObject(GameObject* gameObject);
	void removeGameObject(GameObject* gameObject);

	GameObject* find(std::string name);

	///<summary> Finds a component of the specified type. </summary>
	///<remarks> The type must extend Component </remarks>
	///<returns> Returns the found Component. Otherwise returns null </returns>
	template <typename T>
	T* GetComponentInWorld()
	{
		for (size_t i = 0; i < gameObjects.size(); i++)
		{
			GameObject* gameObject = gameObjects[i];
			T* foundComponent = gameObject->GetComponent<T>();
			if (foundComponent != nullptr)
				return foundComponent;
		}

		return nullptr;
	}

	///<summary> Finds components of the specified type. </summary>
	///<remarks> The type must extend Component. </remarks>
	///<returns> Returns the found Components. Otherwise returns a empty vector. </returns>
	template <typename T>
	std::vector<T*> GetComponentsInWorld()
	{
		std::vector<T*> foundComponents = std::vector<T*>();
		for (size_t i = 0; i < gameObjects.size(); i++)
		{
			GameObject* gameObject = gameObjects[i];
			std::vector<T*> components = gameObject->GetComponents<T>();
			foundComponents.insert(foundComponents.end(), components.begin(), components.end);
		}
		return foundComponents;
	}


	void safeDelete();

	Universe* getUniverse();
	EngineSystems* getEngineSystems();
};