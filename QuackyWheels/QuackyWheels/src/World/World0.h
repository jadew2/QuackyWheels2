#pragma once

#include "World.h"

#include "PxPhysicsAPI.h"
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Quaternion.h"
#include "../Components/ObjectType.h"
#include "../Various/LevelSettings/LevelSettings.h"

class Universe;
class World0 : public World
{
public:
	World0(Universe* universe, LevelSettings levelSettings);
	virtual ~World0();
};

