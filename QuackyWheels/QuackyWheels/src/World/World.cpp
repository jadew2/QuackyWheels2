#include "World.h"

#include "Universe.h"
#include "../ComponentEntity/GameObject.h"
#include "../GameLoop/EngineSystems.h"
#include "../Script/Scripting.h"
#include <algorithm>
#include "../Script/Deleter.h"
#include "../Physics/Physics.h"

#include <vector>
using namespace std;

World::World(Universe* universeRef)
{
	gameObjects = vector<GameObject*>();
	universe = universeRef;
	universe->addWorld(this);

	physx::PxScene* scene = universe->getEngineSystems()->physics->getScene();
	physx::PxActorTypeFlags flags = physx::PxActorTypeFlag::eRIGID_DYNAMIC | physx::PxActorTypeFlag::eRIGID_STATIC;
	physx::PxU32 numberOfActors = scene->getNbActors(flags);
	vector<physx::PxActor*> actors = vector < physx::PxActor*>();
	actors.resize(numberOfActors);
	
	scene->getActors(flags, actors.data(), numberOfActors);
	scene->removeActors(actors.data(),numberOfActors,false);
}

World::~World()
{
	for (size_t i = 0; i < gameObjects.size(); i++)
		delete gameObjects[i];
	gameObjects.clear();

	universe->removeWorld(this);

	getEngineSystems()->scripting->worldDeleter->NullUnQueue(this);
}

void World::addGameObject(GameObject* gameObject)
{
	gameObjects.push_back(gameObject);
}

void World::removeGameObject(GameObject* gameObject)
{
	for (size_t i = 0; i < gameObjects.size(); i++)
	{
		if (gameObjects[i] == gameObject)
		{
			gameObjects.erase(gameObjects.begin() + i);
			return;
		}
	}
}

void World::safeDelete()
{
	for (size_t i = 0; i < gameObjects.size(); i++)
		gameObjects[i]->SafeDelete();

	getEngineSystems()->scripting->worldDeleter->Queue(this);
}

Universe* World::getUniverse()
{
	return universe;
}

EngineSystems* World::getEngineSystems()
{
	return getUniverse()->getEngineSystems();
}

GameObject* World::find(std::string name)
{
	for (size_t i = 0; i < gameObjects.size(); i++)
	{
		if (gameObjects[i]->Name == name)
			return gameObjects[i];
	}

	return nullptr;
}
