//
// Created by Jade White on 2016-02-27.
//

#include "Universe.h"

#include <string>
#include <algorithm>

#include "World.h"
#include "../Exceptions/EngineException.h"

Universe::Universe(EngineSystems* engineSystemsRef) {
	worlds = std::vector<World*>();
	engineSystems = engineSystemsRef;
}

Universe::~Universe() 
{
	while (worlds.size() > 0)
		delete worlds[0];
}

void Universe::addWorld(World* world) {
	worlds.push_back(world);
}

void Universe::removeWorld(World* world)
{
	worlds.erase(std::remove(worlds.begin(),worlds.end(),world),worlds.end());
}

int Universe::getNumberOfWorlds() {
	return (int)worlds.size();
}

World* Universe::getWorld(int index) {
	if (index >= (int)worlds.size() || index < 0) {
		throw new EngineException(std::string("Universe: GetWorld: World index ") + std::to_string(index) + std::string(" out of range!"));
	}
	return worlds[index];
}

void Universe::safeDelete() {
	for (auto world : worlds) {
		world->safeDelete();
	}
}

EngineSystems* Universe::getEngineSystems() {
	return engineSystems;
}

GameObject* Universe::find(std::string name)
{
	for (auto world : worlds)
	{
		GameObject* worldGO = world->find(name);
		if (worldGO != nullptr)
			return worldGO;
	}

	return nullptr;
}