//
// Created by Jade White on 2016-04-13.
//

#pragma once


#include "../ComponentEntity/ScriptBehaviour.h"
#include <vector>

class Camera;
///<summary> Resizes the camera views of the passed cameras depending on how many there are. </summary>
class CameraResizer : public ScriptBehaviour
{
public:
	CameraResizer(GameObject* gameObject);

	void resizeCameras(std::vector<Camera*> cameras);
};


