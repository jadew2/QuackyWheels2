#include "BasicRotate.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"

#include "../../BaseTypes/Quaternion.h"
#include "../../Components/Transform.h"

#include <string>
using namespace std;

BasicRotate::BasicRotate(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	transform = GetTransform();
	time = getEngineSystems()->time;

	updateType = UpdateType::Default;
	method = Method::Incremental;
}

void BasicRotate::Update()
{
	if (updateType != UpdateType::Default)
		return;
	doRotate();
}


void BasicRotate::AfterUpdate()
{
	if (updateType != UpdateType::Late)
		return;
	doRotate();
}


void BasicRotate::doRotate()
{
	float usedTime = 0.0f;
	if (method == Incremental)
	{
		usedTime = time->GetDeltaTime();
	}
	else if (method == Additive)
	{
		usedTime = time->GetTime();
	}
	else
		throw new EngineException(string("BasicRotate: doRotate: Unknown method type :("));

	Vector3 wantedRotation = rotation * (speed * usedTime);
	Quaternion rotationDiff = Quaternion::Euler(wantedRotation);
	transform->SetLocalRotation(transform->GetLocalRotation() * rotationDiff);
}


BasicRotate::~BasicRotate()
{
}
