#include "TextureSlider.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"

#include "../../Renderer/Types/Material.h"

TextureSlider::TextureSlider(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	time = getEngineSystems()->time;
	material = nullptr;
}


TextureSlider::TextureSlider(GameObject* gameObject, Material* material) : TextureSlider(gameObject)
{
	this->material = material;
}


void TextureSlider::Update()
{
	if (material == nullptr)
		return;

	Vector2 wantedOffset = material->getVector2(propertyName) + offsetSpeed * time->GetDeltaTime();
	material->setVector2(propertyName, wantedOffset);
}

TextureSlider::~TextureSlider()
{
}