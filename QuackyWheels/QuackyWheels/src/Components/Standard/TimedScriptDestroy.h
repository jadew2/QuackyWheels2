//
// Created by Jade White on 2016-03-24.
//

#pragma once


#include "TimedAction.h"

class GameObject;
///<summary> A class that destroys the gameObject it's attached to after x seconds. </summary>
class TimedScriptDestroy : public TimedAction
{
	ScriptBehaviour* behaviour;
public:
	TimedScriptDestroy(GameObject* gameObject, ScriptBehaviour* behaviour);
	TimedScriptDestroy(GameObject* gameObject, ScriptBehaviour* behaviour, float timeForDestroy);

	void DoAction() override;
};

