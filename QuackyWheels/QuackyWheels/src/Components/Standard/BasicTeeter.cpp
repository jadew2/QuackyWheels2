#include "BasicTeeter.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"

#include "../../BaseTypes/Quaternion.h"
#include "../../Components/Transform.h"
#include "../../ComponentEntity/GameObject.h"

#include <math.h>

BasicTeeter::BasicTeeter(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	transform = gameObject->GetTransform();
	time = getEngineSystems()->time;

	axis = Vector3::Forward();
	speed = 0.5f;
	angle = 20.0f;
	initialRotation = Quaternion::Identity();
}

void BasicTeeter::Start()
{
	initialRotation = transform->GetLocalRotation();
}

void BasicTeeter::Update()
{
	float newAngle = std::cos(time->GetTime()*speed) * angle;
	Quaternion newRotation = initialRotation * Quaternion::AngleAxis(newAngle, axis);
	transform->SetLocalRotation(newRotation);
}


