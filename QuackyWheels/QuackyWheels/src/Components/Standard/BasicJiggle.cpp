//
// Created by Jade White on 2016-03-23.
//

#include "BasicJiggle.h"

#include "../../Components/Transform.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"

#include <math.h>

BasicJiggle::BasicJiggle(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	transform = GetTransform();
	time = getEngineSystems()->time;

	scaleDifference = Vector3::Zero();
	addedScale = Vector3::Up();
	speed = 1.0f;
}


void BasicJiggle::Update()
{
	//Undo our scale change from last time.
	transform->SetLocalScale(transform->GetLocalScale() - scaleDifference);

	//Calculate the new scale changes
	float multiplier = cosf(time->GetTime()*speed * 6.0f) + 1.0f; //+1 to keep it between 0 and 1., *6 to make is so 1 loop ~= 1 second
	scaleDifference = addedScale * multiplier;

	//Add it
	transform->SetLocalScale(transform->GetLocalScale() + scaleDifference);
}

