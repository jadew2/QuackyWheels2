//
// Created by Jade White on 2016-03-26.
//

#include "BasicMove.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"
#include "../../Components/Transform.h"

#include <string>
using namespace std;


BasicMove::BasicMove(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	velocity = Vector3::Zero();

	time = getEngineSystems()->time;
	transform = GetTransform();
}

BasicMove::~BasicMove()
{

}

void BasicMove::Update()
{
	transform->SetLocalPosition(transform->GetLocalPosition() + velocity * time->GetDeltaTime());
}

