//
// Created by Jade White on 2016-03-24.
//

#include "TimedScriptDestroy.h"
#include "../../ComponentEntity/GameObject.h"


TimedScriptDestroy::TimedScriptDestroy(GameObject* gameObject, ScriptBehaviour* behaviour) : TimedAction(gameObject)
{
	this->behaviour = behaviour;
}

TimedScriptDestroy::TimedScriptDestroy(GameObject* gameObject, ScriptBehaviour* behaviour, float timeForDestroy) : TimedScriptDestroy(gameObject, behaviour)
{
	this->timeForAction = timeForDestroy;
}


void TimedScriptDestroy::DoAction()
{
	//Destroy the gameObject
	if (behaviour != nullptr)
		behaviour->SafeDelete();
	this->SafeDelete();
}

