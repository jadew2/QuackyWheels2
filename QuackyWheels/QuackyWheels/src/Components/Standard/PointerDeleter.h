//
// Created by Jade White on 2016-04-01.
//

#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"
#include <vector>
using namespace std;

///<summary> Deletes every elements in pointers on delete </summary>
template <class T>
class PointerDeleter : public ScriptBehaviour
{
public:
	PointerDeleter(GameObject* gameObject) : ScriptBehaviour(gameObject)
	{
		pointers = vector<T*>();
	}

	virtual ~PointerDeleter()
	{
		for (size_t i = 0; i < pointers.size(); ++i)
			delete pointers[i];

		pointers.clear();
	}

	std::vector<T*> pointers;
};


