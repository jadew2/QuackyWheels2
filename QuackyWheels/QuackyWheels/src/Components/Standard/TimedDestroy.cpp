//
// Created by Jade White on 2016-03-24.
//

#include "TimedDestroy.h"
#include "../../ComponentEntity/GameObject.h"


TimedDestroy::TimedDestroy(GameObject* gameObject) : TimedAction(gameObject)
{

}

TimedDestroy::TimedDestroy(GameObject* gameObject, float timeForDestroy) : TimedDestroy(gameObject)
{
	this->timeForAction = timeForDestroy;
}


void TimedDestroy::DoAction()
{
	//Destroy the gameObject
	GetGameObject()->SafeDelete();
}

