//
// Created by Jade White on 2016-03-23.
//

#pragma once


#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../Time/Time.h"
#include "../../BaseTypes/Vector3.h"

///<summary> Animates the scale of the object so it "jiggles". Uses the local scale of the gameObject it's attached to.</summary>
class BasicJiggle : public ScriptBehaviour
{
private:
	Time* time = nullptr;
	Transform* transform = nullptr;

	///<summary> The difference added to the scale last frame. We use this to reset it, so you can layer other effects on.</summary>
	Vector3 scaleDifference;
public:
	///<summary> The scale added to the local scale (This is animated by cos) </summary>
	Vector3 addedScale;
	///<summary> The time for one full loop of the jiggle. 1 ~= 1 loop/sec, 2.5 ~= 2.5 loops/sec... </summary>
	float speed;

	BasicJiggle(GameObject* gameObject);

protected:
	virtual void Update() override;
};

