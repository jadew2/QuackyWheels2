//
// Created by Jade White on 2016-03-24.
//

#pragma once


#include "TimedAction.h"

class GameObject;
///<summary> A class that destroys the gameObject it's attached to after x seconds. </summary>
class TimedDestroy : public TimedAction
{
public:
	TimedDestroy(GameObject* gameObject);
	TimedDestroy(GameObject* gameObject, float timeForDestroy);

	void DoAction() override;
};

