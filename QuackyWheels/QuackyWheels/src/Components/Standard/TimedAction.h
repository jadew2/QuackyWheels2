//
// Created by Jade White on 2016-03-24.
//

#pragma once


#include "../../ComponentEntity/ScriptBehaviour.h"

class Time;

///<summary> Executes DoAction after timeForAction seconds. Override to implement fancy functionality. </summary>
class TimedAction : public ScriptBehaviour
{
private:
	Time* time;
	float elaspedTime = 0;
public:
	float timeForAction = 1;

	TimedAction(GameObject* gameObject);
	TimedAction(GameObject* gameObject, float timeForAction);

protected:
	virtual void Start() override;
	virtual void Update() override;

	virtual void DoAction();
};


