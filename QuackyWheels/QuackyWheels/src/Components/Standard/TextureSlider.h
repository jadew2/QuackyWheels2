#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"

#include "../../BaseTypes/Vector2.h"
#include <string>
class Time;
class Material;

///<summary> Slides a vector2 on the shader each frame </summary>
class TextureSlider : public ScriptBehaviour
{
private:
	Time* time = nullptr;
public:
	Material* material = nullptr;
	std::string propertyName = std::string("mainTex0Offset");
	Vector2 offsetSpeed = Vector2::Zero();

	TextureSlider(GameObject* gameObject);
	TextureSlider(GameObject* gameObject, Material* material);
	virtual ~TextureSlider();

protected:
	virtual void Update() override;
};
