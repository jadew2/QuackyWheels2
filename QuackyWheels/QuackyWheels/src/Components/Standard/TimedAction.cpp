//
// Created by Jade White on 2016-03-24.
//

#include "TimedAction.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"
#include "../../ComponentEntity/GameObject.h"

TimedAction::TimedAction(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	time = getEngineSystems()->time;
}

TimedAction::TimedAction(GameObject* gameObject, float timeForAction) : TimedAction(gameObject)
{
	this->timeForAction = timeForAction;
}


void TimedAction::Start()
{
	elaspedTime = 0;
}

void TimedAction::Update()
{
	elaspedTime += time->GetDeltaTime();
	if (elaspedTime > timeForAction)
	{
		elaspedTime = -1.0f;

		DoAction();
	}
}


void TimedAction::DoAction()
{
	//..To be overriden
}


