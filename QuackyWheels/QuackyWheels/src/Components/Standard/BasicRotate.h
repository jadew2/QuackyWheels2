#pragma once
#include "../../ComponentEntity/ScriptBehaviour.h"

#include "../../BaseTypes/Vector3.h"
//Rotates the object it's attached to on one axis.
class Time;
class BasicRotate : public ScriptBehaviour
{
private:
	Time* time = nullptr;
	Transform* transform = nullptr;
public:
	enum UpdateType
	{
		Default,  //Use Update
		Late, //Use AfterUpdate
	};

	enum Method
	{
		Incremental,  //Adds small rotations each frame
		Additive, //Adds the full rotation each frame
	};

	Vector3 rotation = Vector3::Zero();
	float speed = 1.0f;
	UpdateType updateType = UpdateType::Default;
	Method method = Method::Incremental;

	BasicRotate(GameObject* gameObject);
	virtual ~BasicRotate();

protected:
	virtual void Update() override;
	virtual void AfterUpdate() override;

	void doRotate();

};

