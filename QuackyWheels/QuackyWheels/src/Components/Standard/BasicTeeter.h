#pragma once
#include "../../ComponentEntity/ScriptBehaviour.h"

#include "../../BaseTypes/Vector3.h"
#include "../../BaseTypes/Quaternion.h"

//Rotates the object it's attached to on one axis.
class Time;
class Transform;
class GameObject;
class BasicTeeter : public ScriptBehaviour
{
private:
	Time* time;
	Transform* transform;
	Quaternion initialRotation;
public:
	Vector3 axis;
	float speed;
	float angle;

	BasicTeeter(GameObject* gameObject);

protected:
	virtual void Start() override;
	virtual void Update() override;
};

