//
// Created by Jade White on 2016-03-26.
//

#pragma once
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../BaseTypes/Vector3.h"

class Time;
class Transform;
class BasicMove : public ScriptBehaviour
{
private:
	Time* time = nullptr;
	Transform* transform = nullptr;
public:
	Vector3 velocity;

	BasicMove(GameObject* gameObject);
	virtual ~BasicMove();

protected:
	virtual void Update() override;
};


