//
// Created by Jade White on 2016-03-22.
//

#pragma once


#include <vector>
#include "../../BaseTypes/Vector3.h"

class TrailMeshSegment
{
public:
	Vector3 position = Vector3::Zero();
	std::vector<Vector3> vertices = std::vector<Vector3>();

	TrailMeshSegment();
};


