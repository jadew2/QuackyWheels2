//
// Created by Jade White on 2016-03-22.
//

#pragma once


#include "../../ComponentEntity/ScriptBehaviour.h"
#include "TrailMeshSegment.h"
#include "../../Renderer/Types/Color.h"
#include "../../Various/ListLerper.h"

#include <deque>
#include <vector>

class Time;
class Mesh;
class TrailMesh : public ScriptBehaviour
{
private:
	Transform* transform = nullptr;
	Time* timeRef = nullptr;
	Mesh* trailMesh = nullptr;

	//Memory to save for the length of the trail
	//const unsigned long reservedSegments = 1000;
	std::deque<TrailMeshSegment> segments;

	ListLerper<Color> colorLerper;
public:
	unsigned int maxSegments = 10;
	float distanceForSegment;
	float maxDistanceForSegment;
	std::vector<Color> colors;

	TrailMesh(GameObject* gameObject);
	virtual ~TrailMesh();

	///<summary> Returns the trail mesh... mesh... </summary>
	Mesh* getMesh();

protected:
	virtual void AfterUpdate() override;


private:

	///<summary> Adds a segment to the front of the queue </summary>
	void addSegment(Vector3 position, bool connected = true);

	///<summary> removes a segment from the end of the queue </summary>
	void removeSegment();

	///<summary> Returns the color for the specified lifetime. Linearly interpolates the color from all the colors in the colors array. </summary>
	Color getColorForIndex(unsigned int index);
};





