//
// Created by Jade White on 2016-03-22.
//

#include "TrailMesh.h"

#include "../../Components/Transform.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../Time/Time.h"
#include "../../BaseTypes/Vector3.h"
#include "../../Renderer/Types/Mesh.h"

using namespace std;

TrailMesh::TrailMesh(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	transform = GetTransform();
	timeRef = getEngineSystems()->time;
	trailMesh = new Mesh();

	segments = deque<TrailMeshSegment>();

	maxSegments = 50;
	distanceForSegment = 1.0f;
	maxDistanceForSegment = 20.0f;
	colors = vector<Color>();
}

TrailMesh::~TrailMesh()
{
	delete trailMesh;
}


void TrailMesh::AfterUpdate()
{
	Vector3 position = transform->GetPosition();
	float deltaTime = timeRef->GetDeltaTime();

	if (segments.size() <= 0)
		addSegment(position);
	else if ((position - segments[0].position).Magnitude() > distanceForSegment)
		addSegment(position);

	if (segments.size() > maxSegments)
		removeSegment();


	//Now generate the mesh
	trailMesh->verticies.clear();

	for (size_t k = 0; k < segments.size(); ++k)
	{
		TrailMeshSegment segment = segments[k];

		for (size_t i = 0; i < segment.vertices.size(); ++i)
			trailMesh->verticies.push_back(segment.vertices[i] + position);


	}


	trailMesh->colors.resize(trailMesh->verticies.size());

	//For each segment...
	for (size_t i = 0; i < segments.size(); ++i)
	{
		//Set the colors
		Color wantedColor = getColorForIndex(i);
		//for (size_t j = 0; j < vertexIndicies.size(); ++j)
		//	trailMesh->colors[vertexIndicies[i]] = wantedColor;

	}

}


Mesh* TrailMesh::getMesh()
{
	return trailMesh;
}


void TrailMesh::addSegment(Vector3 position, bool connected)
{
	//Do mesh modifications
	TrailMeshSegment newSegment = TrailMeshSegment();
	newSegment.vertices = vector<Vector3>{Vector3(-0.5f,0,0),Vector3(0.5f,0,0)};
	newSegment.position = position;
	segments.push_front(newSegment);
}


void TrailMesh::removeSegment()
{
	//Do mesh modifications
	TrailMeshSegment segment = segments.back();
	segments.pop_back();
}


Color TrailMesh::getColorForIndex(unsigned int index)
{
	return colorLerper.getValueForT(colors, (float)index / (float)maxSegments-1);
}



