#pragma once
#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidDynamic;
class Transform;
class PhysicsRigidDynamicSphere : PhysicsShape {

public:
	PhysicsRigidDynamicSphere(GameObject* gameObject, PhysicsRigidDynamic* actor, physx::PxReal radius,
		Transform* transform, physx::PxMaterial* const* materials, physx::PxFilterData* qryFilterData,
		physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidDynamicSphere();

	void setTransform(Transform* transform);

	PhysicsRigidDynamic* getActor();

protected:
	PhysicsRigidDynamic* actor;
};

