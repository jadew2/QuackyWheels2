#pragma once

#include <vector>

#include "PxPhysicsAPI.h"
#include "../ComponentEntity/ScriptBehaviour.h"

class WinLoseMessage;
class GameFinishTracker : public ScriptBehaviour
{
	//Unique list of triggers
	std::vector<int> aiVisitiedTriggers = std::vector<int>();
	//Unique List of triggers
	std::vector<int> playerVisitiedTriggers = std::vector<int>();

	WinLoseMessage* winLose = nullptr;

	physx::PxActor* player = nullptr;
	physx::PxActor* aiPlayer = nullptr;
	std::vector<physx::PxActor*> orderedTriggers = std::vector<physx::PxActor*>();

public:
	int numberOfTriggers = 4;

	GameFinishTracker(GameObject* gameObject, WinLoseMessage* winLoseMessageComponent, physx::PxActor* playerRef, physx::PxActor* aiPlayerRef, std::vector<physx::PxActor*> orderedtriggersRef);
	virtual ~GameFinishTracker();

protected:
	virtual void PhysicsUpdate() override;

private:
	int findTriggerIndex(physx::PxActor* trigger);

	//Returns true if the trigger is next to be visited.
	void visit(int triggerIndex, bool isAIPlayer);
	void checkIfComplete();
};

