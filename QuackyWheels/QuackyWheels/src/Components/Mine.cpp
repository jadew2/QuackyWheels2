#include "Mine.h"

#include "VehicleManager.h"
#include "Transform.h"
#include "../Prefabs/Explosion0Prefab.h"
#include "../Prefabs/MinePrefab.h"


Mine::Mine(GameObject* gameObject, VehicleManager* vehicleManager, int index) : ScriptBehaviour(gameObject)
{
	this->vehicleManager = vehicleManager;

	physx::PxTransform vehicleTransform = vehicleManager->getVehicle(index)->getRigidDynamicActor()->getGlobalPose();
	Vector3 position = Vector3(vehicleTransform.p) + Quaternion(vehicleTransform.q) * Vector3::Down() * 2.05f;
	Quaternion rotation = Quaternion(vehicleTransform.q);
	Vector3 backwards = rotation * Vector3::Backward() * 5.0f;

	gameObject->GetTransform()->SetLocalPosition(position + backwards);
	gameObject->GetTransform()->SetLocalRotation(rotation);

	//Create the mine mesh
	GameObject* mineModel = new MinePrefab(GetGameObject()->getWorld());
	mineModel->GetTransform()->SetParent(gameObject->GetTransform(),false);
}

Mine::~Mine()
{
}

void Mine::Hit()
{
	Explosion0Prefab* explosion = new Explosion0Prefab(this->GetGameObject()->getWorld());
	explosion->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition());
	explosion->GetTransform()->SetLocalRotation(this->GetGameObject()->GetTransform()->GetLocalRotation());

	this->GetGameObject()->SafeDelete();

}

void Mine::Update() {
	for (unsigned int i = 0; i < vehicleManager->getNbVehicles(); i++) {
		if ((vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p - this->GetGameObject()->GetTransform()->GetLocalPosition()).magnitude() < 4.5f) {
			vehicleManager->getVehicle(i)->getRigidDynamicActor()->setLinearVelocity(physx::PxVec3(0.0, 20.0f, 0.0));
			this->Hit();
		}
	}
}
