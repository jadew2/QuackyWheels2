#include "Missile.h"

#include "VehicleManager.h"
#include "Transform.h"
#include "VehiclePositionTracker.h"
#include "../Prefabs/MissilePrefab.h"
#include "../Prefabs/Explosion0Prefab.h"
#include "../Prefabs/MissileSmokePrefab.h"
#include "SoundEffects.h"


Missile::Missile(GameObject *gameObject, VehiclePositionTracker* vehiclePositionTracker, VehicleManager* vehicleManager, int index) : ScriptBehaviour(gameObject)
{
	alive = time(0);

	this->vehicleManager = vehicleManager;
	this->vehiclePositionTracker = vehiclePositionTracker;
	this->vehicleIndex = index;
	this->missileSpeed = 5.0f;

	physx::PxTransform physxVehPos = vehicleManager->getVehicle(index)->getRigidDynamicActor()->getGlobalPose();

	Vector3 vehiclePosition = Vector3(physxVehPos.p);
	Quaternion vehicleRotation = Quaternion(physxVehPos.q);

	gameObject->GetTransform()->SetLocalPosition(vehiclePosition + Vector3(0, 2, 0));
	gameObject->GetTransform()->SetLocalRotation(vehicleRotation);

	MissilePrefab* mp = new MissilePrefab(gameObject->getWorld());
	mp->GetTransform()->SetParent(gameObject->GetTransform(), false);
}

Missile::~Missile()
{

}

void Missile::Start()
{
	ScriptBehaviour::Start();

	trackIndex = vehiclePositionTracker->getIndex()[vehicleIndex] + 1;
	destination = Vector3(vehiclePositionTracker->getTrackSpline()[trackIndex % vehiclePositionTracker->getTrackSpline().size()]);
	travelPath = destination - this->GetGameObject()->GetTransform()->GetLocalPosition();
}

void Missile::PhysicsUpdate()
{
	int place = vehiclePositionTracker->getPlaces()[vehicleIndex];

	Vector3 target;

	if (place != 1)
	{
		for (unsigned int i = 0; i < vehicleManager->getNbVehicles(); i++)
		{
			if (vehiclePositionTracker->getPlaces()[i] == place - 1)
			{
				target = Vector3(vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p);
				targetVehicle = vehicleManager->getVehicle(i);
			}
		}

		if ((this->GetGameObject()->GetTransform()->GetLocalPosition() - target).Magnitude() < 20.0f)
		{
			travelPath = target - this->GetGameObject()->GetTransform()->GetLocalPosition();
			if (travelPath.Magnitude() < 5.0f) {
				Hit();
			}
		}
		else if ((destination - this->GetGameObject()->GetTransform()->GetLocalPosition()).Magnitude() < 7.0f)
		{
			trackIndex++;
			destination = Vector3(vehiclePositionTracker->getTrackSpline()[(trackIndex) % vehiclePositionTracker->getTrackSpline().size()]);
			travelPath = destination - this->GetGameObject()->GetTransform()->GetLocalPosition();
		}
	}

	Vector3 travelVec = travelPath * (missileSpeed / travelPath.Magnitude());

	this->GetGameObject()->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition() + travelVec);
	this->GetGameObject()->GetTransform()->SetLocalRotation(Quaternion::LookRotation((travelPath)*(-1),Vector3(0,1,0)));

	double seconds_since_start = difftime(time(0), alive);
	if (seconds_since_start > 15.0)
	{
		Explosion0Prefab* explosion = new Explosion0Prefab(this->GetGameObject()->getWorld());
		explosion->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition());

		//Delete this missile
		this->GetGameObject()->SafeDelete();
	}

	frameCounter++;
	if (frameCounter % 6 == 0)
	{
		MissileSmokePrefab* missileSmoke = new MissileSmokePrefab(GetGameObject()->getWorld());
		missileSmoke->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition());
	}
}

void Missile::Hit()
{
	targetVehicle->getRigidDynamicActor()->setLinearVelocity(physx::PxVec3(0.0, 30.0f, 0.0));

	Explosion0Prefab* explosion = new Explosion0Prefab(this->GetGameObject()->getWorld());
	explosion->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition());

	//Delete this missile
	this->GetGameObject()->SafeDelete();
}

