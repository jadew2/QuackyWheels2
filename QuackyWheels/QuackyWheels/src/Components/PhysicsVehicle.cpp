#include "PhysicsVehicle.h"

#include "../GameLoop/EngineSystems.h"
#include "Transform.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Vehicle/VehicleTireFriction.h"
#include "../Vehicle/VehicleCooking.h"
#include "../Vehicle/VehicleDesc.h"
#include "../Components/VehicleManager.h"
#include "../Components/PhysicsRigidDynamic.h"
#include "../Components/VehicleTrail.h"
#include "../ComponentEntity/GameObject.h"
#include "../Input/Controller.h"
#include "../Input/DrivingInput.h"
#include "../Physics/Physics.h"
#include "Mine.h"
#include "Missile.h"
#include "Countdown.h"
#include "PhysicsVehicleActor.h"
#include "PhysicsRigidDynamicConvexMesh.h"
#include "Camera.h"
#include "AudioSource.h"
#include "../Prefabs/Explosion0Prefab.h"
#include "../Prefabs/SpeedPowerUpPrefab.h"

PhysicsVehicle::PhysicsVehicle(GameObject* gameObject, physx::PxTransform* transform, ObjectType type, VehicleManager* vehicleManager, unsigned long trackNumber) : ScriptBehaviour(gameObject) {

	this->type = type;
	start = false;

	//Set vehicle manager
	this->vehicleManager = vehicleManager;
	this->trackNumber = trackNumber;
	vehicleDesc = initVehicleDesc(this->getEngineSystems()->physics->getPhysics());

	//Create the wheel convex mesh
	physx::PxVec3 points[2 * 16];
	for (physx::PxU32 i = 0; i < 16; i++) {
		const physx::PxF32 cosTheta = physx::PxCos(i*physx::PxPi*2.0f / 16.0f);
		const physx::PxF32 sinTheta = physx::PxSin(i*physx::PxPi*2.0f / 16.0f);
		const physx::PxF32 y = vehicleDesc.wheelRadius*cosTheta;
		const physx::PxF32 z = vehicleDesc.wheelRadius*sinTheta;
		points[2 * i + 0] = physx::PxVec3(-vehicleDesc.wheelWidth / 2.0f, y, z);
		points[2 * i + 1] = physx::PxVec3(+vehicleDesc.wheelWidth / 2.0f, y, z);
	}

	//Create the chassis convex mesh
	const physx::PxF32 x = vehicleDesc.chassisDims.x*0.5f;
	const physx::PxF32 y = vehicleDesc.chassisDims.y*0.5f;
	const physx::PxF32 z = vehicleDesc.chassisDims.z*0.5f;
	physx::PxVec3 verts[8] = {
		physx::PxVec3(x, y, -z),
		physx::PxVec3(x, y, z),
		physx::PxVec3(x, -y, z),
		physx::PxVec3(x, -y, -z),
		physx::PxVec3(-x, y, -z),
		physx::PxVec3(-x, y, z),
		physx::PxVec3(-x, -y, z),
		physx::PxVec3(-x, -y, -z)
	};

	physx::PxFilterData wheelQryFilterData;
	setupNonDrivableSurface(wheelQryFilterData);
	physx::PxFilterData chassisQryFilterData;
	setupNonDrivableSurface(chassisQryFilterData);

	physx::PxFilterData wheelSimFilterData;
	wheelSimFilterData.word0 = COLLISION_FLAG_WHEEL;
	wheelSimFilterData.word1 = COLLISION_FLAG_WHEEL_AGAINST;

	physx::PxFilterData chassisSimFilterData;
	chassisSimFilterData.word0 = COLLISION_FLAG_CHASSIS;
	chassisSimFilterData.word1 = COLLISION_FLAG_CHASSIS_AGAINST;

	physx::PxMaterial* carMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.5f, 0.5f, 0.6f);
	physx::PxMaterial* const* material2 = &carMaterial;

	physx::PxShapeFlags shapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	PhysicsVehicleActor* playerVehicleActor = new PhysicsVehicleActor(gameObject, gameObject->GetTransform(), ObjectType::Chassis, 1.0f, vehicleDesc.chassisMass, &vehicleDesc.chassisMOI, &vehicleDesc.chassisCMOffset);
	PhysicsRigidDynamicConvexMesh* playerVehicleFrontLeftWheel = new PhysicsRigidDynamicConvexMesh(gameObject, playerVehicleActor, 32, points, 1, material2, gameObject->GetTransform(), &wheelQryFilterData, &wheelSimFilterData, &shapeFlags);
	PhysicsRigidDynamicConvexMesh* playerVehicleFrontRightWheel = new PhysicsRigidDynamicConvexMesh(gameObject, playerVehicleActor, 32, points, 1, material2, gameObject->GetTransform(), &wheelQryFilterData, &wheelSimFilterData, &shapeFlags);
	PhysicsRigidDynamicConvexMesh* playerVehicleBackLeftWheel = new PhysicsRigidDynamicConvexMesh(gameObject, playerVehicleActor, 32, points, 1, material2, gameObject->GetTransform(), &wheelQryFilterData, &wheelSimFilterData, &shapeFlags);
	PhysicsRigidDynamicConvexMesh* playerVehicleBackRightWheel = new PhysicsRigidDynamicConvexMesh(gameObject, playerVehicleActor, 32, points, 1, material2, gameObject->GetTransform(), &wheelQryFilterData, &wheelSimFilterData, &shapeFlags);
	PhysicsRigidDynamicConvexMesh* playerVehicleChassis = new PhysicsRigidDynamicConvexMesh(gameObject, playerVehicleActor, 8, verts, 1, material2, gameObject->GetTransform(), &chassisQryFilterData, &chassisSimFilterData, &shapeFlags);

	vehicle4W = createVehicle4W(vehicleDesc, playerVehicleActor, this->getEngineSystems()->physics->getPhysics());

	vehicleInput = physx::PxVehicleDrive4WRawInputData();
	setSmoothingData();
	setSteerSpeedData();

	vehicle4W->getRigidDynamicActor()->setGlobalPose(*transform);
	previousVehiclePosition = transform->p;
	this->getEngineSystems()->physics->getScene()->addActor(*vehicle4W->getRigidDynamicActor());

	distance = 5.0f;

	VehicleTrail* vehicleTrail = new VehicleTrail(gameObject, this->getEngineSystems()->physics->getScene(), this);

	this->trail = vehicleTrail;

	//Set the vehicle to rest in first gear.
	//Set the vehicle to use auto-gears.
	vehicle4W->setToRestState();
	vehicle4W->mDriveDynData.forceGearChange(physx::PxVehicleGearsData::eFIRST);
	vehicle4W->mDriveDynData.setUseAutoGears(true);

	vehicle4W->getRigidDynamicActor()->setLinearVelocity(physx::PxVec3(0.0, 0.0, 0.0));

	inTrail = false;
	availablePowerUp = 0;
	inUsePowerUp = 0;
	powerUpTimer = 0;
	slowDown = false;
	laps = 0;
	checkpoint = false;
	reset = false;
	manualReset = false;
	topSpeed = 80.0f;
	positionTrackerPosition = vehicle4W->getRigidDynamicActor()->getGlobalPose().p;
}


PhysicsVehicle::~PhysicsVehicle() {

}

void PhysicsVehicle::PhysicsUpdate() {

	for (size_t i = 0; i < obstacles.size(); i++) {
		if ((vehicle4W->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, 0, 1)).dot(obstacles[i]->getRigidDynamic()->getGlobalPose().p - vehicle4W->getRigidDynamicActor()->getGlobalPose().p) > 0) == obstacleRelativePos[i]) {
			if ((abs(vehicle4W->computeForwardSpeed()) > 30.0f) &&
				((vehicle4W->getRigidDynamicActor()->getGlobalPose().p - obstacles[i]->getRigidDynamic()->getGlobalPose().p).magnitude() < 20.0f)) {
				AudioSource* whoosh = this->GetGameObject()->GetComponent<AudioSource>();
				if (whoosh) {
					if (!whoosh->IsPlaying()) {
						whoosh->Play(0);
					}
				}
			}
			obstacleRelativePos[i] = !obstacleRelativePos[i];
		}
	}


	if (!start)
	{
		countdown = new Countdown(this->GetGameObject());
		start = true;
	}
	if (!countdown->isCountdownFinished())
	{
		return;
	}
	DrivingInput* controllerInput = getEngineSystems()->controller->getControllerInput();

	if (controllerInput[vehicleIndex].reset == true) {
		manualReset = true;
	}

	physx::PxVec3 offset = physx::PxVec3(0.0f, -vehicleDesc.chassisDims.y * 0.5f - 0.1f, 0.0f);

	offset = vehicle4W->getRigidDynamicActor()->getGlobalPose().rotate(offset);

	physx::PxVec3 origin = vehicle4W->getRigidDynamicActor()->getGlobalPose().p + offset;

	physx::PxVec3 direction = vehicle4W->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, -1, 0));

	this->getEngineSystems()->physics->getScene()->raycast(origin, direction, distance, buffer);

	if (buffer.hasBlock) {
		physx::PxRaycastHit hit = buffer.block;
		hit.normal.normalize();
		physx::PxVec3 vehUp = vehicle4W->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, 1, 0));
		physx::PxReal cos_theta = vehUp.dot(hit.normal);
		if (cos_theta > 1.0f) {
			cos_theta = 1.0f;
		}
		else if (cos_theta < -1.0f) {
			cos_theta = -1.0f;
		}
		physx::PxReal angle = acos(cos_theta);
		if (angle > 0.0872665f) {
			angle = 0.0872665f;
		}
		else if (angle < -0.0872665f) {
			angle = -0.0872665f;
		}
		physx::PxVec3 w = vehicle4W->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, 1, 0)).cross(hit.normal);
		w.normalize();
		physx::PxTransform transform = physx::PxTransform(vehicle4W->getRigidDynamicActor()->getGlobalPose().p, physx::PxQuat(angle, w) * vehicle4W->getRigidDynamicActor()->getGlobalPose().q);
		
		vehicle4W->getRigidDynamicActor()->setGlobalPose(transform);
		positionTrackerPosition = vehicle4W->getRigidDynamicActor()->getGlobalPose().p;


		Vector3 linearVelocity = vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getLinearVelocity();
		//Stop the player from moving if we're below this velocity this frame.
		if (linearVelocity.Magnitude() < 2.0f && controllerInput[vehicleIndex].acceleration == 0 && controllerInput[vehicleIndex].brake == 0 && hit.distance < 0.9f)
		{
			vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->addForce(linearVelocity * -1.0f, physx::PxForceMode::eVELOCITY_CHANGE);
		}
	}

	//TODO: fix this for curvy hill
	if (trackNumber == 0) {
		if (vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x > -10.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x  < -1.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z > -75.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z < -36.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.y > -19.0f) {
			checkpoint = true;
		}

		if (checkpoint && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x > 0.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x < 20.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.y > -10.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z > -30.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z < 30.0f) {
			checkpoint = false;
			laps++;
		}
	} else if (trackNumber == 1) {
		if (vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x > -45.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x  < -4.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z > -170.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z < -120.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.y < -100.0f) {
			checkpoint = true;
		}

		if (checkpoint && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x > 0.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x < 20.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.y > 0.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z > -30.0f && vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z < 30.0f) {
			checkpoint = false;
			laps++;
		}
	}

	if (vehicleIndex == 0) {
		//printf("checkpoint %d\n", checkpoint);
		//printf("pos %f, %f, %f\n", vehicle4W->getRigidDynamicActor()->getGlobalPose().p.x, vehicle4W->getRigidDynamicActor()->getGlobalPose().p.y, vehicle4W->getRigidDynamicActor()->getGlobalPose().p.z);

	}
	
	//printf("Available Powerup: %d\n", availablePowerUp);
	if (powerUpTimer != 0.0f) {
		//printf("powerUpTimer: %d\n", powerUpTimer);
		powerUpTimer -= 1;
	}

	Vector3 velocity = (vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().p - previousVehiclePosition);
	float velocityMagnitude = (vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().p - previousVehiclePosition).PxVec3::magnitude();
	if (velocityMagnitude >= 2) {

		if (!reset) {
			if (buffer.hasBlock && buffer.block.distance < 1.2f) {
				trail->createTrail(previousVehiclePosition, vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose(), (vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().p - previousVehiclePosition).PxVec3::magnitude());
			}
		}

		previousVehiclePosition = vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().p;
	}


	if ((vehicleManager->getVehicle(vehicleIndex)->computeForwardSpeed() <= 2) && (controllerInput[vehicleIndex].brake > 0)) {
		vehicleManager->getVehicle(vehicleIndex)->mDriveDynData.setTargetGear(physx::PxVehicleGearsData::eREVERSE);
	}
	else if ((vehicleManager->getVehicle(vehicleIndex)->computeForwardSpeed() >= -2) && (controllerInput[vehicleIndex].acceleration > 0)) {
		vehicleManager->getVehicle(vehicleIndex)->mDriveDynData.setTargetGear(physx::PxVehicleGearsData::eFIRST);
	}
	if (vehicleManager->getVehicle(vehicleIndex)->mDriveDynData.getCurrentGear() == physx::PxVehicleGearsData::eREVERSE) {
		if (inTrail) {
			if (trackNumber == 0)
			{
				topSpeed = 70.0f;
			}
			else
			{
				topSpeed = 100.0f;
			}
			
		}
		else {
			if (trackNumber == 0)
			{
				topSpeed = 50.0f;
			}
			else
			{
				topSpeed = 80.0f;
			}
		}
		vehicleInput.setAnalogBrake((physx::PxReal)controllerInput[vehicleIndex].acceleration);
		vehicleInput.setAnalogAccel((physx::PxReal)controllerInput[vehicleIndex].brake);
		vehicleInput.setAnalogHandbrake((physx::PxReal)controllerInput[vehicleIndex].handBrake);
		if (controllerInput[vehicleIndex].leftSteer == 0.0f) {
			vehicleInput.setAnalogSteer((physx::PxReal)-controllerInput[vehicleIndex].rightSteer);
		}
		else {
			vehicleInput.setAnalogSteer((physx::PxReal)controllerInput[vehicleIndex].leftSteer);
		}
	}
	else {
		if (inTrail) {
			if (trackNumber == 0)
			{
				topSpeed = 70.0f;
			}
			else
			{
				topSpeed = 100.0f;
			}
		}
		else {
			if (trackNumber == 0)
			{
				topSpeed = 50.0f;
			}
			else
			{
				topSpeed = 80.0f;
			}
		}
		if (vehicle4W->computeForwardSpeed() > topSpeed) {
			vehicleInput.setAnalogBrake(0.3f);
		}
		else {
			vehicleInput.setAnalogBrake((physx::PxReal)controllerInput[vehicleIndex].brake);
		}
		vehicleInput.setAnalogAccel((physx::PxReal)controllerInput[vehicleIndex].acceleration);
		vehicleInput.setAnalogHandbrake((physx::PxReal)controllerInput[vehicleIndex].handBrake);
		if (controllerInput[vehicleIndex].leftSteer == 0.0f) {
			vehicleInput.setAnalogSteer((physx::PxReal)-controllerInput[vehicleIndex].rightSteer);
		}
		else {
			vehicleInput.setAnalogSteer((physx::PxReal)controllerInput[vehicleIndex].leftSteer);
		}
	}

	powerUp();

	PxVehicleDrive4WSmoothAnalogRawInputsAndSetAnalogInputs(smoothingData, steerVsSpeedTable, vehicleInput, 1.0f / 60.0f, false, *vehicle4W);
}

void PhysicsVehicle::setPowerUp(int index)
{
	availablePowerUp = index;
}

int PhysicsVehicle::getPowerUpTimer()
{
	return powerUpTimer;
}

physx::PxVec3 PhysicsVehicle::getPositionTrackerPosition() {
	return positionTrackerPosition;
}

void PhysicsVehicle::powerUp() {
	DrivingInput* controllerInput = getEngineSystems()->controller->getControllerInput();

	//powerUp on enable
	if (controllerInput[vehicleIndex].powerup && availablePowerUp != 0) {
		if (availablePowerUp == 1) {
			//Speed
			powerUpTimer = 60;
			inUsePowerUp = 1;
			AudioSource* exp = new AudioSource(this->GetGameObject());
			exp->SetAudio("EngineAssets/Sounds/Ignition.wav");
			exp->SetVolume(1.0f);
			exp->Play(0);
		} else if (availablePowerUp == 2) {
			//Jump
			powerUpTimer = 120;
			physx::PxRigidBodyExt::addForceAtLocalPos(*vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor(), vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().transform(physx::PxVec3(0.0f, 1000000.0f, 0.0f)), physx::PxVec3(0.0f, -1.6f, 0.0f));
			inUsePowerUp = 2;
		} else if (availablePowerUp == 3) {
			trail->removeTrail();
			powerUpTimer = 150;
			inUsePowerUp = 3;
		} else if (availablePowerUp == 4) {
			powerUpTimer = 600;
			inUsePowerUp = 4;
		} else if (availablePowerUp == 5) {

			GameObject* aMine = new GameObject(this->GetGameObject()->getWorld(), "Mine");
			Mine* mine = new Mine(aMine, vehicleManager, vehicleIndex);
		}
		else if (availablePowerUp == 6) {
			powerUpTimer = 300;
			trail->solidTrail();
			inUsePowerUp = 6;
			
		} else if (availablePowerUp == 7) {
			
		}
		availablePowerUp = 0;
	}

	//powerUp in use
	if (inUsePowerUp == 1) {
		physx::PxRigidBodyExt::addForceAtLocalPos(*vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor(), vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().transform(physx::PxVec3(0.0f, 0.0f, 200000.0f)), physx::PxVec3(0.0f, -1.6f, 0.0f));
		if (powerUpTimer % 10 == 0) {
			SpeedPowerUpPrefab* flames = new SpeedPowerUpPrefab(this->GetGameObject()->getWorld());
			flames->GetTransform()->SetLocalPosition(this->GetGameObject()->GetTransform()->GetLocalPosition());
			flames->GetTransform()->SetLocalRotation(this->GetGameObject()->GetTransform()->GetLocalRotation());
		}
	} else if (inUsePowerUp == 2) {
		if (controllerInput[vehicleIndex].rightSteer != 0.0f) {
			physx::PxRigidBodyExt::addForceAtLocalPos(*vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor(), vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().transform(physx::PxVec3((physx::PxReal) - controllerInput[vehicleIndex].rightSteer * 10000.0f, 0.0f, 0.0f)), physx::PxVec3(0.0f, -1.6f, 0.0f));
		}
		else if (controllerInput[vehicleIndex].leftSteer != 0.0f) {
			physx::PxRigidBodyExt::addForceAtLocalPos(*vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor(), vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose().transform(physx::PxVec3((physx::PxReal)controllerInput[vehicleIndex].leftSteer * 10000.0f, 0.0f, 0.0f)), physx::PxVec3(0.0f, -1.6f, 0.0f));
		}
	} 
	else if (inUsePowerUp == 3)
	{
		trail->removeTrail();
	}
	else if (inUsePowerUp == 4) {
		if (vehicleManager->getVehicle(vehicleIndex)->mDriveDynData.getCurrentGear() == physx::PxVehicleGearsData::eREVERSE) {
			vehicleInput.setAnalogBrake((physx::PxReal)controllerInput[vehicleIndex].acceleration);
			vehicleInput.setAnalogAccel((physx::PxReal)controllerInput[vehicleIndex].brake);
		} else {
			vehicleInput.setAnalogBrake((physx::PxReal)controllerInput[vehicleIndex].brake);
			vehicleInput.setAnalogAccel((physx::PxReal)controllerInput[vehicleIndex].acceleration);
		}
	}else if (inUsePowerUp == 5) {
		this->vehicle4W->getRigidDynamicActor()->setLinearVelocity(physx::PxVec3(0.0, 10.0f, 0.0));
	}

	//powerUp on disable
	if (powerUpTimer <= 0.0f) {
		if (inUsePowerUp == 1) {
			//Slow the car back down again
			slowDown = true;
		} else if (inUsePowerUp == 2) {
			//Do nothing when jump disabled	
		} else if (inUsePowerUp == 3) {
			//Do nothing when remove trails disabled
		} else if (inUsePowerUp == 6) {
			//Change trails back to trigger shapes
			trail->nonSolidTrail();
			inUsePowerUp = 0;
		}
		trail->nonSolidTrail();
		inUsePowerUp = 0;
	}

	if (slowDown) {
		if (vehicle4W->computeForwardSpeed() > 50.0f) {
			vehicleInput.setAnalogBrake(1.0f);
		} else {
			slowDown = false;
		}
	}
}

void PhysicsVehicle::setInTrail(bool inTrail) {
	this->inTrail = inTrail;
}

void PhysicsVehicle::hitMine() {
	inUsePowerUp = 5;
}

void PhysicsVehicle::setSmoothingData() {
	smoothingData.mRiseRates[0] = 6.0f;
	smoothingData.mRiseRates[1] = 6.0f;
	smoothingData.mRiseRates[2] = 12.0f;
	smoothingData.mRiseRates[3] = 2.5f;
	smoothingData.mRiseRates[4] = 2.5f;

	smoothingData.mFallRates[0] = 10.0f;
	smoothingData.mFallRates[1] = 10.0f;
	smoothingData.mFallRates[2] = 12.0f;
	smoothingData.mFallRates[3] = 5.0f;
	smoothingData.mFallRates[4] = 5.0f;
}

void PhysicsVehicle::setSteerSpeedData() {
	steerVsSpeedData[0] = 0.0f;			steerVsSpeedData[1] = 0.65f;
	steerVsSpeedData[2] = 5.0f;			steerVsSpeedData[3] = 0.65f;
	steerVsSpeedData[4] = 30.0f;		steerVsSpeedData[5] = 0.3f;
	steerVsSpeedData[6] = 60.0f;		steerVsSpeedData[7] = 0.10f;
	steerVsSpeedData[8] = PX_MAX_F32;	steerVsSpeedData[9] = PX_MAX_F32;
	steerVsSpeedData[10] = PX_MAX_F32;	steerVsSpeedData[11] = PX_MAX_F32;
	steerVsSpeedData[12] = PX_MAX_F32;	steerVsSpeedData[13] = PX_MAX_F32;
	steerVsSpeedData[14] = PX_MAX_F32;	steerVsSpeedData[15] = PX_MAX_F32;
	steerVsSpeedTable = physx::PxFixedSizeLookupTable<8>(steerVsSpeedData, 4);

}

void PhysicsVehicle::Update()
{
	physx::PxTransform physXTransform = vehicleManager->getVehicle(vehicleIndex)->getRigidDynamicActor()->getGlobalPose();
	//GetTransform()->SetPosition(physXTransform.p);
	//GetTransform()->SetRotation(physXTransform.q);// //Quaternion(0.2f,0.1f,0.1f,1.0f));
	GetTransform()->setVehicleTransform(getVehicleTransform());
}

physx::PxU32 PhysicsVehicle::getGear()
{
	return vehicle4W->mDriveDynData.getCurrentGear();
}

void PhysicsVehicle::Destroy() {
	this->getEngineSystems()->physics->getScene()->removeActor(*vehicle4W->getRigidDynamicActor());
	ScriptBehaviour::Destroy();
}

void PhysicsVehicle::setVehicleManager(VehicleManager* vehicleManager) {
	this->vehicleManager = vehicleManager;
}

physx::PxVehicleDrive4W* PhysicsVehicle::getVehicle() {
	return vehicle4W;
}

glm::mat4 PhysicsVehicle::getVehicleTransform() {
	physx::PxMat44 physicsTransform = vehicle4W->getRigidDynamicActor()->getGlobalPose();
	glm::mat4 glmMatrix;
	for (int i = 0; i < 4; i++) {
		for (int j = 0; j < 4; j++) {
			glmMatrix[i][j] = physicsTransform[i][j];
		}
	}
	return glmMatrix;
}

glm::vec3 PhysicsVehicle::getVehiclePosition() {
	physx::PxVec3 physicsPosition = vehicle4W->getRigidDynamicActor()->getGlobalPose().p;
	glm::vec3 glmPosition;
	for (int i = 0; i < 4; i++) {
		glmPosition[i] = physicsPosition[i];
	}
	return glmPosition;
}

void PhysicsVehicle::setVehicleIndex(int index)
{
	vehicleIndex = index;
}

int PhysicsVehicle::getVehicleIndex()
{
	return vehicleIndex;
}

void PhysicsVehicle::computeWheelCenterActorOffsets4W(const physx::PxF32 wheelFrontZ, const physx::PxF32 wheelRearZ, const physx::PxVec3& chassisDims,
	const physx::PxF32 wheelWidth, const physx::PxF32 wheelRadius, const physx::PxU32 numWheels, physx::PxVec3* wheelCentreOffsets)
{
	//chassisDims.z is the distance from the rear of the chassis to the front of the chassis.
	//The front has z = 0.5*chassisDims.z and the rear has z = -0.5*chassisDims.z.
	//Compute a position for the front wheel and the rear wheel along the z-axis.
	//Compute the separation between each wheel along the z-axis.
	const physx::PxF32 numLeftWheels = numWheels / 2.0f;
	const physx::PxF32 deltaZ = (wheelFrontZ - wheelRearZ) / (numLeftWheels - 1.0f);
	//Set the outside of the left and right wheels to be flush with the chassis.
	//Set the top of the wheel to be just touching the underside of the chassis.
	//Begin by setting the rear-left/rear-right/front-left,front-right wheels.
	wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT] = physx::PxVec3((-chassisDims.x + wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + 0 * deltaZ*0.5f);
	wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT] = physx::PxVec3((+chassisDims.x - wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + 0 * deltaZ*0.5f);
	wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT] = physx::PxVec3((-chassisDims.x + wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + (numLeftWheels - 1)*deltaZ);
	wheelCentreOffsets[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT] = physx::PxVec3((+chassisDims.x - wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + (numLeftWheels - 1)*deltaZ);
	//Set the remaining wheels.
	for (physx::PxU32 i = 2, wheelCount = 4; i < numWheels - 2; i += 2, wheelCount += 2)
	{
		wheelCentreOffsets[wheelCount + 0] = physx::PxVec3((-chassisDims.x + wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i*deltaZ*0.5f);
		wheelCentreOffsets[wheelCount + 1] = physx::PxVec3((+chassisDims.x - wheelWidth)*0.5f, -(chassisDims.y / 2 + wheelRadius), wheelRearZ + i*deltaZ*0.5f);
	}
}

void PhysicsVehicle::setupWheelsSimulationData
(const physx::PxF32 wheelMass, const physx::PxF32 wheelMOI, const physx::PxF32 wheelRadius, const physx::PxF32 wheelWidth,
const physx::PxU32 numWheels, const physx::PxVec3* wheelCenterActorOffsets,
const physx::PxVec3& chassisCMOffset, const physx::PxF32 chassisMass,
physx::PxVehicleWheelsSimData* wheelsSimData, const VehicleDesc& vehicle4WDesc)
{
	//Set up the wheels.
	physx::PxVehicleWheelData wheels[PX_MAX_NB_WHEELS];
	{
		//Set up the wheel data structures with mass, moi, radius, width.
		for (physx::PxU32 i = 0; i < numWheels; i++)
		{
			wheels[i].mMass = wheelMass;
			wheels[i].mMOI = wheelMOI;
			wheels[i].mRadius = wheelRadius;
			wheels[i].mWidth = wheelWidth;
			wheels[i].mDampingRate = vehicle4WDesc.wheelAngularDamping;
			wheels[i].mMaxBrakeTorque = vehicle4WDesc.maxBrakeTorque;
		}

		//Enable the handbrake for the rear wheels only.
		wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mMaxHandBrakeTorque = vehicle4WDesc.maxHandbrakeTorqueRear;
		wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mMaxHandBrakeTorque = vehicle4WDesc.maxHandbrakeTorqueRear;
		//Enable steering for the front wheels only.
		wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT].mMaxSteer = vehicle4WDesc.maxSteer;
		wheels[physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT].mMaxSteer = vehicle4WDesc.maxSteer;
		wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT].mMaxSteer = 0.0f;
		wheels[physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT].mMaxSteer = 0.0f;
	}

	//Set up the tires.
	physx::PxVehicleTireData tires[PX_MAX_NB_WHEELS];
	{
		//Set up the tires.
		for (physx::PxU32 i = 0; i < numWheels; i++)
		{
			tires[i].mLongitudinalStiffnessPerUnitGravity = vehicle4WDesc.longitudinalStiffnessPerUnitGravity;
			tires[i].mLatStiffX = vehicle4WDesc.lateralStiffnessX;
			tires[i].mLatStiffY = vehicle4WDesc.lateralStiffnessY;
			tires[i].mCamberStiffnessPerUnitGravity = vehicle4WDesc.camberStiffnessPerUnitGravity;
			tires[i].mType = vehicle4WDesc.tireType;
		}
	}

	//Set up the suspensions
	physx::PxVehicleSuspensionData suspensions[PX_MAX_NB_WHEELS];
	{
		//Compute the mass supported by each suspension spring.
		physx::PxF32 suspSprungMasses[PX_MAX_NB_WHEELS];
		PxVehicleComputeSprungMasses
			(numWheels, wheelCenterActorOffsets,
			chassisCMOffset, chassisMass, 1, suspSprungMasses);

		//Set the suspension data.
		for (physx::PxU32 i = 0; i < numWheels; i++)
		{
			suspensions[i].mMaxCompression = vehicle4WDesc.maxCompression;
			suspensions[i].mMaxDroop = vehicle4WDesc.maxDroop;
			suspensions[i].mSpringStrength = vehicle4WDesc.springStrength;
			suspensions[i].mSpringDamperRate = vehicle4WDesc.springDamperRate;
			suspensions[i].mSprungMass = suspSprungMasses[i];
		}

		//Set the camber angles.
		const physx::PxF32 camberAngleAtRest = vehicle4WDesc.camberAtRest;
		const physx::PxF32 camberAngleAtMaxDroop = vehicle4WDesc.camberAtMaxDroop;
		const physx::PxF32 camberAngleAtMaxCompression = vehicle4WDesc.camberAtMaxCompression;
		for (physx::PxU32 i = 0; i < numWheels; i += 2)
		{
			suspensions[i + 0].mCamberAtRest = camberAngleAtRest;
			suspensions[i + 1].mCamberAtRest = -camberAngleAtRest;
			suspensions[i + 0].mCamberAtMaxDroop = camberAngleAtMaxDroop;
			suspensions[i + 1].mCamberAtMaxDroop = -camberAngleAtMaxDroop;
			suspensions[i + 0].mCamberAtMaxCompression = camberAngleAtMaxCompression;
			suspensions[i + 1].mCamberAtMaxCompression = -camberAngleAtMaxCompression;
		}
	}

	//Set up the wheel geometry.
	physx::PxVec3 suspTravelDirections[PX_MAX_NB_WHEELS];
	physx::PxVec3 wheelCentreCMOffsets[PX_MAX_NB_WHEELS];
	physx::PxVec3 suspForceAppCMOffsets[PX_MAX_NB_WHEELS];
	physx::PxVec3 tireForceAppCMOffsets[PX_MAX_NB_WHEELS];
	{
		//Set the geometry data.
		for (physx::PxU32 i = 0; i < numWheels; i++)
		{
			//Vertical suspension travel.
			suspTravelDirections[i] = physx::PxVec3(0, -1, 0);

			//Wheel center offset is offset from rigid body center of mass.
			wheelCentreCMOffsets[i] =
				wheelCenterActorOffsets[i] - chassisCMOffset;

			//Suspension force application point 0.3 metres below 
			//rigid body center of mass.
			suspForceAppCMOffsets[i] =
				physx::PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);

			//Tire force application point 0.3 metres below 
			//rigid body center of mass.
			tireForceAppCMOffsets[i] =
				physx::PxVec3(wheelCentreCMOffsets[i].x, -0.3f, wheelCentreCMOffsets[i].z);
		}
	}

	//Set up the filter data of the raycast that will be issued by each suspension.
	physx::PxFilterData qryFilterData;
	setupNonDrivableSurface(qryFilterData);

	//Set the wheel, tire and suspension data.
	//Set the geometry data.
	//Set the query filter data
	for (physx::PxU32 i = 0; i < numWheels; i++)
	{
		wheelsSimData->setWheelData(i, wheels[i]);
		wheelsSimData->setTireData(i, tires[i]);
		wheelsSimData->setSuspensionData(i, suspensions[i]);
		wheelsSimData->setSuspTravelDirection(i, suspTravelDirections[i]);
		wheelsSimData->setWheelCentreOffset(i, wheelCentreCMOffsets[i]);
		wheelsSimData->setSuspForceAppPointOffset(i, suspForceAppCMOffsets[i]);
		wheelsSimData->setTireForceAppPointOffset(i, tireForceAppCMOffsets[i]);
		wheelsSimData->setSceneQueryFilterData(i, qryFilterData);
		wheelsSimData->setWheelShapeMapping(i, i);
	}
}

physx::PxVehicleDrive4W* PhysicsVehicle::createVehicle4W(const VehicleDesc& vehicle4WDesc, PhysicsRigidDynamic* vehicleActor, physx::PxPhysics* physics)
{
	//Set up the sim data for the wheels.
	physx::PxVehicleWheelsSimData* wheelsSimData = physx::PxVehicleWheelsSimData::allocate(vehicle4WDesc.numWheels);
	{
		//Compute the wheel center offsets from the origin.
		physx::PxVec3 wheelCenterActorOffsets[PX_MAX_NB_WHEELS];
		const physx::PxF32 frontZ = vehicle4WDesc.chassisDims.z*0.3f;
		const physx::PxF32 rearZ = -vehicle4WDesc.chassisDims.z*0.3f;
		computeWheelCenterActorOffsets4W(frontZ, rearZ, vehicle4WDesc.chassisDims, vehicle4WDesc.wheelWidth, vehicle4WDesc.wheelRadius, vehicle4WDesc.numWheels, wheelCenterActorOffsets);

		//Set up the simulation data for all wheels.
		setupWheelsSimulationData
			(vehicle4WDesc.wheelMass, vehicle4WDesc.wheelMOI, vehicle4WDesc.wheelRadius, vehicle4WDesc.wheelWidth,
			vehicle4WDesc.numWheels, wheelCenterActorOffsets,
			vehicle4WDesc.chassisCMOffset, vehicle4WDesc.chassisMass,
			wheelsSimData, vehicle4WDesc);
	}

	//Set up the sim data for the vehicle drive model.
	//Diff
	physx::PxVehicleDifferential4WData diff;
	diff.mType = vehicle4WDesc.differentialType;
	driveSimData.setDiffData(diff);

	//Engine
	physx::PxVehicleEngineData engine;
	engine.mPeakTorque = vehicle4WDesc.enginePeaktorque;
	engine.mMaxOmega = vehicle4WDesc.engineMaxOmega;//approx 6000 rpm
	driveSimData.setEngineData(engine);

	//Gears
	physx::PxVehicleGearsData gears;
	gears.mSwitchTime = vehicle4WDesc.switchTime;
	gears.mNbRatios = vehicle4WDesc.numRatios;
	for (physx::PxU32 i = physx::PxVehicleGearsData::eREVERSE; i < physx::PxVehicleGearsData::eGEARSRATIO_COUNT; ++i) {
		gears.mRatios[i] = vehicleDesc.ratios[i];
	}
	gears.mFinalRatio = vehicle4WDesc.finalRatio;
	driveSimData.setGearsData(gears);

	//Clutch
	physx::PxVehicleClutchData clutch;
	clutch.mStrength = vehicle4WDesc.clutchStrength;
	driveSimData.setClutchData(clutch);

	//Ackermann steer accuracy
	physx::PxVehicleAckermannGeometryData ackermann;
	ackermann.mAccuracy = vehicle4WDesc.accuracy;
	ackermann.mAxleSeparation =
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT).z -
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT).z;
	ackermann.mFrontWidth =
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eFRONT_RIGHT).x -
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eFRONT_LEFT).x;
	ackermann.mRearWidth =
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eREAR_RIGHT).x -
		wheelsSimData->getWheelCentreOffset(physx::PxVehicleDrive4WWheelOrder::eREAR_LEFT).x;
	driveSimData.setAckermannGeometryData(ackermann);


	//Create a vehicle from the wheels and drive sim data.
	physx::PxVehicleDrive4W* vehDrive4W = physx::PxVehicleDrive4W::allocate(vehicle4WDesc.numWheels);
	vehDrive4W->setup(physics, vehicleActor->getRigidDynamic(), *wheelsSimData, driveSimData, vehicle4WDesc.numWheels - 4);

	//Free the sim data because we don't need that any more.
	wheelsSimData->free();

	this->vehicleIndex = vehicleManager->addVehicle(vehDrive4W);

	return vehDrive4W;
}
