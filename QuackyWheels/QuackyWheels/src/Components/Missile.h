#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include <ctime>
#include "../BaseTypes/Vector3.h"
#include "PxPhysicsAPI.h"

class GameObject;
class VehicleManager;
class VehiclePositionTracker;
class Missile : public ScriptBehaviour
{
public:
	Missile(GameObject *gameObject, VehiclePositionTracker* vehiclePositionTracker, VehicleManager* vehicleManager, int index);
	~Missile();
	void Hit();

private:
	time_t alive;
	VehicleManager* vehicleManager;
	VehiclePositionTracker* vehiclePositionTracker;
	int vehicleIndex;
	int trackIndex;
	float missileSpeed;
	Vector3 destination;
	Vector3 travelPath;
	physx::PxVehicleDrive4W* targetVehicle;

	int frameCounter = 0;

protected:
	virtual void Start() override;
	virtual void PhysicsUpdate() override;
};

