#include "VehicleAI.h"
#include "../GameLoop/EngineSystems.h"
#include "Transform.h"
#include "PhysicsVehicle.h"
#include <time.h>
#include "../Input/Controller.h"
#include "PxPhysicsAPI.h"
#include "Countdown.h"

#include <iostream>

#define RADIUS 5.0f
#define WIDTH 2.0f

VehicleAI::VehicleAI(GameObject* gameObject, PhysicsVehicle* veh, NavMesh* mesh) : ScriptBehaviour(gameObject)
{
	pathFinder = AI();
	m_vehicle = veh;
	SetVehicleIndex();
	pathFinder.setNavMesh(mesh);

	start = time(0);

	uniform_dist = std::uniform_real_distribution<>(0.0, 1.0);
	powerupReset = false;
	resetTimer = 0;
}


VehicleAI::~VehicleAI()
{
}

void VehicleAI::Init()
{
	//pathFinder = AI();
}

void VehicleAI::EarlyUpdate()
{
	//drivingInput = result of AI logic
	Vector3 currSpot = m_vehicle->getVehiclePosition();

	bool pathChanged = obstacleTracker();

	pathFinder.update_pos(currSpot, false);
	if (path.empty() || (path[0] != pathFinder.getPath()[0]))
	{
		path = pathFinder.getPath();
		m_drive = true;
	}
	//std::cout << "Path size = " << path.size() << "\n";

	if (Vector3::Distance(currSpot, path[0].Position) < m_ReachTargetThreshold || pathChanged)
	{
		pathFinder.update_pos(currSpot, true);
	}

	calcDrive(path[0]);

	/*cout << "Accel: " << drivingInput.acceleration << "; Brake: " << drivingInput.brake << "; Steer:[" << drivingInput.leftSteer << ", " << drivingInput.rightSteer << "]\nLoc = "
		<< m_vehicle->getVehiclePosition() << "\nDest = " << path.front().Position << "\n";
		*/
	//std::cout << "Loc = " << m_vehicle->getVehiclePosition() << '\n';
	//std::cout << "Dest = " << path.front().Position << '\n';

	double seconds_since_start = difftime(time(0), start);
	if (seconds_since_start > 5)
		getEngineSystems()->controller->setDrivingInput(vehicleIndex, drivingInput); //This is where input data is sent

	if (powerupReset == false)
	{
		drivingInput.powerup = 1.0;
		powerupReset = true;
	}
	else if (powerupReset == true)
	{
		drivingInput.powerup = 0.0;
		powerupReset = false;
	}

	
	/*Debug::DrawLine(currSpot, path[0].Position, Color::Red());
	for (unsigned int i = 1; i < path.size(); i++)
	{
		Vector3 pos0 = path[i - 1].Position;
		Vector3 pos1 = path[i].Position;
		Debug::DrawLine(pos0, pos1, Color::Red());
	}*/
}

void VehicleAI::Update()
{
	if (m_vehicle->reset)
	{
		pathFinder.change_goal();
	}
}

void VehicleAI::PhysicsUpdate()
{
	if (abs(m_vehicle->getVehicle()->computeForwardSpeed()) < 2.0f && m_vehicle->countdown->isCountdownFinished()) {
		resetTimer++;
	}
	if (drivingInput.reset == true) {
		drivingInput.reset = false;
	}
	if (resetTimer == 240) {
		resetTimer = 0;
		drivingInput.reset = true;
		pathFinder.change_goal();
	}
	
}

DrivingInput VehicleAI::getDriveInput()
{
	//Do AI calculations here
	return drivingInput;
}

void VehicleAI::SetNavMesh(NavMesh* playing)
{
	pathFinder.setNavMesh(playing);
}

void VehicleAI::SetGoalList(std::vector<Vector3> list)
{
	std::vector<NavMeshNode> final;
	for (auto element : list)
	{
		final.push_back(NavMeshNode(element));
	}
	pathFinder.setGoalList(final);
}

void VehicleAI::SetVehicleIndex()
{
	vehicleIndex = getEngineSystems()->controller->getIndex();
}

void VehicleAI::calcDrive(NavMeshNode spot)
{
	//This is where the magic happens, motherfucker! Analyze how to properly move to the spot
	//drivingInput.quack, drivingInput.powerup = false;

	
	if (spot.Position == m_vehicle->getVehiclePosition() || !m_drive)
	{
		// Car should not be moving,
		// use handbrake to stop
		drivingInput.acceleration = 0.0; //(0, 0, -1f, 1f);
		drivingInput.brake = 0.1;
		drivingInput.handBrake = 1.0;
		drivingInput.leftSteer = 0.0;
		drivingInput.rightSteer = 0.0;

		if (Vector3::Distance(spot.Position, m_vehicle->getVehiclePosition()) > m_ReachTargetThreshold)
		{
			m_drive = true;
		}
	}
	
	else
	{
		//Vector3 fwd = vec3(Matrix4x4(m_vehicle->getVehicleTransform()).mat4x4() * vec4(0, 0, 1, 0));
		Vector3 fwd = Vector3(m_vehicle->getVehicle()->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0.0, 0.0, 1.0)));
		float speed = m_vehicle->getVehicle()->getRigidDynamicActor()->getLinearVelocity().magnitude();
		if (speed > MAX_VELOCITY*0.1) //m_Rigidbody.velocity.magnitude > m_CarController.MaxSpeed*0.1f)
		{
			fwd = m_vehicle->getVehicle()->getRigidDynamicActor()->getLinearVelocity();
		}

		//Debug::DrawLine(m_vehicle->getVehiclePosition(), m_vehicle->getVehiclePosition()+glm::vec3(fwd.x, fwd.y, fwd.z), Color::Red());

		float desiredSpeed = MAX_VELOCITY;

		// now it's time to decide if we should be slowing down...
		switch (m_BrakeCondition)
		{
		case TARGET_DIRECTION_DIFFERENCE:
		{
											// the car will brake according to the upcoming change in direction of the target. Useful for route-based AI, slowing for corners.
											// check out the angle of our target compared to the current direction of the car
											Vector2 targ = Vector2::Normalize(Vector2(spot.Position.x, spot.Position.z) - Vector2(m_vehicle->getVehiclePosition().x, m_vehicle->getVehiclePosition().z));
											Vector2 fwd2d = Vector2::Normalize(Vector2(fwd.x, fwd.z));
											float approachingCornerAngle = 180.0f*(acos(targ.Dot(fwd2d))) / PI;
											//float approachingCornerAngle = 180.0f*(acos(Vector2::Normalize(Vector2(spot.Position.x, spot.Position.z)).Dot(Vector2::Normalize(Vector2(fwd.x, fwd.z)))))/PI; // angle(m_Target.forward, fwd);
											//float approachingCornerAngler = Vector3.angle(spot.Position, fwd);

											// also consider the current amount we're turning, multiplied up and then compared in the same way as an upcoming corner angle
											float spinningAngle = ((float)(m_vehicle->getVehicle()->getRigidDynamicActor()->getAngularVelocity().magnitude())*m_CautiousAngularVelocityFactor);

											// if it's different to our current angle, we need to be cautious (i.e. slow down) a certain amount
											//float cautiousnessRequired = glm::inv(0, m_CautiousMaxAngle, (spinningAngle, approachingCornerAngle));
											float cautiousnessRequired = glm::max(spinningAngle, approachingCornerAngle) / m_CautiousMaxAngle;  //Inverse linear interpolation
											//(0, m_CautiousMaxAngle, glm::max(spinningAngle, approachingCornerAngle)); //t = (z - x)/(y - x)
											if (cautiousnessRequired > 1.0)
											{
												cautiousnessRequired = 1.0;
											}

											desiredSpeed = glm::lerp(MAX_VELOCITY, MAX_VELOCITY*m_CautiousSpeedFactor, cautiousnessRequired);

											break;
		}

		case TARGET_DISTANCE:
		{
								// the car will brake as it approaches its target, regardless of the target's direction. Useful if you want the car to
								// head for a stationary target and come to rest when it arrives there.

								// check out the distance to target
								Vector3 delta = spot.Position - m_vehicle->getVehiclePosition();
								float distanceCautiousFactor = (delta.Magnitude() - m_CautiousMaxDistance) / (-m_CautiousMaxDistance); //.InverseLerp(m_CautiousMaxDistance, 0, delta.Magnitude); Inverse Lerp

								// also consider the current amount we're turning, multiplied up and then compared in the same way as an upcoming corner angle
								float spinningAngle = ((float)(m_vehicle->getVehicle()->getRigidDynamicActor()->getAngularVelocity().magnitude())*m_CautiousAngularVelocityFactor);

								// if it's different to our current angle, we need to be cautious (i.e. slow down) a certain amount
								float linInv = spinningAngle/m_CautiousMaxAngle; //Inverse Lerp
								if (linInv > 1.0)
								{
									linInv = 1.0;
								}
								float cautiousnessRequired = glm::max(linInv, distanceCautiousFactor);

								desiredSpeed = glm::lerp(MAX_VELOCITY, MAX_VELOCITY*m_CautiousSpeedFactor, cautiousnessRequired);

								break;
		}

		case NEVER_BRAKE:
			break;
		}

		// Evasive action due to collision with other cars:

		// our target position starts off as the 'real' target position
		Vector3 offsetTargetPos = spot.Position;

		// if are we currently taking evasive action to prevent being stuck against another car:
		//Currently not implemented yet
		/*if (Time.time < m_AvoidOtherCarTime)
		{
		// slow down if necessary (if we were behind the other car when collision occured)
		desiredSpeed *= m_AvoidOtherCarSlowdown;

		// and veer towards the side of our path-to-target that is away from the other car
		offsetTargetPos += m_Target.right*m_AvoidPathOffset;
		}*/
		//else
		//{
		// no need for evasive action, we can just wander across the path-to-target in a random way,
		// which can help prevent AI from seeming too uniform and robotic in their driving
		randGen1 = std::minstd_rand((unsigned int)(time(0)*m_LateralWanderSpeed));
		offsetTargetPos += m_vehicle->GetTransform()->Right()*((float) uniform_dist(randGen1)*2.0f - 1.0f)*m_LateralWanderDistance;
		//}

		// use different sensitivity depending on whether accelerating or braking:

		float accelBrakeSensitivity = (desiredSpeed < speed)
			? m_BrakeSensitivity
			: m_AccelSensitivity;

		// decide the actual amount of accel/brake input to achieve desired speed.
		float accel = glm::clamp((desiredSpeed - speed)*accelBrakeSensitivity, -1.0f, 1.0f);

		// add acceleration 'wander', which also prevents AI from seeming too uniform and robotic in their driving
		// i.e. increasing the accel wander amount can introduce jostling and bumps between AI cars in a race
		randGen2 = std::minstd_rand((unsigned int)(time(0)*m_AccelWanderSpeed));
		accel *= (1.0f - m_AccelWanderAmount) + ((float) uniform_dist(randGen2)*m_AccelWanderAmount);
			//Mathf.PerlinNoise(Time.time*m_AccelWanderSpeed, m_RandomPerlin)*m_AccelWanderAmount);

		// calculate the local-relative position of the target, to steer towards
		Vector3 localTarget = offsetTargetPos - m_vehicle->getVehiclePosition(); 
		
		//Vector3 localTarget = m_vehicle->GetTransform()->ToLocalPoint(offsetTargetPos);//InverseTransformPoint(offsetTargetPos);

		/*float x = Vector2(fwd.x, fwd.z).Dot(Vector2(localTarget.x, localTarget.z));
		float y = Vector2(fwd.x, fwd.z).Cross(Vector2(localTarget.x, localTarget.z));
		float crs2 = crs.Magnitude();
		float pos = Vector3(0, 1, 0).Dot(crs);*/
		//cout << "x: " << x << "; y: " << y << "; ";
		//float dotProduct = Vector3::Dot(localTarget.Normalized(), fwd.Normalized()); //Negative = away, positive = towards

		Vector3 crs = fwd.Cross(localTarget);
		float x = fwd.Dot(localTarget);
		float y = crs.Magnitude();

		// work out the local angle towards the target
		float targetAngle = -atan2(fwd.x*localTarget.z - fwd.z*localTarget.x, x);
		/*float targetAngle = atan2(y, x);// *(180 / PI);

		if (pos < 0.0)
		{
		targetAngle = -targetAngle;
		}*/

		//std::cout << "Target Angle: " << targetAngle << "\n";

		// get the amount of steering needed to aim the car towards the target
		float steer = glm::clamp(targetAngle*m_SteerSensitivity, -1.0f, 1.0f)*glm::sign(speed);

		// feed input to the car controller.
		if (accel < 0.0)
		{
			drivingInput.acceleration = 0;
			drivingInput.brake = -accel;
		}
		else
		{
			drivingInput.acceleration = accel;
			drivingInput.brake = 0;
		}

		drivingInput.handBrake = 0;

		if (steer > 0.0f)
		{
			drivingInput.leftSteer = steer;
			drivingInput.rightSteer = 0;
		}
		else
		{
			drivingInput.leftSteer = 0;
			drivingInput.rightSteer = -steer;
		}
		//m_CarController.Move(steer, accel, accel, 0.0f);

		// if appropriate, stop driving when we're close enough to the target.
		if (m_StopWhenTargetReached && (sqrt(y*y + x*x) < m_ReachTargetThreshold))
		{
			m_drive = false;
		}
	}
}

bool VehicleAI::obstacleTracker()
{
	bool timeToChange = false;
	for (size_t i = 0; i < m_vehicle->obstacles.size(); i++)
	{
		PhysicsRigidDynamic* obstRef = m_vehicle->obstacles[i];
		physx::PxVec3 position = obstRef->getRigidDynamic()->getGlobalPose().p;

		NearestNode* nearest = pathFinder.nearestTree->getNearest(position);
		NavMeshNode* goodRef = nearest->pnt_nn;

		float totalDist = abs(Vector3::Distance(goodRef->Position, position));

		if (totalDist < RADIUS)
		{
			goodRef->blockNode();
		}
		else
		{
			goodRef->unblockNode();
		}
		for (size_t k = 0; k < path.size(); k++)
		{
			if (path[k].Position == goodRef->Position)
			{
				timeToChange = true;
				break;
			}
		}

		if (totalDist < 11.0f)
		{
			for (size_t j = 0; j < goodRef->ConnectedNodes.size(); j++)
			{
				NavMeshNode* sideRefs = goodRef->ConnectedNodes[j];
				float secondDist = abs(Vector3::Distance(sideRefs->Position, position));

				if (secondDist < RADIUS)
				{
					sideRefs->blockNode();
				}
				else
				{
					sideRefs->unblockNode();
				}
				size_t l = 0;
				while (timeToChange == false && l < path.size())
				{
					if (path[l].Position == sideRefs->Position)
					{
						timeToChange = true;
					}
					l++;
				}
			}
		}	
	}

	//second part for trail obstacles - to be implemented later when John's finished creating the new thing.
	/*for (int i = 0; i < m_vehicle->trail_obstacles.size(); i++)
	{
		PhysicsRigidDynamic* obstRef = m_vehicle->trail_obstacles[i];
		physx::PxVec3 position = obstRef->getRigidDynamic()->getGlobalPose().p;

		NearestNode* nearest = pathFinder.nearestTree->getNearest(position);
		NavMeshNode* goodRef = nearest->pnt_nn;

		float totalDist = abs(Vector3::Distance(goodRef->Position, position));

		if (totalDist < WIDTH)
		{
			goodRef->blockNode();
		}
		else
		{
			goodRef->unblockNode();
		}
		int k = 0;
		while (timeToChange == true && k < path.size())
		{
			if (path[k].Position == goodRef->Position)
			{
				timeToChange = true;
			}
			k++;
		}

		if (totalDist < 5.0f)
		{
			for (int j = 0; j < goodRef->ConnectedNodes.size(); j++)
			{
				NavMeshNode* sideRefs = goodRef->ConnectedNodes[j];
				float secondDist = abs(Vector3::Distance(sideRefs->Position, position));

				if (secondDist < RADIUS)
				{
					sideRefs->blockNode();
				}
				else
				{
					sideRefs->unblockNode();
				}
				int l = 0;
				while (timeToChange == false && l < path.size())
				{
					if (path[l].Position == sideRefs->Position)
					{
						timeToChange = true;
					}
					l++;
				}
			}
		}
	}*/

	return timeToChange;
}