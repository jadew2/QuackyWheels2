#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include <vector>
#include "../BaseTypes/Vector3.h"

class VehicleManager;
class World;
class BolderSpawner : public ScriptBehaviour {
public:
	BolderSpawner(GameObject *gameObject, VehicleManager* vehicleManager, int killPlane, Vector3 spawnPoint);
	~BolderSpawner();

protected:
	virtual void PhysicsUpdate() override;

private:
	VehicleManager* vehicleManager;
	std::vector<GameObject*> bolderObstacles;

	Vector3 spawnPoint;
	int killPlane;
};

