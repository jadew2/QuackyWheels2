//
// Created by Jade White on 2016-04-07.
//

#pragma once


#include "../ComponentEntity/ScriptBehaviour.h"
#include "../Renderer/IRenderer.h"
#include "../Renderer/Layers/RenderLayerMask.h"

class RenderCollection;
class Mesh;
class Material;
class Shader;
class Texture;
class MeshRenderGL;
class FrameBufferTexture;
class ImageEffectRenderer : public ScriptBehaviour, public IRenderer
{
private:
	RenderCollection* renderCollection;
	Mesh* fullScreenQuad;

	MeshRenderGL* meshRenderGL;

	Material* material;
public:
	///<summary> The mask for what layer this MeshRenderer is on. </summary>
	RenderLayerMask mask;

	ImageEffectRenderer(GameObject* gameObject);

	virtual ~ImageEffectRenderer();

	virtual void render(Camera* camera, bool doCulling = true) override;

	Material* getMaterial();
	void setMaterial(Material* material);
protected:
	virtual void OnEnable() override;
	virtual void OnDisable() override;


};


