#pragma once
#include "PxPhysicsAPI.h"
#include "../Components/PhysicsRigidDynamic.h"

class GameObject;
class PhysicsVehicleActor : public PhysicsRigidDynamic {

public:
	PhysicsVehicleActor(GameObject* gameObject, Transform* transform, ObjectType type, physx::PxReal density, physx::PxReal chassisMass, physx::PxVec3* chassisMOI, physx::PxVec3* chassisCMOffset, bool kinematicFlag = false);
	virtual ~PhysicsVehicleActor();

};

