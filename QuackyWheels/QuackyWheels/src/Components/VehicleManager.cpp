#include "VehicleManager.h"

#include "../GameLoop/EngineSystems.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Vehicle/VehicleTireFriction.h"
#include "../Physics/Physics.h"
#include "../Time/Time.h"

VehicleManager::VehicleManager(GameObject* gameObject, physx::PxMaterial* const* material) : ScriptBehaviour(gameObject) {

	//Set material
	this->drivableMaterial = material;

	//Allocate simulation data
	mWheelsSimData4W = physx::PxVehicleWheelsSimData::allocate(4);
	gVehicleSceneQueryData = VehicleSceneQueryData::allocate(MAX_NUM_4W_VEHICLES, PX_MAX_NB_WHEELS, MAX_NUM_4W_VEHICLES, *this->getEngineSystems()->physics->getAllocator());
	gBatchQuery = VehicleSceneQueryData::setUpBatchedSceneQuery(0, *gVehicleSceneQueryData, this->getEngineSystems()->physics->getScene());

	//Create the friction table for each combination of tire and surface type.
	gFrictionPairs = createFrictionPairs(*drivableMaterial);

	mNumVehicles = 0;
	for (physx::PxU32 i = 0; i < MAX_NUM_4W_VEHICLES; i++)
	{
		mVehicles[i] = NULL;
	}
}


VehicleManager::~VehicleManager() {
	//Deallocate simulation data that was used to switch from 3-wheeled to 4-wheeled cars by switching simulation data.
	mWheelsSimData4W->free();
	for (physx::PxU32 i = 0; i < mNumVehicles; i++) {
		if (mVehicles[i]->getVehicleType() == physx::PxVehicleTypes::eDRIVE4W) {
			physx::PxVehicleDrive4W* veh = (physx::PxVehicleDrive4W*)mVehicles[i];
			veh->free();
		}
		else {
			//Error, all vehicles currently must be 4W
			exit(1);
		}
	}
}

void VehicleManager::setDrivableMaterial(physx::PxMaterial* const* material) {
	this->drivableMaterial = material;
}


void VehicleManager::PhysicsUpdate() {
	//Raycasts.

	physx::PxVehicleWheels* vehicles[4] = { mVehicles[0] , mVehicles[1], mVehicles[2], mVehicles[3]};
	physx::PxRaycastQueryResult* raycastResults = gVehicleSceneQueryData->getRaycastQueryResultBuffer(0);
	const physx::PxU32 raycastResultsSize = gVehicleSceneQueryData->getRaycastQueryResultBufferSize();
	PxVehicleSuspensionRaycasts(gBatchQuery, 4, vehicles, raycastResultsSize, raycastResults);

	//Vehicle update.
	const physx::PxVec3 grav = this->getEngineSystems()->physics->getScene()->getGravity();
	physx::PxWheelQueryResult wheelQueryResults[PX_MAX_NB_WHEELS];
	physx::PxVehicleWheelQueryResult vehicleQueryResults[MAX_NUM_4W_VEHICLES] = { { wheelQueryResults, mVehicles[0]->mWheelsSimData.getNbWheels() } };
	//use nullptr as last arg? vehicleQueryResults

	physx::PxVehicleUpdates((physx::PxReal)this->getEngineSystems()->time->GetPhysicsTimestep(), grav, *gFrictionPairs, 4, vehicles, nullptr);

}
