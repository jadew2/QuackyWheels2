#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "../Components/PhysicsRigidDynamic.h"
#include <vector>
#include <random>
#include "../Input/DrivingInput.h"
#include "../Various/AI.h"

#define MAX_VELOCITY 150.0f
#define PI 3.14159f

class GameObject;
class NavMesh;
class NavMeshNode;
class PhysicsVehicle;
class VehicleAI : public ScriptBehaviour
{
public:
	VehicleAI(GameObject* gameObject, PhysicsVehicle* veh, NavMesh* mesh);
	virtual ~VehicleAI();
	void Init() override;
	void EarlyUpdate() override;
	void Update() override;
	void PhysicsUpdate() override;

	DrivingInput getDriveInput();

	void SetNavMesh(NavMesh* playing);
	void SetGoalList(std::vector<Vector3> list);
	void SetVehicleIndex();

	enum BrakeCondition
	{
		NEVER_BRAKE,                 // the car simply accelerates at full throttle all the time.
		TARGET_DIRECTION_DIFFERENCE,  // the car will brake according to the upcoming change in direction of the target. Useful for route-based AI, slowing for corners.
		TARGET_DISTANCE,             // the car will brake as it approaches its target, regardless of the target's direction. Useful if you want the car to
		// head for a stationary target and come to rest when it arrives there.
	};

private:
	bool obstacleTracker();

	unsigned int resetTimer;

	DrivingInput drivingInput;
	PhysicsVehicle* m_vehicle;
	int vehicleIndex;
	AI pathFinder;
	std::vector<NavMeshNode> path;


	float m_CautiousSpeedFactor = 0.10f;               // percentage of max speed to use when being maximally cautious
	float m_CautiousMaxAngle = 45.0f;           // angle of approaching corner to treat as warranting maximum caution
	float m_CautiousMaxDistance = 2.0f;               // distance at which distance-based cautiousness begins
	float m_CautiousAngularVelocityFactor = 8.0f;    // how cautious the AI should be when considering its own current angular velocity (i.e. easing off acceleration if spinning!)
	float m_SteerSensitivity = 1.0f;                  // how sensitively the AI uses steering input to turn to the desired direction
	float m_AccelSensitivity = 0.6f;                  // How sensitively the AI uses the accelerator to reach the current desired speed
	float m_BrakeSensitivity = 1.0f;                  // How sensitively the AI uses the brake to reach the current desired speed
	float m_LateralWanderDistance = 2.0f;             // how far the car will wander laterally towards its target
	float m_LateralWanderSpeed = 0.1f;                // how fast the lateral wandering will fluctuate
	float m_AccelWanderAmount = 0.1f;                 // how much the cars acceleration will wander
	float m_AccelWanderSpeed = 0.1f;                  // how fast the cars acceleration wandering will fluctuate
	BrakeCondition m_BrakeCondition = TARGET_DIRECTION_DIFFERENCE; // what should the AI consider when accelerating/braking?
	bool m_drive;                                     // whether the AI is currently actively driving or stopped.
	bool m_StopWhenTargetReached;                     // should we stop driving when we reach the target?
	float m_ReachTargetThreshold = 20.0;               // proximity to target to consider we 'reached' it, and stop driving.

	void calcDrive(NavMeshNode spot);
	bool powerupReset;
	time_t start;

	std::minstd_rand randGen1;
	std::minstd_rand randGen2;
	std::uniform_real_distribution<> uniform_dist;

	/*To do: -Put all AI stuff here
			-Generate DrivingInput
			-Return it
	*/
};
/*
float m_CautiousSpeedFactor = 0.5f;               // percentage of max speed to use when being maximally cautious
float m_CautiousMaxAngle = 30.0f;                  // angle of approaching corner to treat as warranting maximum caution
float m_CautiousMaxDistance = 5.0f;                              // distance at which distance-based cautiousness begins
float m_CautiousAngularVelocityFactor = 20.0f;                     // how cautious the AI should be when considering its own current angular velocity (i.e. easing off acceleration if spinning!)
float m_SteerSensitivity = 0.8f;                                // how sensitively the AI uses steering input to turn to the desired direction
float m_AccelSensitivity = 0.05f;                                // How sensitively the AI uses the accelerator to reach the current desired speed
float m_BrakeSensitivity = 1.0f;                                   // How sensitively the AI uses the brake to reach the current desired speed
float m_LateralWanderDistance = 8.0f;                              // how far the car will wander laterally towards its target
float m_LateralWanderSpeed = 0.1f;                               // how fast the lateral wandering will fluctuate
float m_AccelWanderAmount = 0.1f;                  // how much the cars acceleration will wander
float m_AccelWanderSpeed = 0.1f;                                 // how fast the cars acceleration wandering will fluctuate
BrakeCondition m_BrakeCondition = TARGET_DIRECTION_DIFFERENCE; // what should the AI consider when accelerating/braking?
bool m_drive;                                      // whether the AI is currently actively driving or stopped.
bool m_StopWhenTargetReached;                                    // should we stop driving when we reach the target?
float m_ReachTargetThreshold = 6.0;                                // proximity to target to consider we 'reached' it, and stop driving.
*/