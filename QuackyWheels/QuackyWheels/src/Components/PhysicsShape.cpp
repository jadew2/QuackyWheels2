#include "PhysicsShape.h"


PhysicsShape::PhysicsShape(GameObject* gameObject, Transform* transform) : ScriptBehaviour(gameObject) {
	this->transform = transform;
	//TODO synch physics local transform with actual transform
}


PhysicsShape::~PhysicsShape() {

}

physx::PxShape* PhysicsShape::getShape() {
	return this->physicsShape;
}

Transform* PhysicsShape::getTransform() {
	return this->transform;
}
