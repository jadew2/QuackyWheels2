#pragma once
#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidStatic;
class Transform;
class PhysicsRigidStaticSphere : PhysicsShape {

public:
	PhysicsRigidStaticSphere(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxReal radius, 
							 Transform* transform, physx::PxMaterial* const* materials, physx::PxFilterData* qryFilterData,
							 physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidStaticSphere();

	void setTransform(Transform* transform);

	PhysicsRigidStatic* getActor();

protected:
	PhysicsRigidStatic* actor;
};

