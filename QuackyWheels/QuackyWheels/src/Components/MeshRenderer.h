//
// Created by Jade White on 2016-02-10.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "../Renderer/Layers/RenderLayerMask.h"
#include "../Renderer/IRenderer.h"
#include <vector>
#include <memory>

class Camera;
class CullingHelper; //Because CullingHelper has Camera
class GameObject;
class Transform;
class EngineSystems;
class Mesh;
class Material;
class MeshRenderGL;

class MeshRenderer : public ScriptBehaviour, public IRenderer
{
	EngineSystems* es;
	Transform* transform;

	MeshRenderGL* meshRendererGL;
	CullingHelper* cullingHelper;
	bool uiMode;
public:
	MeshRenderer(GameObject* gameObject);
	virtual ~MeshRenderer();

	///<summary> The mask for what layer this MeshRenderer is on. </summary>
	RenderLayerMask mask;

	///<summary> This Renderer's Mesh </summary>
	Mesh* mesh;
	///<summary> The mateirals it uses. [0] is used for SubMesh 0, [1] for SubMesh 1... and so forth. </summary>
	Material* material;

	enum DrawType
	{
		///<summary> Default filled faces mode. </summary>
		Default,
		///<summary> Wireframe mesh. </summary>
		WireFrame,
		///<summary> Mesh vertices are drawn in a connected line by order. Ignores triangles. </summary>
		VertexLine,
	};
	///<summary> The method this mesh will be drawn using. </summary>
	DrawType drawType = DrawType::Default;

	void setMaterial(Material* material);
	Material* getMaterial();
	void setDrawType(DrawType drawType);

	bool backFaceCulling = true;
	bool depthTest = true;

	///<summary> Returns whether the mesh will be drawn by the camera in this state. </summary>
	bool isCulled(Camera* camera);

	///<summary> Renders the mesh by the specified camera. </summary>
	void render(Camera* camera, bool doCulling = true) override;

	///<summary> When set to true, this function takes MeshRenderer out of the 3D list and into the 2D list. Vice versa if false. </summary>
	void setUI(bool value);
	bool isUI();

};

