#pragma once
#include "../Components/PhysicsActor.h"
#include "PxPhysicsAPI.h"
#include "ObjectType.h"

class GameObject;
class Transform;
class PhysicsRigidStatic : public PhysicsActor {

public:
	PhysicsRigidStatic(GameObject* gameObject, Transform* transform, ObjectType type, bool kinematicFlag);
	virtual ~PhysicsRigidStatic();

	void setTransform(Transform* transform);

	physx::PxRigidStatic* getRigidStatic();

protected:

	physx::PxRigidStatic* rigidStatic;

	virtual void EarlyUpdate() override;
	virtual void Destroy() override;
};

