#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "../BaseTypes/Vector3.h"

class GameObject;
class EngineSystems;
class Transform;
class PlayerCamera : public ScriptBehaviour
{
public:
	PlayerCamera(GameObject* gameObject);
	virtual ~PlayerCamera();

	void SetPlayerGameObject(GameObject* gameObject);
	GameObject* getPlayerCar();
private:
	EngineSystems* engineSystems = nullptr;
	GameObject* playerCar = nullptr;
	Transform* transform = nullptr;

	Transform* target = nullptr;

	float minDistance;
	float damping;
	Vector3 toCameraFromPlayer;

protected:
	virtual void AfterUpdate() override;

};

