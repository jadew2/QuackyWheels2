#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include <vector>
#include "PxPhysicsAPI.h"

class VehicleManager;
class VehiclePositionTracker : public ScriptBehaviour {

public:
	VehiclePositionTracker(GameObject* gameObject, VehicleManager* vehicleManager, std::vector<physx::PxVec3> trackSpline, int killplane);
	virtual ~VehiclePositionTracker();

	void calculatePlace(std::vector<physx::PxVec3> vehiclePositions);
	void PhysicsUpdate() override;

	int getPlace(int ind);
	
	physx::PxReal calculateT(physx::PxVec3 x0, physx::PxVec3 x1, physx::PxVec3 x2);
	
	std::vector<int> getPlaces();
	std::vector<int> getIndex();
	std::vector<physx::PxVec3> getTrackSpline();

private:

	VehicleManager* vehicles = nullptr;
	int kp;

	std::vector<int> places;
	std::vector<int> index;
	std::vector<physx::PxVec3> trackSpline;
};