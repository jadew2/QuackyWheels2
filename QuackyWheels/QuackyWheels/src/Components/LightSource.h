//
// Created by Jade White on 2016-03-04.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "../Renderer/Types/Color.h"

///<summary> Class that represents a lightsource in the scene </summary>
class LightSource : public ScriptBehaviour
{
public:
	enum Type
	{
		Point,
		Directional,
		Spot
	};

	Type lightType = Type::Point;
	Color color = Color::White();
	float intensity = 1.0f;

	float pointRadius = 20.0f;
	float spotRadius = 20.0f;

	LightSource(GameObject* gameObject);
	virtual ~ LightSource();

};
