//
// Created by Jade White on 2016-02-24.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "../Renderer/Layers/RenderLayerMask.h"
#include "../BaseTypes/Matrix4x4.h"
#include "../Renderer/Types/FrameBuffer.h"

class GameObject;
class EngineSystems;
class Transform;

class Frustum;
class Camera : public ScriptBehaviour
{
private:
	EngineSystems* es;
	Transform* transform;

	float fieldOfView = 1.0472f;
	float nearClippingDistance = 1.0f;
	float farClippingDistance = 2000.0f;

	Vector2 viewAspect; //X and Y size of camera
	Vector2 viewOffset; //Screen offset of camera

	FrameBuffer* frameBuffer; //Camera frame buffer
public:
	///<summary> The Layers this mask will render </summary>
	RenderLayerMask mask;

	Camera(GameObject* gameObject);
	virtual ~Camera();

	///<summary> Generates the view matrix based off the camera's position. </summary>
	Matrix4x4 getViewMatrix();
	///<summary> Generates this camera's perspective matrix. </summary>
	Matrix4x4 getPerspectiveMatrix();
	///<summary> Generates this camera's orthographic matrix. If UI mode is true, the space is normalized to a -1 -> 1 cube in all dimensions. </summary>
	Matrix4x4 getOrthographicMatrix(bool uiMode = false);
	///<summary> Generates the camera frustum. </summary>
	Frustum* getFrustum();

	void setFOV(float radians);
	void setNearClipping(float distance);
	void setFarClipping(float distance);
	void setView(Vector2 viewOffset, Vector2 viewAspect);

	float getFOV();
	float getNearClipping();
	float getFarClipping();
	Vector2 getViewOffset();
	Vector2 getViewAspect();

	void generateFrameBuffer();
	FrameBuffer* getFrameBuffer();

	///<summary> Sets this camera up for use with the current rendering context. Use this if you're doing custom rendering. </summary>
	void bindCamera();
	///<summary> Cleans up afyer bindCamera() </summary>
	void unbindCamera();

	///<summary> Width / Height of the camera's aspect. </summary>
	float getAspect();

private:
	Matrix4x4 viewMat;
	Matrix4x4 orthoMat;
	Matrix4x4 uiOrthoMat;
	Matrix4x4 perspectiveMat;
	Frustum* frustum;

	bool viewMatrixDirty;
	bool perspectiveMatDirty;
	bool orthoMatDirty;
	bool frustumDirty;

	void setPropertiesDirty();

protected:
};

