//
// Created by Jade White on 2016-04-13.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "PhysicsVehicle.h"

class Material;
class PhysicsVehicle;
class Camera;
class GameObject;
class MotionBlurEffect : public ScriptBehaviour
{
	PhysicsVehicle* vehicle;
	Camera* camera;

	Material* motionBlurMaterial;
public:
	float minSpeedForBlur;
	float maxSpeedForBlur;

	float minBlurStrength;
	float maxBlurStrength;

	float minBlurDistance;
	float maxBlurDistance;

	MotionBlurEffect(GameObject* gameObject);

	void setParameters(PhysicsVehicle* vehicle, Camera* camera, int unit);

protected:
	virtual void Update() override ;

	float lerp(float from, float to, float t);
};


