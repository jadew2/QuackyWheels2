#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"

class GameObject;
class Transform;
class PhysicsShape : public ScriptBehaviour {

public:
	PhysicsShape(GameObject* gameObject, Transform* transform);
	virtual ~PhysicsShape();

	physx::PxShape* getShape();
	Transform* getTransform();

protected:
	physx::PxShape* physicsShape;

	//Local transform in the actor
	Transform* transform;
};

