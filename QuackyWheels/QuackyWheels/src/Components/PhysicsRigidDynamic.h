#pragma once
#include "../Components/PhysicsActor.h"
#include "PxPhysicsAPI.h"

class PhysicsRigidDynamic : public PhysicsActor {

public:
	PhysicsRigidDynamic(GameObject* gameObject, Transform* transform, ObjectType type, bool kinematicFlag);
	virtual ~PhysicsRigidDynamic();

	void setTransform(Transform* transform);

	physx::PxRigidDynamic* getRigidDynamic();

protected:

	physx::PxRigidDynamic* rigidDynamic;

	virtual void PhysicsUpdate() override;
	virtual void Update() override;

	virtual void Destroy() override;
	
};

