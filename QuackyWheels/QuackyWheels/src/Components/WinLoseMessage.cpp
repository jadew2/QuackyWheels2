//
// Created by Jade White on 2016-03-07.
//

#include "WinLoseMessage.h"
#include "../Components/Transform.h"
#include "../ComponentEntity/GameObject.h"
#include "../Renderer/Types/Material.h"
#include "../Components/MeshRenderer.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"


WinLoseMessage::WinLoseMessage(GameObject* gameObject, Transform* cameraTransformRef) : ScriptBehaviour(gameObject)
{
	transform = GetTransform();
	cameraTransform = cameraTransformRef;
	winLoseModel = Assets::Load<Model>("EngineAssets/DebugModels/WinLooseMessages.obj");
}

WinLoseMessage::~WinLoseMessage()
{
	
}


GameObject* WinLoseMessage::createMessageGO()
{
	GameObject* messageGo = new GameObject(GetGameObject()->getWorld());
	messageGo->GetTransform()->SetParent(cameraTransform, false);
	messageGo->GetTransform()->SetLocalPosition(messageOffset);

	return messageGo;
}


void WinLoseMessage::ShowWinMessage()
{
	if (messageTriggered)
		return;

	GameObject* messageGO = createMessageGO();
	MeshRenderer* winMessageRenderer = new MeshRenderer(messageGO);
	winMessageRenderer->mesh = winLoseModel->getMeshes()[0];
	winMessageRenderer->GetTransform()->SetLocalRotation(Quaternion(0.0f,0.7071f,0.0f,0.7071f));
	winMessageRenderer->GetTransform()->SetLocalScale(Vector3(3.0f,3.0f,3.0f));
	winMessageRenderer->setMaterial(new Material(Assets::Load<Shader>("EngineAssets/Shaders/AmbientShader.glsl")));
	winMessageRenderer->getMaterial()->setColor("ambient",Color::Green());

	messageTriggered = true;
	elaspedTime = 0;
}

void WinLoseMessage::ShowLoseMessage()
{
	if (messageTriggered)
		return;

	GameObject* messageGO = createMessageGO();
	MeshRenderer* loseMessageRenderer = new MeshRenderer(messageGO);
	loseMessageRenderer->mesh = winLoseModel->getMeshes()[1]; //1 for the second mesh.
	loseMessageRenderer->GetTransform()->SetLocalRotation(Quaternion(0.0f, 0.7071f, 0.0f, 0.7071f));
	loseMessageRenderer->GetTransform()->SetLocalScale(Vector3(3.0f, 3.0f, 3.0f));
	loseMessageRenderer->setMaterial( new Material(Assets::Load<Shader>("EngineAssets/Shaders/AmbientShader.glsl")));
	loseMessageRenderer->getMaterial()->setColor("ambient", Color::Red());

	messageTriggered = true;
	elaspedTime = 0;
}

void WinLoseMessage::Update()
{
	/*
	//If no message was displayed...
	if (!messageTriggered)
		return;
	//Otherwise, a message is being displayed. We'll reset/end the game in timeForApplicationReset seconds

	float deltaTime = getEngineSystems()->time->GetDeltaTime();
	elaspedTime += deltaTime;

	if (elaspedTime > timeForApplicationReset)
	{
		//Do nothing...

		//Quit game
		//GetGameObject()->getWorld()->getUniverse()->safeDelete();

		//Reload world...
		//GetGameObject()->getWorld()->safeDelete();
		//World0* newWorld = new World0(GetGameObject()->getWorld()->getUniverse());
	}
	*/
}
