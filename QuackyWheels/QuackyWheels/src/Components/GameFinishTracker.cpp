#include "GameFinishTracker.h"

#include <iostream>
#include "../GameLoop/EngineSystems.h"
#include "../Physics/Physics.h"
#include "../Physics/CollisionHandler.h"

GameFinishTracker::GameFinishTracker(GameObject* gameObject, WinLoseMessage* winLoseMessageComponentRef, physx::PxActor* playerRef, physx::PxActor* aiPlayerRef, std::vector<physx::PxActor*> orderedtriggersRef) : ScriptBehaviour(gameObject)
{
	winLose = winLoseMessageComponentRef;

	player = playerRef;
	aiPlayer = aiPlayerRef;
	orderedTriggers = orderedtriggersRef;
}


GameFinishTracker::~GameFinishTracker()
{
}


void GameFinishTracker::PhysicsUpdate()
{
	std::vector<physx::PxTriggerPair*> pairsLastFrame = getEngineSystems()->physics->collisionHandler->pairsLastFrame;

	for (size_t i = 0; i < pairsLastFrame.size(); i++)
	{
		physx::PxTriggerPair* currPair = pairsLastFrame[i];

		int triggerIndex = findTriggerIndex(currPair->triggerActor);
		if (triggerIndex < 0)
			continue;

		if (currPair->otherActor == player)
			visit(triggerIndex, false);
		else if (currPair->otherActor == aiPlayer)
			visit(triggerIndex, true);
	}
	pairsLastFrame.clear();
}

int GameFinishTracker::findTriggerIndex(physx::PxActor* trigger)
{
	for (size_t i = 0; i < orderedTriggers.size(); i++)
	{
		if (orderedTriggers[i] == trigger)
			return i;
	}
	return -1;
}

void GameFinishTracker::visit(int triggerIndex, bool isAIPlayer)
{
	//The trigger list we'll be modifying
	std::vector<int>* triggerList;

	if (isAIPlayer)
		triggerList = &aiVisitiedTriggers;
	else
		triggerList = &playerVisitiedTriggers;

	//Prevent duplicates
	for (size_t i = 0; i < triggerList->size(); i++)
	{
		if (triggerList->at(i) == triggerIndex)
			return;
	}

	//Find the max trigger index
	int max = -1;
	for (size_t i = 0; i < triggerList->size(); i++)
	{
		if (triggerList->at(i) > max)
			max = triggerList->at(i);
	}

	//If we're one element above the max, add that trigger - check if someone has won the game.
	if (triggerIndex - max == 1)
	{
		triggerList->push_back(triggerIndex);
		checkIfComplete();
	}
	return;
}

void GameFinishTracker::checkIfComplete()
{
	if (aiVisitiedTriggers.size() >= (unsigned int)numberOfTriggers)
	{
		//The Ai has won!
		std::cout << "GameFinishTracker: Ai has won!" << std::endl;
		//winLose->ShowLoseMessage();
		return;
	}

	if (playerVisitiedTriggers.size() >= (unsigned int)numberOfTriggers)
	{
		//The Player has won!
		std::cout << "GameFinishTracker: Player has won!" << std::endl;
		//winLose->ShowWinMessage();
		return;
	}
}
