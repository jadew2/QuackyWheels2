//
// Created by Jade White on 2016-02-21.
//

#include "AudioSource.h"
#include "../GameLoop/EngineSystems.h"
#include "../Sound/Types/Audio.h"
#include "../AssetImport/Assets.h"
#include <SDL_mixer.h>
#include "../Sound/Sound.h"

AudioSource::AudioSource(GameObject *gameObject) : ScriptBehaviour(gameObject)
{
	es = getEngineSystems();
	audio = nullptr;
	channelID = es->sound->GetNewChannel(); //Get our reserved audio channel.
	import = new AudioImporter();

	if (PlayOnInit)
		Play(0);
}

void AudioSource::SetAudio(std::string filePath)
{
	SetAudio(Assets::Load<Audio>(filePath));
}

void AudioSource::SetAudio(Audio* audioRef)
{
	audio = audioRef;
}

void AudioSource::Play(int loops = 0)
{
	if (audio == nullptr)
		return;

	Mix_PlayChannel(channelID,audio->getChunk(),loops);
}

void AudioSource::Resume()
{
	Mix_Resume(channelID);
}

void AudioSource::Pause()
{
	Mix_Pause(channelID);
}

void AudioSource::Stop()
{
	Mix_HaltChannel(channelID);
}

void AudioSource::SetVolume(float value)
{
	//First constrain value from 0-1
	if (value > 1.0f)
		value = 1.0f;
	else if (value < 0.0f)
		value = 0.0f;

	int SDLVolume = (int)(128.0f * value);

	//Volume has a range of 0-128
	Mix_Volume(channelID,SDLVolume);
}

void AudioSource::Init()
{
	ScriptBehaviour::Init();
	
}

void AudioSource::OnEnable()
{
	//TODO: Resume playing audio
}

void AudioSource::OnDisable()
{
	//TODO: Pause playing audio
}

void AudioSource::Destroy()
{
	Stop(); //make sure the audio is stopped.

	es->sound->ReturnChannel(channelID); //Return our reserved audio channel back to the list.
	ScriptBehaviour::Destroy();
}


int AudioSource::IsPlaying()
{
	return Mix_Playing(channelID);
}

int AudioSource::IsPaused()
{
	return Mix_Paused(channelID);
}


