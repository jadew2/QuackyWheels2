#pragma once
#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidStatic;
class Transform;
class PhysicsRigidStaticBox : public PhysicsShape {

public:
	PhysicsRigidStaticBox(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxReal length, physx::PxReal height,
		physx::PxReal width, Transform* transform, physx::PxMaterial* const* materials,
		physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidStaticBox();

	void setTransform(Transform* transform);

	PhysicsRigidStatic* getActor();

protected:
	PhysicsRigidStatic* actor;

};

