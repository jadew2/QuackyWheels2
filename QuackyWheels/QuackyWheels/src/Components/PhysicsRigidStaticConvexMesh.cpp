#include "PhysicsRigidStaticConvexMesh.h"

#include "../GameLoop/EngineSystems.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Physics/Physics.h"

PhysicsRigidStaticConvexMesh::PhysicsRigidStaticConvexMesh(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxU32 numVerticies,
	physx::PxVec3 verticies[], physx::PxU16 numMaterials, physx::PxMaterial* const* meshMaterials,
	Transform* transform, physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData,
	physx::PxShapeFlags* shapeFlags) : PhysicsShape(gameObject, transform) {

	//Store convex mesh desc
	physx::PxConvexMeshDesc convexMeshDesc;
	convexMeshDesc.points.count = numVerticies;
	convexMeshDesc.points.stride = sizeof(physx::PxVec3);
	convexMeshDesc.points.data = verticies;
	convexMeshDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX;

	physx::PxDefaultMemoryOutputStream buf;
	physx::PxConvexMeshCookingResult::Enum result;
	if (!this->getEngineSystems()->physics->getCooking()->cookConvexMesh(convexMeshDesc, buf, &result)) {
		//fatalError("cook shape failed!");
	}
	physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	physx::PxConvexMesh* convexMesh = this->getEngineSystems()->physics->getPhysics()->createConvexMesh(input);
	this->physicsShape = actor->getRigidStatic()->createShape(physx::PxConvexMeshGeometry(convexMesh), meshMaterials, numMaterials);

	this->physicsShape->setFlags(*shapeFlags);

	//Set the query filter data of the ground plane so that the vehicle raycasts can hit the ground.
	this->physicsShape->setQueryFilterData(*qryFilterData);

	//Set the simulation filter data of the ground plane so that it collides with the chassis of a vehicle but not the wheels.
	this->physicsShape->setSimulationFilterData(*simFilterData);
}


PhysicsRigidStaticConvexMesh::~PhysicsRigidStaticConvexMesh() {
	
}

void PhysicsRigidStaticConvexMesh::setTransform(Transform* transform) {
	this->transform = transform;
}

PhysicsRigidStatic* PhysicsRigidStaticConvexMesh::getActor() {
	return this->actor;
}