#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include <time.h>

class Countdown : public ScriptBehaviour
{
private:
	bool countdownFinished;
	time_t start;
	double total_time;
protected:
	virtual void PhysicsUpdate() override;

public:
	Countdown(GameObject *gameObject);
	virtual ~Countdown();

	bool isCountdownFinished();
	float countDown();
};

