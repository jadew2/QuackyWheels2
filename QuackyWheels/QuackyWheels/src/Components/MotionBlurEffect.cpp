//
// Created by Jade White on 2016-04-13.
//

#include "MotionBlurEffect.h"
#include "../Renderer/Types/FrameBuffer.h"
#include "../BaseTypes/Vector2.h"
#include "ImageEffectRenderer.h"
#include "../AssetImport/Assets.h"
#include "Animators/MaterialPropertyAnimator.h"
#include "Camera.h"
#include "../ComponentEntity/GameObject.h"
#include "../Renderer/Types/FrameBufferTexture.h"
#include <glew.h>

MotionBlurEffect::MotionBlurEffect(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	minSpeedForBlur = 20.0f;
	maxSpeedForBlur = 100.0f;

	minBlurStrength = 0.0f;
	maxBlurStrength = 2.0f;

	minBlurDistance = 0.5f;
	maxBlurDistance = 0.5f;

	vehicle = nullptr;
	camera = nullptr;
	motionBlurMaterial = nullptr;
}


void MotionBlurEffect::setParameters(PhysicsVehicle* vehicle, Camera* camera, int unit)
{
	this->vehicle = vehicle;
	this->camera = camera;

	//First, set up the camera's framebuffer.
	camera->generateFrameBuffer();
	FrameBuffer* cameraFrameBuffer = camera->getFrameBuffer();
	Vector2 cameraAspect = camera->getViewAspect();

	//Now create our material
	motionBlurMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/MotionBlurEffect.glsl"));
	motionBlurMaterial->setTexture(new FrameBufferTexture(cameraFrameBuffer, (GLuint)cameraAspect.x, (GLuint)cameraAspect.y, unit));

	//Attach the renderer.
	ImageEffectRenderer* imageEffectRenderer = new ImageEffectRenderer(camera->GetGameObject());
	imageEffectRenderer->setMaterial(motionBlurMaterial);
	imageEffectRenderer->mask = camera->mask;
}


void MotionBlurEffect::Update()
{
	if (motionBlurMaterial == nullptr)
		return;

	//First, get the vehicles velocity
	float velocityMagnitude = vehicle->getVehicle()->computeForwardSpeed();
	if (velocityMagnitude < minSpeedForBlur)
	{
		motionBlurMaterial->setFloat("blurStrength",0.0f);
		return;
	}

	//Interpolate values
	float t = (velocityMagnitude - minSpeedForBlur) / (maxSpeedForBlur - minSpeedForBlur); //Inverse lerp.
	float blurStrength = lerp(minBlurStrength,maxBlurStrength,t);
	float blurDistance = lerp(minBlurDistance,maxBlurDistance,t);

	//Set the values
	motionBlurMaterial->setFloat("blurStrength",blurStrength);
	motionBlurMaterial->setFloat("blurWidth",blurDistance);
}


float MotionBlurEffect::lerp(float from, float to, float t)
{
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;

	return (to - from)*t + from;
}

