#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"

class VehicleManager;
class PowerUpManager;
class MeshRenderer;
class PowerUpBox : public ScriptBehaviour {

public:
	PowerUpBox(GameObject* gameObject, VehicleManager* vehicleManager, PowerUpManager* powerUpManager);
	~PowerUpBox();
	virtual void PhysicsUpdate() override;

private:
	VehicleManager* vehicleManager;
	PowerUpManager* powerUpManager;
	MeshRenderer* powerUpRenderer;
	int cooldown;
public:
	float triggerRadius;
};

