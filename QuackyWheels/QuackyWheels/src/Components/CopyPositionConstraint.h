//
// Created by Jade White on 2016-03-07.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "../BaseTypes/Vector3.h"

///<summary> This component simply positions it's attached gameobject to the targets location. Plus some offset. </summary>
class GameObject;
class Transform;
class CopyPositionConstraint : public ScriptBehaviour
{
	Transform* transform = nullptr;
public:
	CopyPositionConstraint(GameObject* gameObject);
	virtual ~CopyPositionConstraint();

	Transform* target = nullptr;
	Vector3 offset = Vector3::Zero();

protected:
	virtual void AfterUpdate() override;

};
