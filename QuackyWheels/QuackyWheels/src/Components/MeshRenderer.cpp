//
// Created by Jade White on 2016-02-10.
//

#include "MeshRenderer.h"
#include "../Components/Transform.h"
#include "../GameLoop/EngineSystems.h"
#include "../AssetImport/Assets.h"
#include "../Renderer/Types/Material.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/MeshRenderGL.h"
#include "../Renderer/Renderer.h"
#include "../Components/Camera.h"
#include "../Renderer/CullingHelper.h"

#include "../GameLoop/EngineSystems.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/RenderCollection.h"

#include <glew.h>

MeshRenderer::MeshRenderer(GameObject *gameObject) : ScriptBehaviour(gameObject)
{
	mesh = nullptr;

	//Create the default material
	material = new Material(Assets::Load<Shader>("EngineAssets/Shaders/DefaultShader.glsl"));
	material->setColor("ambient",Color::Black());
	material->setColor("tint", Color::White());

	drawType = DrawType::Default;

	es = getEngineSystems();
	transform = GetTransform();

	meshRendererGL =  new MeshRenderGL();
	cullingHelper = new CullingHelper();

	getEngineSystems()->renderer->renderCollection->addMeshRender(this);

	uiMode = false;
	backFaceCulling = true;
}

MeshRenderer::~MeshRenderer()
{
	if (uiMode)
		getEngineSystems()->renderer->renderCollection->removeUIRender(this);
	else
		getEngineSystems()->renderer->renderCollection->removeMeshRender(this);
	delete meshRendererGL;
	delete cullingHelper;
}


void MeshRenderer::setMaterial(Material* material)
{
	this->material = material;
}

Material* MeshRenderer::getMaterial()
{
	return material;
}


void MeshRenderer::setDrawType(MeshRenderer::DrawType drawType)
{
	this->drawType = drawType;
}


bool MeshRenderer::isCulled(Camera* camera)
{
	if (mesh == nullptr)
		return true;

	//Test if this is on the right layer
	if (!camera->mask.collides(mask))
		return true;

	/*
	//Test if this is culled by the camera
	Bounds worldSpaceBounds = mesh->getBounds()->deform(transform->GetTRSMatrix());
	if (cullingHelper->isCulled(camera,worldSpaceBounds))
		return true;
	 */

	return false;
}


void MeshRenderer::render(Camera* camera, bool doCulling)
{
	if (!GetEnabled())
		return;

	if (mesh == nullptr || material == nullptr)
		return;

	if (doCulling && isCulled(camera))
		return;

	//Generate/Update the GL mesh.
	GLMeshBuffers* glMesh = mesh->getGLMesh();

	//First set up the shader
	vector<Texture*> textures = material->getTextures();
	Shader* shader = material->getShader();
	glUseProgram(shader->getGLShader());

	Material::DrawType matDrawType = material->drawType;

	//Enable transparency if wanted
	bool isTransparentDrawType = (matDrawType != Material::DrawType::Default);
	if (isTransparentDrawType)
	{
		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD); //The default blend equation.
		
		if (matDrawType == Material::DrawType::Transparent)
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		if (matDrawType == Material::DrawType::Mulplicative)
			glBlendFunc(GL_ZERO, GL_SRC_COLOR);
		else if (matDrawType == Material::DrawType::Additive)
			glBlendFunc(GL_ONE, GL_ONE);

		glDepthMask(GL_FALSE); //Disable the depth buffer for writing (we re-enable it at the bottom)
	}

	//Now set some default shader properites
	Matrix4x4 viewMat = camera->getViewMatrix();
	Matrix4x4 perspectiveMat = camera->getPerspectiveMatrix(); 
	Matrix4x4 modelViewMat = viewMat * transform->GetTRSMatrix();

	material->setMatrix4x4("ModelViewMat", modelViewMat);
	material->setMatrix4x4("PerpectiveMat", perspectiveMat);
	//material->setVector3("LightPosition",Vector3(50.0f, 100.0f, 100.0f)); //Hardcoded for now, should be fetched from a list of lights in the renderer.

	//Set up things for UI drawing if wanted
	if (uiMode)
	{
		Matrix4x4 orthographicMat = camera->getOrthographicMatrix(true); 
		material->setMatrix4x4("ModelViewMat", transform->GetTRSMatrix());
		material->setMatrix4x4("PerpectiveMat", orthographicMat); //Send in the orthographic matrix.
	}

	//Now load the textures into the GPU
	meshRendererGL->setUpTextures(material);
	material->applyAllPropertiesOnShader();

	if (depthTest)
		glEnable(GL_DEPTH_TEST);
	else
		glDisable(GL_DEPTH_TEST);

	if (backFaceCulling)
		glEnable(GL_CULL_FACE);
	else
		glDisable(GL_CULL_FACE);

	//Toggle wireframe
	MeshRenderer::DrawType mrDrawType = drawType;
	if (mrDrawType == MeshRenderer::DrawType::Default)
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	else if (mrDrawType == MeshRenderer::WireFrame)
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	//Draw the mesh
	bool vertexLines = (mrDrawType == MeshRenderer::DrawType::VertexLine);
	meshRendererGL->drawMesh(glMesh, vertexLines);


	//Now clean up.
	if (isTransparentDrawType)
	{
		glDisable(GL_BLEND);
		glDepthMask(GL_TRUE); //Make sure to re-enable the depth buffer for writing. If you don't, glClear won't work, and we'll fade to nothing OuO.
	}

	glBindTexture(GL_TEXTURE_2D,0);
	glUseProgram(0);

}


void MeshRenderer::setUI(bool value)
{
	uiMode = value;
	if (value)
	{
		depthTest = false;
		getEngineSystems()->renderer->renderCollection->removeMeshRender(this);
		getEngineSystems()->renderer->renderCollection->addUIRender(this);
	}
	else
	{
		depthTest = true;
		getEngineSystems()->renderer->renderCollection->removeUIRender(this);
		getEngineSystems()->renderer->renderCollection->addMeshRender(this);
	}
}

bool MeshRenderer::isUI()
{
	return uiMode;
}
