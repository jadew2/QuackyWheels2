#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"

class EngineSystems;
class GameObject;
class AudioSource;
class PhysicsVehicle;
class SoundEffects : public ScriptBehaviour
{
	AudioSource* accel = nullptr;

	AudioSource* brk = nullptr;
	AudioSource* quack = nullptr;
	AudioSource* idle = nullptr;
	AudioSource* collide = nullptr;

	EngineSystems* es;

public:
	SoundEffects(GameObject* gameObject, GameObject* gameObject2, int index);
	virtual ~SoundEffects();

protected:
	virtual void Update() override;
	void accelerate();
	void breaking();
	void softHit();
	void hardHit();
	void quacking();
	void idling();
	void SetGear(float gear);
	void setCol(bool collision);

	bool isQuack;
	bool isBrake;
	bool isCol;
	bool fHit;
	bool collisionHandler;
	float rpm;
	float currGear = 0.0f;
	int vindex;
	PhysicsVehicle* vehicle;
};

