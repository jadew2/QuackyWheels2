//
// Created by Jade White on 2016-04-11.
//

#include "MainMenuLoader.h"
#include "../ComponentEntity/GameObject.h"
#include "../World/World.h"
#include "../World/MenuWorld.h"


MainMenuLoader::MainMenuLoader(GameObject* gameObject) : ScriptBehaviour(gameObject)
{

}

void MainMenuLoader::Start()
{
	updateHit = false;
}


void MainMenuLoader::AfterUpdate()
{
	if (!updateHit)
	{
		updateHit = true;
		return;
	}

	//Load the main menu. We've waited a frame.
	Assets::UnloadAll();
	World* menu = new MenuWorld(GetGameObject()->getWorld()->getUniverse());
	GetGameObject()->getWorld()->safeDelete();
}




