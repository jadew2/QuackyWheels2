//
// Created by Jade White on 2016-02-26.
//

#pragma once


#include "../ComponentEntity/ScriptBehaviour.h"

//<summary> Manages playing sound for the vehicle </summary>
class VehicleSound : public ScriptBehaviour
{
public:
	VehicleSound(GameObject* gameObject);

protected:
	virtual void Init() override;

};


