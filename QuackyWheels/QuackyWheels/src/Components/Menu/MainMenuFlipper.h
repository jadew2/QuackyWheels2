//
// Created by Jade White on 2016-03-28.
//

#pragma once

#include "System/UIElement.h"
#include "System/IFlippable.h"

class MainMenuFlipper : public UIElement, public IFlippable
{
public:
	MainMenuFlipper(GameObject* gameObject, MenuManager* menuManager);


};

