#pragma once
#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../Transform.h"
#include "../../Various/LevelSettings/LevelSettings.h"
#include "../../World/World2.h"

class TrackButton :	public MainMenuButton
{
private:
	int track;
	std::vector<PlayerSettings> players;
	unsigned long nplay;
public:
	TrackButton(GameObject* gameObject, MenuManager* menu, int trackNum, std::vector<PlayerSettings> numPlayers);
	virtual ~TrackButton();

	void push();
};

