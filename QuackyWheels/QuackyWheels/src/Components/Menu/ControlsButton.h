#pragma once

#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../../Prefabs/Menu/InstructionsScreen.h"

class ControlsButton : public MainMenuButton
{
public:
	ControlsButton(GameObject* gameObject, MenuManager* menu);
	virtual  ~ControlsButton();

	void push() override;
};

