#pragma once

#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"

class QuitButton : public MainMenuButton
{
public:
	QuitButton(GameObject* gameObject, MenuManager* menu);
	virtual ~QuitButton();

	void push() override;
};

