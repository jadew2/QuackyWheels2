#pragma once

#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../../Prefabs/Menu/DuckMenuScreen.h"
#include "../../Various/LevelSettings/PlayerSettings.h"

class NumPlayerButton :	public MainMenuButton
{
private:
	int humanPlayers;

public:
	NumPlayerButton(GameObject* gameObject, MenuManager* menu, int players);
	virtual ~NumPlayerButton();

	void push();
};

