#include "BackButton.h"

#include "../../Various/LevelSettings/PlayerSettings.h"



BackButton::BackButton(GameObject* gameObject, MenuManager* menu, char lastMenu) : MainMenuButton(gameObject, menu)
{
	lastRef = lastMenu;
	setTextTexture(Assets::Load<Texture>("EngineAssets/Images/BackLabel.png"));
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 1.2f, 0.9f));
}

BackButton::BackButton(GameObject* gameObject, MenuManager* menu, char lastMenu, std::vector<PlayerSettings> players) : MainMenuButton(gameObject, menu)
{
	lastRef = lastMenu;
	playerNum = players;
	setTextTexture(Assets::Load<Texture>("EngineAssets/Images/BackLabel.png"));
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 1.2f, 0.9f));
}

BackButton::~BackButton()
{
}

void BackButton::push()
{
	//menuRef->select(nullptr);
	//Need code to go back to the previous menu - likely there should be reference to what the current menu is
	if (lastRef == 'm')
	{
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		MainMenuScreen* mainMenu = new MainMenuScreen(GetGameObject()->getWorld(), menuRef);
		//create MenuScreen0 - main
	}
	else if (lastRef == 'p')
	{
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		NumberOfPlayersScreen* numMenu = new NumberOfPlayersScreen(GetGameObject()->getWorld(), menuRef);
		//create MenuScreen1 - playerSelect
	}
	else if (lastRef == 'd')
	{
		std::vector<PlayerSettings> playerVector;
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		DuckMenuScreen* duckMenu = new DuckMenuScreen(GetGameObject()->getWorld(), menuRef, playerVector, playerNum.size(), 1);
		//create MenuScreen3 - duckSelect
	}
}