#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"
#include <string>
#include "../../ComponentEntity/GameObject.h"
#include "../Transform.h"
#include "../MeshRenderer.h"
#include "../../Renderer/Types/Mesh.h"
#include "../../Renderer/Types/Color.h"
#include "../../Renderer/Types/Material.h"
#include "../../Renderer/Types/Texture.h"
#include "../../BaseTypes/Vector3.h"
#include "System/MenuManager.h"

#include "System/UIElement.h"
#include "System/IButton.h"
#include "../../Prefabs/Menu/ButtonRenderersPrefab.h"

class MeshRenderer;
class Texture;
class MenuManager;
///<summary> Formerly known as MenuItem,  </summary>
class MainMenuButton : public UIElement, public IButton
{
protected:
	ButtonRenderersPrefab* renderersPrefab = nullptr;
	Vector3 defaultScale = Vector3(0.3f, 0.15f, 0.25f);
public:
	int controllablePlayerIndex = 0; //The player that's allowed to control this button

	MainMenuButton(GameObject* gameObject, MenuManager* menu);

	void setTextTexture(Texture* text);
};

