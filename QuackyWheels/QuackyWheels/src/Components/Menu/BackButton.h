#pragma once

#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../../Prefabs/Menu/MainMenuScreen.h"
#include "../../Prefabs/Menu/NumberOfPlayersScreen.h"
#include "../../Prefabs/Menu/DuckMenuScreen.h"
#include "../../Various/LevelSettings/PlayerSettings.h"

class BackButton :	public MainMenuButton
{
private:
	char lastRef;

	//This should only be instantiated when created in MenuScreen4
	std::vector<PlayerSettings> playerNum;

public:
	BackButton(GameObject* gameObject, MenuManager* menu, char lastMenu);
	BackButton(GameObject* gameObject, MenuManager* menu, char lastMenu, std::vector<PlayerSettings> players);
	virtual ~BackButton();

	void push();
};

