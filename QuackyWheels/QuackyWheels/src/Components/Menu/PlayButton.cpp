#include "PlayButton.h"

#include "../../AssetImport/Assets.h"
#include "../../Prefabs/Explosion0Prefab.h"
#include "../Animators/MaterialPropertyAnimator.h"
#include "../Standard/TimedScriptDestroy.h"

#include <vector>
using namespace std;

PlayButton::PlayButton(GameObject* gameObject, MenuManager* menu) : MainMenuButton(gameObject, menu)
{
	setTextTexture(Assets::Load<Texture>("EngineAssets/Images/PlayButton.png"));
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 0.8f, 0.9f));
}


PlayButton::~PlayButton()
{
}

void PlayButton::push()
{
	MainMenuButton::push();

	//Make a explosion
	Explosion0Prefab* explosion = new Explosion0Prefab(GetGameObject()->getWorld());
	explosion->GetTransform()->SetLocalPosition(GetTransform()->GetLocalPosition() + Vector3(0, 0, 5));

	//Flash the sky red
	GameObject* skyboxGO = GetGameObject()->getWorld()->find("SkyBox");
	if (skyboxGO != nullptr)
	{
		MeshRenderer* skyboxRenderer = skyboxGO->GetComponent<MeshRenderer>();
		if (skyboxRenderer != nullptr)
		{
			MaterialPropertyAnimator<Color>* skyboxAnimator = new MaterialPropertyAnimator<Color>(skyboxGO, skyboxRenderer->getMaterial(), "tint");
			skyboxAnimator->values = vector<Color>{Color::Black(), Color::Red(), Color::Black(), Color::White()};
			skyboxAnimator->loopType = MaterialPropertyAnimator<Color>::LoopType::Once;
			skyboxAnimator->duration = 1.5f;
			TimedScriptDestroy* skyboxAnimatorDestroyer = new TimedScriptDestroy(skyboxGO, skyboxAnimator);
			skyboxAnimatorDestroyer->timeForAction = 3.0f;
		}
	}

	//Delete MenuScreen0
	//menuRef->select(nullptr);
	GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();

	//GetGameObject()->GetTransform()->getRoot();
	//Create Player Items, push them
	//Include the character 'm'
	NumberOfPlayersScreen* numMenu = new NumberOfPlayersScreen(GetGameObject()->getWorld(), menuRef);

}