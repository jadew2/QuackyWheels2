#include "NumPlayerButton.h"


NumPlayerButton::NumPlayerButton(GameObject* gameObject, MenuManager* menu, int players) : MainMenuButton(gameObject, menu)
{
	humanPlayers = players;

	//renderersPrefab->compensateTextScaleFor(defaultScale);

	if (players == 1)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/OneLabel.png"));
		//renderersPrefab->textRenderer->GetTransform()->SetLocalScale()
	}
	else if (players == 2)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/TwoLabel.png"));
	}
	else if (players == 3)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/ThreeLabel.png"));
	}
	else if (players == 4)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/FourLabel.png"));
	}
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 0.9f, 0.9f));
}


NumPlayerButton::~NumPlayerButton()
{
}

void NumPlayerButton::push()
{
	//menuRef->select(nullptr);
	std::vector<PlayerSettings> playerVector;
	GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();

	DuckMenuScreen* duckMenu = new DuckMenuScreen(GetGameObject()->getWorld(), menuRef, playerVector, humanPlayers, 1);
	//Make a call to create player-select buttons - use the humanPlayers reference

}