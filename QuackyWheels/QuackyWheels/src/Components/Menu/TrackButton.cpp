#include "TrackButton.h"

#include "../../AssetImport/Assets.h"
#include "../../Renderer/Types/Texture.h"
#include "../../Various/LevelSettings/PlayerSettings.h"


TrackButton::TrackButton(GameObject* gameObject, MenuManager* menu, int trackNum, std::vector<PlayerSettings> numPlayers) : MainMenuButton(gameObject, menu)
{
	track = trackNum;

	if (track == 0)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/Track0Label.png"));
	}
	else if (track == 1)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/dmm.png"));
		this->renderersPrefab->text->GetTransform()->SetLocalPosition(this->renderersPrefab->text->GetTransform()->GetLocalPosition() + Vector3(0.0f, 0.05f, 0.0f));
	}

	nplay = (unsigned long)numPlayers.size();
	if (numPlayers.size() < 4)
	{
		int q = 0;
		int d = 1;
		int f = 2;
		int p = 3;

		for (unsigned int i = 0; i < numPlayers.size(); i++)
		{
			int model = numPlayers[i].getModel();
			if ( model == q)
			{
				q = -1;
			}
			else if (model == d)
			{
				d = -1;
			}
			else if (model == f)
			{
				f = -1;
			}
			else if (model == p)
			{
				p = -1;
			}
		}
		
		if (q != -1)
		{
			numPlayers.push_back(PlayerSettings(q));
		}
		if (d != -1)
		{
			numPlayers.push_back(PlayerSettings(d));
		}
		if (f != -1)
		{
			numPlayers.push_back(PlayerSettings(f));
		}
		if (p != -1)
		{
			numPlayers.push_back(PlayerSettings(p));
		}
	}
	players = numPlayers;
}


TrackButton::~TrackButton()
{
}

void TrackButton::push()
{
	LevelSettings* level = new LevelSettings(players, track, nplay);
	
	menuRef->select(nullptr);
	GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
	GetGameObject()->getWorld()->safeDelete();

	World* world2 = new World2(GetGameObject()->getWorld()->getUniverse(), *level);
		//Load up world of the track button based on number of players
}