#pragma once
#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../../Prefabs/Menu/DuckMenuScreen.h"
#include "../../Prefabs/Menu/TrackMenuScreen.h"
#include <vector>
#include "../../Various/LevelSettings/PlayerSettings.h"

class DuckPrefab;
class CharSelec : public MainMenuButton
{
private:
	int playerNum;
	int totalPlayers;
	int duckIndex = 0;

	std::vector<PlayerSettings>* playerVect;
	DuckPrefab* duck;

public:
	CharSelec(GameObject* gameObject, MenuManager* menu, int num, int id, int duck, std::vector<PlayerSettings>& players);
	virtual ~CharSelec();

	void push();
	void onSelected() override;
	void onDeselected() override;

	//This'll be used 
	void onScrollUp();
	void onScrollDown();

	//May possibly need either a scroll to move through character choices, or a variable to keep track of player
};

