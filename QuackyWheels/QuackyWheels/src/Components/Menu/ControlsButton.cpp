#include "ControlsButton.h"


ControlsButton::ControlsButton(GameObject* gameObject, MenuManager* menu) : MainMenuButton(gameObject, menu)
{
	setTextTexture(Assets::Load<Texture>("EngineAssets/Images/ControlLabel.png"));
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 0.8f, 0.9f));
}


ControlsButton::~ControlsButton()
{
}

void ControlsButton::push()
{
	MainMenuButton::push();
	//menuRef->select(nullptr);
	GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
	//Make a call to show instructions
	//Include the character 'm'
	InstructionsScreen* mainMenu = new InstructionsScreen(GetGameObject()->getWorld(), menuRef);
}