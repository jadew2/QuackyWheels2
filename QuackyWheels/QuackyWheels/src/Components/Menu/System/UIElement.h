//
// Created by Jade White on 2016-03-26.
//

#pragma once


#include "../../../ComponentEntity/ScriptBehaviour.h"

class MenuManager;

///<summary> A class that can be selected by the MenuManager </summary>
class UIElement : public ScriptBehaviour
{
protected:
	MenuManager* menuRef;
public:
	UIElement(GameObject* gameObject, MenuManager* menuManager);
	virtual ~UIElement();

protected:
	virtual void Destroy() override;
};

