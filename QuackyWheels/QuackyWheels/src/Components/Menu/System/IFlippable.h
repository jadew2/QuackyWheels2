//
// Created by Jade White on 2016-03-28.
//

#pragma once


#include "../../../BaseTypes/Vector2.h"

///<summary> Interface for flippable user control </summary>
class IFlippable
{
public:
	///<summary> The direction the control stick is pointed is passed in. </summary>
	virtual void onFlipRequested(Vector2 direction){}

	///<summary> Called when the user is done (they press A) </summary>
	virtual void accept(){}

	///<summary> Called when the user wants to go back (they press B) </summary>
	virtual void reject(){}
};
