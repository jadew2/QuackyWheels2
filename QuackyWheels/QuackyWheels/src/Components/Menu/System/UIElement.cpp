//
// Created by Jade White on 2016-03-26.
//

#include "UIElement.h"

#include "MenuManager.h"

UIElement::UIElement(GameObject* gameObject, MenuManager* menuManagerRef) : ScriptBehaviour(gameObject)
{
	menuRef = menuManagerRef;
	menuRef->addItem(this);
}

void UIElement::Destroy()
{
	//Here so we can get at the transform component before it's gone.
	menuRef->removeItem(this);
}

UIElement::~UIElement()
{
	//Uncomment if destroy isn't being called for some reason
	//menuRef->removeItem(this);
}



