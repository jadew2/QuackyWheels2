#pragma once
#include <vector>


#include "../../../BaseTypes/Vector3.h"
#include "../MainMenuButton.h"
/*struct MenuItem
{
	//coordinates - may need these if this class is needed to render

	Action act;
};*/
class MeshRenderer;
class GameObject;
class MainMenuButton;
class Mesh;
class Time;
class UIElement;
class MenuManager : public ScriptBehaviour
{
private:
	std::vector<UIElement*> elements;

	GameObject* cursor = nullptr;
	UIElement* currSelected = nullptr;

	Time* time;
	float directionCooldown;
	float aButtonCooldown;
	float bButtonCooldown;
public:
	MenuManager(GameObject* gameObject);
	virtual ~MenuManager();

	virtual void Update() override;

	///<summary> Called when we want to fire the A button event. </summary>
	void onAPressed();
	///<summary> Called when we want to fire the B button event. </summary>
	void onBPressed();
	///<summary> Called when we want to fire the joystick direction event. </summary>
	void onDirection(Vector2 direction);

	///<summary> Forces the manager to select a element. </summary>
	void select(UIElement* element);

	void addItem(UIElement* element);
	void removeItem(UIElement* element);

private:
	///<summary> Returns the nearest UI element to the selected cursor. </summary>
	UIElement* findNearestElement();
	///<summary> Returns the nearest UI item in the specified direction. </summary>
	UIElement* findNearestElement(Vector2 direction);

	void animateCursor(UIElement* currentSelected);

};
