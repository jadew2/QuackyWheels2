//
// Created by Jade White on 2016-03-28.
//

#pragma once

///<summary> Interface for pushable buttons. </summary>
class IButton
{
public:
	///<summary> Pushes the button </summary>
	virtual void push(){}

	///<summary> Button is being hovered over. </summary>
	virtual void onSelected(){}
	///<summary> Button is no longer being hovered over. </summary>
	virtual void onDeselected(){}
};


