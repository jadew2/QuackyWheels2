#include "MenuManager.h"

#include "../MainMenuButton.h"
#include "../../../ComponentEntity/GameObject.h"
#include "../../Transform.h"
#include "../../MeshRenderer.h"

#include "../../../GameLoop/EngineSystems.h"
#include "../../../Input/Controller.h"
#include "../../../Time/Time.h"
#include "../../../World/World0.h"

#include "../../../BaseTypes/Matrix4x4.h"
#include "../../../Renderer/Types/Mesh.h"
#include "../../../Renderer/Types/Material.h"
#include "../../../Renderer/Types/Color.h"

#include "../../../World/World.h"
#include "../../../World/Universe.h"
#include "../../../Various/VectorMath.h"
#include "IFlippable.h"

#include "../../../Renderer/Renderer.h"
#include "../../../Renderer/RenderCollection.h"
#include "../../../AssetImport/Assets.h"
#include "../../Animators/MaterialPropertyAnimator.h"

#include <vector>
using namespace std;

MenuManager::MenuManager(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	time = getEngineSystems()->time;
	directionCooldown = 0;
	aButtonCooldown = 0;
	bButtonCooldown = 0;

	elements = vector<UIElement*>();
	currSelected = nullptr;

	cursor = new GameObject(gameObject->getWorld(), "Cursor");
	MeshRenderer* cursRenderer = new MeshRenderer(cursor);
	cursRenderer->mesh = Mesh::Quad();
	cursRenderer->getMaterial()->drawType = Material::DrawType::Transparent;
	cursRenderer->getMaterial()->setColor("ambient", Color(0.5f,0.5f,0.5f,0));
	cursRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Menu/MenuOutline.png"));
	cursRenderer->setUI(true);
	
	MaterialPropertyAnimator<Color>* tintAnimator = new MaterialPropertyAnimator<Color>(cursor, cursRenderer->getMaterial(), "tint");
	tintAnimator->loopType = ValueAnimator<Color>::LoopType::Loop;
	tintAnimator->values = vector<Color>{Color(1, 1, 1, 0.5), Color::Blue(), Color::Blue(), Color(1, 1, 1, 0.5)};
	tintAnimator->duration = 0.5f;

	cursor->GetTransform()->SetLocalPosition(Vector3(0.5f, 0.5f, -1));
	cursor->GetTransform()->SetLocalScale(Vector3::One() * 0.99f); //0.01 units smaller in every dimension from the buttons.
}


MenuManager::~MenuManager()
{
}

void MenuManager::Update()
{
	if (currSelected == nullptr)
		return; //Welp, nothing to do here.
	//If we're here, it's legit, and we can use it.

	//Let's see which player is allowed to move the cursor.
	int playerControllerIndex = 0;
	MainMenuButton* mainMenuButton = dynamic_cast<MainMenuButton*>(currSelected);
	if (mainMenuButton != nullptr)
		playerControllerIndex = mainMenuButton->controllablePlayerIndex;
	
	Controller* controller = getEngineSystems()->controller;
	if (controller->controllerleftJoyXY.size() <= 0)
		return;

	//Clamp the index if we are out of range
	else if (playerControllerIndex > controller->getPlayers() - 1)
		playerControllerIndex = (int)controller->controllerleftJoyXY.size() - 1;

	//Forced 0 if it isn't working...
	//playerControllerIndex = 0;

	//Cool! now get the input
	float x = (float)controller->controllerleftJoyXY[playerControllerIndex]->x;
	float y = (float)controller->controllerleftJoyXY[playerControllerIndex]->y;
	bool aDown = controller->getControllerInput()[playerControllerIndex].handBrake > 0;
	bool bDown = controller->getControllerInput()[playerControllerIndex].powerup;

	Vector2 joyInput = Vector2(x,y);
	float joyMagnitude = joyInput.Magnitude();

	//First, let's see what we have selected.
	animateCursor(currSelected);

	//A button
	if (aButtonCooldown <= 0 && aDown)
	{
		onAPressed();
		aButtonCooldown = 1;
	}
	if (!aDown)
	{
		aButtonCooldown = 0;
	}
	aButtonCooldown -= time->GetDeltaTime();

	//B Button
	if (bButtonCooldown <= 0 && bDown)
	{
		onBPressed();
		bButtonCooldown = 1;
	}
	if (!bDown)
	{
		bButtonCooldown = 0;
	}
	bButtonCooldown -= time->GetDeltaTime();

	//Directional Joystick
	if (directionCooldown <= 0 && joyMagnitude > 0.9f)
	{
		onDirection(joyInput.Normalized());
		directionCooldown = 0.3f;
	}
	if (joyMagnitude < 0.1f)
	{
		directionCooldown = 0;
	}
	directionCooldown -= time->GetDeltaTime();

}


void MenuManager::onAPressed()
{
	IButton* button = dynamic_cast<IButton*>(currSelected);
	if (button != nullptr)
		button->push();

	IFlippable* flipper = dynamic_cast<IFlippable*>(currSelected);
	if (flipper != nullptr)
		flipper->accept();
}

void MenuManager::onBPressed()
{
	IFlippable* flipper = dynamic_cast<IFlippable*>(currSelected);
	if (flipper != nullptr)
		flipper->reject();
}

void MenuManager::onDirection(Vector2 direction)
{
	IFlippable* flipper = dynamic_cast<IFlippable*>(currSelected);
	if (flipper != nullptr)
		flipper->onFlipRequested(direction); //Send the direction input to the flipper, but don't change the current selected element. It'll take care of it.
	else
		select(findNearestElement(direction)); //Move the cursor to the nearest element to be selected.
}


UIElement* MenuManager::findNearestElement()
{

	if (elements.size() <= 0)
		return nullptr;
	if (currSelected == nullptr)
		return elements[0];

	Vector3 selectedPosition = currSelected->GetTransform()->GetPosition();

	UIElement* nearest = currSelected;
	float smallestDistance = FLT_MAX;
	for (size_t i = 0; i < elements.size(); ++i)
	{
		//Ignore the item if it's ourselves
		if (elements[i] == currSelected)
			continue;

		Vector3 elementPosition = elements[i]->GetTransform()->GetLocalPosition();

		float distance = (elementPosition - selectedPosition).SqrMagnitude();
		if (distance < smallestDistance)
		{
			nearest = elements[i];
			smallestDistance = distance;
		}
	}

	return nearest;
}


UIElement* MenuManager::findNearestElement(Vector2 direction)
{
	if (elements.size() <= 0)
		return nullptr;
	//If we have nothing selected, return the first item.
	if (currSelected == nullptr)
		return elements[0];

	//If we're here,
	Vector3 direction3D = Vector3(direction).Normalized();
	Vector3 selectedPosition = currSelected->GetTransform()->GetPosition();
	selectedPosition.z = 0;

	UIElement* nearest = currSelected;
	float smallestDistance = FLT_MAX;

	for (size_t i = 0; i < elements.size(); ++i)
	{
		//Ignore the item if it's ourselves
		if (elements[i] == currSelected)
			continue;

		Vector3 elementPosition = elements[i]->GetTransform()->GetLocalPosition();
		elementPosition.z = 0;

		//If it's under/sideways to our direction, ignore it.
		Vector3 toElementPosition = (elementPosition - selectedPosition).Normalized();
		if (Vector3::Dot(toElementPosition,direction3D) <= 0)
			continue;

		//Find the point with the shortest distance to the line.
		Vector3 closestPoint = VectorMath::ClosestPointOnLine(elementPosition, selectedPosition,selectedPosition + direction3D);

		float distanceToLine = (elementPosition - closestPoint).Magnitude();
		if (distanceToLine < smallestDistance)
		{
			smallestDistance = distanceToLine;
			nearest = elements[i];
		}
	}

	return nearest;
}

void MenuManager::animateCursor(UIElement* currentSelected)
{
	//Now animate moving the cursor to the wanted position
	Transform* cursorTransform = cursor->GetTransform();
	float damping = 15.0f; //Just a multiplier for the speed of the animation.
	float deltaTime = getEngineSystems()->time->GetDeltaTime(); //The time since last frame

	Vector3 wantedPosition = currentSelected->GetTransform()->GetPosition() + Vector3(0,0,-1);
	wantedPosition = Vector3::Lerp(cursorTransform->GetPosition(), wantedPosition, deltaTime * damping);
	cursorTransform->SetLocalPosition(wantedPosition);

	Vector3 wantedScale = currentSelected->GetTransform()->GetLocalScale();
	wantedScale = Vector3::Lerp(cursorTransform->GetLocalScale(), wantedScale, deltaTime * damping);
	cursorTransform->SetLocalScale(wantedScale);
}


void MenuManager::addItem(UIElement* element)
{
	if (elements.size() <= 0)
		select(element);
		//currSelected = element;

	elements.push_back(element);
}

void MenuManager::removeItem(UIElement* element)
{
	if (element == currSelected)
	{
		//If this is called from Destroy()
		//currSelected = findNearestElement();

		//Otherwise, we can't touch it's Transform, so
		currSelected = nullptr;
		//Select any other element.
		for (size_t i = 0; i < elements.size(); i++)
		{
			if (elements[i] == element)
				continue;
			else
			{
				currSelected = elements[i];
				break;
			}
		}
	}

	//Now remove it from the elements list.
	elements.erase(std::remove(elements.begin(), elements.end(), element), elements.end());
}

void MenuManager::select(UIElement* element)
{
	UIElement* previousSelected = currSelected;

	if (element == nullptr)
	{
		if (elements.size() > 0)
			element = elements[0];
	}
	currSelected = element;

	//Deselect the button
	if (previousSelected != nullptr)
	{
		IButton* button = dynamic_cast<IButton*>(previousSelected);
		if (button != nullptr)
			button->onDeselected();
	}

	//Select the button
	if (currSelected != nullptr)
	{
		IButton* button = dynamic_cast<IButton*>(currSelected);
		if (button != nullptr)
			button->onSelected();
	}
}



