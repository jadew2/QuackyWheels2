#pragma once
#include "MainMenuButton.h"
#include "../../ComponentEntity/GameObject.h"
#include "../../Prefabs/Menu/NumberOfPlayersScreen.h"

class PlayButton : public MainMenuButton
{
public:
	PlayButton(GameObject* gameObject, MenuManager* menu);
	virtual ~PlayButton();

	void push() override;
};

