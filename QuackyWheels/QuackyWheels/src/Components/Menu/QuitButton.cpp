#include "QuitButton.h"

#include "../../World/World.h"
#include "../../World/Universe.h"

#include "../../AssetImport/Assets.h"
#include "../../Renderer/Types/Texture.h"

QuitButton::QuitButton(GameObject* gameObject, MenuManager* menu) : MainMenuButton(gameObject, menu)
{
	setTextTexture(Assets::Load<Texture>("EngineAssets/Images/QuitButton.png"));
	this->renderersPrefab->text->GetTransform()->SetLocalScale(Vector3(0.9f, 0.8f, 0.9f));
}


QuitButton::~QuitButton()
{
}

void QuitButton::push()
{
	//Delete everything
	menuRef->GetGameObject()->getWorld()->getUniverse()->safeDelete();

}