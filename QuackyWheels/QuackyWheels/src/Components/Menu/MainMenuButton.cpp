#include "MainMenuButton.h"

MainMenuButton::MainMenuButton(GameObject* gameObject, MenuManager* menu) : UIElement(gameObject, menu)
{
	GetTransform()->SetLocalScale(defaultScale);
	renderersPrefab = new ButtonRenderersPrefab(gameObject->getWorld());
	renderersPrefab->GetTransform()->SetParent(GetTransform(),false);
	controllablePlayerIndex = 0;
}

void MainMenuButton::setTextTexture(Texture* texture)
{
	renderersPrefab->setTextTexture(texture);
}
