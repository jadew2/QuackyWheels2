#include "CharSelec.h"
#include "../../Prefabs/DuckPrefab.h"


CharSelec::CharSelec(GameObject* gameObject, MenuManager* menu, int num, int id, int duck, std::vector<PlayerSettings>& players) : MainMenuButton(gameObject, menu)
{
	duckIndex = duck;
	totalPlayers = num;
	playerNum = id;
	controllablePlayerIndex = id - 1;

	playerVect = &players;

	if (duckIndex == 0)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/quackers.png"));
		this->renderersPrefab->text->GetTransform()->SetLocalPosition(this->renderersPrefab->text->GetTransform()->GetLocalPosition() + Vector3(0.0f, 0.2f, 0.0f));

		DuckPrefab* quacky = new DuckPrefab(this->GetGameObject()->getWorld());
		quacky->GetTransform()->SetParent(gameObject->GetTransform()->getRoot());
		quacky->GetTransform()->SetLocalPosition(Vector3(1.9f, 1.0f, 2.0f));
		quacky->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(20.0f, 240.0f, 0.0f)));
		quacky->GetTransform()->SetLocalScale(Vector3(0.1f, 0.1f, 0.1f));

		this->duck = quacky;
	}
	else if (duckIndex == 1)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/Mac.png"));
		this->renderersPrefab->text->GetTransform()->SetLocalPosition(this->renderersPrefab->text->GetTransform()->GetLocalPosition() + Vector3(0.0f, 0.05f, 0.0f));

		DuckPrefab* Mac = new DuckPrefab(this->GetGameObject()->getWorld());
		Mac->GetTransform()->SetParent(gameObject->GetTransform()->getRoot());
		Mac->GetTransform()->SetLocalPosition(Vector3(-0.5f, 1.0f, 2.0f));
		Mac->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(25.0f, 200.0f, 0.0f)));
		Mac->GetTransform()->SetLocalScale(Vector3(0.1f, 0.1f, 0.1f));
		Mac->setTexture(Assets::Load<Texture>("EngineAssets/Images/DuckyMacDuck.png"));

		this->duck = Mac;
	}
	else if (duckIndex == 2)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/fireq.png"));
		this->renderersPrefab->text->GetTransform()->SetLocalPosition(this->renderersPrefab->text->GetTransform()->GetLocalPosition() + Vector3(0.0f, 0.2f, 0.0f));

		DuckPrefab* fire = new DuckPrefab(this->GetGameObject()->getWorld());
		fire->GetTransform()->SetParent(gameObject->GetTransform()->getRoot());
		fire->GetTransform()->SetLocalPosition(Vector3(1.65f, 0.2f, 2.0f));
		fire->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(20.0f, 240.0f, 0.0f)));
		fire->GetTransform()->SetLocalScale(Vector3(0.1f, 0.1f, 0.1f));
		fire->setTexture(Assets::Load<Texture>("EngineAssets/Images/Firequacker.png"));

		this->duck = fire;
	}
	else if (duckIndex == 3)
	{
		setTextTexture(Assets::Load<Texture>("EngineAssets/Images/Fowl.png"));
		this->renderersPrefab->text->GetTransform()->SetLocalPosition(this->renderersPrefab->text->GetTransform()->GetLocalPosition() + Vector3(0.0f, 0.2f, 0.0f));

		DuckPrefab* Fowl = new DuckPrefab(this->GetGameObject()->getWorld());
		Fowl->GetTransform()->SetParent(gameObject->GetTransform()->getRoot());
		Fowl->GetTransform()->SetLocalPosition(Vector3(-0.4f, 0.2f, 2.0f));
		Fowl->GetTransform()->SetLocalRotation(Quaternion::Euler(Vector3(20.0f, 200.0f, 0.0f)));
		Fowl->GetTransform()->SetLocalScale(Vector3(0.1f, 0.1f, 0.1f));
		Fowl->setTexture(Assets::Load<Texture>("EngineAssets/Images/FowlPlayDuck.png"));

		this->duck = Fowl;
	}
}


CharSelec::~CharSelec()
{
}

void CharSelec::push()
{
	playerVect->push_back(PlayerSettings(duckIndex));

	if (playerNum == totalPlayers)
	{
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		TrackMenuScreen* trackMenu = new TrackMenuScreen(GetGameObject()->getWorld(), menuRef, *playerVect);
	}
	else
	{
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		DuckMenuScreen* duckMenu = new DuckMenuScreen(GetGameObject()->getWorld(), menuRef, *playerVect, totalPlayers, playerNum+1);
	}
	/*
	//This ONLY works for one player. Need an alternate version for 2 or more to not delete until all characters have been selected
	if (playerNum == totalPlayers)
	{
		//menuRef->select(nullptr);
		GetGameObject()->GetTransform()->getRoot()->GetGameObject()->SafeDelete();
		TrackMenuScreen* trackMenu = new TrackMenuScreen(GetGameObject()->getWorld(), menuRef, *playerVect);
	}
	else
	{
		//Do stuff to black out the first one and move onto the second
	}*/
}

void CharSelec::onScrollUp()
{
	if (duckIndex == 0)
	{
		duckIndex = 3;
	}
	else
	{
		duckIndex--;
	}
}

void CharSelec::onScrollDown()
{
	if (duckIndex == 3)
	{
		duckIndex = 0;
	}
	else
	{
		duckIndex++;
	}
}

void CharSelec::onSelected()
{
	this->duck->GetTransform()->SetLocalScale(Vector3(0.15f, 0.15f, 0.15f));
}

void CharSelec::onDeselected()
{
	this->duck->GetTransform()->SetLocalScale(Vector3(0.1f, 0.1f, 0.1f));
}