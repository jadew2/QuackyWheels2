#include "BolderSpawner.h"
#include "../ComponentEntity/GameObject.h"
#include "../GameLoop/EngineSystems.h"
#include "VehicleManager.h"
#include "PhysicsRigidDynamic.h"
#include "PhysicsRigidDynamicSphere.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "../Physics/Physics.h"
#include "../AssetImport/Assets.h"
#include "PhysicsVehicle.h"
#include "../Renderer/Types/Material.h"


BolderSpawner::BolderSpawner(GameObject *gameObject, VehicleManager* vehicleManager, int killPlane, Vector3 spawnPoint) : ScriptBehaviour(gameObject) {
	this->vehicleManager = vehicleManager;
	this->killPlane = killPlane;
	this->spawnPoint = spawnPoint;

	physx::PxFilterData bolderQryFilterData;
	setupDrivableSurface(bolderQryFilterData);

	physx::PxFilterData bolderSimFilterData;
	bolderSimFilterData.word0 = COLLISION_FLAG_OBSTACLE;
	bolderSimFilterData.word1 = COLLISION_FLAG_OBSTACLE_AGAINST;

	physx::PxShapeFlags bolderShapeFlags = physx::PxShapeFlag::eVISUALIZATION | physx::PxShapeFlag::eSIMULATION_SHAPE | physx::PxShapeFlag::eSCENE_QUERY_SHAPE;

	physx::PxMaterial* bolderMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.5f, 0.5f, 0.0f);
	physx::PxMaterial* const* bolderMaterialPtr = &bolderMaterial;

	GameObject* bolderGameObject = new GameObject(this->GetGameObject()->getWorld(), "Bolder");
	PhysicsRigidDynamic* bolderActor = new PhysicsRigidDynamic(bolderGameObject, bolderGameObject->GetTransform(), ObjectType::Obstacle, false);
	PhysicsRigidDynamicSphere* bolderShape = new PhysicsRigidDynamicSphere(bolderGameObject, bolderActor, 5.0f, bolderGameObject->GetTransform(), bolderMaterialPtr, &bolderQryFilterData, &bolderSimFilterData, &bolderShapeFlags);
	bolderActor->getRigidDynamic()->setGlobalPose(physx::PxTransform(spawnPoint.pxVec3()));
	physx::PxRigidBodyExt::updateMassAndInertia(*bolderActor->getRigidDynamic(), 100000.0f, NULL, true);
	MeshRenderer* bolderRenderer = new MeshRenderer(bolderGameObject);
	Mesh* importedBolder = Assets::Load<Mesh>("EngineAssets/Models/Boulder.obj");
	Mesh* bolderMesh = importedBolder->Clone();
	bolderRenderer->mesh = bolderMesh;
	bolderRenderer->mesh->Deform(Matrix4x4::TRS(Vector3::Zero(), Quaternion::Identity(), Vector3(5, 5, 5)));
	bolderRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/ice.png"));
	this->getEngineSystems()->physics->getScene()->addActor(*bolderActor->getRigidDynamic());

	GameObject* bolderGameObject2 = new GameObject(this->GetGameObject()->getWorld(), "Bolder");
	PhysicsRigidDynamic* bolderActor2 = new PhysicsRigidDynamic(bolderGameObject2, bolderGameObject2->GetTransform(), ObjectType::Obstacle, false);
	PhysicsRigidDynamicSphere* bolderShape2 = new PhysicsRigidDynamicSphere(bolderGameObject2, bolderActor2, 5.0f, bolderGameObject2->GetTransform(), bolderMaterialPtr, &bolderQryFilterData, &bolderSimFilterData, &bolderShapeFlags);
	spawnPoint.x = -180.0f;
	spawnPoint.z = 295.0f;
	bolderActor2->getRigidDynamic()->setGlobalPose(physx::PxTransform(spawnPoint.pxVec3()));
	physx::PxRigidBodyExt::updateMassAndInertia(*bolderActor2->getRigidDynamic(), 100000.0f, NULL, true);
	MeshRenderer* bolderRenderer2 = new MeshRenderer(bolderGameObject2);
	Mesh* importedBolder2 = Assets::Load<Mesh>("EngineAssets/Models/Boulder.obj");
	Mesh* bolderMesh2 = importedBolder2->Clone();
	bolderRenderer2->mesh = bolderMesh2;
	bolderRenderer2->mesh->Deform(Matrix4x4::TRS(Vector3::Zero(), Quaternion::Identity(), Vector3(5, 5, 5)));
	bolderRenderer2->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/ice.png"));
	this->getEngineSystems()->physics->getScene()->addActor(*bolderActor2->getRigidDynamic());

	for (size_t i = 0; i < vehicleManager->getNbVehicles(); i++) {
		PhysicsVehicle* focusVehicle = ((GameObject*)vehicleManager->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>();
		focusVehicle->obstacles.push_back(bolderActor);
		focusVehicle->obstacleRelativePos.push_back(focusVehicle->getVehicle()->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, 0, 1)).dot(focusVehicle->obstacles[0]->getRigidDynamic()->getGlobalPose().p - focusVehicle->getVehicle()->getRigidDynamicActor()->getGlobalPose().p) > 0);
		focusVehicle->obstacles.push_back(bolderActor2);
		focusVehicle->obstacleRelativePos.push_back(focusVehicle->getVehicle()->getRigidDynamicActor()->getGlobalPose().rotate(physx::PxVec3(0, 0, 1)).dot(focusVehicle->obstacles[1]->getRigidDynamic()->getGlobalPose().p - focusVehicle->getVehicle()->getRigidDynamicActor()->getGlobalPose().p) > 0);
	}
	bolderObstacles.push_back(bolderGameObject);
	bolderObstacles.push_back(bolderGameObject2);
}


BolderSpawner::~BolderSpawner() {
	for (size_t i = 0; i < bolderObstacles.size(); i++) {
		this->getEngineSystems()->physics->getScene()->removeActor(*bolderObstacles[i]->GetComponent<PhysicsRigidDynamic>()->getRigidDynamic());
	}
}

void BolderSpawner::PhysicsUpdate() {
	for (size_t i = 0; i < bolderObstacles.size(); i++) {
		physx::PxRigidDynamic* bolderActor = bolderObstacles[i]->GetComponent<PhysicsRigidDynamic>()->getRigidDynamic();
		if (bolderActor->getGlobalPose().p.y < killPlane) {
			bolderActor->setGlobalPose(physx::PxTransform(spawnPoint.pxVec3()));
			bolderActor->setAngularVelocity(physx::PxVec3(0.0f, 0.0f, 0.0f));
			bolderActor->setLinearVelocity(physx::PxVec3(0.0f, 0.0f, 0.0f));
		}
	}
}
