//
// Created by Jade White on 2016-01-29.
//

#pragma once

#include <stdio.h>
#include <vector>
#include <string>

#include "../ComponentEntity/Component.h"
#include "../Exceptions/NotImplementedException.h"
#include "../Exceptions/EngineException.h"
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Quaternion.h"
#include "../BaseTypes/Matrix4x4.h"


class Transform : public Component
{
private:
	Transform* parent;
	std::vector<Transform*> children;

	Vector3 localPosition = Vector3::Zero();
	Quaternion localRotation = Quaternion::Identity();
	Vector3 localScale = Vector3::One();

	bool localMatrixDirty = false; //A flag on whether one of the transform's properties changed this frame.
	Matrix4x4 localTRSMatrix = Matrix4x4::Identity();

public:
	//Transform(); //NOTE: You should never be able to create a Transform without a gameObject to attach it to!
	Transform(GameObject* gameObject);
	Transform(GameObject* gameObject, Vector3* position, Quaternion* rotation);
	Transform(GameObject* gameObject, physx::PxTransform* transform);

public:
	Vector3 Forward();
	Vector3 Left();
	Vector3 Right();
	Vector3 Up();

//region Parent-Child Methods
public:
	///<summary> Returns the parent of this Transform. Null if one doesn't exist. </summary>
	Transform* GetParent();
	///<summary> Sets the parent of this Transform.
	void SetParent(Transform* transform, bool keepWorldPosRot = false);

	///<summary> Returns the root transform of this transform </summary>
	Transform* getRoot();

	///<summary> Returns a list of the ancestors of this Transform, up until a object has no parent. [0] Has the nearest parent </summary>
	std::vector<Transform*> GetAncestors();

	///<summary> Returns a list of the children of this Transform. If recursive is set to true, returns all children instead of only direct children. </summary>
	std::vector<Transform*> GetChildren(bool recursive = false);

	///<summary> Adds the passed transform as a child of this Transform. </summary>
	///<remarks> The same as calling childTransform.SetParent(parent) </remarks>
	void AddChild(Transform* transform, bool keepWorldPosition = true);

	///<summary> Removes the passed transform as a child of this Transform. </summary>
	///<remarks> The same as calling childTransform.SetParent(nullptr) </remarks>
	void RemoveChild(Transform* transform, bool keepWorldPosition = true);

	///<summary> Removes all the children of this transform. </summary>
	///<remarks> The same as calling childTransform.SetParent(nullptr) for each child. </remarks>
	void RemoveAllChildren(bool keepWorldPositions = true);

private:
	void internalAddChild(Transform* child);
	void internalRemoveChild(Transform* child);

//endregion

//region Position/Matrix Methods
public:
	///<summary> Gets the world position of this transform. </summary>
	Vector3 GetPosition();
	///<summary> Sets the world position of this transform. </summary>
	void SetPosition(Vector3 value);

	///<summary> Gets the local position of this transform. </summary>
	Vector3 GetLocalPosition();
	///<summary> Sets the local position of this transform. </summary>
	void SetLocalPosition(Vector3 value);

	///<summary> Gets the world rotation of this transform. </summary>
	Quaternion GetRotation();
	///<summary> Sets the world rotation of this transform. </summary>
	void SetRotation(Quaternion rotation);

	///<summary> Gets the local rotation of this transform. </summary>
	Quaternion GetLocalRotation();
	///<summary> Sets the local rotation of this transform. </summary>
	void SetLocalRotation(Quaternion localRotation);

	///<summary> Gets the world scale of this transform. Warning, this is lossy (can be skewed!) due to the nature of matrices. </summary>
	Vector3 GetLossyScale();

	///<summary> Gets the world scale of this transform. Warning, this is lossy (can be skewed!) due to the nature of matrices. </summary>
	//Vector3 GetScale();
	///<summary> Sets the world scale of this transform. Warning, this is lossy due to the nature of matrices. </summary>
	//void SetScale(Vector3 value);

	///<summary> Gets the local scale of this transform. </summary>
	Vector3 GetLocalScale();
	///<summary> Sets the local scale of this transform. </summary>
	void SetLocalScale(Vector3 value);

	///<summary> Gets the world model matrix of this transform. </summary>
	Matrix4x4 GetTRSMatrix();
	///<summary> Gets the local model matrix of this transform. </summary>
	Matrix4x4 GetLocalTRSMatrix();

	Matrix4x4 getLocalToWorldMatrix();
	Matrix4x4 getWorldToLocalMatrix();

	void setVehicleTransform(glm::mat4 vehicleTransform);
	Matrix4x4 getVehicleTransform();

	///<summary> Returns the point in local space. </summary>
	///<remarks> This is like InverseTransformPoint() in Unity </remarks>
	Vector3 ToLocalPoint(Vector3 worldPoint);

	operator physx::PxTransform();
private:
	void setMatrixDirty();
	//endregion

	///<summary> Returns the point in world space. </summary>
	///<remarks> This is like TransformPoint() in Unity </remarks>
	Vector3 ToWorldPoint(Vector3 localPoint);


	Matrix4x4 vTransform;
};