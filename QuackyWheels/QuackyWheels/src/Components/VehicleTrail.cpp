#include "VehicleTrail.h"
#include "../GameLoop/EngineSystems.h"
#include "../ComponentEntity/GameObject.h"
#include "MeshRenderer.h"
#include "PhysicsRigidStatic.h"
#include "PhysicsRigidStaticBox.h"
#include "../AssetImport/Assets.h"
#include "../Vehicle/VehicleFilterShader.h"
#include "../Vehicle/VehicleSceneQueryData.h"
#include "Transform.h"
#include "ObjectType.h"
#include "../Physics/Physics.h"
#include "../Renderer/Types/Material.h"
#include "../Components/Standard/TextureSlider.h"

VehicleTrail::VehicleTrail(GameObject* gameObject, physx::PxScene* scene, PhysicsVehicle* physicsVehicle) : ScriptBehaviour(gameObject) {
	this->gameObject = gameObject;
	this->scene = scene;
	this->physicsVehicle = physicsVehicle;
	this->solid = false;
	numPieces = 0;

	this->trailMaterial = this->getEngineSystems()->physics->getPhysics()->createMaterial(0.0f, 0.0f, 0.0f);
	this->trailQryFilterData = new physx::PxFilterData();
	setupNonDrivableSurface(*this->trailQryFilterData);
	this->trailSimFilterData = new physx::PxFilterData();
	trailSimFilterData->word0 = COLLISION_FLAG_TRIGGER;
	trailSimFilterData->word1 = COLLISION_FLAG_TRIGGER_AGAINST;
	this->trailShapeFlags = new physx::PxShapeFlags(physx::PxShapeFlag::eTRIGGER_SHAPE);

	this->waterMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/WaterShader.glsl"));
	waterMaterial->setTexture(Assets::Load<Texture>("EngineAssets/Images/Water02.png"));
	Texture* texture = Assets::Load<Texture>("EngineAssets/Images/WaterMaskRadial.png");
	texture->setTextureUnit(1);
	waterMaterial->setTexture(texture, 1);
	waterMaterial->drawType = Material::DrawType::Transparent;
	this->iceMaterial = new Material(Assets::Load<Shader>("EngineAssets/Shaders/DefaultShader.glsl"));
	this->iceMaterial->setColor("tint", Color::White());
	iceMaterial->setTexture(Assets::Load<Texture>("EngineAssets/Images/ice.png"));
	TextureSlider* textureSlider = new TextureSlider(gameObject, waterMaterial);
	textureSlider->offsetSpeed = Vector2(0.5, 0.5);
}


VehicleTrail::~VehicleTrail() {
	delete trailQryFilterData;
	delete trailSimFilterData;
	delete trailShapeFlags;

	for (std::list<GameObject*>::iterator it = trail.begin(); it != trail.end(); it++) {
		//physx::PxRigidActor* actor = (*it)->GetComponent<PhysicsRigidStatic>()->getRigidStatic();
		//scene->removeActor(*actor);
		(*it)->SafeDelete();
	}
	trail.clear();
}

bool stagger = false;
void VehicleTrail::createTrail(physx::PxVec3 previousPosition, physx::PxTransform newPosition, physx::PxReal length) {
	physx::PxTransform trailTransform = newPosition.transform(physx::PxTransform(physx::PxVec3(0, -1.0f, (-length / 2) - 2.5f), physx::PxQuat(physx::PxIdentity)));
	GameObject* trailPiece = new GameObject(this->GetGameObject()->getWorld(), "trailPiece", &trailTransform);
	PhysicsRigidStatic* trailComponentActor = new PhysicsRigidStatic(trailPiece, trailPiece->GetTransform(), ObjectType::Trail, false);
	PhysicsRigidStaticBox* trailComponentShape = new PhysicsRigidStaticBox(trailPiece, trailComponentActor, length, 1.0f, 2.0f, trailPiece->GetTransform(), &trailMaterial, trailQryFilterData, trailSimFilterData, trailShapeFlags);
	MeshRenderer* trailRenderer = new MeshRenderer(trailPiece);
	trailPiece->GetTransform()->SetLocalScale(Vector3(2.0, 1.0, length));
	if (solid) {
		trailSimFilterData->word0 = COLLISION_FLAG_OBSTACLE;
		trailSimFilterData->word1 = COLLISION_FLAG_OBSTACLE_AGAINST;
		trailComponentShape->getShape()->setSimulationFilterData(*trailSimFilterData);
		trailComponentShape->getShape()->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, false);
		trailComponentShape->getShape()->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, true);
		trailComponentShape->getShape()->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, true);
		trailRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/UVCube.obj");
		trailRenderer->setMaterial(iceMaterial);
	}
	else {
		trailRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/WaterQuad.obj");
		trailRenderer->setMaterial(waterMaterial);
	}
	scene->addActor(*trailComponentActor->getRigidStatic());
	if (numPieces < 50) {
		trail.push_back(trailPiece);
		numPieces++;
	} else {
		//scene->removeActor(*trail.front()->GetComponent<PhysicsRigidStatic>()->getRigidStatic());
		//Currently safe delete does not work
		trail.front()->SafeDelete();
		trail.pop_front();
		trail.push_back(trailPiece);
	}	
}

void VehicleTrail::removeTrail() {

	for (std::list<GameObject*>::iterator it = trail.begin(); it != trail.end(); it++) {
		//scene->removeActor(*(*it)->GetComponent<PhysicsRigidStatic>()->getRigidStatic());
		(*it)->SafeDelete();
	}
	trail.clear();
	numPieces = 0;
}

void VehicleTrail::solidTrail() {
	for (std::list<GameObject*>::iterator it = trail.begin(); it != trail.end(); it++) {
		physx::PxShape* shape = (*it)->GetComponent<PhysicsRigidStaticBox>()->getShape();
		trailSimFilterData->word0 = COLLISION_FLAG_OBSTACLE;
		trailSimFilterData->word1 = COLLISION_FLAG_OBSTACLE_AGAINST;
		shape->setSimulationFilterData(*trailSimFilterData);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, true);
		shape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, true);
		(*it)->GetComponent<MeshRenderer>()->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/UVCube.obj");
		(*it)->GetComponent<MeshRenderer>()->setMaterial(iceMaterial);
	}
	solid = true;
}

void VehicleTrail::nonSolidTrail() {
	for (std::list<GameObject*>::iterator it = trail.begin(); it != trail.end(); it++) {
		physx::PxShape* shape = (*it)->GetComponent<PhysicsRigidStaticBox>()->getShape();
		trailSimFilterData->word0 = COLLISION_FLAG_TRIGGER;
		trailSimFilterData->word1 = COLLISION_FLAG_TRIGGER_AGAINST;
		shape->setSimulationFilterData(*trailSimFilterData);
		shape->setFlag(physx::PxShapeFlag::eSIMULATION_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eSCENE_QUERY_SHAPE, false);
		shape->setFlag(physx::PxShapeFlag::eTRIGGER_SHAPE, true);
		(*it)->GetComponent<MeshRenderer>()->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/WaterQuad.obj");
		(*it)->GetComponent<MeshRenderer>()->setMaterial(waterMaterial);
	}
	solid = false;
}