//
// Created by Jade White on 2016-02-21.
//

#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include <string>

///<summary> Class representing a audio source component. </summary>
class GameObject;
class EngineSystems;
class Audio;
class AudioImporter;
class AudioSource : public ScriptBehaviour
{
private:
	EngineSystems* es; //Optimization
	Audio* audio;
	AudioImporter* import;

	enum State {Stopped,Paused,Playing};
	///<summary> The current state of the audio. </summary>
	State state;

	///<summary> The SDL mixer channel ID for this AudioSource. Initialized from Sound.cpp on Init. </summary>
	int channelID = -1;
public:
	bool PlayOnInit = false;

	AudioSource(GameObject* gameObject);

public:
	///<summary> Sets the audio used in this audio source. </summary>
	void SetAudio(std::string filePath);
	///<summary> Sets the audio used in this audio source. </summary>
	void SetAudio(Audio* audio);

	///<summary> Plays the audio. If audio was previously paused, resumes it ignoring changes to loop. Loop: -1 = infinite loops, 0 = play once, 1 = one loop...</summary>
	void Play(int loops);


	///<summary> Resumes the audio. </summary>
	void Resume();
	///<summary> Pauses the audio. </summary>
	void Pause();
	///<summary> Stops and Rewinds the audio. </summary>
	void Stop();

	///<summary> Sets the audio volume. Ranges from 0 --> 1. </summary>
	void SetVolume(float value);

	int IsPlaying();
	int IsPaused();

protected:
	virtual void Init() override;
	virtual void OnEnable() override ;
	virtual void OnDisable() override ;
	virtual void Destroy() override;


};



