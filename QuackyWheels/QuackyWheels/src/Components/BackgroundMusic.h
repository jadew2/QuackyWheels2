//
// Created by Jade White on 2016-02-26.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"

class AudioSource;
class EngineSystems;
class BackgroundMusic: public ScriptBehaviour
{
	AudioSource* countDown = nullptr;

	AudioSource* darudeSandstorm = nullptr;
	AudioSource* backgroundMusic = nullptr;

	bool oldSandstorm = false;

	EngineSystems* es;
public:
	BackgroundMusic(GameObject* gameObject);

protected:
	virtual void Start() override;
	virtual void Update() override;

};
