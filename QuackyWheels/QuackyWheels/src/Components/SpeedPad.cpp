#include "SpeedPad.h"
#include "../GameLoop/EngineSystems.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "PhysicsVehicle.h"
#include "VehicleManager.h"
#include "Transform.h"
#include "../ComponentEntity/GameObject.h"
#include "../Physics/Physics.h"
#include "MeshRenderer.h"
#include "../Renderer/Types/Material.h"
#include "AudioSource.h"
#include "../Prefabs/SpeedPowerUpPrefab.h"
#include "Standard/TextureSlider.h"


SpeedPad::SpeedPad(GameObject* gameObject, VehicleManager* vehicleManager, Vector3 pos, Quaternion rot) : ScriptBehaviour(gameObject) {
	this->vehicleManager = vehicleManager;
	this->hit.resize(vehicleManager->getNbVehicles());
	for (size_t i = 0; i < hit.size(); i++) {
		hit[i] = 0;
	}

	gameObject->GetTransform()->SetLocalPosition(pos);
	gameObject->GetTransform()->SetLocalRotation(rot);

	gameObject->GetTransform()->SetLocalScale(Vector3(4,4,4));
	MeshRenderer* meshRenderer = new MeshRenderer(gameObject);
	Mesh* importedSpeedPad = Assets::Load<Mesh>("EngineAssets/DebugModels/WaterQuad.obj");
	Mesh* speedPadMesh = importedSpeedPad->Clone();
	meshRenderer->mesh = speedPadMesh;
	meshRenderer->mesh->Deform(Matrix4x4::TRS(Vector3::Zero(), Quaternion::Identity(), Vector3(5, 5, 5)));
	meshRenderer->setMaterial(new Material(Assets::Load<Shader>("EngineAssets/Shaders/WaterShader.glsl")));
	meshRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/Water.png"));

	Texture* texture = Assets::Load<Texture>("EngineAssets/Images/WaterMaskRadial.png");
	texture->setTextureUnit(1);
	meshRenderer->getMaterial()->setTexture(texture, 1);
	meshRenderer->getMaterial()->drawType = Material::DrawType::Transparent;

	TextureSlider* textureSlider = new TextureSlider(gameObject, meshRenderer->getMaterial());
	textureSlider->offsetSpeed = Vector2(0.5, 0.5);
}


SpeedPad::~SpeedPad() {

}

void SpeedPad::PhysicsUpdate() {
	for (size_t i = 0; i < hit.size(); i++) {
		if ((vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p - this->GetGameObject()->GetTransform()->GetLocalPosition()).magnitude() < 20.0f) {
			if (hit[i] == 0) {
				AudioSource* exp = new AudioSource(this->GetGameObject());
				exp->SetAudio("EngineAssets/Sounds/Ignition.wav");
				exp->SetVolume(1.0f);
				exp->Play(0);
			}
			hit[i] = 61;
		}
		if (hit[i] > 0) {
			physx::PxRigidBodyExt::addForceAtLocalPos(*vehicleManager->getVehicle(i)->getRigidDynamicActor(), vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().transform(physx::PxVec3(0.0f, 0.0f, 100000.0f)), physx::PxVec3(0.0f, -1.6f, 0.0f));
			if (hit[i] % 10 == 0) {
				SpeedPowerUpPrefab* flames = new SpeedPowerUpPrefab(this->GetGameObject()->getWorld());
				flames->GetTransform()->SetLocalPosition(((GameObject*)vehicleManager->getVehicle(i)->getRigidDynamicActor()->userData)->GetTransform()->GetLocalPosition());
				flames->GetTransform()->SetLocalRotation(((GameObject*)vehicleManager->getVehicle(i)->getRigidDynamicActor()->userData)->GetTransform()->GetLocalRotation());
			}
			hit[i]--;
		}
	}
}
