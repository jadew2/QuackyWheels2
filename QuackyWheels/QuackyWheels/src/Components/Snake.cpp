#include "Snake.h"

#include "VehicleManager.h"
#include "Animators\SnakeAnimator.h"
#include "VehiclePositionTracker.h"

Snake::Snake(GameObject* gameObject, VehicleManager* vehicleManager, SnakeAnimator* snakeAnimator, VehiclePositionTracker* vehiclePositionTracker) : ScriptBehaviour(gameObject){
	this->vehicleManager = vehicleManager;
	this->snakeAnimator = snakeAnimator;
	this->vehiclePositionTracker = vehiclePositionTracker;
	this->aggro = -1;
	//Tweak these defaults
	this->aggroRadius = 120.0f;
	this->animationTime = 0.6f;
	this->currentAnimation = 0;
	this->cooldown = 0;
	this->hitRadius = 7.0f;
	this->hitLocation = physx::PxVec3(0.0f, 0.0f, 0.0f);
	this->snakeLocation = physx::PxVec3(210.9f, -207.5f, 79.0f);
}


Snake::~Snake() {

}

void Snake::PhysicsUpdate() {
	if (cooldown <= 0) {
		if (aggro == -1) {
			for (unsigned int i = 0; i < vehicleManager->getNbVehicles(); i++) {
				if ((vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p - snakeLocation).magnitude() < aggroRadius) {
					if (aggro != -1) {
						if (vehiclePositionTracker->getPlace(i) < vehiclePositionTracker->getPlace(aggro)) {
							aggro = i;
						}
					} else {
						aggro = i;
					}
				}
			}
		}
		if ((aggro != -1) && (currentAnimation <= 0)) {
			physx::PxRigidDynamic* aggroVehicleActor = vehicleManager->getVehicle(aggro)->getRigidDynamicActor();
			physx::PxVec3 vehicleForward = aggroVehicleActor->getGlobalPose().rotate(physx::PxVec3(0.0, 0.0, 1.0));
			vehicleForward.normalize();
			hitLocation = aggroVehicleActor->getGlobalPose().p + (vehicleForward * animationTime * vehicleManager->getVehicle(aggro)->computeForwardSpeed());
			currentAnimation = 120.0;
			snakeAnimator->setTarget(Vector3(hitLocation));
		}
		if (currentAnimation > 0) {
			currentAnimation = currentAnimation - 1.0f;
			snakeAnimator->animate((120.0f - currentAnimation) / 120.0f);
			if (currentAnimation == 84) {
				if ((vehicleManager->getVehicle(aggro)->getRigidDynamicActor()->getGlobalPose().p - hitLocation).magnitude() < hitRadius) {
					vehicleManager->getVehicle(aggro)->getRigidDynamicActor()->setLinearVelocity(physx::PxVec3(0.0, 30.0f, 0.0));
				}
				aggro = -1;
			}
			if (currentAnimation == 0) {
				cooldown = 120;
			}
		}
	} else {
		//Retract the neck, reverse the texture slide
		cooldown--;
	}
}
