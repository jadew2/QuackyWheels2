//
// Created by Jade White on 2016-04-13.
//

#include <glew.h>
#include "CameraResizer.h"

#include "../Components/Camera.h"
#include "../Exceptions/EngineException.h"

CameraResizer::CameraResizer(GameObject* gameObject) : ScriptBehaviour(gameObject)
{

}

void CameraResizer::resizeCameras(std::vector<Camera*> cameras)
{
	unsigned long numScreens = cameras.size();

	if (numScreens > 4)
		throw EngineException("CameraResizer: resizeCameras: The internal method was hardcoded to support only 1-4 players.");

	int x = 0;
	int y = 0;
	int three_Width = 960;

	if (numScreens > 1)
	{
		x = 0;
		y = 540;
	}

	for (auto camera : cameras)
	{
		if (numScreens == 2)
		{
			camera->setView(Vector2((float) x,(float) y),Vector2(1920.0f, 540.0f));
			y -= 540;
		}
		else if (numScreens == 3)
		{
			camera->setView(Vector2((float)x, (float)y), Vector2((float)three_Width, 540.0f));
			if (x < 960)
				x += 960;
			else
			{
				x = 0;
				y -= 540;
				three_Width = 1920;
			}

		}
		else if (numScreens == 4)
		{
			camera->setView(Vector2((float)x, (float)y), Vector2(960.0f, 540.0f));
			if (x < 960)
				x += 960;
			else
			{
				x = 0;
				y -= 540;
			}

		}
	}

}

