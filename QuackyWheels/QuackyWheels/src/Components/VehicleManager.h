#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"

class VehicleSceneQueryData;
class VehicleManager : public ScriptBehaviour {

public:
	//Number of player vehicles + number of AI vehicles
	static const int MAX_NUM_4W_VEHICLES = 4;

	VehicleManager(GameObject* gameObject, physx::PxMaterial* const* material);
	virtual ~VehicleManager();
	int addVehicle(physx::PxVehicleDrive4W* vehDrive4W) {
		mVehicles[mNumVehicles] = vehDrive4W; 
		mNumVehicles++;
		return (mNumVehicles - 1);
	}
	physx::PxU32 getNbVehicles() { return mNumVehicles; }
	physx::PxVehicleDrive4W* getVehicle(const physx::PxU32 i) { return mVehicles[i]; }

	void setDrivableMaterial(physx::PxMaterial* const* material);

	void PhysicsUpdate() override;

private:
	//Number of vehicles registered in the manager
	physx::PxU32 mNumVehicles;

	//Array of registered vehicles
	physx::PxVehicleDrive4W* mVehicles[MAX_NUM_4W_VEHICLES];

	VehicleSceneQueryData* gVehicleSceneQueryData;
	physx::PxBatchQuery* gBatchQuery;
	physx::PxVehicleDrivableSurfaceToTireFrictionPairs* gFrictionPairs;

	physx::PxMaterial* const* drivableMaterial;

	//Cached simulation data of focus vehicle in 4W mode.
	physx::PxVehicleWheelsSimData* mWheelsSimData4W;
};

