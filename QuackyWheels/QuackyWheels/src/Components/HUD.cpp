
#include "HUD.h"

#include <glew.h>
#include "../Renderer/Renderer.h"
#include "../Renderer/Types/Texture.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Material.h"

#include "../AssetImport/Assets.h"

#include "../Components/MeshRenderer.h"

#include <glm.hpp>
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Vector2.h"
#include "../BaseTypes/Quaternion.h"
#include "../BaseTypes/Matrix4x4.h"

#include "VehiclePositionTracker.h"
#include "../Time/Timer.h"
#include "Countdown.h"
#include "../ComponentEntity/GameObject.h"
#include "Transform.h"
#include "../Components/PowerUpManager.h"
#include "../Components/VehicleManager.h"
#include "../Components/PhysicsVehicle.h"
#include <ctime>
#include "../World/World.h"
#include "../World/Universe.h"
#include "../World/MenuWorld.h"

HUD::HUD(GameObject *gameObject, VehiclePositionTracker* position, Countdown* count, int playNum, PowerUpManager* powerUpManager, VehicleManager* vehicleManager) : ScriptBehaviour(gameObject)
{
	time = new Timer();
	end = new Timer();
	pum = powerUpManager;
	vM = vehicleManager;
	finish = false;
	menuLoaded = false;

	uiTexture = Assets::Load<Texture>("EngineAssets/Images/Powerups/None.png");
	speed = Assets::Load<Texture>("EngineAssets/Images/Powerups/Speed.png");
	remove = Assets::Load<Texture>("EngineAssets/Images/Powerups/Remove.png");
	missile = Assets::Load<Texture>("EngineAssets/Images/Powerups/Missile.png");
	mine = Assets::Load<Texture>("EngineAssets/Images/Powerups/Mine.png");
	solid = Assets::Load<Texture>("EngineAssets/Images/Powerups/Wall.png");
	one = Assets::Load<Texture>("EngineAssets/Images/OneLabel.png");
	two = Assets::Load<Texture>("EngineAssets/Images/TwoLabel.png");
	three = Assets::Load<Texture>("EngineAssets/Images/ThreeLabel.png");
	four = Assets::Load<Texture>("EngineAssets/Images/FourLabel.png");
	youWin = Assets::Load<Texture>("EngineAssets/Images/You're Winner.png");
	st = Assets::Load<Texture>("EngineAssets/Images/st.png");
	nd = Assets::Load<Texture>("EngineAssets/Images/nd.png");
	rd = Assets::Load<Texture>("EngineAssets/Images/rd.png");
	th = Assets::Load<Texture>("EngineAssets/Images/th.png");
	lp = Assets::Load<Texture>("EngineAssets/Images/lap.png");
	youLose = Assets::Load<Texture>("EngineAssets/Images/youlose.png");

	itemBoxImage = new MeshRenderer(gameObject);
	itemBoxImage->mesh = Mesh::Quad();
	itemBoxImage->setUI(true);
	itemBoxImage->getMaterial()->setTexture(speed);
	itemBoxImage->mask.add(RenderLayer::Player0);
	itemBoxImage->mask.remove(RenderLayer::Default);
	gameObject->GetTransform()->SetLocalScale(Vector3(0.1f, 0.2f, 0.5f));
	gameObject->GetTransform()->SetPosition(Vector3(0.5f, 0.85f, 0.0f));

	itemBoxImage2 = new MeshRenderer(gameObject);
	itemBoxImage2->mesh = Mesh::Quad();
	itemBoxImage2->setUI(true);
	itemBoxImage2->getMaterial()->setTexture(uiTexture);
	itemBoxImage2->mask.add(RenderLayer::Player1);
	itemBoxImage2->mask.remove(RenderLayer::Default);
	

	itemBoxImage3 = new MeshRenderer(gameObject);
	itemBoxImage3->mesh = Mesh::Quad();
	itemBoxImage3->setUI(true);
	itemBoxImage3->getMaterial()->setTexture(uiTexture);
	itemBoxImage3->mask.add(RenderLayer::Player2);
	itemBoxImage3->mask.remove(RenderLayer::Default);

	itemBoxImage4 = new MeshRenderer(gameObject);
	itemBoxImage4->mesh = Mesh::Quad();
	itemBoxImage4->setUI(true);
	itemBoxImage4->getMaterial()->setTexture(uiTexture);
	itemBoxImage4->mask.add(RenderLayer::Player3);
	itemBoxImage4->mask.remove(RenderLayer::Default);

	GameObject* lap = new GameObject(gameObject->getWorld());
	MeshRenderer* lapCountImage = new MeshRenderer(lap);
	lapCountImage->mesh = Mesh::Quad();
	lapCountImage->setUI(true);
	lapCountImage->getMaterial()->setTexture(lp);
	lapCountImage->getMaterial()->drawType = Material::DrawType::Transparent;
	lapCountImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
	lap->GetTransform()->SetLocalScale(Vector3(0.7f, 0.7f, 0.5f));
	lap->GetTransform()->SetPosition(Vector3(0.75f, 1.0f, 0.0f));

	GameObject* laps = new GameObject(gameObject->getWorld());
	lapImage = new MeshRenderer(laps);
	lapImage->mesh = Mesh::Quad();
	lapImage->setUI(true);
	lapImage->getMaterial()->setTexture(one);
	lapImage->mask.add(RenderLayer::Player0);
	lapImage->mask.remove(RenderLayer::Default);
	lapImage->getMaterial()->drawType = Material::DrawType::Transparent;
	lapImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
	laps->GetTransform()->SetLocalScale(Vector3(0.2f, 0.2f, 0.5f));
	laps->GetTransform()->SetPosition(Vector3(0.9f, 0.9f, 0.0f));

	lapImage2 = new MeshRenderer(laps);
	lapImage2->mesh = Mesh::Quad();
	lapImage2->setUI(true);
	lapImage2->getMaterial()->setTexture(one);
	lapImage2->mask.add(RenderLayer::Player1);
	lapImage2->mask.remove(RenderLayer::Default);
	lapImage2->getMaterial()->drawType = Material::DrawType::Transparent;
	lapImage2->getMaterial()->setColor("ambient", Color::BlackInvisible());

	lapImage3 = new MeshRenderer(laps);
	lapImage3->mesh = Mesh::Quad();
	lapImage3->setUI(true);
	lapImage3->getMaterial()->setTexture(one);
	lapImage3->mask.add(RenderLayer::Player2);
	lapImage3->mask.remove(RenderLayer::Default);
	lapImage3->getMaterial()->drawType = Material::DrawType::Transparent;
	lapImage3->getMaterial()->setColor("ambient", Color::BlackInvisible());

	lapImage4 = new MeshRenderer(laps);
	lapImage4->mesh = Mesh::Quad();
	lapImage4->setUI(true);
	lapImage4->getMaterial()->setTexture(one);
	lapImage4->mask.add(RenderLayer::Player3);
	lapImage4->mask.remove(RenderLayer::Default);
	lapImage4->getMaterial()->drawType = Material::DrawType::Transparent;
	lapImage4->getMaterial()->setColor("ambient", Color::BlackInvisible());

	GameObject* place = new GameObject(gameObject->getWorld());
	placeImage = new MeshRenderer(place);
	placeImage->mesh = Mesh::Quad();
	placeImage->setUI(true);
	placeImage->getMaterial()->setTexture(st);
	placeImage->mask.add(RenderLayer::Player0);
	placeImage->mask.remove(RenderLayer::Default);
	placeImage->getMaterial()->drawType = Material::DrawType::Transparent;
	placeImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
	place->GetTransform()->SetLocalScale(Vector3(0.1f, 0.2f, 0.5f));
	place->GetTransform()->SetPosition(Vector3(0.1f, 0.15f, 0.0f));

	placeImage2 = new MeshRenderer(place);
	placeImage2->mesh = Mesh::Quad();
	placeImage2->setUI(true);
	placeImage2->getMaterial()->setTexture(nd);
	placeImage2->mask.add(RenderLayer::Player1);
	placeImage2->mask.remove(RenderLayer::Default);
	placeImage2->getMaterial()->drawType = Material::DrawType::Transparent;
	placeImage2->getMaterial()->setColor("ambient", Color::BlackInvisible());

	placeImage3 = new MeshRenderer(place);
	placeImage3->mesh = Mesh::Quad();
	placeImage3->setUI(true);
	placeImage3->getMaterial()->setTexture(rd);
	placeImage3->mask.add(RenderLayer::Player2);
	placeImage3->mask.remove(RenderLayer::Default);
	placeImage3->getMaterial()->drawType = Material::DrawType::Transparent;
	placeImage3->getMaterial()->setColor("ambient", Color::BlackInvisible());

	placeImage4 = new MeshRenderer(place);
	placeImage4->mesh = Mesh::Quad();
	placeImage4->setUI(true);
	placeImage4->getMaterial()->setTexture(th);
	placeImage4->mask.add(RenderLayer::Player3);
	placeImage4->mask.remove(RenderLayer::Default);
	placeImage4->getMaterial()->drawType = Material::DrawType::Transparent;
	placeImage4->getMaterial()->setColor("ambient", Color::BlackInvisible());


	countdown = new GameObject(gameObject->getWorld());
	countImage = new MeshRenderer(countdown);
	countImage->mesh = Mesh::Quad();
	countImage->setUI(true);
	countImage->getMaterial()->setTexture(three);
	countImage->getMaterial()->drawType = Material::DrawType::Transparent;
	countImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
	countdown->GetTransform()->SetLocalScale(Vector3(0.4f, 0.8f, 0.5f));
	countdown->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 0.0f));

	tracker = position;

	player = playNum;

	counter = count;
	//getEngineSystems()->renderer->renderCollection->AddUIRender(itemBoxImage);
}


HUD::~HUD()
{
	delete time;
	//getEngineSystems()->renderer->renderCollection->RemoveMeshRender(itemBoxImage);
}

void HUD::PhysicsUpdate()
{
	if (time->isStarted())
	{
		/*if (tracker->getLaps(player) > 3 && !time->isPaused())
		{
			time->pause();
		}*/
	}
	else
	{
		if (counter->isCountdownFinished())
		{
			time->start();

			countdown->SafeDelete();
		}
		else if (!counter->isCountdownFinished())
		{
			float t = counter->countDown();
			if (t < 3.0f)
			{
				countImage->getMaterial()->setTexture(three);
			}
			else if (t < 4.0f)
			{
				countImage->getMaterial()->setTexture(two);
			}
			else if (t < 5.0f)
			{
				countImage->getMaterial()->setTexture(one);
			}
		}

	}

}

void HUD::Update()
{
	for (int i = 0; i < 4; i++)
	{
		int currLap = ((GameObject*)vM->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps;

		if ((currLap == 3) && (finish == false))
		{
			finish = true;

			GameObject* win = new GameObject(this->GetGameObject()->getWorld());
			MeshRenderer* winImage = new MeshRenderer(win);
			winImage->mesh = Mesh::Quad();
			winImage->setUI(true);
			winImage->getMaterial()->setTexture(youWin);
			winImage->getMaterial()->drawType = Material::DrawType::Transparent;
			winImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
			win->GetTransform()->SetLocalScale(Vector3(0.4f, 0.8f, 0.5f));
			win->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 0.0f));

			GameObject* lose = new GameObject(this->GetGameObject()->getWorld());
			MeshRenderer* loseImage = new MeshRenderer(lose);
			loseImage->mesh = Mesh::Quad();
			loseImage->setUI(true);
			loseImage->getMaterial()->setTexture(youLose);
			loseImage->getMaterial()->drawType = Material::DrawType::Transparent;
			loseImage->getMaterial()->setColor("ambient", Color::BlackInvisible());
			lose->GetTransform()->SetLocalScale(Vector3(0.8f, 1.6f, 0.5f));
			lose->GetTransform()->SetPosition(Vector3(0.5f, 0.5f, 0.0f));

			if (i == 0)
			{
				winImage->mask.add(RenderLayer::Player0);
				winImage->mask.remove(RenderLayer::Default);

				loseImage->mask.add(RenderLayer::Player1);
				loseImage->mask.add(RenderLayer::Player2);
				loseImage->mask.add(RenderLayer::Player3);
				loseImage->mask.remove(RenderLayer::Default);
			}
			else if (i == 1)
			{
				winImage->mask.add(RenderLayer::Player1);
				winImage->mask.remove(RenderLayer::Default);

				loseImage->mask.add(RenderLayer::Player0);
				loseImage->mask.add(RenderLayer::Player2);
				loseImage->mask.add(RenderLayer::Player3);
				loseImage->mask.remove(RenderLayer::Default);
			}
			else if (i == 2)
			{
				winImage->mask.add(RenderLayer::Player2);
				winImage->mask.remove(RenderLayer::Default);

				loseImage->mask.add(RenderLayer::Player1);
				loseImage->mask.add(RenderLayer::Player0);
				loseImage->mask.add(RenderLayer::Player3);
				loseImage->mask.remove(RenderLayer::Default);
			}
			else if (i == 3)
			{
				winImage->mask.add(RenderLayer::Player3);
				winImage->mask.remove(RenderLayer::Default);

				loseImage->mask.add(RenderLayer::Player1);
				loseImage->mask.add(RenderLayer::Player2);
				loseImage->mask.add(RenderLayer::Player0);
				loseImage->mask.remove(RenderLayer::Default);
			}

			end->start();
		}


		MeshRenderer* imageBox;
		MeshRenderer* lapBox;
		MeshRenderer* placeBox;

		if (i == 0)
		{
			imageBox = itemBoxImage;
			lapBox = lapImage;
			placeBox = placeImage;
		}
		else if (i == 1)
		{
			imageBox = itemBoxImage2;
			lapBox = lapImage2;
			placeBox = placeImage2;
		}
		else if (i == 2)
		{
			imageBox = itemBoxImage3;
			lapBox = lapImage3;
			placeBox = placeImage3;
		}
		else if (i == 3)
		{
			imageBox = itemBoxImage4;
			lapBox = lapImage4;
			placeBox = placeImage4;
		}

		int power = pum->getPowerUp(i);

		if (power == 0)
		{
			imageBox->getMaterial()->setTexture(uiTexture);
		}
		else if (power == 1)
		{
			imageBox->getMaterial()->setTexture(speed);
		}
		else if (power == 3)
		{
			imageBox->getMaterial()->setTexture(remove);
		}
		else if (power == 5)
		{
			imageBox->getMaterial()->setTexture(mine);
		}
		else if (power == 6)
		{
			imageBox->getMaterial()->setTexture(solid);
		}
		else if (power == 7)
		{
			imageBox->getMaterial()->setTexture(missile);
		}
		


		if (currLap == 0)
		{
			lapBox->getMaterial()->setTexture(one);
		}
		else if (currLap == 1)
		{
			lapBox->getMaterial()->setTexture(two);
		}
		else if (currLap == 2)
		{
			lapBox->getMaterial()->setTexture(three);
		}

		int place = tracker->getPlace(i);

		if (place == 1)
		{
			placeBox->getMaterial()->setTexture(st);
		}
		else if (place == 2)
		{
			placeBox->getMaterial()->setTexture(nd);
		}
		else if (place == 3)
		{
			placeBox->getMaterial()->setTexture(rd);
		}
		else if (place == 4)
		{
			placeBox->getMaterial()->setTexture(th);
		}

		
		//finish = true;
		if (finish == true)
		{
			if (end->GetSeconds() > 5.0f)
			{

				loadMenu();

			}
		}
	}

	
}

void HUD::loadMenu()
{
	if (menuLoaded)
		return;
	menuLoaded = true;
	World* menuWorld = new MenuWorld(this->GetGameObject()->getWorld()->getUniverse());
	this->GetGameObject()->getWorld()->safeDelete();
}