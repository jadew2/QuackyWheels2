//
// Created by Jade White on 2016-03-07.
//

#pragma  once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "../BaseTypes/Vector3.h"

///<summary> This component simply positions it's attached gameobject to the targets location. Plus some offset. </summary>
class GameObject;
class Model;
class Transform;

class WinLoseMessage : public ScriptBehaviour
{
	Transform* transform = nullptr;
	Transform* cameraTransform = nullptr;

	Model* winLoseModel = nullptr;

	bool messageTriggered = false;
	float elaspedTime = 0.0f;

public:
	WinLoseMessage(GameObject* gameObject, Transform* cameraTransform);
	virtual ~WinLoseMessage();

	///<summary> Time after a message being triggered to restart/end the game. </summary>
	float timeForApplicationReset = 5.0f;
	///<summary> Local position inrelation to this gameobject where the message is created.  </summary>
	Vector3 messageOffset = Vector3(0,0,10);

	///<summary> Shows the YOU WIN! mesh at the offset in front of the camera. </summary>
	void ShowWinMessage();
	///<summary> Shows the YOU LOSE! mesh at the offset in front of the camera. </summary>
	void ShowLoseMessage();

protected:
	virtual void Update() override;

private:
	GameObject* createMessageGO();

};