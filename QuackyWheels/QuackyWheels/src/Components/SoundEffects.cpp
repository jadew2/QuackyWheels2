#include "SoundEffects.h"
#include "../ComponentEntity/GameObject.h"
#include "../GameLoop/EngineSystems.h"
#include "AudioSource.h"
#include "PhysicsVehicle.h"
#include "../Input/Controller.h"
#include "../Physics/Physics.h"

SoundEffects::SoundEffects(GameObject* gameObject, GameObject* gameObject2, int index) : ScriptBehaviour(gameObject)
{
	accel = new AudioSource(gameObject);
	accel->SetAudio("EngineAssets/Sounds/Car Accelerating.wav");

	brk = new AudioSource(gameObject);
	brk->SetAudio("EngineAssets/Sounds/Tires_Squealing.wav");
	
	quack = new AudioSource(gameObject);
	quack->SetAudio("EngineAssets/Sounds/Quack.wav");

	idle = new AudioSource(gameObject);
	idle->SetAudio("EngineAssets/Sounds/278188_debsound_rally-car-idle-loop-14.wav");

	collide = new AudioSource(gameObject);
	collide->SetAudio("EngineAssets/Sounds/Crash.wav");
	
	es = getEngineSystems();

	isQuack = false;
	isBrake = false;
	isCol = false;
	fHit = false;
	collisionHandler = false;

	rpm = 0.0;
	vindex = index;

	vehicle = gameObject2->GetComponent<PhysicsVehicle>();
}

SoundEffects::~SoundEffects()
{
}

void SoundEffects::Update()
{
	ScriptBehaviour::Update();

	setCol(es->physics->getCollision());
	SetGear((float)vehicle->getGear());
	idling();
	quacking();
	breaking();
	accelerate();
	hardHit();
}

void SoundEffects::accelerate()
{
	if ((es->controller->getControllerInput()[vindex].acceleration > 0.0) && (currGear != physx::PxVehicleGearsData::eREVERSE))
	{
		rpm = (float)(es->controller->getControllerInput()[vindex].acceleration / 1.0f)*(0.8f);
		//cout << std::to_string(vindex) << " RPM " << std::to_string(rpm)  << endl;
		
		isBrake = false;
		if (!accel->IsPlaying())
		{
			accel->Play(0);
			brk->Stop();
		}
		accel->SetVolume(rpm);

	}
}

void SoundEffects::breaking()
{
	if (((es->controller->getControllerInput()[vindex].brake > 0.0) || (es->controller->getControllerInput()[vindex].handBrake > 0.0)) && (currGear != physx::PxVehicleGearsData::eREVERSE))
	{
		if (isBrake == false)
		{
			brk->Play(0);
			isBrake = true;
			accel->Stop();
		}
	}
	else if (((es->controller->getControllerInput()[vindex].acceleration > 0.0) || (es->controller->getControllerInput()[vindex].handBrake > 0.0)) && (currGear == physx::PxVehicleGearsData::eREVERSE))
	{
		if (isBrake == false)
		{
			brk->Play(0);
			isBrake = true;
			accel->Stop();
		}
	}
	else if ((es->controller->getControllerInput()[vindex].brake > 0.0) && (currGear == physx::PxVehicleGearsData::eREVERSE))
	{
		if (!accel->IsPlaying())
		{
			accel->SetVolume(0.25f);
			accel->Play(0);
			isBrake = false;
		}
	}
}

void SoundEffects::hardHit()
{
	if((collisionHandler == true) && (isCol == false))
	{
		fHit = true;
		isCol = true;
	}
	else if ((collisionHandler == true) && (isCol == true))
	{
		fHit = false;
	}
	else if ((collisionHandler == false) && (isCol == true))
	{
		fHit = false;
		isCol = false;
	}

	if ((!collide->IsPlaying()) && (fHit))
	{
		collide->SetVolume(1.0f);
		collide->Play(0);
	}
}

void SoundEffects::quacking()
{
	if (es->controller->getControllerInput()[vindex].quack == true)
	{
		if (isQuack == false)
		{
			quack->Play(0);
			isQuack = true;
		}
	}
	else if (es->controller->getControllerInput()[vindex].quack == false)
	{
		isQuack = false;
	}
}

void SoundEffects::idling()
{
	if ((es->controller->getControllerInput()[vindex].acceleration <= 0.0) && (es->controller->getControllerInput()[vindex].brake <= 0.0) && (es->controller->getControllerInput()[vindex].handBrake <= 0.0))
	{
		if (!idle->IsPlaying())
		{
			idle->Play(0);
		}
	}
	else
	{
		idle->Stop();
	}
}

void SoundEffects::SetGear(float gear)
{
	currGear = gear;
}

void SoundEffects::setCol(bool collision)
{
	collisionHandler = collision;
	es->physics->setCollision(false);
}