#include "PowerUpManager.h"

#include "../ComponentEntity/GameObject.h"
#include "../Components/VehicleManager.h"
#include "../GameLoop/EngineSystems.h"
#include "PhysicsVehicle.h"
#include "Missile.h"
#include "Mine.h"
#include <stdlib.h> 
#include <time.h>
#include "../Input/Controller.h"
#include "VehiclePositionTracker.h"
#include <random>

PowerUpManager::PowerUpManager(GameObject* gameObject, PhysicsVehicle* list[], VehiclePositionTracker* vehiclePositionTracker) : ScriptBehaviour(gameObject)
{
	es = getEngineSystems();
	
	manager = gameObject;

	vpt = vehiclePositionTracker;

	for (int i = 0; i < 4; i++)
	{
		powerUp[i] = false;
	}
	
	for (int i = 0; i < 4; i++)
	{
		vehList[i] = list[i];
	}
	
	for (int i = 0; i < 4; i++)
	{
		active[i] = false;
	}

	for (int i = 0; i < 4; i++)
	{
		power[i] = 0;
	}
}


PowerUpManager::~PowerUpManager()
{
}

void PowerUpManager::usePowerUp()
{
	for (int i = 0; i < 4; i++)
	{

		if ((es->controller->getControllerInput()[i].powerup == true) && (powerUp[i] == false) && (active[i] == true))
		{
			powerUp[i] = true;

			if (power[i] < 7)
			{
				vehList[i]->setPowerUp(power[i]);
			}
			else if (power[i] = 7)
			{
				GameObject* missileGameObject = new GameObject(manager->getWorld(), "missile");
				Missile* missile = new Missile(missileGameObject, vpt, manager->GetComponent<VehicleManager>(), i);
			}
			active[i] = false;
			power[i] = 0;
		}
		else if ((es->controller->getControllerInput()[i].powerup == false) && (vehList[i]->getPowerUpTimer() == 0))
		{
			powerUp[i] = false;

		}
	}
}

void PowerUpManager::Update()
{
	usePowerUp();

}

void PowerUpManager::randomPowerUp(int i)
{
	srand((unsigned int)time(NULL));

	int place = vpt->getPlace(i);
	int rando;

	std::mt19937 rng;
	rng.seed(std::random_device()());
	std::uniform_int_distribution<std::mt19937::result_type> dist(1, 100);
	rando = dist(rng);

	if (place == 1)
	{
		//0% missile
		//10% ice
		//40% mine
		//50% remove trails
		//0% speed

		if(rando <= 40)
		{
			randomIndex = 5;
		}
		else if ((41 <= rando) && (rando <= 90))
		{
			randomIndex = 3;
		}
		else if ((rando >= 91) && (rando <=100))
		{
			randomIndex = 6;
		}
	}
	else if (place == 2)
	{
		//45% missile
		//10% ice
		//15% mine
		//10% remove trails
		//20% speed

		if (rando <= 45)
		{
			randomIndex = 7;
		}
		else if ((46 <= rando) && (rando <= 65))
		{
			randomIndex = 1;
		}
		else if ((66 <= rando) && (rando <= 75))
		{
			randomIndex = 6;
		}
		else if ((76 <= rando) && (rando <= 90))
		{
			randomIndex = 5;
		}
		else if ((rando >= 91) && (rando <= 100))
		{
			randomIndex = 3;
		}
	}
	else if (place == 3)
	{
		//25% missile
		//20% ice
		//25% mine
		//10% remove trails
		//20% speed

		if (rando <= 25)
		{
			randomIndex = 7;
		}
		else if ((26 <= rando) && (rando <= 45))
		{
			randomIndex = 1;
		}
		else if ((46 <= rando) && (rando <= 65))
		{
			randomIndex = 6;
		}
		else if ((66 <= rando) && (rando <= 90))
		{
			randomIndex = 5;
		}
		else if ((rando >= 91) && (rando <= 100))
		{
			randomIndex = 3;
		}
	}
	else if (place == 4)
	{
		//25% missile
		//0% ice
		//5% mine
		//0% remove trails
		//70% speed

		if (rando <= 25)
		{
			randomIndex = 7;
		}
		else if ((26 <= rando) && (rando <= 95))
		{
			randomIndex = 1;
		}
		else if ((rando >= 96) && (rando <= 100))
		{
			randomIndex = 5;
		}
		
	}
	power[i] = randomIndex;
}

void PowerUpManager::setPowerUp(int i)
{
	active[i] = true;
	randomPowerUp(i);
}

int PowerUpManager::getPowerUp(int i)
{
	return power[i];
}


