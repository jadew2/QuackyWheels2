#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"
#include <list>

class GameObject;
class PhysicsVehicle;
class PhysicsRigidStatic;
class MeshRenderer;
class Material;
class VehicleTrail : public ScriptBehaviour {

public:
	bool solid;
	unsigned int numPieces;

	VehicleTrail(GameObject* gameObject, physx::PxScene* scene, PhysicsVehicle* physicsVehicle);
	virtual ~VehicleTrail();

	void createTrail(physx::PxVec3 previousPosition, physx::PxTransform newPosition, physx::PxReal length);
	void removeTrail();
	void solidTrail();
	void nonSolidTrail();

private:
	PhysicsVehicle* physicsVehicle;
	GameObject* gameObject;
	physx::PxScene* scene;
	std::list<GameObject*> trail;
	physx::PxMaterial* trailMaterial;
	physx::PxFilterData* trailQryFilterData;
	physx::PxFilterData* trailSimFilterData;
	physx::PxShapeFlags* trailShapeFlags;
	Material* waterMaterial;
	Material* iceMaterial;
};
