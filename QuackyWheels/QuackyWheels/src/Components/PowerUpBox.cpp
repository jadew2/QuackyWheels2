#include "PowerUpBox.h"

#include "../GameLoop/EngineSystems.h"
#include "../AssetImport/Model.h"
#include "../AssetImport/Assets.h"
#include "VehicleManager.h"
#include "Transform.h"
#include "../ComponentEntity/GameObject.h"
#include "MeshRenderer.h"
#include "../Renderer/Types/Material.h"
#include "../Components/PowerUpManager.h"
#include "../Prefabs/Explosion1Prefab.h"

PowerUpBox::PowerUpBox(GameObject* gameObject, VehicleManager* vehicleManager, PowerUpManager* powerUpManager) : ScriptBehaviour(gameObject) {
	this->vehicleManager = vehicleManager;
	this->powerUpManager = powerUpManager;

	powerUpRenderer = new MeshRenderer(gameObject);
	powerUpRenderer->mesh = Assets::Load<Mesh>("EngineAssets/DebugModels/UVCube.obj");
	powerUpRenderer->getMaterial()->setTexture(Assets::Load<Texture>("EngineAssets/Images/QuestionBox.png"));
	cooldown = -1;
	triggerRadius = 5.0f;
}


PowerUpBox::~PowerUpBox() {

}

void PowerUpBox::PhysicsUpdate() {
	//Consider making a cooldown
	if (cooldown >= 0)
	{
		cooldown--;
		if (cooldown == 0)
			powerUpRenderer->SetEnabled(true);
	}
	else
	{

		for (unsigned int i = 0; i < vehicleManager->getNbVehicles(); i++) {
			if ((vehicleManager->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p - this->GetGameObject()->GetTransform()->GetLocalPosition()).magnitude() < triggerRadius) {
				powerUpManager->setPowerUp(i);
				cooldown = 60;

				powerUpRenderer->SetEnabled(false);
				Explosion1Prefab* explosion1 = new Explosion1Prefab(GetGameObject()->getWorld());
				explosion1->GetTransform()->SetPosition(GetTransform()->GetLocalPosition());
			}
		}
	}
}
