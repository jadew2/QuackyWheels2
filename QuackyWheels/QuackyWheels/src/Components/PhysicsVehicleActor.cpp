#include "PhysicsVehicleActor.h"

PhysicsVehicleActor::PhysicsVehicleActor(GameObject* gameObject, Transform* transform, ObjectType type, physx::PxReal density, physx::PxReal chassisMass, physx::PxVec3* chassisMOI, physx::PxVec3* chassisCMOffset, bool kinematicFlag) : PhysicsRigidDynamic(gameObject, transform, type, kinematicFlag) {
	this->rigidDynamic->setMass(chassisMass);
	this->rigidDynamic->setMassSpaceInertiaTensor(*chassisMOI);
	this->rigidDynamic->setCMassLocalPose(physx::PxTransform(*chassisCMOffset, physx::PxQuat(physx::PxIdentity)));
}


PhysicsVehicleActor::~PhysicsVehicleActor() {

}
