#pragma once
#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidStatic;
class Transform;
class PhysicsRigidStaticConvexMesh : PhysicsShape {

public:
	PhysicsRigidStaticConvexMesh(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxU32 numVerticies,
		physx::PxVec3 verticies[], physx::PxU16 numMaterials, physx::PxMaterial* const* meshMaterials,
		Transform* transform, physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData,
		physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidStaticConvexMesh();

	void setTransform(Transform* transform);

	PhysicsRigidStatic* getActor();

protected:
	PhysicsRigidStatic* actor;
};

