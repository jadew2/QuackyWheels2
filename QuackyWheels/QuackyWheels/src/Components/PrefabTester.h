#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"

class PrefabTester : public ScriptBehaviour
{
public:
	PrefabTester(GameObject* gameObejct);
	virtual ~PrefabTester();

	int count = 0;
protected:
	virtual void Update() override;
};

