#pragma once


#include "../ComponentEntity/ScriptBehaviour.h"

class MeshRenderer;
class VehiclePositionTracker;
class Timer;
class Countdown;
class PowerUpManager;
class Texture;
class VehicleManager;

//This class will be meant to display the HUD to the screen. From what I've heard, it will just display font textures as an overlay.
class HUD : public ScriptBehaviour
{
private:
	//vector<MeshRenderer*> timerTexts = vector<MeshRenderer*>();
	//vector<Texture*> placeTexts = vector<Texture*>();

	MeshRenderer* itemBoxImage = nullptr;
	MeshRenderer* itemBoxImage2 = nullptr;
	MeshRenderer* itemBoxImage3 = nullptr;
	MeshRenderer* itemBoxImage4 = nullptr;
	MeshRenderer* lapImage = nullptr;
	MeshRenderer* lapImage2 = nullptr;
	MeshRenderer* lapImage3 = nullptr;
	MeshRenderer* lapImage4 = nullptr;
	MeshRenderer* placeImage = nullptr;
	MeshRenderer* placeImage2 = nullptr;
	MeshRenderer* placeImage3 = nullptr;
	MeshRenderer* placeImage4 = nullptr;
	MeshRenderer* countImage = nullptr;
	VehiclePositionTracker* tracker;
	PowerUpManager* pum;
	GameObject* countdown;

	Texture* uiTexture;
	Texture* speed;
	Texture* remove;
	Texture* missile;
	Texture* mine;
	Texture* solid;
	Texture* one;
	Texture* two;
	Texture* three;
	Texture* four;
	Texture* youWin;

	Texture* st;
	Texture* nd;
	Texture* rd;
	Texture* th;
	Texture* lp;
	Texture* youLose;

	Timer* time;

	int player;
	bool finish;
	Timer* end;

	Countdown* counter;

	VehicleManager* vM;
	
	

public:
	HUD(GameObject *gameObject, VehiclePositionTracker *position, Countdown* counter, int playNum, PowerUpManager* powerUpManager, VehicleManager* vehicleManager);
	virtual ~HUD();

	virtual void PhysicsUpdate() override;
	virtual void Update() override;

	//Will likely need functions and variables to tell on what part of the screen the overlay gets mapped to.

	//References needed:
	// reference to navMesh + goalNodes (for position)
	//	-Reference to playerVehicle as well (to compare against navMesh+goalNodes) - vehiclePositionTracker has all of these already
	//	-Reference to Timer

private:
	bool menuLoaded = false;
	void loadMenu();

};

