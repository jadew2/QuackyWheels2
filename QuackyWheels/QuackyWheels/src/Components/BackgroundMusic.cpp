//
// Created by Jade White on 2016-02-26.
//

#include "BackgroundMusic.h"
#include "../GameLoop/EngineSystems.h"
#include "AudioSource.h"
#include "../Input/Controller.h"

BackgroundMusic::BackgroundMusic(GameObject *gameObject) : ScriptBehaviour(gameObject)
{
	countDown = new AudioSource(GetGameObject());
	countDown->SetAudio("EngineAssets/Sounds/165991_unnecro_race-countdown1.wav");

	darudeSandstorm = new AudioSource(GetGameObject());
	darudeSandstorm->SetAudio("EngineAssets/Sounds/Darude_-_Sandstorm_full.ogg");

	backgroundMusic = new AudioSource(GetGameObject());
	backgroundMusic->SetAudio("EngineAssets/Sounds/237089__foolboymedia__race-track.wav");
	backgroundMusic->SetVolume(1.0f);
	es = getEngineSystems();
	oldSandstorm = false;
}

void BackgroundMusic::Start()
{
	ScriptBehaviour::Start();

	countDown->SetVolume(1.0f);
	countDown->Play(0);
	
}

void BackgroundMusic::Update()
{
	ScriptBehaviour::Update();
	
	bool sandStorm = es->controller->getControllerInput()->sandstorm;

	if ((!backgroundMusic->IsPlaying()) && !oldSandstorm)
		backgroundMusic->Play(-1);

	if (sandStorm && !oldSandstorm)
	{
		backgroundMusic->Pause();
		if (darudeSandstorm->IsPaused())
			darudeSandstorm->Resume();
		else
			darudeSandstorm->Play(-1);
		oldSandstorm = true;
	}
	else if (!sandStorm && oldSandstorm)
	{
		darudeSandstorm->Pause();
		if (backgroundMusic->IsPaused())
			backgroundMusic->Resume();
		else
			backgroundMusic->Play(-1);
		oldSandstorm = false;
	}
	
	/*//If there is no music playing
	if ((Mix_Playing(5) == 0) && (sandstorm == false))
	{
		//Play the music
		Mix_PlayChannel( 5, bgMusic, -1 );
	}

	if ((controllerInput->sandstorm == true) && (sandstorm == false))
	{
		Mix_Pause(5);
		if (Mix_Paused(6))
		{
			Mix_Resume(6);
		}
		else
		{
			Mix_PlayChannel(6, bgMusic2, -1);
		}
		sandstorm = true;
	}
	else if ((controllerInput->sandstorm == false) && (sandstorm == true))
	{
		Mix_Pause(6);
		if (Mix_Paused(5))
		{
			Mix_Resume(5);
		}
		else
		{
			Mix_PlayChannel(5, bgMusic, -1);
		}
		sandstorm = false;
	}*/

}


