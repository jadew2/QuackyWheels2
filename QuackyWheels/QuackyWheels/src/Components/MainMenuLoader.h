//
// Created by Jade White on 2016-04-11.
//

#pragma once


#include "../ComponentEntity/ScriptBehaviour.h"

class MainMenuLoader : public ScriptBehaviour
{
	bool updateHit = false;
public:
	MainMenuLoader(GameObject* gameObject);

protected:
	virtual void Start() override;
	virtual void AfterUpdate() override;
};


