#include "PhysicsRigidStaticBox.h"
#include "../Components/PhysicsRigidStatic.h"

PhysicsRigidStaticBox::PhysicsRigidStaticBox(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxReal length, physx::PxReal height,
	physx::PxReal width, Transform* transform, physx::PxMaterial* const* materials,
	physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags) : PhysicsShape(gameObject, transform) {
	//Set the actor for this shape
	this->actor = actor;

	this->physicsShape = actor->getRigidStatic()->createShape(physx::PxBoxGeometry(length / 2, height / 2, width / 2), materials, 1);
	this->physicsShape->setFlags(*shapeFlags);

	//Set the query filter data of the ground plane so that the vehicle raycasts can hit the ground.
	this->physicsShape->setQueryFilterData(*qryFilterData);

	//Set the simulation filter data of the ground plane so that it collides with the chassis of a vehicle but not the wheels.
	this->physicsShape->setSimulationFilterData(*simFilterData);
}


PhysicsRigidStaticBox::~PhysicsRigidStaticBox() {

}

void PhysicsRigidStaticBox::setTransform(Transform* transform) {
	this->transform = transform;
}

PhysicsRigidStatic* PhysicsRigidStaticBox::getActor() {
	return this->actor;
}
