//
// Created by Jade White on 2016-02-24.
//

#include "Camera.h"
#include "../Components/Transform.h"
#include "../GameLoop/EngineSystems.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/Types/Frustum.h"
#include "../Renderer/RenderCollection.h"

Camera::Camera(GameObject *gameObject) : ScriptBehaviour(gameObject)
{
	es = getEngineSystems();
	transform = GetTransform();

	frustum = new Frustum();
	setPropertiesDirty();

	ScriptBehaviour::Init();
	es->renderer->renderCollection->addCamera(this);
	
	viewAspect = Vector2(es->renderer->SCREEN_WIDTH, es->renderer->SCREEN_HEIGHT);
	viewOffset = Vector2::Zero();
	frameBuffer = nullptr;
}

Camera::~Camera()
{
	es->renderer->renderCollection->removeCamera(this);

	if (frameBuffer != nullptr)
		delete frameBuffer;
}


void Camera::bindCamera()
{
	if (frameBuffer != nullptr)
	{
		glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer->getGLFrameBuffer());
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
		if (status != GL_FRAMEBUFFER_COMPLETE)
		{
			printf("Camera: bindCamera(): FrameBufferTexture was not constructed correctly. Do you have the correct attachments and aspect ratio?");
			glBindFramebuffer(GL_FRAMEBUFFER,0);
		}
	}
	glViewport((int)viewOffset.x, (int)viewOffset.y, (GLsizei)viewAspect.x, (GLsizei)viewAspect.y);
}


void Camera::unbindCamera()
{
	if (frameBuffer != nullptr)
		glBindFramebuffer(GL_FRAMEBUFFER,0);
	//glViewport(0, 0, (GLsizei)es->renderer->SCREEN_WIDTH, (GLsizei)es->renderer->SCREEN_HEIGHT);

}


Matrix4x4 Camera::getViewMatrix()
{
	glm::vec3 eye = transform->GetLocalPosition();
	glm::vec3 center = transform->GetLocalPosition() + transform->GetLocalRotation() * Vector3::Forward();
	glm::vec3 up = transform->Up();
	viewMat = Matrix4x4(glm::lookAt(eye, center, up));
	return viewMat;
}

Matrix4x4 Camera::getPerspectiveMatrix()
{
	if (perspectiveMatDirty)
	{
		perspectiveMat = Matrix4x4(glm::perspective(fieldOfView, getAspect(), nearClippingDistance, farClippingDistance));

		perspectiveMatDirty = false;
	}
	return perspectiveMat;
}

Matrix4x4 Camera::getOrthographicMatrix(bool uiMode)
{
	if (orthoMatDirty)
	{
		uiOrthoMat = Matrix4x4(glm::ortho(0.0f, 1.0f, 1.0f, 0.0f,-1.0f,1.0f));
		orthoMat = Matrix4x4(glm::ortho(0.0f, es->renderer->SCREEN_WIDTH, es->renderer->SCREEN_HEIGHT, 0.0f, nearClippingDistance,farClippingDistance));

		orthoMatDirty = false;
	}

	return uiMode ? uiOrthoMat : orthoMat;
}


///<summary> See http://gamedev.stackexchange.com/questions/71058/how-extract-frustum-planes-from-clip-coordinates </summary>
Frustum* Camera::getFrustum()
{
	if (frustumDirty)
	{
		frustum->Set(this);
		frustumDirty = false;
	}

	return frustum;
}


float Camera::getAspect()
{
	float width = es->renderer->SCREEN_WIDTH;
	float height = es->renderer->SCREEN_HEIGHT;
	return width/height;
}


void Camera::setPropertiesDirty()
{
	perspectiveMatDirty = true;
	orthoMatDirty = true;
	frustumDirty = true;
}


void Camera::setFOV(float radians)
{
	fieldOfView = radians;
	setPropertiesDirty();
}

void Camera::setNearClipping(float value)
{
	nearClippingDistance = value;
	setPropertiesDirty();
}

void Camera::setFarClipping(float value)
{
	farClippingDistance = value;
	setPropertiesDirty();
}

void Camera::setView(Vector2 pixelOffset, Vector2 pixelSize)
{
	this->viewOffset = pixelOffset;
	this->viewAspect = pixelSize;
}

float Camera::getFOV()
{
	return fieldOfView;
}

float Camera::getNearClipping()
{
	return nearClippingDistance;
}

float Camera::getFarClipping()
{
	return farClippingDistance;
}


Vector2 Camera::getViewOffset()
{
	return viewOffset;
}

Vector2 Camera::getViewAspect()
{
	return viewAspect;
}


void Camera::generateFrameBuffer()
{
	frameBuffer = new FrameBuffer((int)viewAspect.x,(int)viewAspect.y);
}


FrameBuffer* Camera::getFrameBuffer()
{
	return frameBuffer;
}




