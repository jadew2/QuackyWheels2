#include "PlayerCamera.h"
#include "../Components/Transform.h"
#include "../GameLoop/EngineSystems.h"
#include "../ComponentEntity/GameObject.h"
#include "../Time/Time.h"
#include "PhysicsVehicle.h"
#include "Camera.h"
#include "../Physics/Physics.h"
#include "../Input/Controller.h"

PlayerCamera::PlayerCamera(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	engineSystems = getEngineSystems();
	transform = GetTransform();
	this->toCameraFromPlayer = Vector3(0.0f, 5.0f, -15.0f);
	this->minDistance = 15.8f;
	this->damping = 3.0f;
}


PlayerCamera::~PlayerCamera()
{
}

void PlayerCamera::AfterUpdate()
{
	if (playerCar == nullptr)
		return;

		Vector3 cameraPoint = target->GetLocalPosition() + (target->GetLocalRotation() * toCameraFromPlayer);
	damping = 3.0f;
	if ((transform->GetLocalPosition() - target->GetLocalPosition()).Magnitude() < minDistance) {
		damping = 100.0f;
	}
	Vector3 lerpedCameraPoint = Vector3::Lerp(transform->GetLocalPosition(), cameraPoint, engineSystems->time->GetDeltaTime() * damping);

	physx::PxVec3 direction = lerpedCameraPoint - playerCar->GetComponent<PhysicsVehicle>()->getVehicle()->getRigidDynamicActor()->getGlobalPose().p;

	float distance = direction.magnitude();

	physx::PxVec3 origin = lerpedCameraPoint * (3.0f / distance) + playerCar->GetComponent<PhysicsVehicle>()->getVehicle()->getRigidDynamicActor()->getGlobalPose().p * (1 - (3.0f / distance));

	direction.normalize();

	//Debug::DrawRay(origin, direction, distance, Color::Red());

	physx::PxRaycastBuffer buffer;

	this->getEngineSystems()->physics->getScene()->raycast(origin, direction, distance, buffer);

	if (buffer.hasBlock) {
		if (!((GameObject*)buffer.block.actor->userData)->GetComponent<PhysicsVehicle>()) {
			lerpedCameraPoint = buffer.block.position;
		}
	}

	transform->SetLocalPosition(lerpedCameraPoint);
	
	transform->SetLocalRotation(Quaternion::LookRotation(transform->GetLocalPosition() - target->GetLocalPosition(), Vector3::Up()));

	float vehicleSpeedRatio = playerCar->GetComponent<PhysicsVehicle>()->getVehicle()->computeForwardSpeed() / 60.0f;

	if (vehicleSpeedRatio > 0.0f) {
		if (vehicleSpeedRatio > 1.0f) {
			vehicleSpeedRatio = 1.0f;
		}
		this->GetGameObject()->GetComponent<Camera>()->setFOV(0.959931f + (0.174533f * vehicleSpeedRatio));
	} else {
		this->GetGameObject()->GetComponent<Camera>()->setFOV(0.959931f);
	}
}

void PlayerCamera::SetPlayerGameObject(GameObject* playerGameObject)
{
	playerCar = playerGameObject;
	target = playerCar->GetTransform();
}


GameObject* PlayerCamera::getPlayerCar()
{
	return playerCar;
}

