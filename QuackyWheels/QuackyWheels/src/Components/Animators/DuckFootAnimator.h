//
// Created by Jade White on 2016-04-06.
//

#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../BaseTypes/Vector3.h"

class Time;
class MeshRenderer;
class VehicleManager;
///<summary> Responsible for animating the feet of the duck </summary>
class DuckFootAnimator : public ScriptBehaviour
{
	Time* time;

	Transform* transform;
	Transform* spinningFootTransform;
	MeshRenderer* spinningFootRenderer;
	MeshRenderer* stillFootRenderer;

	Vector3 previousPosition;
public:
	///<summary> The minimum speed to have spinny legs. </summary>
	float velocityForRotation = 0.2f;
	///<summary> The axis for spinny feet rotation. </summary>
	Vector3 rotationAxis;
	///<summary> The rotation in degrees for 1 unit of distance. </summary>
	float rotationAngle;


	DuckFootAnimator(GameObject* gameObject, VehicleManager* vehicleManager, int index);

	///<summary> Basically Initializes this class. </summary>
	void setDuckFootProperties(Transform* spinningFootTransform, MeshRenderer* spinningFootRenderer, MeshRenderer* stillFootRenderer);

protected:
	virtual void AfterUpdate() override;

private:
	///<summary> Returns the current speed forward. Is negative if heading backwards.  </summary>
	VehicleManager* vehicleManager;
	int index;
};


