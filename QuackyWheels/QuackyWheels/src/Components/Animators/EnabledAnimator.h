//
// Created by Jade White on 2016-04-10.
//

#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"
#include <vector>

class EnabledAnimatorKeyframe;
class Time;
///<summary>
// A super simple class that enables/disables behaviours.
// It uses their start/end times and a internal timer that starts immediately.
// </summary>
class EnabledAnimator : public ScriptBehaviour
{
	std::vector<EnabledAnimatorKeyframe*> keyframes;

	Time* time;
	float elapsedTime = 0.0f;

public:
	EnabledAnimator(GameObject* gameObject);
	virtual ~EnabledAnimator();

	///<summary> Adds a script behaviour to have it's enabled animated. It is enabled after startTime, and disabled after EndTime. </summary>
	void AddKeyframe(float startTime, float endTime, ScriptBehaviour* behaviour);
	///<summary> Resets the timer to 0 </summary>
	void Rewind();

protected:
	virtual void Start() override ;
	virtual void Update() override;
};

