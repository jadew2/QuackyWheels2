//
// Created by Jade White on 2016-03-26.
//

#pragma once

#include "ValueAnimator.h"

///<summary> Animates the scale it's attached gameObject </summary>
class ScaleAnimator : public ValueAnimator<Vector3>
{
	Transform* transform;
public:
	ScaleAnimator(GameObject* gameObject);

	virtual void OnValueComputed(Vector3 newScale);
};

