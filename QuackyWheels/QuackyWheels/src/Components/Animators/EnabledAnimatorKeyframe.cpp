//
// Created by Jade White on 2016-04-11.
//

#include "EnabledAnimatorKeyframe.h"
#include "../../ComponentEntity/ScriptBehaviour.h"


EnabledAnimatorKeyframe::EnabledAnimatorKeyframe(float enabledT, float disabledT, ScriptBehaviour* behav)
{
	enabledTime = enabledT;
	disabledTime = disabledT;
	behaviour = behav;
}


void EnabledAnimatorKeyframe::Evaluate(float time)
{
	//If globalT is after enabled time but before disabled time, enable. Else disable.
	behaviour->SetEnabled(enabledTime <= time && time <= disabledTime);
}

