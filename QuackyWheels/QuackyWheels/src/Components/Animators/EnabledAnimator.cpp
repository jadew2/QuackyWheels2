//
// Created by Jade White on 2016-04-10.
//

#include "EnabledAnimator.h"
#include "EnabledAnimatorKeyframe.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"
#include <vector>
using namespace std;

EnabledAnimator::EnabledAnimator(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	time = getEngineSystems()->time;
	keyframes = vector<EnabledAnimatorKeyframe*>();
}

EnabledAnimator::~EnabledAnimator()
{

}


void EnabledAnimator::AddKeyframe(float startTime, float endTime, ScriptBehaviour* behaviour)
{
	if (endTime < startTime)
		endTime = startTime;

	EnabledAnimatorKeyframe* newKeyframe = new EnabledAnimatorKeyframe(startTime,endTime,behaviour);
	keyframes.push_back(newKeyframe);
}


void EnabledAnimator::Rewind()
{
	elapsedTime = 0;
}


void EnabledAnimator::Start()
{
	elapsedTime = 0;
}

void EnabledAnimator::Update()
{
	elapsedTime += time->GetDeltaTime();

	for (size_t i = 0; i < keyframes.size(); ++i)
		keyframes[i]->Evaluate(elapsedTime);
}



