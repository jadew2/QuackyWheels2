#include "SnakeAnimator.h"

#include "../../ComponentEntity/GameObject.h"
#include "../../Components/Transform.h"

#include "../../GameLoop/EngineSystems.h"
#include "../../Time/Time.h"


using namespace std;

SnakeAnimator::SnakeAnimator(GameObject* gameObject, GameObject* headGO, GameObject* neckGO, vector<Vector3> neckPoints) : ScriptBehaviour(gameObject)
{
	this->headGO = headGO;
	this->neckGO = neckGO;
	this->neckPoints = neckPoints;

	target = Vector3::Zero();
	time = getEngineSystems()->time;

	animationDamping = 10.0f;
	
}


SnakeAnimator::~SnakeAnimator()
{
}


void SnakeAnimator::setTarget(Vector3 target)
{
	this->target = target;
}

void SnakeAnimator::animate(float t)
{
	//Clamp
	if (t > 1.0f)
		t = 1.0f;
	else if (t < 0.0f)
		t = 0.0f;

	Transform* headTrans = headGO->GetTransform();
	Transform* neckTrans = neckGO->GetTransform();
	Vector3 neckOrigin = neckPoints[0];
	Vector3 headRestOrigin = neckPoints[1];
	Vector3 toHeadFromNeck = headRestOrigin - neckOrigin;

	float aimTime = 0.2f;
	float attackTime = 0.3f;
	
	Vector3 toTarget = (target - neckOrigin);


	//We animate the snake by positioning the head at 3 different stages. The neck just mathematically follows.
	if (t <= aimTime)
	{
		//Aim Animation
		float localT = t / aimTime;

		//Position the head by rotating it around neck origin only on the y axis.
		Vector3 toTargetNoY = Vector3(toTarget.x, 0, toTarget.z).Normalized();
		Vector3 headPosition = (Quaternion::LookRotation(toTargetNoY, Vector3::Up()) * toHeadFromNeck * -1.0f) + neckOrigin;
		headTrans->SetLocalPosition(headPosition);

		//Rotate the head to look at the player
		Quaternion toTargetRot = Quaternion::LookRotation(toTarget.Normalized(), Vector3::Up());
		headTrans->SetLocalRotation(Quaternion::Slerp(headTrans->GetLocalRotation(), toTargetRot, time->GetPhysicsTimestep() * animationDamping));
	}
	else if (t < attackTime)
	{
		//Attack
		float localT = t / aimTime;

		//First position the head
		float backOffDistance = 1.0f; //The distance to shy away the origin of the head from the player
		Vector3 wantedHeadPosition = target + (toTarget.Normalized() * -backOffDistance);
		wantedHeadPosition = Vector3::Lerp(headTrans->GetLocalPosition(), wantedHeadPosition, time->GetPhysicsTimestep() *animationDamping);
		headTrans->SetLocalPosition(wantedHeadPosition);

		//Rotate the head to look at the player
		Quaternion toTargetRot = Quaternion::LookRotation(toTarget.Normalized(), Vector3::Up());
		headTrans->SetLocalRotation(Quaternion::Slerp(headTrans->GetLocalRotation(), toTargetRot, time->GetPhysicsTimestep() * animationDamping));
	}
	else // Last 60%
	{
		//Retract (So we want the rest position)

		//Position the head back to rest
		Vector3 wantedHeadPosition = toHeadFromNeck + neckOrigin;
		headTrans->SetLocalPosition(Vector3::Lerp(headTrans->GetLocalPosition(), wantedHeadPosition, time->GetPhysicsTimestep() * animationDamping));

		//Rotate the head back to rest
		Quaternion wantedHeadRot = Quaternion::LookRotation(toHeadFromNeck.Normalized(), Vector3::Up());
		headTrans->SetLocalRotation(Quaternion::Slerp(headTrans->GetLocalRotation(), wantedHeadRot, time->GetPhysicsTimestep() * animationDamping));
	}

	//Set the neck's rotation and scale
	{
		Vector3 toHead = headTrans->GetLocalPosition() - neckOrigin;
		//Rotate the neck to look at the head
		Quaternion neckRotation = Quaternion::LookRotation(toHead.Normalized(), Vector3::Up());
		neckTrans->SetLocalRotation(neckRotation);

		//Scale the neck to extend to the head
		float neckScaling = (toHead.Magnitude() / toHeadFromNeck.Magnitude());
		neckTrans->SetLocalScale(Vector3(1.0f,1.0f,neckScaling));
	}
}

void SnakeAnimator::Update()
{
	//Empty now...
}