//
// Created by Jade White on 2016-04-06.
//

#include "DuckFootAnimator.h"

#include "../Transform.h"
#include "../../Time/Time.h"
#include "../../GameLoop/EngineSystems.h"
#include "../../Components/VehicleManager.h"

DuckFootAnimator::DuckFootAnimator(GameObject* gameObject, VehicleManager* vehicleManager, int index) : ScriptBehaviour(gameObject)
{
	this->vehicleManager = vehicleManager;
	this->index = index;
	time = getEngineSystems()->time;

	previousPosition = Vector3::Zero();
	transform = GetTransform();

	spinningFootTransform = nullptr;
	spinningFootRenderer = nullptr;
	stillFootRenderer = nullptr;

	rotationAxis = Vector3::Right();
	rotationAngle = 90.0f;
}


void DuckFootAnimator::setDuckFootProperties(Transform* spinningFootTrans, MeshRenderer* spinningFootRend, MeshRenderer* stillFootRend)
{
	spinningFootTransform = spinningFootTrans;
	spinningFootRenderer = spinningFootRend;
	stillFootRenderer = stillFootRend;
}


void DuckFootAnimator::AfterUpdate()
{
	//Assume if the transform isn't set, we aren't ready to act.
	if (spinningFootTransform == nullptr)
		return;

	float vehicleSpeed = vehicleManager->getVehicle(index)->computeForwardSpeed();

	if (abs(vehicleSpeed) < velocityForRotation)
	{
		//Only show the still foot.
		spinningFootRenderer->SetEnabled(false);
		stillFootRenderer->SetEnabled(true);
	}
	else //velocity >= velocityForRotation
	{
		//Only show the spinning foot
		stillFootRenderer->SetEnabled(false);
		spinningFootRenderer->SetEnabled(true);

		//Now let's actually rotate the feet..

		float angle = vehicleSpeed * rotationAngle; //Calculate the rotation angle in eulers

		//Set the rotation
		Quaternion wantedRotation = spinningFootTransform->GetLocalRotation() * Quaternion::AngleAxis(angle,rotationAxis);
		spinningFootTransform->SetLocalRotation(wantedRotation);
	}

}



