#pragma once
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../BaseTypes/Vector3.h"

#include <vector>

class GameObject;
class Time;
///<summary> Gives some sweet animation to the snake so that it attacks a target. </summary>
class SnakeAnimator : public ScriptBehaviour
{
private:
	GameObject* headGO;
	GameObject* neckGO;
	std::vector<Vector3> neckPoints;

	Vector3 target;

	Time* time;
	float animationDamping;
public:
	SnakeAnimator(GameObject* gameObject, GameObject* headGO, GameObject* neckGO, std::vector<Vector3> neckPoints);

	///<summary> Sets the attack point of the snake </summary>
	void setTarget(Vector3 target);
	///<summary> Animates the snake at time t. t ranges from 0 --> 1f </summary>
	void animate(float t);

	virtual ~SnakeAnimator();


protected:
	virtual void Update() override;
};

