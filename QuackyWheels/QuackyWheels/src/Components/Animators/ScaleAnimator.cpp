//
// Created by Jade White on 2016-03-26.
//

#include "ScaleAnimator.h"
#include "../Transform.h"


ScaleAnimator::ScaleAnimator(GameObject* gameObject) : ValueAnimator<Vector3>(gameObject)
{
	transform = GetTransform();
}


void ScaleAnimator::OnValueComputed(Vector3 newScale)
{
	transform->SetLocalScale(newScale);
}

