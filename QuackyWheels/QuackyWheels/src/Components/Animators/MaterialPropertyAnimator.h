//
// Created by Jade White on 2016-03-24.
//

#pragma once

#include <string>
#include <vector>

#include "../../GameLoop/EngineSystems.h"
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../BaseTypes/Vector2.h"
#include "../../Renderer/Types/Color.h"

#include "../../Various/ListLerper.h"
#include "../../Time/Time.h"
#include "../../Renderer/Types/Material.h"
#include "../../Renderer/Types/Shader.h"

#include "ValueAnimator.h"


///<summary> Animates a property on a shader by interpolating through a list of values. (Note this can be replaced by tweens!) </summary>
///<remarks> This uses the Shader class as a optimization. </remarks>
template <typename T>
class MaterialPropertyAnimator : public ValueAnimator<T>
{
private:
	Material* material;

	std::string propertyName;
	GLint propertyIndex;
public:

	MaterialPropertyAnimator(GameObject* gameObject) : ValueAnimator<T>(gameObject)
	{
		material = nullptr;

		propertyName = "tint";
		propertyIndex = -1;
	}

	MaterialPropertyAnimator(GameObject* gameObject, Material* material, std::string propertyName) : MaterialPropertyAnimator(gameObject)
	{
		this->material = material;
		this->propertyIndex = material->getPropertyIndex(propertyName);
	}


	void setMaterial(Material* material)
	{
		this->material = material;
		//Reset the property index, it probably changed.
		setPropertyName(propertyName);
	}

	void setPropertyName(std::string propertyName)
	{
		this->propertyName = propertyName;
		propertyIndex = material->getPropertyIndex(propertyName);
	}

protected:
	///<summary> Called every time a new animated value is avaliable. </summary>
	virtual void OnValueComputed(T value) override
	{
		if (material == nullptr)
			return;

		material->set(propertyIndex, value); //Optimization.
	}

};

