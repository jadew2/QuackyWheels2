//
// Created by Jade White on 2016-03-26.
//

#pragma once

#include <string>
#include <vector>

#include "../../GameLoop/EngineSystems.h"
#include "../../ComponentEntity/ScriptBehaviour.h"
#include "../../BaseTypes/Vector2.h"
#include "../../Renderer/Types/Color.h"

#include "../../Various/ListLerper.h"
#include "../../Time/Time.h"
#include "../../Renderer/Types/Material.h"


///<summary> Tweens a value in a list. To be replaced by tweens eventually. </summary>
template <typename T>
class ValueAnimator : public ScriptBehaviour
{
private:
	Time* time;
	ListLerper<T> listLerper;
	float elaspedTime = 0;
public:
	enum LoopType
	{
		Once, //Plays once, from the front of the list to the back
		Loop, //Reaches the end of the colors list, and starts from the front again instantly.
		//PingPong //Reaches the end of the list, reverses and heads back
	};
	LoopType loopType = LoopType::Once;
	
	std::vector<T> values = std::vector<T>();
	///<summary> The length for one iteration in seconds. </summary>
	float duration = 1;

	ValueAnimator(GameObject* gameObject) : ScriptBehaviour(gameObject)
	{
		time = getEngineSystems()->time;
		listLerper = ListLerper<T>();
	}

protected:
	void Start() override
	{
		Rewind();
	}

	void Update() override
	{
		float t = elaspedTime / duration;

		if (loopType == LoopType::Once)
		{
			if (t > 1.0f)
				t = 1.0f;
		}
		else if (loopType == LoopType::Loop)
		{
			t = t - (float)((int)t); //Like t % 1, but like, 60 times faster.
		}

		T value = listLerper.getValueForT(values, t);
		OnValueComputed(value);

		elaspedTime += time->GetDeltaTime();
	}

	virtual void OnValueComputed(T value)
	{
		//To be overridden.
	}

	void Rewind()
	{
		elaspedTime = 0;
	}
};


