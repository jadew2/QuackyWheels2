//
// Created by Jade White on 2016-04-11.
//

#pragma once

class ScriptBehaviour;
class EnabledAnimatorKeyframe
{
	float enabledTime = 0.0f;
	float disabledTime = 0.0f;
	ScriptBehaviour* behaviour;
public:
	EnabledAnimatorKeyframe(float enabledT, float disabledT, ScriptBehaviour* behav);

	///<summary> Enables/Disables the ScriptBehaviour if globalT is in range. </summary>
	void Evaluate(float time);

};


