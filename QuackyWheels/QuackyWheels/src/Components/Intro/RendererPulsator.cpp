//
// Created by Jade White on 2016-04-11.
//

#include "RendererPulsator.h"
#include "../MeshRenderer.h"
#include "../Animators/MaterialPropertyAnimator.h"
#include <vector>
using namespace std;

RendererPulsator::RendererPulsator(GameObject* gameObject, MeshRenderer* imageRenderer) : ScriptBehaviour(gameObject)
{
	this->imageRenderer = imageRenderer;
	duration = 2.0f;
}


void RendererPulsator::OnEnable()
{
	if (imageRenderer == nullptr)
		return;
	MaterialPropertyAnimator<Color>* materialPropertyAnimator = new MaterialPropertyAnimator<Color>(GetGameObject(),imageRenderer->getMaterial(),"tint");
	materialPropertyAnimator->values = vector<Color>{Color::BlackInvisible(),Color::White(),Color::White(),Color::BlackInvisible()};
	materialPropertyAnimator->duration = duration;
}

void RendererPulsator::OnDisable()
{

}


