//
// Created by Jade White on 2016-04-11.
//

#pragma once

#include "../../ComponentEntity/ScriptBehaviour.h"

class MeshRenderer;
class RendererPulsator : public ScriptBehaviour
{
	MeshRenderer* imageRenderer;
public:
	float duration = 2.0f;

	RendererPulsator(GameObject* gameObject,MeshRenderer* imageRenderer);

protected:
	virtual void OnEnable() override ;
	virtual void OnDisable() override ;
};


