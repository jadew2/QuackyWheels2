#include "PhysicsRigidDynamicSphere.h"
#include "../Components/PhysicsRigidDynamic.h"

PhysicsRigidDynamicSphere::PhysicsRigidDynamicSphere(GameObject* gameObject, PhysicsRigidDynamic* actor, physx::PxReal radius,
	Transform* transform, physx::PxMaterial* const* materials, physx::PxFilterData* qryFilterData,
	physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags) : PhysicsShape(gameObject, transform) {
	//Set the actor for this shape
	this->actor = actor;

	//TODO set the local transform of the shape
	this->physicsShape = actor->getRigidDynamic()->createShape(physx::PxSphereGeometry(radius), materials, 1);
	this->physicsShape->setFlags(*shapeFlags);

	//Set the query filter data of the ground plane so that the vehicle raycasts can hit the ground.
	this->physicsShape->setQueryFilterData(*qryFilterData);

	//Set the simulation filter data of the ground plane so that it collides with the chassis of a vehicle but not the wheels.
	this->physicsShape->setSimulationFilterData(*simFilterData);
}


PhysicsRigidDynamicSphere::~PhysicsRigidDynamicSphere() {

}

void PhysicsRigidDynamicSphere::setTransform(Transform* transform) {
	this->transform = transform;
}

PhysicsRigidDynamic* PhysicsRigidDynamicSphere::getActor() {
	return this->actor;
}
