#pragma once
#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidStatic;
class Transform;
class PhysicsTriangleMesh : PhysicsShape {

public:
	PhysicsTriangleMesh(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxU32 numVerticies,
		physx::PxVec3 verticies[], physx::PxU32 numIndicies, physx::PxU32 indicies[],
		physx::PxU16 numMaterials, physx::PxMaterial* const* materials, physx::PxU16 materialIndex[], Transform* transform,
		physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsTriangleMesh();

	void setTransform(Transform* transform);

	PhysicsRigidStatic* getActor();

protected:
	PhysicsRigidStatic* actor;
};

