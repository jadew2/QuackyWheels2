//
// Created by Jade White on 2016-02-10.
//

#include "PhysicsActor.h"

#include "../GameLoop/EngineSystems.h"

PhysicsActor::PhysicsActor(GameObject *gameObject, Transform* transform, ObjectType type) : ScriptBehaviour(gameObject)
{
	this->transform = transform;
	this->type = type;
	time = getEngineSystems()->time;
}

PhysicsActor::~PhysicsActor() {

}

void PhysicsActor::addShape(physx::PxShape* shape) {
	this->physicsShapes.push_back(shape);
}

void PhysicsActor::removeShape(physx::PxShape* shape) {
	physicsShapes.erase(std::remove(physicsShapes.begin(), physicsShapes.end(), shape), physicsShapes.end());
}

std::vector<physx::PxShape*> PhysicsActor::getShapes() {
	return physicsShapes;
}

Transform* PhysicsActor::getTransform() {
	return this->transform;
}

ObjectType PhysicsActor::getObjectType() {
	return type;
}
