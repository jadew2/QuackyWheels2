#pragma once

#include "../Components/PhysicsShape.h"

#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidDynamic;
class Transform;
class PhysicsRigidDynamicConvexMesh : public PhysicsShape {

public:
	PhysicsRigidDynamicConvexMesh(GameObject* gameObject, PhysicsRigidDynamic* actor, physx::PxU32 numVerticies,
		physx::PxVec3 verticies[], physx::PxU16 numMaterials, physx::PxMaterial* const* meshMaterials,
		Transform* transform, physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData,
		physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidDynamicConvexMesh();

	void setTransform(Transform* transform);

	PhysicsRigidDynamic* getActor();

protected:
	PhysicsRigidDynamic* actor;
};

