//
// Created by Jade White on 2016-03-07.
//
//
#include "CopyPositionConstraint.h"
#include "../Components/Transform.h"

CopyPositionConstraint::CopyPositionConstraint(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	transform = GetTransform();
}

CopyPositionConstraint::~CopyPositionConstraint()
{

}

void CopyPositionConstraint::AfterUpdate()
{
	if (target == nullptr)
		return;

	Vector3 newPosition = target->GetLocalPosition() + offset;
	transform->SetLocalPosition(newPosition);
}
