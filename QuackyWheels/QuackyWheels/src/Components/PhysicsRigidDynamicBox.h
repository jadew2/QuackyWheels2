#pragma once
#include "../Components/PhysicsShape.h"
#include "PxPhysicsAPI.h"

class GameObject;
class PhysicsRigidDynamic;
class Transform;
class PhysicsRigidDynamicBox : public PhysicsShape {

public:
	PhysicsRigidDynamicBox(GameObject* gameObject, PhysicsRigidDynamic* actor, physx::PxReal length, physx::PxReal height,
		physx::PxReal width, Transform* transform, physx::PxMaterial* const* materials,
		physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags);
	virtual ~PhysicsRigidDynamicBox();
	
	void setTransform(Transform* transform);

	PhysicsRigidDynamic* getActor();

protected:
	PhysicsRigidDynamic* actor;

};

