#pragma once
#include "..\ComponentEntity\ScriptBehaviour.h"
#include "PxPhysicsAPI.h"

class GameObject;
class VehicleManager;
class SnakeAnimator;
class VehiclePositionTracker;
class Snake : public ScriptBehaviour {

public:
	Snake(GameObject* gameObject, VehicleManager* vehicleManager, SnakeAnimator* snakeAnimator, VehiclePositionTracker* vehiclePositionTracker);
	~Snake();

protected:
	virtual void PhysicsUpdate() override;

private:
	int aggro;
	float aggroRadius;
	float animationTime;
	float currentAnimation;
	unsigned int cooldown;
	float hitRadius;
	physx::PxVec3 hitLocation;
	physx::PxVec3 snakeLocation;
	VehicleManager* vehicleManager;
	SnakeAnimator* snakeAnimator;
	VehiclePositionTracker* vehiclePositionTracker;
};

