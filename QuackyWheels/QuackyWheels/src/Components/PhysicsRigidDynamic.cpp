#include "PhysicsRigidDynamic.h"

#include "../GameLoop/EngineSystems.h"
#include "../Components/Transform.h"
#include "../Physics/Physics.h"
#include "../Time/Time.h"

PhysicsRigidDynamic::PhysicsRigidDynamic(GameObject* gameObject, Transform* transform, ObjectType type,
	bool kinematicFlag) : PhysicsActor(gameObject, transform, type) {
	this->type = type;
	rigidDynamic = this->getEngineSystems()->physics->getPhysics()->createRigidDynamic(physx::PxTransform(physx::PxIdentity));
	rigidDynamic->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, kinematicFlag);
	rigidDynamic->userData = gameObject;
}


PhysicsRigidDynamic::~PhysicsRigidDynamic() 
{
	this->getEngineSystems()->physics->getScene()->removeActor(*rigidDynamic);
}

void PhysicsRigidDynamic::PhysicsUpdate() {
	PhysicsActor::PhysicsUpdate();
	//Apply gravity or something
	if (updateType != UpdateType::None && time->GetPhysicsUpdateCount() == 0) //?!?
	{
		previousPhysicsTimeStamp = time->GetRealTime();
		previousPhysicsPos = Vector3(rigidDynamic->getGlobalPose().p);
		previousPhysicsRot = Quaternion(rigidDynamic->getGlobalPose().q);
	}
}

void PhysicsRigidDynamic::Update() 
{
	PhysicsActor::Update();
	if (updateType == UpdateType::None)
	{
		//Just set the transform hardcore.
		transform->SetPosition(Vector3(rigidDynamic->getGlobalPose().p));
		transform->SetRotation(Quaternion(rigidDynamic->getGlobalPose().q));
	}
	else if (updateType == UpdateType::Interpolate)
	{
		float t = (time->GetRealTime() - previousPhysicsTimeStamp) / time->GetPhysicsTimestep();

		transform->SetPosition(Vector3::Lerp(previousPhysicsPos, Vector3(rigidDynamic->getGlobalPose().p), t));
		transform->SetRotation(Quaternion::Slerp(previousPhysicsRot, Quaternion(rigidDynamic->getGlobalPose().q), t));
	}
	else if (updateType == UpdateType::Extrapolate)
	{
		Vector3 posDifference = rigidDynamic->getLinearVelocity() * (time->GetRealTime() - previousPhysicsTimeStamp);
		transform->SetPosition(previousPhysicsPos + posDifference);
	}
}


void PhysicsRigidDynamic::Destroy() {	
	PhysicsActor::Destroy();
}

void PhysicsRigidDynamic::setTransform(Transform* transform) {
	this->transform = transform;
	//TODO synch with physics
}

physx::PxRigidDynamic* PhysicsRigidDynamic::getRigidDynamic() {
	return this->rigidDynamic;
}