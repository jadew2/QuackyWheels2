//
// Created by Jade White on 2016-02-10.
//

#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"
#include <vector>
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Quaternion.h"
#include "ObjectType.h"

class GameObject;
class Transform;
class Time;
class PhysicsActor : public ScriptBehaviour {
		
public:

	enum UpdateType{None,Interpolate,Extrapolate};
	UpdateType updateType = UpdateType::Interpolate;

	PhysicsActor(GameObject* gameObject, Transform* transform, ObjectType type);
	virtual ~PhysicsActor();

	void addShape(physx::PxShape* shape);
	void removeShape(physx::PxShape* shape);

	std::vector<physx::PxShape*> getShapes();
	Transform* getTransform();
	ObjectType getObjectType();

protected:
	//Interpolation variables
	Time* time;
	float previousPhysicsTimeStamp;

	float prevToCurrTime;
	Vector3 previousPhysicsPos;
	Quaternion previousPhysicsRot;

	Vector3 currPhysicsPos;
	Quaternion currPhysicsRot;

	std::vector<physx::PxShape*> physicsShapes;

	Transform* transform;

	ObjectType type;
};
