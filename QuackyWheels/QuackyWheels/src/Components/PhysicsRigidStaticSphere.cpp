#include "PhysicsRigidStaticSphere.h"
#include "../Components/PhysicsRigidStatic.h"

PhysicsRigidStaticSphere::PhysicsRigidStaticSphere(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxReal radius,
	Transform* transform, physx::PxMaterial* const* materials, physx::PxFilterData* qryFilterData,
	physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags) : PhysicsShape(gameObject, transform){

	//Set the actor for this shape
	this->actor = actor;

	this->physicsShape = actor->getRigidStatic()->createShape(physx::PxSphereGeometry(radius), materials, 1);
	this->physicsShape->setFlags(*shapeFlags);

	//Set the query filter data of the ground plane so that the vehicle raycasts can hit the ground.
	this->physicsShape->setQueryFilterData(*qryFilterData);

	//Set the simulation filter data of the ground plane so that it collides with the chassis of a vehicle but not the wheels.
	this->physicsShape->setSimulationFilterData(*simFilterData);
}


PhysicsRigidStaticSphere::~PhysicsRigidStaticSphere() {

}


void PhysicsRigidStaticSphere::setTransform(Transform* transform) {
	this->transform = transform;
}

PhysicsRigidStatic* PhysicsRigidStaticSphere::getActor() {
	return this->actor;
}

