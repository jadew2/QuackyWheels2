#include "VehiclePositionTracker.h"

#include "../ComponentEntity/GameObject.h"
#include "../Components/VehicleManager.h"
#include "../Components/PhysicsVehicle.h"
#include "../Various/Debug.h"
#include "../BaseTypes/Quaternion.h"

VehiclePositionTracker::VehiclePositionTracker(GameObject* gameObject, VehicleManager* vehicles, std::vector<physx::PxVec3> trackSpline, int killplane) : ScriptBehaviour(gameObject) {
	this->vehicles = vehicles;
	this->trackSpline = trackSpline;
	this->places.resize(vehicles->getNbVehicles());
	this->index.resize(vehicles->getNbVehicles());
	kp = killplane;

	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		places[i] = i + 1;
		index[i] = trackSpline.size() - 1;
	}
}


VehiclePositionTracker::~VehiclePositionTracker() {

}

void VehiclePositionTracker::PhysicsUpdate() {
	std::vector<physx::PxVec3> positions;
 
	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		positions.push_back(((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->getPositionTrackerPosition());
	}
	calculatePlace(positions);
}

void VehiclePositionTracker::calculatePlace(std::vector<physx::PxVec3> vehiclePositions) {
	std::vector<physx::PxReal> distances;
	distances.resize(trackSpline.size());
	std::vector<physx::PxReal> tVec;
	tVec.resize(trackSpline.size());
	std::vector<physx::PxReal> tMin;
	tMin.resize(vehicles->getNbVehicles());

	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		for (size_t j = 0; j < trackSpline.size(); j++) {
			physx::PxReal t = calculateT(vehiclePositions[i], trackSpline[j % trackSpline.size()], trackSpline[(j + 1) % trackSpline.size()]);
			if (t > 1.0f) {
				t = 1.0f;
			}
			else if (t < 0.0f) {
				t = 0.0f;
			}
			tVec[j] = t;
			physx::PxVec3 p = trackSpline[(j + 1) % trackSpline.size()] * t + trackSpline[j % trackSpline.size()] * (1 - t);
			distances[j] = (vehiclePositions[i] - p).physx::PxVec3::magnitude();
		}

		physx::PxReal dMin = distances[0];
		index[i] = 0;

		for (size_t j = 1; j < distances.size(); j++) {
			if (distances[j] < dMin) {
				dMin = distances[j];
				tMin[i] = tVec[j];
				index[i] = j;
			}
		}

		//Check trigger volumes for lapping
	}

	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		places[i] = 1;
	}
	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		for (unsigned int j = i + 1; j < vehicles->getNbVehicles(); j++) {
			if (((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps < ((GameObject*)vehicles->getVehicle(j)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps) {
				places[i]++;
			} else if (((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps == ((GameObject*)vehicles->getVehicle(j)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps) {
				if (index[i] < index[j]) {
					places[i]++;
				} else if (index[i] == index[j]) {
					if (tMin[i] < tMin[j]) {
						places[i]++;
					} else {
						places[j]++;
					}
				} else {
					places[j]++;
				}
			} else {
				places[j]++;
			}
		}
	}

	for (unsigned int i = 0; i < vehicles->getNbVehicles(); i++) {
		//printf("player%d: place:%d, lap:%d, index:%d ; ", i, places[i], ((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->laps, index[i]);
		if (vehicles->getVehicle(i)->getRigidDynamicActor()->getGlobalPose().p.y <= kp || ((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->manualReset == true) {
			
			vehicles->getVehicle(i)->getRigidDynamicActor()->setGlobalPose(physx::PxTransform(physx::PxIdentity));
			physx::PxVec3 p = trackSpline[(index[i] + 1) % trackSpline.size()] * tMin[i] + trackSpline[index[i] % trackSpline.size()] * (1 - tMin[i]);
			physx::PxVec3 alignVec;
			if (p == trackSpline[(index[i] + 1) % trackSpline.size()]) {
				alignVec = p - trackSpline[(index[i] + 2) % trackSpline.size()];
			} else {
				alignVec = p - trackSpline[(index[i] + 1) % trackSpline.size()];
			}
			alignVec.normalize();
			Quaternion newRotation = Quaternion::LookRotation(Vector3(alignVec), Vector3::Up());
			physx::PxTransform startTransform(p, newRotation.pxQuat());
			vehicles->getVehicle(i)->getRigidDynamicActor()->setGlobalPose(startTransform);

			vehicles->getVehicle(i)->setToRestState();
			vehicles->getVehicle(i)->mDriveDynData.forceGearChange(physx::PxVehicleGearsData::eFIRST);
			((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->reset = true;
			((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->manualReset = false;
		} else {
			((GameObject*)vehicles->getVehicle(i)->getRigidDynamicActor()->userData)->GetComponent<PhysicsVehicle>()->reset = false;
		}
	}
	//printf("\n");

	

}

int VehiclePositionTracker::getPlace(int ind)
{
	return places[ind];
}

physx::PxReal VehiclePositionTracker::calculateT(physx::PxVec3 x0, physx::PxVec3 x1, physx::PxVec3 x2) {
	return -(x1 - x0).dot(x2 - x1) / (x2 - x1).magnitudeSquared();
}

std::vector<int> VehiclePositionTracker::getPlaces() {
	return places;
}

std::vector<int> VehiclePositionTracker::getIndex() {
	return index;
}

std::vector<physx::PxVec3> VehiclePositionTracker::getTrackSpline() {

	return trackSpline;
}
