//
// Created by Jade White on 2016-04-07.
//

#include "ImageEffectRenderer.h"
#include "../Renderer/RenderCollection.h"

#include "../GameLoop/EngineSystems.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/Types/Mesh.h"
#include "../Renderer/Types/Material.h"
#include "../Renderer/Types/Shader.h"
#include "../Renderer/MeshRenderGL.h"
#include "../Renderer/Types/FrameBufferTexture.h"
#include "Camera.h"
#include <vector>
using namespace std;


ImageEffectRenderer::ImageEffectRenderer(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	renderCollection = getEngineSystems()->renderer->renderCollection;

	//Create a quad and move it over so it's final coords are
	//(0,0),(1,0), (0,1), (1,1)
	fullScreenQuad = Mesh::Quad();
	fullScreenQuad->Deform(Matrix4x4::TRS(Vector3::Zero(),Quaternion::Identity(),Vector3(2.0,-2.0f,0.0f)));
	fullScreenQuad->getGLMesh(); //Force generate the GL Mesh for it (Optimization).

	material = nullptr;

	meshRenderGL = new MeshRenderGL();

	renderCollection->addImageEffect(this);
}


ImageEffectRenderer::~ImageEffectRenderer()
{
	renderCollection->removeImageEffect(this);

	delete fullScreenQuad;
	delete meshRenderGL;
}


void ImageEffectRenderer::render(Camera* camera, bool doCulling)
{
	if (!GetEnabled())
		return;
	if (material == nullptr)
		return;
	if (!camera->mask.collides(mask))
		return;

	GLMeshBuffers* glMesh = fullScreenQuad->getGLMesh();

	//Set up the shader and textures
	glUseProgram(material->getShader()->getGLShader());
	meshRenderGL->setUpTextures(material);

	material->applyAllPropertiesOnShader();

	glDisable(GL_BLEND); //No Blending
	glDisable(GL_DEPTH_TEST); //Don't use a depth test for this effect.
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); //No Wireframe.
	glEnable(GL_CULL_FACE); //Backface culling

	//Draw the mesh.
	meshRenderGL->drawMesh(glMesh);

	//Now clean up.
	glBindTexture(GL_TEXTURE_2D,0);
	glUseProgram(0);
}


Material* ImageEffectRenderer::getMaterial()
{
	return this->material;
}


void ImageEffectRenderer::setMaterial(Material* material)
{
	this->material = material;
}


void ImageEffectRenderer::OnEnable()
{

}

void ImageEffectRenderer::OnDisable()
{
}

