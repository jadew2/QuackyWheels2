#include "PhysicsRigidStatic.h"

#include "../GameLoop/EngineSystems.h"
#include "../Components/Transform.h"
#include "ObjectType.h"
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Quaternion.h"
#include "../Physics/Physics.h"
#include "../ComponentEntity/GameObject.h"

PhysicsRigidStatic::PhysicsRigidStatic(GameObject* gameObject, Transform* transform, ObjectType type, bool kinematicFlag) : PhysicsActor(gameObject, transform, type) {
	this->type = type;
	rigidStatic = this->getEngineSystems()->physics->getPhysics()->createRigidStatic(physx::PxTransform(*transform));
	rigidStatic->userData = gameObject;
}

PhysicsRigidStatic::~PhysicsRigidStatic() 
{
	rigidStatic->userData = nullptr;
	this->getEngineSystems()->physics->getScene()->removeActor(*rigidStatic);
}

void PhysicsRigidStatic::EarlyUpdate() {
	PhysicsActor::EarlyUpdate();
	if (updateType == UpdateType::None)
	{
		//Just set the transform hardcore.
		transform->SetPosition(Vector3(rigidStatic->getGlobalPose().p));
		transform->SetRotation(Quaternion(rigidStatic->getGlobalPose().q));
	}
	else if (updateType == UpdateType::Interpolate)
	{
		//printf("Error: Attempted to interpolate a rigid static!\n");
	}
	else if (updateType == UpdateType::Extrapolate)
	{
		//printf("Error: Attempted to extraploate a rigid static!\n");
	}
}

void PhysicsRigidStatic::Destroy() {
	
}

void PhysicsRigidStatic::setTransform(Transform* transform) {
	this->transform = transform;
	//TODO synch with physics
}

physx::PxRigidStatic* PhysicsRigidStatic::getRigidStatic() {
	return this->rigidStatic;
}