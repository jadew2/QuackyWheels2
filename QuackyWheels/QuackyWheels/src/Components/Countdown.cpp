#include "Countdown.h"


Countdown::Countdown(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	countdownFinished = false;
	start = time(0);
}


Countdown::~Countdown()
{
}

void Countdown::PhysicsUpdate()
{
	if (!countdownFinished)
	{
		total_time = difftime(time(0), start);
		if (total_time > 4)
		{
			countdownFinished = true;
		}
	}
}

bool Countdown::isCountdownFinished()
{
	return countdownFinished;
}

float Countdown::countDown()
{
	return (float)total_time;
}