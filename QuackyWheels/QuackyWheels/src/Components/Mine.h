#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"

class GameObject;
class PhysicsVehicle;
class VehicleManager;
class Mine : public ScriptBehaviour
{
public:
	Mine(GameObject *gameObject, VehicleManager* vehicleManager, int index);
	~Mine();
	void Hit();

private:
	PhysicsVehicle* vehicle;
	VehicleManager* vehicleManager;

protected:
	virtual void Update() override;
};

