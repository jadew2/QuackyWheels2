#include "PhysicsTriangleMesh.h"

#include "../GameLoop/EngineSystems.h"
#include "../Renderer/Types/Mesh.h"
#include "../Components/PhysicsRigidStatic.h"
#include "../Physics/Physics.h"
#include "MeshRenderer.h"

PhysicsTriangleMesh::PhysicsTriangleMesh(GameObject* gameObject, PhysicsRigidStatic* actor, physx::PxU32 numVerticies,
	physx::PxVec3 verticies[], physx::PxU32 numIndicies, physx::PxU32 indicies[],
	physx::PxU16 numMaterials, physx::PxMaterial* const* materials, physx::PxU16 materialIndex[], Transform* transform,
	physx::PxFilterData* qryFilterData, physx::PxFilterData* simFilterData, physx::PxShapeFlags* shapeFlags) : PhysicsShape(gameObject, transform) {
	
	physx::PxTriangleMeshDesc triangleMeshDesc;
	triangleMeshDesc.points.count = numVerticies;
	triangleMeshDesc.points.stride = sizeof(physx::PxVec3);
	triangleMeshDesc.points.data = verticies;

	triangleMeshDesc.triangles.count = numIndicies / 3;
	triangleMeshDesc.triangles.stride = 3 * sizeof(physx::PxU32);
	triangleMeshDesc.triangles.data = indicies;

	/*triangleMeshDesc.materialIndices.stride = sizeof(PxU16);
	triangleMeshDesc.materialIndices.data = materialIndex;*/

	bool val = this->getEngineSystems()->physics->getCooking()->validateTriangleMesh(triangleMeshDesc);
	printf("validate: %s\n", val ? "true" : "false");

	physx::PxDefaultMemoryOutputStream buf;

	if (!this->getEngineSystems()->physics->getCooking()->cookTriangleMesh(triangleMeshDesc, buf)) {
		printf("cook shape failed!");
	}
	physx::PxDefaultMemoryInputData input(buf.getData(), buf.getSize());
	physx::PxTriangleMesh* triangleMesh = this->getEngineSystems()->physics->getPhysics()->createTriangleMesh(input);

	this->physicsShape = actor->getRigidStatic()->createShape(physx::PxTriangleMeshGeometry(triangleMesh), materials, numMaterials);

	this->physicsShape->setFlags(*shapeFlags);

	//Set the query filter data of the ground plane so that the vehicle raycasts can hit the ground.
	this->physicsShape->setQueryFilterData(*qryFilterData);

	//Set the simulation filter data of the ground plane so that it collides with the chassis of a vehicle but not the wheels.
	this->physicsShape->setSimulationFilterData(*simFilterData);
}


PhysicsTriangleMesh::~PhysicsTriangleMesh() {
	
}

void PhysicsTriangleMesh::setTransform(Transform* transform) {
	this->transform = transform;
}

PhysicsRigidStatic* PhysicsTriangleMesh::getActor() {
	return this->actor;
}
