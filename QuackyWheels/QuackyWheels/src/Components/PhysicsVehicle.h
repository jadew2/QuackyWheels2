#pragma once
#include <glm.hpp>
#include "../ComponentEntity/ScriptBehaviour.h"
#include "PxPhysicsAPI.h"
#include "../Vehicle/VehicleDesc.h"
#include "ObjectType.h"
#include <vector>

class GameObject;
class VehicleManager;
class PhysicsRigidDynamic;
class VehicleTrail;
class VehicleSceneQueryData;
class Countdown;
class Camera;
class PhysicsVehicle : public ScriptBehaviour {

public:
	bool manualReset;
	unsigned long trackNumber;
	bool reset;
	int laps;
	bool checkpoint;
	std::vector<PhysicsRigidDynamic*> obstacles;
	std::vector<bool> obstacleRelativePos;

	PhysicsVehicle(GameObject* gameObject, physx::PxTransform* transform, ObjectType type, VehicleManager* vehicleManager, unsigned long trackNumber);
	~PhysicsVehicle();

	static void setupWheelsSimulationData(const physx::PxF32 wheelMass, const physx::PxF32 wheelMOI, const physx::PxF32 wheelRadius, const physx::PxF32 wheelWidth,
		const physx::PxU32 numWheels, const physx::PxVec3* wheelCenterActorOffsets,
		const physx::PxVec3& chassisCMOffset, const physx::PxF32 chassisMass,
		physx::PxVehicleWheelsSimData* wheelsSimData, const VehicleDesc& vehicle4WDesc);

	static void computeWheelCenterActorOffsets4W(const physx::PxF32 wheelFrontZ, const physx::PxF32 wheelRearZ, const physx::PxVec3& chassisDims,
		const physx::PxF32 wheelWidth, const physx::PxF32 wheelRadius, const physx::PxU32 numWheels, physx::PxVec3* wheelCentreOffsets);

	physx::PxVehicleDrive4W* createVehicle4W(const VehicleDesc& vehicle4WDesc, PhysicsRigidDynamic* vehicleActor, physx::PxPhysics* physics);

	void setVehicleManager(VehicleManager* vehicleManager);

	physx::PxVehicleDrive4W* getVehicle();
	physx::PxU32 getGear();
	glm::mat4 getVehicleTransform();
	glm::vec3 getVehiclePosition();
	VehicleSceneQueryData* getVehicleSceneQueryData();

	void setInTrail(bool inTrail);
	void powerUp();
	void hitMine();

	void setVehicleIndex(int index);

	int getVehicleIndex();
	void setPowerUp(int index);
	int getPowerUpTimer();
	physx::PxVec3 getPositionTrackerPosition();

	void setSmoothingData();
	void setSteerSpeedData();

	void Update() override;
	void PhysicsUpdate() override;
	void Destroy() override;

	Countdown* countdown;
	
protected:
	

private:

	VehicleManager* vehicleManager;
	int vehicleIndex;

	physx::PxVehicleDrive4W* vehicle4W;
	VehicleDesc vehicleDesc;
	physx::PxVehicleDriveSimData4W driveSimData;

	ObjectType type;

	physx::PxVec3 positionTrackerPosition;

	physx::PxVec3 previousVehiclePosition;

	VehicleTrail* trail;

	physx::PxVehicleDrive4WRawInputData vehicleInput;

	physx::PxVehiclePadSmoothingData smoothingData;
	physx::PxF32 steerVsSpeedData[2 * 8];
	physx::PxFixedSizeLookupTable<8> steerVsSpeedTable;

	physx::PxReal distance;

	physx::PxRaycastBuffer buffer;

	bool inTrail;

	int availablePowerUp;
	int inUsePowerUp;
	int powerUpTimer;
	bool slowDown;
	bool start;
	float topSpeed;
};

