#pragma once

#include "../ComponentEntity/ScriptBehaviour.h"


class GameObject;
class PhysicsVehicle;
class EngineSystems;
class VehicleManager;
class VehiclePositionTracker;
class PowerUpManager : public ScriptBehaviour
{
public:
	PowerUpManager(GameObject* gameObject, PhysicsVehicle* list[], VehiclePositionTracker* vehiclePositionTracker);
	virtual ~PowerUpManager();
	void usePowerUp();
	void setPowerUp(int i);
	void randomPowerUp(int i);
	int getPowerUp(int i);
	virtual void Update() override;

private:
	GameObject* manager;
	EngineSystems* es;
	VehicleManager* vehicleManager;
	PhysicsVehicle* vehList[4];
	VehiclePositionTracker* vpt;
	int randomIndex;
	bool active[4];
	bool powerUp[4];
	int power[4];
};

