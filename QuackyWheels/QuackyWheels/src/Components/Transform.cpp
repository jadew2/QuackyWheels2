//
// Created by Jade White on 2016-01-29.
//

#include "Transform.h"

Transform::Transform(GameObject* gameObject) : Transform(gameObject, new Vector3(), new Quaternion())
{

}

Transform::Transform(GameObject* gameObject, Vector3* position, Quaternion* rotation) : Component(gameObject) {
	parent = nullptr;
	children = std::vector<Transform*>();

	localPosition = *position;
	localRotation = *rotation;

	localMatrixDirty = true;

	localTRSMatrix = Matrix4x4::Identity();
}

Transform::Transform(GameObject* gameObject, physx::PxTransform* transform) : Component(gameObject) {
	parent = nullptr;
	children = std::vector<Transform*>();

	localPosition = transform->p;
	localRotation = transform->q;

	localMatrixDirty = true;
	localTRSMatrix = Matrix4x4::Identity();
}


void Transform::SetParent(Transform *transform, bool keepWorldPosRot)
{
	if (transform == this)
		throw new EngineException("Transform: Cannot set parent to ourselves!");

	setMatrixDirty();

	if (keepWorldPosRot)
	{
		Vector3 worldPos = GetPosition();
		Quaternion worldRot = GetRotation();
		//Vector3 worldScale = GetLossyScale();

		SetPosition(worldPos);
		SetRotation(worldRot);
		//SetLocalScale(worldScale); //SetScale would be better, but the correct math for it is tricky. Stretch goal perhaps.
	}

	if (parent != nullptr)
		parent->internalRemoveChild(this);
	parent = transform;
	if (parent != nullptr)
		parent->internalAddChild(this);
}

Transform* Transform::GetParent()
{
	return parent;
}


Transform* Transform::getRoot()
{
	Transform* curr = this;
	while (curr->parent != nullptr)
		curr = curr->parent;

	return curr;
}


std::vector<Transform *> Transform::GetAncestors()
{
	std::vector<Transform*> result = std::vector<Transform*>();
	Transform* currParent = parent;

	while (currParent != nullptr)
	{
		result.push_back(currParent);
		currParent = currParent->parent;
	}
	return result;
}

std::vector<Transform*> Transform::GetChildren(bool recursive)
{
	if (!recursive)
		return std::vector<Transform*>(children); //This should make a copy.

	//Otherwise, we are recursive, return all the children.
	std::vector<Transform*> resultChildren(children); //First, add our children

	for (Transform* child : children)
	{
		std::vector<Transform*> subChildren = child->GetChildren(recursive);
		resultChildren.insert(resultChildren.end(), subChildren.begin(), subChildren.end()); //resultChildren.addRange(subChildren)
	}

	return resultChildren;
}

void Transform::AddChild(Transform *transform, bool keepWorldPosition)
{
	transform->SetParent(this,keepWorldPosition); //SetParent handles adding to our list.
}

void Transform::RemoveChild(Transform *transform, bool keepWorldPosition)
{
	transform->SetParent(nullptr,keepWorldPosition); //SetParent handles removing from our list.
}

void Transform::RemoveAllChildren(bool keepWorldPositions)
{
	std::vector<Transform*> childrenCopy(children);
	for (size_t i = 0; i < childrenCopy.size(); ++i)
		RemoveChild(childrenCopy[i],keepWorldPositions);
}


Vector3 Transform::GetPosition()
{
	//return GetLocalPosition(); //For now
	return GetTRSMatrix().GetPosition();
}

void Transform::SetPosition(Vector3 value)
{
	SetLocalPosition(value);
	//Matrix4x4 trs = GetTRSMatrix();
	//SetLocalPosition(trs.Inverse().MultiplyPoint(value)); //TODO: Check
}

Vector3 Transform::GetLocalPosition()
{
	return localPosition;
}

void Transform::SetLocalPosition(Vector3 value)
{
	setMatrixDirty();
	localPosition = value;
}

Quaternion Transform::GetRotation()
{
	return GetTRSMatrix().GetRotation();
}

void Transform::SetRotation(Quaternion rotation)
{
	//SetLocalRotation(rotation);
	/*
	cout << "We want to set the rotation! " << endl;
	cout << "Wanted " << rotation << endl;

	cout << "LocalRotation " << GetLocalRotation() << endl;
	cout << "WorldRotation " << GetRotation() << endl;
	cout << "WorldRotation Inverse" << GetRotation().Inverse() << endl;

	cout << "Result0" << GetLocalRotation() * GetRotation().Inverse() << endl;
	cout << "Result1" << GetLocalRotation() * GetRotation().Inverse() * rotation << endl;
	*/
	SetLocalRotation(rotation);


	//SetLocalRotation(GetLocalRotation() * GetRotation().Inverse() * rotation); // Now we know this line is the culprit
}

Quaternion Transform::GetLocalRotation()
{
	return localRotation;
}

void Transform::SetLocalRotation(Quaternion value)
{
	setMatrixDirty();
	localRotation = value;
}

Vector3 Transform::GetLossyScale()
{
	return GetTRSMatrix().GetScale();
}

Vector3 Transform::GetLocalScale()
{
	return localScale;
}

void Transform::SetLocalScale(Vector3 value)
{
	setMatrixDirty();
	localScale = value;
}

Matrix4x4 Transform::GetTRSMatrix()
{
	if (parent == nullptr)
		return GetLocalTRSMatrix();
	else
		return (parent->GetTRSMatrix()) * GetLocalTRSMatrix();
	//throw new NotImplementedException("GetTRSMatrix: In works of working out a compiler bug...");
}

Matrix4x4 Transform::GetLocalTRSMatrix()
{
	if (localMatrixDirty)
	{
		localTRSMatrix = Matrix4x4::TRS(localPosition, localRotation, localScale);
		localMatrixDirty = false;
	}
	return localTRSMatrix;
}

Matrix4x4 Transform::getLocalToWorldMatrix()
{
	return GetTRSMatrix();
}

Matrix4x4 Transform::getWorldToLocalMatrix()
{
	if (parent == nullptr)
		return Matrix4x4::Identity();
	else
		return parent->GetTRSMatrix().Inverse(); //* GetLocalTRSMatrix();
}

void Transform::setMatrixDirty()
{
	localMatrixDirty = true;
}

Matrix4x4 Transform::getVehicleTransform()
{
	return vTransform;
}

void Transform::setVehicleTransform(glm::mat4 vehicleTransform)
{
	vTransform = Matrix4x4(vehicleTransform);
}

Vector3 Transform::ToWorldPoint(Vector3 localPoint)
{
	return GetTRSMatrix().MultiplyPoint(localPoint);
}

Vector3 Transform::ToLocalPoint(Vector3 worldPoint)
{
	return getWorldToLocalMatrix().MultiplyPoint(worldPoint);
}

Transform::operator physx::PxTransform()
{
	return physx::PxTransform(physx::PxVec3(localPosition), physx::PxQuat(localRotation));
}

Vector3 Transform::Forward()
{
	return GetRotation() * Vector3::Forward();  //GetTRSMatrix().MultiplyPoint(Vector3::Forward());
}

Vector3 Transform::Left()
{
	return GetRotation() * Vector3::Left(); //GetTRSMatrix().MultiplyPoint(Vector3::Left());
}

Vector3 Transform::Right()
{
	return GetRotation() * Vector3::Right();
}

Vector3 Transform::Up()
{
	return GetRotation() * Vector3::Up(); //This is fine.
}

void Transform::internalAddChild(Transform* child)
{
	children.push_back(child);
}

void Transform::internalRemoveChild(Transform* child)
{
	children.erase(std::remove(children.begin(), children.end(), child), children.end());
}
