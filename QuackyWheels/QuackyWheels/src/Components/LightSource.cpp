//
// Created by Jade White on 2016-03-04.
//

#include "LightSource.h"

#include "../GameLoop/EngineSystems.h"
#include "../Renderer/Renderer.h"
#include "../Renderer/RenderCollection.h"

LightSource::LightSource(GameObject* gameObject) : ScriptBehaviour(gameObject)
{
	getEngineSystems()->renderer->renderCollection->AddLight(this);
}

LightSource::~LightSource()
{
	getEngineSystems()->renderer->renderCollection->RemoveLight(this);
}
