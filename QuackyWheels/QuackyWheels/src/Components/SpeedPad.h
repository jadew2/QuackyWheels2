#pragma once
#include "../ComponentEntity/ScriptBehaviour.h"
#include <vector>
#include "../BaseTypes/Vector3.h"
#include "../BaseTypes/Quaternion.h"

class VehicleManager;
class SpeedPad : public ScriptBehaviour {

public:
	SpeedPad(GameObject* gameObject, VehicleManager* vehicleManager, Vector3 pos, Quaternion rot);
	~SpeedPad();

protected:
	virtual void PhysicsUpdate() override;

private:
	VehicleManager* vehicleManager;
	std::vector<int> hit;
};

