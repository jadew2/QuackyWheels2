

#include "VehicleDesc.h"
#include "../Vehicle/VehicleTireFriction.h"

VehicleDesc initVehicleDesc(physx::PxPhysics* physics) {
	//Set up the chassis mass, dimensions, moment of inertia, and center of mass offset.
	//The moment of inertia is just the moment of inertia of a cuboid but modified for easier steering.
	//Center of mass offset is 0.65m above the base of the chassis and 0.25m towards the front.
	//Set up the wheel mass, radius, width, moment of inertia, and number of wheels.
	//Moment of inertia is just the moment of inertia of a cylinder.
	VehicleDesc vehicleDesc;
	vehicleDesc.chassisMass = 1500.0f;
	vehicleDesc.chassisDims = physx::PxVec3(2.5f, 2.0f, 5.0f);
	//Doubled inertia along x axis and 1.5 times along z axis. Simulates a car with a very wide base of support
	vehicleDesc.chassisMOI = physx::PxVec3(((vehicleDesc.chassisDims.y*vehicleDesc.chassisDims.y + vehicleDesc.chassisDims.z*vehicleDesc.chassisDims.z)*vehicleDesc.chassisMass / 12.0f) * 2.0f,
		((vehicleDesc.chassisDims.x*vehicleDesc.chassisDims.x + vehicleDesc.chassisDims.z*vehicleDesc.chassisDims.z)*0.8f*vehicleDesc.chassisMass / 12.0f) * 5.0f,
		((vehicleDesc.chassisDims.x*vehicleDesc.chassisDims.x + vehicleDesc.chassisDims.y*vehicleDesc.chassisDims.y)*vehicleDesc.chassisMass / 12.0f) * 10.0f);
	//-1.7 may still be too low, possibly put to -1.6 or -1.5
	vehicleDesc.chassisCMOffset = physx::PxVec3(0.0f, -1.6f, 0.0f);
	vehicleDesc.chassisMaterial = physics->createMaterial(0.5f, 0.5f, 0.6f);

	vehicleDesc.wheelMass = 20.0f;
	vehicleDesc.wheelWidth = 0.4f;
	vehicleDesc.wheelRadius = 0.5f;
	vehicleDesc.wheelMOI = 0.5f*vehicleDesc.wheelMass*vehicleDesc.wheelRadius*vehicleDesc.wheelRadius;
	vehicleDesc.wheelAngularDamping = 0.25f;
	//Increased to 10,000,000 from 1500
	vehicleDesc.maxBrakeTorque = 10000000.0f;
	vehicleDesc.maxHandbrakeTorqueFront = 0.0f;
	vehicleDesc.maxHandbrakeTorqueRear = 30000.0f;
	vehicleDesc.maxSteer = physx::PxPi*0.333333f;
	vehicleDesc.numWheels = 4;
	vehicleDesc.wheelMaterial = physics->createMaterial(0.9f, 0.9f, 0.1f);

	vehicleDesc.maxCompression = 0.3f;
	vehicleDesc.maxDroop = 0.1f;
	//Base 35,000
	vehicleDesc.springStrength = 50000.0f;
	vehicleDesc.springDamperRate = 4500.0f;
	vehicleDesc.camberAtRest = 0.0f;
	vehicleDesc.camberAtMaxCompression = -0.01f;
	vehicleDesc.camberAtMaxDroop = 0.01f;

	vehicleDesc.longitudinalStiffnessPerUnitGravity = 1000.0f;
	vehicleDesc.lateralStiffnessX = 2.0f;
	vehicleDesc.lateralStiffnessY = 1.0f*(180.0f / physx::PxPi);
	vehicleDesc.camberStiffnessPerUnitGravity = 0.1f*(180.0f / physx::PxPi);
	vehicleDesc.tireType = TIRE_TYPE_NORMAL;

	vehicleDesc.engineMOI = 1.0f;
	//Increased engine torque from 500 to 100000
	vehicleDesc.enginePeaktorque = 5000.0f;
	vehicleDesc.engineMaxOmega = 120000.0f;
	//Decreased form 0.15 to 0.05. Engine is damped less at high throttle
	vehicleDesc.dampingRateAtFullThrottle = 0.05f;
	vehicleDesc.dampingRateAtZeroThrottle = 2.0f;
	vehicleDesc.dampingRateAtZeroThrottleClutchDisengaged = 0.35f;

	vehicleDesc.numRatios = 7;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eREVERSE] = -4.0f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eNEUTRAL] = 0.0f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eFIRST] = 4.0f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eSECOND] = 2.0f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eTHIRD] = 1.5f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eFOURTH] = 1.1f;
	vehicleDesc.ratios[physx::PxVehicleGearsData::eFIFTH] = 1.0f;
	for (physx::PxU32 i = physx::PxVehicleGearsData::eSIXTH; i < physx::PxVehicleGearsData::eGEARSRATIO_COUNT; ++i) {
		vehicleDesc.ratios[i] = 0.f;
	}
	vehicleDesc.finalRatio = 4.0f;
	//Reduced switch time from 0.5 to 0.1
	vehicleDesc.switchTime = 0.1f;

	//Autobox data here

	vehicleDesc.clutchStrength = 10.0f;

	vehicleDesc.accuracy = 1.0f;

	vehicleDesc.differentialType = physx::PxVehicleDifferential4WData::eDIFF_TYPE_LS_4WD;

	return vehicleDesc;
}
