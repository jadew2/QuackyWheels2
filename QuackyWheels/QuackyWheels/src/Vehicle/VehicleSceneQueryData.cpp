#include <new>

#include "VehicleSceneQueryData.h"

void setupDrivableSurface(physx::PxFilterData& filterData)
{
	filterData.word3 = (physx::PxU32)DRIVABLE_SURFACE;
}

void setupNonDrivableSurface(physx::PxFilterData& filterData)
{
	filterData.word3 = UNDRIVABLE_SURFACE;
}

physx::PxQueryHitType::Enum WheelRaycastPreFilter
(physx::PxFilterData filterData0, physx::PxFilterData filterData1,
const void* constantBlock, physx::PxU32 constantBlockSize,
physx::PxHitFlags& queryFlags)
{
	//filterData0 is the vehicle suspension raycast.
	//filterData1 is the shape potentially hit by the raycast.
	PX_UNUSED(constantBlockSize);
	PX_UNUSED(constantBlock);
	PX_UNUSED(filterData0);
	PX_UNUSED(queryFlags);
	return ((0 == (filterData1.word3 & DRIVABLE_SURFACE)) ? physx::PxQueryHitType::eNONE : physx::PxQueryHitType::eBLOCK);
}

VehicleSceneQueryData::VehicleSceneQueryData()
: mNumRaycastsPerBatch(0),
mSqResults(NULL),
mSqHitBuffer(NULL),
mPreFilterShader(WheelRaycastPreFilter)
{
}

VehicleSceneQueryData::~VehicleSceneQueryData()
{
}

VehicleSceneQueryData* VehicleSceneQueryData::allocate(const physx::PxU32 maxNumVehicles, const physx::PxU32 maxNumWheelsPerVehicle, const physx::PxU32 numVehiclesInBatch, physx::PxAllocatorCallback& allocator)
{
	PX_COMPILE_TIME_ASSERT(0 == (sizeof(physx::PxRaycastQueryResult) & 15));
	PX_COMPILE_TIME_ASSERT(0 == (sizeof(physx::PxRaycastHit) & 15));

	const physx::PxU32 sqDataSize = ((sizeof(VehicleSceneQueryData)+15) & ~15);

	const physx::PxU32 maxNumWheels = maxNumVehicles*maxNumWheelsPerVehicle;

	const physx::PxU32 size = sqDataSize + sizeof(physx::PxRaycastQueryResult)*maxNumWheels + sizeof(physx::PxRaycastHit)*maxNumWheels;
	physx::PxU8* buffer = (physx::PxU8*)allocator.allocate(size, NULL, NULL, 0);

	VehicleSceneQueryData* sqData = new(buffer)VehicleSceneQueryData();
	buffer += sqDataSize;

	sqData->mNumRaycastsPerBatch = numVehiclesInBatch*maxNumWheelsPerVehicle;

	sqData->mSqResults = (physx::PxRaycastQueryResult*)buffer;
	buffer += sizeof(physx::PxRaycastQueryResult)*maxNumWheels;

	sqData->mSqHitBuffer = (physx::PxRaycastHit*)buffer;
	buffer += sizeof(physx::PxRaycastHit)*maxNumWheels;

	for (physx::PxU32 i = 0; i < maxNumVehicles; i++)
	{
		for (physx::PxU32 j = 0; j < maxNumWheelsPerVehicle; j++)
		{
			PX_ASSERT((size_t)(sqData->mSqResults + i*maxNumWheelsPerVehicle + j) < (size_t)buffer);
			PX_ASSERT((size_t)(sqData->mSqHitBuffer + i*maxNumWheelsPerVehicle + j) < (size_t)buffer);
			new(sqData->mSqResults + i*maxNumWheelsPerVehicle + j) physx::PxRaycastQueryResult();
			new(sqData->mSqHitBuffer + i*maxNumWheelsPerVehicle + j) physx::PxRaycastHit();
		}
	}

	return sqData;
}

void VehicleSceneQueryData::free(physx::PxAllocatorCallback& allocator)
{
	allocator.deallocate(this);
}

physx::PxBatchQuery* VehicleSceneQueryData::setUpBatchedSceneQuery(const physx::PxU32 batchId, const VehicleSceneQueryData& vehicleSceneQueryData, physx::PxScene* scene)
{
	const physx::PxU32 maxNumRaycastsInBatch = vehicleSceneQueryData.mNumRaycastsPerBatch;
	physx::PxBatchQueryDesc sqDesc(maxNumRaycastsInBatch, 0, 0);
	sqDesc.queryMemory.userRaycastResultBuffer = vehicleSceneQueryData.mSqResults + batchId*maxNumRaycastsInBatch;
	sqDesc.queryMemory.userRaycastTouchBuffer = vehicleSceneQueryData.mSqHitBuffer + batchId*maxNumRaycastsInBatch;
	sqDesc.queryMemory.raycastTouchBufferSize = maxNumRaycastsInBatch;
	sqDesc.preFilterShader = vehicleSceneQueryData.mPreFilterShader;
	return scene->createBatchQuery(sqDesc);
}

physx::PxRaycastQueryResult* VehicleSceneQueryData::getRaycastQueryResultBuffer(const physx::PxU32 batchId)
{
	return (mSqResults + batchId*mNumRaycastsPerBatch);
}

physx::PxU32 VehicleSceneQueryData::getRaycastQueryResultBufferSize() const
{
	return mNumRaycastsPerBatch;
}