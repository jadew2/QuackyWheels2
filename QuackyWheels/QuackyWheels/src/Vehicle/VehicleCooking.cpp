#include "VehicleCooking.h"

physx::PxConvexMesh* createConvexMesh(const physx::PxVec3* verts, const physx::PxU32 numVerts, physx::PxPhysics& physics, physx::PxCooking& cooking) {
	// Create descriptor for convex mesh
	physx::PxConvexMeshDesc convexDesc;
	convexDesc.points.count = numVerts;
	convexDesc.points.stride = sizeof(physx::PxVec3);
	convexDesc.points.data = verts;
	convexDesc.flags = physx::PxConvexFlag::eCOMPUTE_CONVEX | physx::PxConvexFlag::eINFLATE_CONVEX;

	physx::PxConvexMesh* convexMesh = NULL;
	physx::PxDefaultMemoryOutputStream buf;
	if (cooking.cookConvexMesh(convexDesc, buf)) {
		physx::PxDefaultMemoryInputData id(buf.getData(), buf.getSize());
		convexMesh = physics.createConvexMesh(id);
	}

	return convexMesh;
}

physx::PxConvexMesh* createChassisMesh(const physx::PxVec3 dims, physx::PxPhysics& physics, physx::PxCooking& cooking) {
	const physx::PxF32 x = dims.x*0.5f;
	const physx::PxF32 y = dims.y*0.5f;
	const physx::PxF32 z = dims.z*0.5f;
	physx::PxVec3 verts[8] = {
		physx::PxVec3(x, y, -z),
		physx::PxVec3(x, y, z),
		physx::PxVec3(x, -y, z),
		physx::PxVec3(x, -y, -z),
		physx::PxVec3(-x, y, -z),
		physx::PxVec3(-x, y, z),
		physx::PxVec3(-x, -y, z),
		physx::PxVec3(-x, -y, -z)
	};

	return createConvexMesh(verts, 8, physics, cooking);
}

physx::PxConvexMesh* createWheelMesh(const physx::PxF32 width, const physx::PxF32 radius, physx::PxPhysics& physics, physx::PxCooking& cooking) {
	physx::PxVec3 points[2 * 16];
	for (physx::PxU32 i = 0; i < 16; i++) {
		const physx::PxF32 cosTheta = physx::PxCos(i*physx::PxPi*2.0f / 16.0f);
		const physx::PxF32 sinTheta = physx::PxSin(i*physx::PxPi*2.0f / 16.0f);
		const physx::PxF32 y = radius*cosTheta;
		const physx::PxF32 z = radius*sinTheta;
		points[2 * i + 0] = physx::PxVec3(-width / 2.0f, y, z);
		points[2 * i + 1] = physx::PxVec3(+width / 2.0f, y, z);
	}

	return createConvexMesh(points, 32, physics, cooking);
}