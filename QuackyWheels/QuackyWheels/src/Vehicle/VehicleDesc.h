#pragma once

#include "PxPhysicsAPI.h"

struct VehicleDesc {

	//Chassis Data
	physx::PxReal chassisMass; //kg
	physx::PxVec3 chassisDims;
	physx::PxVec3 chassisMOI;
	physx::PxVec3 chassisCMOffset;
	physx::PxMaterial* chassisMaterial;

	//Wheel Data
	physx::PxReal wheelMass; //kg
	physx::PxReal wheelWidth; //m
	physx::PxReal wheelRadius; //m
	physx::PxReal wheelMOI;
	physx::PxReal wheelAngularDamping;
	physx::PxReal maxBrakeTorque; //Nm
	//Only put handbrake on rear wheels
	physx::PxReal maxHandbrakeTorqueFront; //Nm
	physx::PxReal maxHandbrakeTorqueRear; //Nm
	//Onlt put steering on drive(front) wheels
	physx::PxReal maxSteer; //rad
	//Ignore toe angle
	physx::PxMaterial* wheelMaterial;
	physx::PxU32 numWheels;

	//Suspension Data
	physx::PxReal maxCompression; //m
	physx::PxReal maxDroop; //m
	physx::PxReal springStrength;
	physx::PxReal springDamperRate;
	physx::PxReal camberAtRest;
	physx::PxReal camberAtMaxCompression;
	physx::PxReal camberAtMaxDroop;

	//Tire Data
	physx::PxReal longitudinalStiffnessPerUnitGravity;
	physx::PxReal lateralStiffnessX;
	physx::PxReal lateralStiffnessY;
	physx::PxReal camberStiffnessPerUnitGravity;
	physx::PxU32 tireType;

	//Engine Data
	physx::PxReal engineMOI;
	physx::PxReal enginePeaktorque;
	physx::PxReal engineMaxOmega;
	physx::PxReal dampingRateAtFullThrottle;
	physx::PxReal dampingRateAtZeroThrottle;
	physx::PxReal dampingRateAtZeroThrottleClutchDisengaged;
	
	//Gear Data
	physx::PxU32 numRatios;
	physx::PxReal ratios[physx::PxVehicleGearsData::eGEARSRATIO_COUNT];
	physx::PxReal finalRatio;
	physx::PxReal switchTime;

	//Autobox Data
	physx::PxReal mUpRatios[physx::PxVehicleGearsData::eGEARSRATIO_COUNT];
	
	//Clutch Data
	physx::PxReal clutchStrength;

	//Ackermann Steering
	physx::PxReal accuracy;

	//Differential Data
	physx::PxVehicleDifferential4WData::Enum differentialType;
};

VehicleDesc initVehicleDesc(physx::PxPhysics* physics);

