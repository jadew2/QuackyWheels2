#include "VehicleFilterShader.h"

physx::PxFilterFlags VehicleFilterShader
(physx::PxFilterObjectAttributes attributes0, physx::PxFilterData filterData0,
physx::PxFilterObjectAttributes attributes1, physx::PxFilterData filterData1,
physx::PxPairFlags& pairFlags, const void* constantBlock, physx::PxU32 constantBlockSize)
{
	PX_UNUSED(constantBlock);
	PX_UNUSED(constantBlockSize);

	if (physx::PxFilterObjectIsTrigger(attributes0) || physx::PxFilterObjectIsTrigger(attributes1))
	{
		pairFlags = physx::PxPairFlag::eTRIGGER_DEFAULT;
		return physx::PxFilterFlag::eDEFAULT;
	}

	if (((filterData0.word0 & filterData1.word1) == 0) && ((filterData1.word0 & filterData0.word1) == 0)) {
		return physx::PxFilterFlag::eSUPPRESS;
	}

	pairFlags = physx::PxPairFlag::eCONTACT_DEFAULT;

	if ((filterData0.word0 == COLLISION_FLAG_CHASSIS) | (filterData0.word0 == COLLISION_FLAG_OBSTACLE) | (filterData0.word0 == COLLISION_FLAG_DRIVABLE_OBSTACLE) |
		(filterData1.word0 == COLLISION_FLAG_CHASSIS) | (filterData1.word0 == COLLISION_FLAG_OBSTACLE) | (filterData1.word0 == COLLISION_FLAG_DRIVABLE_OBSTACLE)) {
		pairFlags |= physx::PxPairFlag::eNOTIFY_TOUCH_FOUND;
	}

	return physx::PxFilterFlags();
}