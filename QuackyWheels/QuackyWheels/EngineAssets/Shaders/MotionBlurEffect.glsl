//#Version is included automatically

#ifdef Vertex_Shader

#define Default_Image_Effect_Vertex_Shader

#endif

#ifdef Fragment_Shader


uniform float blurStrength = 2.00; //The influence of the blur on the final image
uniform float blurWidth = 0.5; //The distance between samples when drawn

uniform sampler2D mainTex0; //The main texture


// Input from vertex shader
in VS_OUT
{
    vec2 uv0;
} fs_in;

out vec4 color;

void main()
{
    //Radial Blur Shader: Thanks to http://stackoverflow.com/questions/16432297/weird-issue-with-glsl-radial-blur
	
    // some sample positions
	float samples[10] = float[](-0.08,-0.05,-0.03,-0.02,-0.01,0.01,0.02,0.03,0.05,0.08);

    // 0.5,0.5 is the center of the screen
    // so subtracting uv from it will result in
    // a vector pointing to the middle of the screen
    vec2 dir = 0.5 - fs_in.uv0;

    // calculate the distance to the center of the screen
    float dist = sqrt(dir.x*dir.x + dir.y*dir.y);

    // normalize the direction
    dir /= dist;

    //This is the original colour of this fragment
    vec4 sampleColor = texture2D(mainTex0,fs_in.uv0);

    vec4 sum = sampleColor;
    //Take 10 additional blur samples in the direction towards the center of the screen.
    for (int i = 0; i < 10; i++)
    {
      sum += texture2D(mainTex0, fs_in.uv0 + dir * samples[i] * blurWidth );
    }

    // we have taken eleven samples
    sum *= 1.0/11.0;

    // weighten the blur effect with the distance to the
    // center of the screen (further out is blurred more)
    float t = dist * blurStrength;
    t = clamp(t,0.0,1.0); //0 <= t <= 1

    //Blend the original color with the averaged pixels
    color = mix( sampleColor, sum, t );
	
	//color =  texture2D(mainTex0,fs_in.uv0);
}

#endif