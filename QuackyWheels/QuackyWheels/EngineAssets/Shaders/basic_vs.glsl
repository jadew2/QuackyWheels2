#version 450 core
layout( location = 0 ) in vec3 vert_modelSpace;
layout( location = 1 ) in vec3 vert_color;
layout( location = 2 ) in vec3 normal;

uniform mat4 MV;
uniform mat4 P;
uniform vec3 light_Pos;

// Inputs from vertex shader
out VS_OUT
{
    vec3 N;
    vec3 L;
    vec3 V;
	vec3 Color;
} vs_out;

void main()
{
	vec4 position = MV * vec4( vert_modelSpace, 1.0 );

	//Calculate normal in view-space
    vs_out.N = mat3(MV) * normal;
 
    //Calculate light vector
    vs_out.L = light_Pos - position.xyz;

    //Calculate view vector
    vs_out.V = -position.xyz;

	gl_Position = P * position;
	vs_out.Color = vert_color;
}

