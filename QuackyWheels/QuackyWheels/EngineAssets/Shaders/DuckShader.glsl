//#Version is included automatically

#ifdef Vertex_Shader

//Appends the default vertex shader
#define Default_Vertex_Shader

#endif




#ifdef Fragment_Shader

uniform vec3 ambient; //Ambient light
uniform sampler2D mainTex0; //The main texture
uniform sampler2D mainTex1; //The secondary texture

// Input from vertex shader
in VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} fs_in;


out vec4 color;

void main()
{

	//Normalize the incoming N, L and V vectors
    vec3 N = normalize(fs_in.normal);
    vec3 L = normalize(fs_in.lightDir);
    vec3 V = normalize(fs_in.cameraDir);

	vec3 R = reflect(-L, N);

	const vec3 diffuse = vec3(0.7, 0.7, 0.7);
	const vec3 specular = vec3( 0.5, 0.5, 0.5 );

	float cosAlpha = clamp( dot( R, V ), 0, 1 );
	float cosTheta = clamp( dot( N, L ), 0, 1 );

    //color = ambient
           // + texture2D(mainTex,fs_in.uv0).xyz
            //+ diffuse * cosTheta
           // + specular * pow( cosAlpha, 32);

	color = vec4(ambient,1.0) + texture2D(mainTex0,fs_in.uv0).xyzw;
	//color = fs_in.Color; // vec3( 1.0, 0.0, 0.0 );
}

#endif