/* Shader Macros
 *
 * These are common shader code macros you'll want in several of your shaders.
 */

#ifdef Default_Vertex_Shader

//region
//Default vertex attributes
layout( location = 0 ) in vec3 vertex; //Vertex in modelSpace
layout( location = 1 ) in vec3 normal;
layout( location = 2 ) in vec3 tangent;
layout( location = 3 ) in vec2 uv0; //Primary texture coords
layout( location = 4 ) in vec2 uv1; //Secondary texture coords
layout( location = 5 ) in vec4 vcolor; //Vertex color

uniform mat4 ModelViewMat;
uniform mat4 PerpectiveMat;
uniform vec3 LightPosition;
//endregion

// Outputs from vertex shader (These values are interpolated for you when they reach the fragment shader?)
out VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec4 vcolor;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} vs_out;

void main()
{
	vec4 position = ModelViewMat * vec4( vertex, 1.0 );

	//Calculate normal and tangent in view-space (Easier + Faster to to lighting this way)
    vs_out.normal = mat3(ModelViewMat) * normal;
    vs_out.tangent = mat3(ModelViewMat) * tangent;

    vs_out.uv0 = uv0;
    vs_out.uv1 = uv1;

    vs_out.vcolor = vcolor;

    vs_out.lightDir = LightPosition - position.xyz; //Calculate light vector
    vs_out.cameraDir = -position.xyz; //Calculate view vector

    //Set Clipped Vertex Position
	gl_Position = PerpectiveMat * position;
}

#endif

#ifdef Default_Image_Effect_Vertex_Shader

//region
//Default vertex attributes
layout( location = 0 ) in vec3 vertex; //Vertex in modelSpace
layout( location = 1 ) in vec3 normal; //To be removed
layout( location = 2 ) in vec3 tangent; //To be removed
layout( location = 3 ) in vec2 uv0; //Primary texture coords

//uniform mat4 ModelViewMat;
//uniform mat4 PerpectiveMat;

// Outputs from vertex shader (These values are interpolated for you when they reach the fragment shader?)
out VS_OUT
{
    vec2 uv0;
} vs_out;

void main()
{
    vs_out.uv0 = uv0;

    //See if you can make this faster...
    //vec4 position = ModelViewMat * vec4( vertex, 1.0 );
	//gl_Position = PerpectiveMat * position;

    //We don't care about anything except for the xy coords. They should be in screen space.
	gl_Position = vec4(vertex.xy,0.0,1.0);
}

#endif