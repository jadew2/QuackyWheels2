//#Version is included automatically

#ifdef Vertex_Shader

//region
//Default vertex attributes
layout( location = 0 ) in vec3 vertex; //Vertex in modelSpace
layout( location = 1 ) in vec3 normal;
layout( location = 2 ) in vec3 tangent;
layout( location = 3 ) in vec2 uv0; //Primary texture coords
layout( location = 4 ) in vec2 uv1; //Secondary texture coords
layout( location = 5 ) in vec4 vcolor; //Vertex color

uniform mat4 ModelViewMat;
uniform mat4 PerpectiveMat;
uniform vec3 LightPosition;

uniform vec3 ParticleScale;
uniform float ParticleRotation;
//endregion


// Outputs from vertex shader (These values are interpolated for you when they reach the fragment shader?)
out VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec4 vcolor;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} vs_out;

void main()
{
    mat4 modelView = ModelViewMat;

    //Replace the rotation part of the matrix with the identity to get billboarding
    // First colunm.
    modelView[0][0] = ParticleScale.x;
    modelView[0][1] = 0.0;
    modelView[0][2] = 0.0;

    // Second colunm.
    modelView[1][0] = 0.0;
    modelView[1][1] = ParticleScale.y;
    modelView[1][2] = 0.0;
	
    // Third colunm.
    modelView[2][0] = 0.0;
    modelView[2][1] = 0.0;
    modelView[2][2] = ParticleScale.z;




	
	
	vec4 position = modelView * vec4( vertex, 1.0 );

	//Calculate normal and tangent in view-space (Easier + Faster to to lighting this way)
    vs_out.normal = mat3(modelView) * normal;
    vs_out.tangent = mat3(modelView) * tangent;

    vs_out.uv0 = uv0;
    vs_out.uv1 = uv1;

    vs_out.vcolor = vcolor;

    vs_out.lightDir = LightPosition - position.xyz; //Calculate light vector
    vs_out.cameraDir = -position.xyz; //Calculate view vector

    //Set Clipped Vertex Position
	gl_Position = PerpectiveMat * position;
}

#endif


#ifdef Fragment_Shader

uniform vec4 ambient; //Ambient light
uniform vec4 tint; //The color tint on the texture
uniform sampler2D mainTex0; //The main texture

// Input from vertex shader
in VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec4 vcolor;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} fs_in;


out vec4 color;

void main()
{
    //Totally Shadeless
	color = ambient + texture2D(mainTex0,fs_in.uv0).xyzw * tint;
}

#endif