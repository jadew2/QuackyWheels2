//#Version is included automatically

#ifdef Vertex_Shader

//Appends the default vertex shader
#define Default_Vertex_Shader

#endif


#ifdef Fragment_Shader

uniform sampler2D mainTex0; //The main texture
uniform vec2 mainTex0Offset;
uniform vec2 mainTex0Scale;
uniform sampler2D mainTex1; //The secondary texture
uniform vec2 mainTex1Offset;
uniform vec2 mainTex1Scale;

// Input from vertex shader
in VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec4 vcolor;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} fs_in;


out vec4 color;

void main()
{
    //Totally Shadeless
	color = texture2D(mainTex0,(fs_in.uv0 * mainTex0Scale) + mainTex0Offset).xyzw * texture2D(mainTex1, fs_in.uv0).xyzw;
}

#endif