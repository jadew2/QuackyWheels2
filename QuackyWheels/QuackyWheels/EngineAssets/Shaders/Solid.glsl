//#Version is included automatically

#ifdef Vertex_Shader

//Appends the default vertex shader
#define Default_Vertex_Shader

#endif


#ifdef Fragment_Shader

uniform vec4 ambient; //Ambient light
uniform sampler2D mainTex0; //The main texture
uniform sampler2D mainTex1; //The secondary texture

// Input from vertex shader
in VS_OUT
{
    vec3 normal;
    vec3 tangent;

    vec2 uv0;
    vec2 uv1;

    vec3 lightDir;  //Light vector
    vec3 cameraDir; //View vector
} fs_in;


out vec4 color;

void main()
{
    //Totally Shadeless
	color = ambient;
}

#endif