//#Version is included automatically

#ifdef Vertex_Shader

#define Default_Image_Effect_Vertex_Shader

#endif

#ifdef Fragment_Shader

uniform sampler2D mainTex0; //The main texture

uniform vec4 tint; //Tint

// Input from vertex shader
in VS_OUT
{
    vec2 uv0;
} fs_in;

out vec4 color;

void main()
{
    vec4 texColor = texture2D(mainTex0,fs_in.uv0);
    color = texColor * tint;
}

#endif