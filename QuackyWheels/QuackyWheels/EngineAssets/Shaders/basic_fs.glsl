#version 450 core

// Input from vertex shader
in VS_OUT
{
    vec3 N;
    vec3 L;
    vec3 V;
	vec3 Color;
} fs_in;

out vec3 color;

void main()
{

	//Normalize the incoming N, L and V vectors
    vec3 N = normalize(fs_in.N);
    vec3 L = normalize(fs_in.L);
    vec3 V = normalize(fs_in.V);
	vec3 ambient = fs_in.Color;

	vec3 R = reflect(-L, N);

	const vec3 diffuse = vec3(0.7, 0.7, 0.7);
	const vec3 specular = vec3( 0.5, 0.5, 0.5 );

	float cosAlpha = clamp( dot( R, V ), 0, 1 );
	float cosTheta = clamp( dot( N, L ), 0, 1 );

	color = ambient + diffuse * cosTheta + specular * pow( cosAlpha, 128);

	//color = fs_in.Color; // vec3( 1.0, 0.0, 0.0 );
}

